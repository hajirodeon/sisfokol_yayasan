-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 12 Bulan Mei 2022 pada 18.10
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisfokol_yayasan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `adminx`
--

CREATE TABLE `adminx` (
  `kd` varchar(50) NOT NULL,
  `usernamex` varchar(100) DEFAULT NULL,
  `passwordx` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `adminx`
--

INSERT INTO `adminx` (`kd`, `usernamex`, `passwordx`, `postdate`) VALUES
('21232f297a57a5a743894a0e4a801fc3', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2021-03-11 10:28:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_artikel`
--

CREATE TABLE `cp_artikel` (
  `kd` varchar(50) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `judul` longtext DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `kd_submenu` varchar(50) DEFAULT NULL,
  `kd_posisi` varchar(50) DEFAULT NULL,
  `filex` longtext DEFAULT NULL,
  `jml_dilihat` varchar(100) DEFAULT NULL,
  `jml_komentar` varchar(100) DEFAULT NULL,
  `jml_suka` varchar(100) DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `verifikasi` enum('true','false') DEFAULT 'false',
  `verifikasi_postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_artikel`
--

INSERT INTO `cp_artikel` (`kd`, `kategori`, `judul`, `isi`, `postdate`, `kd_submenu`, `kd_posisi`, `filex`, `jml_dilihat`, `jml_komentar`, `jml_suka`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `verifikasi`, `verifikasi_postdate`) VALUES
('edc341d345cd7287d7b239a0bbcd7c5d', 'd1eeeda6267f79fbedceb394fd2f0f7f', 'coba1', 'xkkirixpxkkananxcoba11...xkkirixxgmringxpxkkananx', '2021-04-14 05:13:04', 'e2dfe5f56a6fa5d9f4b7951decbdbdfe', '', 'edc341d345cd7287d7b239a0bbcd7c5d-1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1a6751317e7f8a17d2d3fdbd849d8bdf', '484452ea4cd76ede4ea1f426cf81b347', 'coba...', 'xkkirixpxkkananxcoba...xkkirixxgmringxpxkkananx', '2021-08-11 08:56:12', '', '', '1a6751317e7f8a17d2d3fdbd849d8bdf-1.png', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL),
('3305a6504aa890233416870e2d7a5953', 'Agenda', 'sddff', 'xkkirixpxkkananxsdsffxkkirixxgmringxpxkkananx', '2021-08-11 17:16:48', NULL, NULL, NULL, NULL, NULL, NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'false', NULL),
('2fec8acd2124ae2e7d3f33adf785fd8c', 'Kegiatan', 'sdddf1', 'xkkirixpxkkananxdfdffffxkkirixxgmringxpxkkananx', '2021-08-11 17:17:55', NULL, NULL, NULL, NULL, NULL, NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'false', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_artikel_komentar`
--

CREATE TABLE `cp_artikel_komentar` (
  `kd` varchar(50) NOT NULL,
  `kd_artikel` varchar(50) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `aktif` enum('true','false') DEFAULT 'false'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_bukutamu`
--

CREATE TABLE `cp_bukutamu` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `situs` longtext DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `aktif` enum('true','false') DEFAULT 'false'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_foto`
--

CREATE TABLE `cp_foto` (
  `kd` varchar(50) NOT NULL,
  `nama` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `verifikasi` enum('true','false') NOT NULL DEFAULT 'false',
  `verifikasi_postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `cp_foto`
--

INSERT INTO `cp_foto` (`kd`, `nama`, `postdate`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `verifikasi`, `verifikasi_postdate`) VALUES
('b8c3466c0ec788dc497e138b1039bd0c', 'sfffff', '2021-08-11 17:23:43', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'false', NULL),
('a0accd90fa3c3e444de0109579b9f073', 'drrrr', '2021-08-11 17:23:56', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'false', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_katamutiara`
--

CREATE TABLE `cp_katamutiara` (
  `kd` varchar(50) NOT NULL,
  `isi` longtext DEFAULT NULL,
  `oleh` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_katamutiara`
--

INSERT INTO `cp_katamutiara` (`kd`, `isi`, `oleh`, `postdate`) VALUES
('13cc8ea09947007e8791ca79ef6a1c9d', 'Kesuksesan adalah hasil dari kesempurnaan, kerja keras, belajar dari pengalaman, loyalitas, dan kegigihan', 'Colin Powell', '2019-09-13 16:09:23'),
('d86a0b34cf9b3994c987128e6f0ea873', 'Beberapa orang memimpikan kesuksesan, sementara yang lain bangun setiap pagi untuk mewujudkannya', 'Wayne Huizenga', '2019-09-13 16:09:46'),
('d1db1511f00f709e6b27e9b0f78cd6e1', 'Untuk sukses, sikap sama pentingnya dengan kemampuan.', 'Walter Scott', '2019-09-13 16:10:08'),
('4154841ff42b935fa6644c3d7e354b3c', 'Kegagalan adalah kunci kesuksesan. Setiap kesalahan mengajarkan kita sesuatu.', 'Morihei Ueshiba', '2019-09-13 16:10:30'),
('5dc34b412229a24f22533c63defafbd5', 'Sukses adalah sebuah perjalanan, bukan sebuah tujuan. Usaha sering lebih penting daripada hasilnya.', 'Arthur Ashe', '2019-09-13 16:10:48'),
('d047a7029ed858b118025bd806b16b7c', 'Saat aku bersamamu, rasanya seperti sedang bermimpi sepanjang hari. Kamu adalah cahaya bintangku. Cintamu seperti sebuah hadiah.', 'Taeyeon feat. Dean, Starlight', '2019-09-13 16:12:01'),
('67e884ad953ab46bbb37f05a326a430d', 'Tak peduli berapa banyaknya hal yang orang lain miliki, mereka semua punya kesalahan.', 'Reply 1988', '2019-09-13 16:12:19'),
('d34b48093a919c802e3453e480028b70', 'Lebih banyak tersenyum, kurangi khawatir. Lebih banyak berbelas kasih, kurangi menghakimi. Lebih bersyukur, kurangi stres. Lebih banyak mencintai, kurangi membenci.', 'Roy T. Bennett, The Light in the Heart', '2019-09-13 16:12:35'),
('708b4767d8a10aa6605f59bcc9fa2228', 'Saat kamu menyadari kalau kamu diberkati tanpa batas, kamu akan sadar bahwa hidup adalah harta yang sangat berharga.', 'Gift Gugu Mona', '2019-09-13 16:12:50'),
('642f2e8b20ceb5b87b9d99e301290839', 'Lebih baik mensyukuri apa yang kita miliki daripada menyesali apa yang tidak kita miliki.', 'Ridwan Mets', '2019-09-13 16:13:07'),
('0edb4281ca0b4763fb977aacbb5c6956', 'Jika tanganmu tak bisa menggapai bintang, bersyukurlahxpentungx Setidaknya kakimu masih berpijak di bumi.', 'Harly Umboh', '2019-09-13 16:13:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_m_kategori`
--

CREATE TABLE `cp_m_kategori` (
  `kd` varchar(50) NOT NULL,
  `no` varchar(5) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_m_kategori`
--

INSERT INTO `cp_m_kategori` (`kd`, `no`, `nama`, `postdate`) VALUES
('484452ea4cd76ede4ea1f426cf81b347', '1', 'Berita', '2014-02-04 09:15:31'),
('59dc891c5aae50c57619b0b27865353b', '2', 'Agenda', '2014-02-04 09:15:42'),
('fa0216c860a6e8ce2f438f0b8fd49421', '3', 'Kegiatan', '2021-04-14 05:12:33'),
('d1eeeda6267f79fbedceb394fd2f0f7f', '4', 'Profil', '2014-02-05 08:55:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_m_link`
--

CREATE TABLE `cp_m_link` (
  `kd` varchar(50) NOT NULL,
  `nama` longtext DEFAULT NULL,
  `urlnya` longtext DEFAULT NULL,
  `filex` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_m_link`
--

INSERT INTO `cp_m_link` (`kd`, `nama`, `urlnya`, `filex`, `postdate`) VALUES
('c688d2c0cdc6fd6358d43693f234d184', 'ccccc', 'cccc.com', 'c688d2c0cdc6fd6358d43693f234d184-1.png', '2019-09-23 03:35:59'),
('9b3c3351bd1c4a98bd6a458dc12b6024', '23333', '23333.com', '9b3c3351bd1c4a98bd6a458dc12b6024-1.png', '2019-09-23 03:36:10'),
('af2bf13fe692c765594b1119d9666f75', 'csssdsd', 'sdsdsd.com', 'af2bf13fe692c765594b1119d9666f75-1.png', '2021-04-14 05:00:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_m_menu`
--

CREATE TABLE `cp_m_menu` (
  `kd` varchar(50) NOT NULL,
  `no` varchar(10) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_m_menu`
--

INSERT INTO `cp_m_menu` (`kd`, `no`, `nama`, `postdate`) VALUES
('d0a10240fc1a8fdc210b47b4f5704f45', '2', 'PROGRAM KERJA', '2021-04-14 05:06:34'),
('1477a55e2095460cf297784e1ee4bfe9', '1', 'PROFIL', '2014-02-04 08:18:30'),
('588825681b691bc5ddec60f699f78410', '3', 'MAJELIS LEMBAGA', '2021-04-14 05:06:44'),
('497413b075a99cfb57ab754618ad2677', '4', 'DIREKTORI', '2021-04-14 05:07:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_m_mitra`
--

CREATE TABLE `cp_m_mitra` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `urlnya` longtext DEFAULT NULL,
  `filex` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `cp_m_mitra`
--

INSERT INTO `cp_m_mitra` (`kd`, `nama`, `urlnya`, `filex`, `postdate`) VALUES
('3792339476b362dbb98a113f29d01e2c', '1', '1', '3792339476b362dbb98a113f29d01e2c-1.png', '2021-04-14 04:59:38'),
('b21d14076673c1e7eb33011e4d3d9fe2', '2', 'xxx2', 'b21d14076673c1e7eb33011e4d3d9fe2-1.png', '2019-09-15 15:51:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_m_posisi`
--

CREATE TABLE `cp_m_posisi` (
  `no` varchar(5) DEFAULT NULL,
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kd_artikel` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_m_posisi`
--

INSERT INTO `cp_m_posisi` (`no`, `kd`, `nama`, `kd_artikel`) VALUES
('7', '8f14e45fceea167a5a36dedd4bea2543', 'headline7', ''),
('6', '1679091c5a880faf6fb5e6087eb1b2dc', 'headline6', ''),
('5', 'e4da3b7fbbce2345d7772b0674a318d5', 'headline5', ''),
('4', 'a87ff679a2f3e71d9181a67b7542122c', 'headline4', ''),
('3', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 'headline3', ''),
('2', 'c81e728d9d4c2f636f067f89cc14862c', 'headline2', ''),
('1', 'c4ca4238a0b923820dcc509a6f75849b', 'headline1', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_m_slideshow`
--

CREATE TABLE `cp_m_slideshow` (
  `kd` varchar(50) NOT NULL,
  `filex` longtext DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `urlnya` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `cp_m_slideshow`
--

INSERT INTO `cp_m_slideshow` (`kd`, `filex`, `nama`, `isi`, `postdate`, `urlnya`) VALUES
('91016edb6a1ca125e674007b5d98f642', '91016edb6a1ca125e674007b5d98f642-1.jpg', '1', '1', '2019-09-13 02:40:21', '1'),
('8ac89ecf8c9e191473fa1e34c1034baf', '8ac89ecf8c9e191473fa1e34c1034baf-1.jpg', '2', '2', '2019-09-15 12:03:04', '2'),
('ad6d08c2ebd2dac9a9657b7eb2dbb235', 'ad6d08c2ebd2dac9a9657b7eb2dbb235-1.jpg', 'coba 123', 'yang penting, coba ya...', '2021-04-14 04:58:59', 'google.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_m_submenu`
--

CREATE TABLE `cp_m_submenu` (
  `kd` varchar(50) NOT NULL,
  `kd_menu` varchar(50) DEFAULT NULL,
  `no` varchar(10) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `kd_artikel` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_m_submenu`
--

INSERT INTO `cp_m_submenu` (`kd`, `kd_menu`, `no`, `nama`, `postdate`, `kd_artikel`) VALUES
('699b86c60e38388ded0037c39664bd58', '1477a55e2095460cf297784e1ee4bfe9', '1', 'Sejarah', '2021-04-14 05:04:20', ''),
('efd91bc9d2f52cd2db999cd239af629f', '1477a55e2095460cf297784e1ee4bfe9', '2', 'Anggaran Dasar', '2021-04-14 05:04:32', ''),
('c67d19f3153b91fdb4398f5b7a6695c0', '', '3', 'Anggaran RT', '2021-04-14 05:04:45', ''),
('4cbb112d50e32615ad425ee5e89a28da', '1477a55e2095460cf297784e1ee4bfe9', '3', 'Anggaran RT', '2021-04-14 05:35:26', ''),
('ca8e7327dfdb09e9f557543aed2cce3f', '1477a55e2095460cf297784e1ee4bfe9', '4', 'Struktur Organisasi', '2021-04-14 05:04:59', ''),
('aa94ed701f1c2f2c59cb2cdb300fa7d1', 'd0a10240fc1a8fdc210b47b4f5704f45', '1', 'Visi dan Misi', '2021-04-14 05:07:33', ''),
('82ce06efe29f4d3a4809e0a2e524574b', 'd0a10240fc1a8fdc210b47b4f5704f45', '2', 'Prinsip Pelaksanaan', '2021-04-14 05:07:51', ''),
('a6b7e1f90a1896c8d24dfbad7350292c', 'd0a10240fc1a8fdc210b47b4f5704f45', '3', 'Landasan', '2021-04-14 05:08:03', ''),
('04bac5f138b1d2b63fb9f61bfd241428', 'd0a10240fc1a8fdc210b47b4f5704f45', '4', 'Tujuan', '2021-04-14 05:08:18', ''),
('516f402d9d18b28860a49a53f4fa4806', 'd0a10240fc1a8fdc210b47b4f5704f45', '5', 'Prioritas', '2021-04-14 05:08:31', ''),
('a77b8f776d01ebb430d492de22998cab', 'd0a10240fc1a8fdc210b47b4f5704f45', '6', 'Program Utama', '2021-04-14 05:08:47', ''),
('e2dfe5f56a6fa5d9f4b7951decbdbdfe', '497413b075a99cfb57ab754618ad2677', '1', 'Amal Usaha', '2021-04-14 05:09:29', 'edc341d345cd7287d7b239a0bbcd7c5d'),
('074be109c5cb1e0fa8b5ae020adea902', '497413b075a99cfb57ab754618ad2677', '2', 'Dakwah', '2021-04-14 05:09:43', ''),
('5630851c8b246ff982591bb7781576d2', '497413b075a99cfb57ab754618ad2677', '3', 'Kegiatan', '2021-04-14 05:09:54', ''),
('db9b85ec4b9d515462c14b2b159bc568', '30046c78bb9e4b26512a0b55de552a44', '1', 'Direktori Alumni', '2014-02-04 08:48:34', ''),
('a55c8842202f5aea6b72c349c82aa0a2', '30046c78bb9e4b26512a0b55de552a44', '2', 'Info Alumni', '2014-02-04 08:48:48', ''),
('4a35f75e766dabcc48c7eff26d13638d', '8814ad1979caa193243e02e861f89dc4', '1', 'Lowongan Kerja', '2014-02-04 08:53:56', ''),
('550f09847da0fa789be7aa7a749900c4', '8814ad1979caa193243e02e861f89dc4', '2', 'Data Pelamar', '2014-02-04 08:54:05', ''),
('d9632a9a2243b4d0316f2414ebd4fbcc', '8814ad1979caa193243e02e861f89dc4', '3', 'Data Ujian', '2014-02-04 08:54:19', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_newsletter`
--

CREATE TABLE `cp_newsletter` (
  `kd` varchar(50) NOT NULL,
  `email` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_newsletter`
--

INSERT INTO `cp_newsletter` (`kd`, `email`, `postdate`) VALUES
('12333', 'hajirodeon@gmail.com', '2019-09-15 00:00:00'),
('cfa9aa02de9cb076046531f47471a39c', 'emailkuxtkeongxcoba.com', '2019-09-23 03:54:46'),
('2141ee4dc204c2a234af95b7cccbcaec', 'emailkuxtkeongxcoba.com.id', '2019-09-23 04:00:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_polling`
--

CREATE TABLE `cp_polling` (
  `kd` varchar(50) NOT NULL,
  `topik` varchar(100) DEFAULT NULL,
  `opsi1` varchar(50) DEFAULT NULL,
  `opsi2` varchar(50) DEFAULT NULL,
  `opsi3` varchar(50) DEFAULT NULL,
  `opsi4` varchar(50) DEFAULT NULL,
  `opsi5` varchar(50) DEFAULT NULL,
  `nil_opsi1` varchar(5) DEFAULT '0',
  `nil_opsi2` varchar(5) DEFAULT '0',
  `nil_opsi3` varchar(5) DEFAULT '0',
  `nil_opsi4` varchar(5) DEFAULT '0',
  `nil_opsi5` varchar(5) DEFAULT '0',
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_polling`
--

INSERT INTO `cp_polling` (`kd`, `topik`, `opsi1`, `opsi2`, `opsi3`, `opsi4`, `opsi5`, `nil_opsi1`, `nil_opsi2`, `nil_opsi3`, `nil_opsi4`, `nil_opsi5`, `postdate`) VALUES
('a92fdd662162b20e16a5414c1b7db321', 'coba polling', '1', '2', '3', '4', '5', '0', '0', '0', '0', '0', '2021-04-14 05:14:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_profil`
--

CREATE TABLE `cp_profil` (
  `kd` varchar(50) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `x_lat` longtext DEFAULT NULL,
  `x_long` longtext DEFAULT NULL,
  `fb` longtext DEFAULT NULL,
  `twitter` longtext DEFAULT NULL,
  `youtube` longtext DEFAULT NULL,
  `wa` longtext DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `telp` longtext DEFAULT NULL,
  `web` longtext DEFAULT NULL,
  `fax` longtext DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `filex` longtext DEFAULT NULL,
  `alamat_googlemap` longtext DEFAULT NULL,
  `instagram` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cp_profil`
--

INSERT INTO `cp_profil` (`kd`, `judul`, `isi`, `postdate`, `x_lat`, `x_long`, `fb`, `twitter`, `youtube`, `wa`, `alamat`, `telp`, `web`, `fax`, `email`, `filex`, `alamat_googlemap`, `instagram`) VALUES
('e807f1fcf82d132f9bb018ca6738a19f', 'DIKDASMEN KABUPATEN KENDAL', 'DIKDASMEN KABUPATEN KENDAL', '2019-09-12 03:05:39', '', '', 'x', 'x', 'x', 'x', 'Jl. Raya', 'x', 'x', 'x', 'x', 'logo.jpg', '', 'xx');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_video`
--

CREATE TABLE `cp_video` (
  `kd` varchar(50) NOT NULL,
  `judul` longtext DEFAULT NULL,
  `filex` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `verifikasi` enum('true','false') NOT NULL DEFAULT 'false',
  `verifikasi_postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `cp_video`
--

INSERT INTO `cp_video` (`kd`, `judul`, `filex`, `postdate`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `verifikasi`, `verifikasi_postdate`) VALUES
('cac179dbd3401f4d756a1933a0682481', 'video 1', 'https:xgmringxxgmringxwww.youtube.comxgmringxwatch?v=F59hbu3qWp4', '2021-04-14 05:19:07', NULL, NULL, NULL, NULL, 'false', NULL),
('be3056d8d0a8ea59423d55d2118072a9', 'video 2', 'https:xgmringxxgmringxwww.youtube.comxgmringxwatch?v=0DvJ51wQuJ0', '2021-04-14 05:19:26', NULL, NULL, NULL, NULL, 'false', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cp_visitor`
--

CREATE TABLE `cp_visitor` (
  `kd` varchar(50) NOT NULL,
  `ipnya` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `online` enum('true','false') DEFAULT 'false'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `cp_visitor`
--

INSERT INTO `cp_visitor` (`kd`, `ipnya`, `postdate`, `online`) VALUES
('9359ccdb7a8c882e664ed14b78314b55', '127.0.0.1', '2019-09-23 04:23:17', 'false'),
('5b456a5f3a8ba2e9965b2cf6eb07952e', '127.0.0.1', '2019-09-23 04:23:19', 'false'),
('0298acf77b9dcb607005bf72f50ec757', '127.0.0.1', '2019-09-23 04:23:23', 'false'),
('e8155e762f4be3ebdebc8c200a3d08f4', '127.0.0.1', '2019-09-23 04:23:52', 'false'),
('c2a73c7be4584fef52f36bc5cb2b2c25', '127.0.0.1', '2019-09-23 04:24:24', 'false'),
('41cb6e33381ef28d2f83546c8acc8d15', '127.0.0.1', '2019-09-23 04:24:42', 'false'),
('2d309b2a792fb9221d3d2ba596920e38', '127.0.0.1', '2019-09-23 04:25:12', 'false'),
('43352941402206ccdec2b54255b4ae67', '127.0.0.1', '2019-09-23 04:25:31', 'false'),
('bfbf8eee63cb2618343f231ac3778c58', '127.0.0.1', '2019-09-23 04:25:54', 'false'),
('fe10b33509caa31aecc9ed7de7b7b972', '127.0.0.1', '2019-09-23 04:27:02', 'false'),
('5eefcfae0da263ce415329131112f5fb', '127.0.0.1', '2019-09-23 04:27:08', 'false'),
('1537ce45fba401ff25b752c8afae4613', '127.0.0.1', '2019-09-23 04:27:23', 'false'),
('0bf3ae89fed704552e7e04c0f03e74ef', '127.0.0.1', '2019-09-23 04:30:39', 'false'),
('075c20d92b8be10d222a4611a4579f08', '127.0.0.1', '2019-09-23 04:31:41', 'false'),
('6c4cecc5bd07c0c5e42e767674074069', '127.0.0.1', '2019-09-23 04:31:42', 'false'),
('072ccf14a76e8aa289534eeef0cbea6f', '127.0.0.1', '2019-09-23 04:32:07', 'false'),
('c88daa6fb654d5da43f5a0285d1b7723', '127.0.0.1', '2019-09-23 04:33:50', 'false'),
('7a519178a23002388b2c053aec18b3e8', '127.0.0.1', '2019-09-23 04:34:11', 'false'),
('41fd9c45f0f165ce0db265b1cdb7eb88', '127.0.0.1', '2019-09-23 04:34:49', 'false'),
('3ea9f36b00913ba40ec30b253f526d30', '127.0.0.1', '2019-09-23 04:35:07', 'false'),
('6572fbb51e69d4fcdb6b36213393d743', '127.0.0.1', '2019-09-23 04:35:18', 'false'),
('298e2819e3b284660517fae048a77d43', '127.0.0.1', '2019-09-23 04:35:55', 'false'),
('b1eeb0624d4f7b35c9a208bc2ce76696', '127.0.0.1', '2019-09-23 04:36:33', 'false'),
('3fbbece76f61c0d7432805cdba001ae8', '127.0.0.1', '2019-09-23 04:37:10', 'false'),
('7854c0dccea0ed4a4ab33589bc987894', '127.0.0.1', '2019-09-23 04:38:07', 'false'),
('4d3bf1f9b3ebbda847c7e4585e963232', '127.0.0.1', '2019-09-23 04:39:16', 'false'),
('112ae760b32e3aa0c7088d868b6e14a2', '127.0.0.1', '2019-09-23 04:39:46', 'false'),
('951ff2bc8d282aeec8a0b82d718fe067', '127.0.0.1', '2019-09-23 04:40:22', 'false'),
('2aa30d54a382b6f96c91558e389e61b8', '127.0.0.1', '2019-09-23 04:40:43', 'false'),
('7eb900c8f6fcedfe5f9845d2cb155692', '127.0.0.1', '2019-09-23 04:41:06', 'false'),
('cf1bf96e5bec48c417fb80a90a9e34b4', '127.0.0.1', '2019-09-23 04:41:32', 'false'),
('c72f1db342b674df4ff9d36870523a8e', '127.0.0.1', '2019-09-23 04:41:45', 'false'),
('8ff3642c959fa86142f6fa93b5b4cb26', '127.0.0.1', '2019-09-23 04:42:22', 'false'),
('95d0372a57c0d3e4d2d7302d43c6004d', '127.0.0.1', '2019-09-23 04:42:54', 'false'),
('4ad899f4dcfa72d0d354cc4769c601f4', '127.0.0.1', '2019-09-23 04:44:05', 'false'),
('a12359c978c6d037f3ea58ff5a31d028', '127.0.0.1', '2019-09-23 04:44:39', 'false'),
('a145fe7181d5f9bd867298b2a6b252a1', '127.0.0.1', '2019-09-23 04:49:29', 'false'),
('03e678cd8c0b03f90efacb0bdc85e396', '127.0.0.1', '2019-09-23 04:49:43', 'false'),
('68492c7976b7ac7af591823a2503f9c4', '127.0.0.1', '2019-09-23 04:50:04', 'false'),
('cae29efc6e5be076a7718ed645281453', '127.0.0.1', '2019-09-23 04:50:12', 'false'),
('b8cb2d6808db4f2b873ce75013b292d5', '127.0.0.1', '2019-09-23 04:50:40', 'false'),
('08649ccd6e301e58f73cd89c8c326bdd', '127.0.0.1', '2019-09-23 04:50:53', 'false'),
('582f81a8e33842271dc0cb02dec79767', '127.0.0.1', '2019-09-23 04:51:43', 'false'),
('560d4cb8272ce0f2baa42a9e795a1a05', '127.0.0.1', '2019-09-23 04:52:25', 'false'),
('42c95cc1c5824a3e437958884badab8c', '127.0.0.1', '2019-09-23 04:52:53', 'false'),
('7e980d653ebebb552331a3a417425e09', '127.0.0.1', '2019-09-23 04:53:49', 'false'),
('63d95c875c9e8b121b95158f0a333511', '127.0.0.1', '2019-09-23 04:54:24', 'false'),
('ca53c124a30cbb2b66c8af6c440732e5', '127.0.0.1', '2019-09-23 04:55:54', 'false'),
('d9ee0a5a57b25cfc19f6ad5c3f2028d2', '127.0.0.1', '2019-09-23 04:56:06', 'false'),
('b3b559da40c2e91e127b7cc0fe920544', '127.0.0.1', '2019-09-23 04:57:06', 'false'),
('d62e8572513725440b5090b414d518a9', '127.0.0.1', '2019-09-23 05:00:45', 'true'),
('ed048bb272a5c23a6cb3bf82167c3b27', '127.0.0.1', '2019-09-23 05:01:24', 'true'),
('86cc9e7843fa711ef2fab98cdcc12b2b', '127.0.0.1', '2019-09-23 05:01:39', 'true'),
('b1256d746d925485987a08b00ad867b5', '127.0.0.1', '2019-09-23 05:01:55', 'true'),
('dd9e37d0deeb5e40a254ed7336e5dc85', '127.0.0.1', '2019-09-23 05:02:09', 'true'),
('f5ad431bb245ea3c3fc02526d25fd40e', '127.0.0.1', '2019-09-23 05:02:20', 'true'),
('2789fdfd1ed15ac110b58759598d97dd', '127.0.0.1', '2019-09-23 05:02:52', 'true'),
('0eb9697701dcd2a140545e183be08394', '127.0.0.1', '2019-09-24 15:57:55', 'true'),
('596f50110691b0b04e984287170511fb', '127.0.0.1', '2019-09-25 03:20:07', 'false'),
('6911057f7f37d1344c6f4927f8332349', '127.0.0.1', '2019-09-25 03:20:41', 'false'),
('1346d6dc480239f46b03efd871ab0775', '127.0.0.1', '2019-09-25 03:23:55', 'false'),
('6260ab0fcae41bd73cd37a2b2156cbc8', '127.0.0.1', '2019-09-25 03:25:31', 'false'),
('88d2616a74108e3e81aa66aadf856c64', '127.0.0.1', '2019-09-25 03:27:32', 'false'),
('6c32db4e258c980a9563a6cc9cc21f9d', '127.0.0.1', '2019-09-25 03:28:26', 'false'),
('6ed67e9cbf88e0158e7fc2e29deb91a4', '127.0.0.1', '2019-09-25 03:31:31', 'false'),
('0a94512185c8f193eca8400c5634a7de', '127.0.0.1', '2019-09-25 03:38:00', 'false'),
('d516eb76e5f03de84eeee73ca8ee1ad7', '127.0.0.1', '2019-09-25 03:39:16', 'false'),
('1ed3b7071d7fb61257bee2c74aa71033', '127.0.0.1', '2019-09-25 03:39:18', 'false'),
('2e12fb067201ec13fce3ae281e025ae6', '127.0.0.1', '2019-09-25 03:39:21', 'false'),
('17cbf16848206a58bf8631a97dc15fd8', '127.0.0.1', '2019-09-25 03:39:24', 'false'),
('e23d5be7929b137a560ec1a585a24fb6', '127.0.0.1', '2019-09-25 03:39:51', 'false'),
('fe1b5f3f0cb23d660fa7f589b1d22aec', '127.0.0.1', '2019-09-25 03:39:53', 'false'),
('637d73f8e4c7fde90a841b13a430c170', '127.0.0.1', '2019-09-25 03:41:33', 'false'),
('f1417fe28b11295f1a5b06c8d596dc60', '127.0.0.1', '2019-09-25 03:42:16', 'false'),
('162f23f2e2dd25409e481826023a6cb9', '127.0.0.1', '2019-09-25 03:54:47', 'false'),
('9e8086675de83158ce938f48b33765e0', '127.0.0.1', '2019-09-25 03:54:51', 'false'),
('c0f793e11a81201786fe46aeb50a8190', '127.0.0.1', '2019-09-25 03:55:05', 'false'),
('be991d1128f7398f73b782596f181564', '127.0.0.1', '2019-09-25 03:55:07', 'false'),
('7806eb68963011d8f77561991759d726', '127.0.0.1', '2019-09-25 03:55:10', 'false'),
('d1c982c368bd9ecb2a404db58fac7306', '127.0.0.1', '2019-09-25 03:55:12', 'false'),
('722765486b31c54ceb4b3c203c1e53d3', '127.0.0.1', '2019-09-25 03:55:25', 'false'),
('e7cf2185a11efc7b29c979b8b3f95d5f', '127.0.0.1', '2019-09-25 03:55:52', 'false'),
('3d09c3c6f211dc1b0ec649f4cffb641c', '127.0.0.1', '2019-09-25 03:55:55', 'false'),
('49dd74ba8589382eca9b76f3e15dcd58', '127.0.0.1', '2019-09-25 03:55:56', 'false'),
('5bc2922ae41f1f73252b12affb2bdc39', '127.0.0.1', '2019-09-25 03:55:58', 'false'),
('39aae5eea3e4bcfc38018b396abd0812', '127.0.0.1', '2019-09-25 03:57:24', 'false'),
('408e8eb7726cebc2567fae0a0fda4486', '127.0.0.1', '2019-09-25 03:57:27', 'false'),
('7627707cd14d87c497e108a64f5cc889', '127.0.0.1', '2019-09-25 03:57:30', 'false'),
('c5559a88b8968b1017083c31045447f4', '127.0.0.1', '2019-09-25 03:57:50', 'false'),
('cd89eb786153284586202c59b53f39ac', '127.0.0.1', '2019-09-25 03:58:01', 'false'),
('d8e5cd312c8fc0a23764f480fe211517', '127.0.0.1', '2019-09-25 03:58:11', 'false'),
('66f242d1a30d26111e7421ad682634b2', '127.0.0.1', '2019-09-25 03:58:13', 'false'),
('5242c567cd2a78137b92342d72f4129b', '127.0.0.1', '2019-09-25 04:05:31', 'false'),
('86233f324f648133bb24ea7e4a80f41d', '127.0.0.1', '2019-09-25 04:05:33', 'false'),
('b46b2f7919c853d57728ef8e147223bb', '127.0.0.1', '2019-09-25 04:05:43', 'false'),
('9b7d910000d00b6301b552704647d46a', '127.0.0.1', '2019-09-25 04:05:46', 'false'),
('615cadf239228e15890ec999f23d9ff3', '127.0.0.1', '2019-09-25 04:06:40', 'false'),
('67b2c2d20c1377948ba477759d0c36f6', '127.0.0.1', '2019-09-25 04:06:43', 'false'),
('3bb610d6c09beebefbfaf99b58a03d41', '127.0.0.1', '2019-09-25 04:06:46', 'false'),
('6e7476d74eb65b28440a01d5284c7930', '127.0.0.1', '2019-09-25 04:06:58', 'false'),
('7f07f2d77ffdfcfd07712d8fd6665f2f', '127.0.0.1', '2019-09-25 04:07:01', 'false'),
('11b9c1547451963bbfceb80c98ca3e64', '127.0.0.1', '2019-09-25 04:07:04', 'false'),
('6fe01487e63f83a5af6f29844634285e', '127.0.0.1', '2019-09-25 04:07:11', 'false'),
('e07bd0d1527cb013135ace269f1ff5cc', '127.0.0.1', '2019-09-25 04:07:34', 'false'),
('24a8acae4e42eb54b7cf1b33c8b769f8', '127.0.0.1', '2019-09-25 04:07:37', 'false'),
('104d2628977385bd77e6d991ebe2f02f', '127.0.0.1', '2019-09-25 04:07:45', 'false'),
('a3ce73330a469ae6c5ae83ce7a705248', '127.0.0.1', '2019-09-25 04:07:48', 'false'),
('a152f3e8a29a6702b02b90871c1042ef', '127.0.0.1', '2019-09-25 04:07:56', 'false'),
('8f271c99398c8b9af061647080fec3b2', '127.0.0.1', '2019-09-25 04:07:59', 'false'),
('7a691000499aae0f8ec9f3a12053b4c3', '127.0.0.1', '2019-09-25 04:08:02', 'false'),
('252e46e57215291fd414c77e5a5dc2c1', '127.0.0.1', '2019-09-25 04:15:41', 'false'),
('3c295f27c4149f016dd9de230856a6c4', '127.0.0.1', '2019-09-25 04:17:00', 'false'),
('3f46a669fee47d50d543b8664d00f43f', '127.0.0.1', '2019-09-25 04:31:23', 'false'),
('8105de667134136c7112a5690a27425d', '127.0.0.1', '2019-09-25 04:31:32', 'false'),
('deacd1f1a544a4a38aa8a4d5cd54896a', '127.0.0.1', '2019-09-25 04:32:17', 'false'),
('836140fa694549e7ad022bea2b6a2f05', '127.0.0.1', '2019-09-25 04:32:26', 'false'),
('aadeaae546ace11cca846c1fb6e53ac5', '127.0.0.1', '2019-09-25 04:37:08', 'false'),
('923ac318fc5c3c929203a478b2d15c98', '127.0.0.1', '2019-09-25 04:37:40', 'false'),
('4d01e181365cd0f296ce0a6c0f1ec725', '127.0.0.1', '2019-09-25 04:37:54', 'false'),
('65dc85387daf8b271c2bc15b8b0a4496', '127.0.0.1', '2019-09-25 04:41:12', 'false'),
('3d8559ff8a3e2c1466bd075e2c95a439', '127.0.0.1', '2019-09-25 04:43:50', 'false'),
('0646f7f7eb6bb73257819e86f6d9c13b', '127.0.0.1', '2019-09-25 04:43:54', 'false'),
('d4ea5041733691b864dc03490789dd0b', '127.0.0.1', '2019-09-25 05:11:03', 'false'),
('3d5dc783e7523330483b527ae0204501', '127.0.0.1', '2019-09-25 05:16:17', 'false'),
('400de0adfcd405402199234589958612', '127.0.0.1', '2019-09-25 05:17:44', 'false'),
('39cc20766f5315689fb55e68030b4831', '127.0.0.1', '2019-09-25 05:18:03', 'false'),
('9597a4724684b648d0d8a6a2d9bc058a', '127.0.0.1', '2019-09-25 05:19:15', 'false'),
('7c7fa3745065071e4661449652077a78', '127.0.0.1', '2019-09-25 05:20:45', 'false'),
('d71450b56ddd50a4f8971cd63b836c7b', '127.0.0.1', '2019-09-25 05:26:38', 'false'),
('4b5f8122ae7ec6990ae83767fec71fdb', '127.0.0.1', '2019-09-25 05:28:04', 'false'),
('b94c6c1159bbf750cb8e52c2fa785631', '127.0.0.1', '2019-09-25 05:34:04', 'false'),
('609a6ec12fb5e2ae1dc3ea3f9278d3e8', '127.0.0.1', '2019-09-25 05:34:35', 'false'),
('5f8f7bd80242dd4fc015051ff4ce44c4', '127.0.0.1', '2019-09-25 05:35:17', 'false'),
('a7a0c45b34f9cfc59fec13e897c4d1b0', '127.0.0.1', '2019-09-25 05:35:19', 'false'),
('48578ffa467d226b981955277a46bf93', '127.0.0.1', '2019-09-25 05:35:21', 'false'),
('46ca30db9c09d92d4a521b15d560249e', '127.0.0.1', '2019-09-25 05:35:22', 'false'),
('74ec800f75c3f028a411cf86a2c20716', '127.0.0.1', '2019-09-25 05:36:16', 'false'),
('0b00a4e8565ff7c7641690304079adae', '127.0.0.1', '2019-09-25 05:36:18', 'false'),
('fe6f03ac0d1f384eed74f8f8450251f7', '127.0.0.1', '2019-09-25 11:00:04', 'false'),
('3fe525fd105d40f6dfdf2d4c426146a7', '127.0.0.1', '2019-09-25 11:00:41', 'false'),
('fe93a5067210e5ed2f09428fa76761a4', '127.0.0.1', '2019-09-25 11:03:21', 'false'),
('854aee34d7353261ca4b8b0cf4bd86fa', '127.0.0.1', '2019-09-25 11:03:23', 'false'),
('d63121ac10995c00d557b03491935f17', '127.0.0.1', '2019-09-25 11:03:40', 'false'),
('314201d52347d911eb20f589942146e1', '127.0.0.1', '2019-09-25 11:05:27', 'false'),
('f18e99ccccd20af1d03c17c920672e17', '127.0.0.1', '2019-09-25 11:06:00', 'false'),
('8507fbf00b26ca3d6dd050b55c8bf658', '127.0.0.1', '2019-09-25 11:06:57', 'false'),
('cbc5a392dd42dd13fda877c0c73bf916', '127.0.0.1', '2019-09-25 11:07:43', 'false'),
('433b7609488ccbfb803751393522fee2', '127.0.0.1', '2019-09-25 11:08:09', 'false'),
('843f69b9ea1c410fd0f2334d50d119be', '127.0.0.1', '2019-09-25 11:09:31', 'false'),
('c6914a18ae5db436d446f666efead50a', '127.0.0.1', '2019-09-25 11:09:43', 'false'),
('de628e401343f4e09f54ac062a2f759e', '127.0.0.1', '2019-09-25 11:10:08', 'false'),
('bc0a14197baa80d09d74d71359b11e03', '127.0.0.1', '2019-09-25 11:10:23', 'false'),
('e5d9d30245bab19e84ca9a81a395e1ae', '127.0.0.1', '2019-09-25 11:11:07', 'false'),
('14d1b15b7241cf57b880b1abb8a0b90a', '127.0.0.1', '2019-09-25 11:12:30', 'false'),
('a3c9cc1dbc81720b47f26bf709be60d1', '127.0.0.1', '2019-09-25 11:12:47', 'false'),
('436b1e7e8738e90aa84d855034e56e44', '127.0.0.1', '2019-09-25 11:15:36', 'false'),
('caaade0d63dbd06ca3182880ef12cbd7', '127.0.0.1', '2019-09-25 11:17:15', 'false'),
('19ec2022c6fd9033963a4d7f3f0a308b', '127.0.0.1', '2019-09-25 11:17:20', 'false'),
('a08baad0ff189bde61057719fa0ee2de', '127.0.0.1', '2019-09-25 11:26:17', 'false'),
('425a00ab9f07f5a738796668fb83b255', '127.0.0.1', '2019-09-25 11:31:40', 'false'),
('79f48ec8fce3b921759295b50bdd962a', '127.0.0.1', '2019-09-25 11:32:31', 'false'),
('faf5494f87b05294fbf97046e10781dc', '127.0.0.1', '2019-09-25 11:33:10', 'false'),
('febc3c4adc5451e0b599015e49f08625', '127.0.0.1', '2019-09-25 11:37:52', 'false'),
('d85fb9b881d68e17ab8797faf5ed522e', '127.0.0.1', '2019-09-25 11:43:49', 'false'),
('e1c152a8b48640b5d6054d72c9ec3cb2', '127.0.0.1', '2019-09-25 11:44:52', 'false'),
('23c91475c67831ef7848b584624eaae1', '127.0.0.1', '2019-09-25 11:45:04', 'false'),
('a20e7ea9c5b7b3168be1e02a5d2c75cf', '127.0.0.1', '2019-09-25 11:46:03', 'false'),
('02bbea85758e865cbce7072c2dd60972', '127.0.0.1', '2019-09-25 11:46:37', 'false'),
('10c3ad1e9d98a12b7ea366ce4f651056', '127.0.0.1', '2019-09-25 11:52:59', 'false'),
('6a6c78566a276a344864597e1997a2a7', '127.0.0.1', '2019-09-25 11:53:03', 'false'),
('176a3e14ed1b1a1ee97cab586133f14b', '127.0.0.1', '2019-09-25 11:55:37', 'false'),
('6437cb3712ca8724cccd2f4fb2fbb04d', '127.0.0.1', '2019-09-25 11:56:55', 'false'),
('8f7c78af8e7036644a55084274bd6d2e', '127.0.0.1', '2019-09-25 12:06:17', 'true'),
('b763ab9124c407c924bf3d748183e471', '127.0.0.1', '2019-09-25 12:06:44', 'true'),
('1c642828ed2a5ad256ea42744c30a6d3', '127.0.0.1', '2019-09-25 12:08:12', 'true'),
('c0b8b13b4fda3cbe283f9befe28a0630', '127.0.0.1', '2019-09-25 12:09:47', 'true'),
('f89efc293c21e0687163ad81353c45ca', '127.0.0.1', '2019-09-25 12:11:24', 'true'),
('14353d57963fdbb8c06d4f58a62df9bd', '127.0.0.1', '2019-09-25 12:17:11', 'true'),
('91169fa89457bf25665242912fd60412', '127.0.0.1', '2019-09-25 12:18:49', 'true'),
('2a944a022ae2c385874e81c8776d0c94', '127.0.0.1', '2019-09-25 12:20:20', 'true'),
('183650f6954b53b6797e6d2a29e21a92', '127.0.0.1', '2019-09-25 12:22:43', 'true'),
('fa9c543c0e9c2992e53e63f215b7d8b3', '127.0.0.1', '2019-09-25 12:23:58', 'true'),
('9ff3384d301f908437f1946b0c9ecc18', '127.0.0.1', '2019-09-25 12:25:33', 'true'),
('d77215426c072a7b50de1bfc873a7eb3', '127.0.0.1', '2019-09-25 12:25:52', 'true'),
('3ced8cffa300d39824e5029f8673f755', '127.0.0.1', '2019-09-25 12:25:59', 'true'),
('4964264cab74580a76840a6ac88d2db7', '127.0.0.1', '2019-09-25 12:26:18', 'true'),
('d4d936e7dc0ce0842466c4ca1a99c967', '127.0.0.1', '2019-09-25 12:27:53', 'true'),
('459df3fde38af9adabca4d86a9551726', '127.0.0.1', '2019-09-25 12:28:05', 'true'),
('985ee970a5b5e69134be5d278c0cf198', '127.0.0.1', '2019-09-25 12:28:13', 'true'),
('0fb6100e7204070e65cd6cf364340cfe', '127.0.0.1', '2019-09-25 12:28:47', 'true'),
('43e0107e9dbba0bc2499a812b4903d5c', '127.0.0.1', '2019-09-25 12:29:01', 'true'),
('074b5e54daf5a3ea2c367c14dfc61066', '127.0.0.1', '2021-04-14 03:52:20', 'false'),
('bbf216668de27cbfc7b4ed759aaa86cf', '127.0.0.1', '2021-04-14 03:52:56', 'false'),
('8f478d3bff049b56c83b9b7c29596943', '127.0.0.1', '2021-04-14 03:57:28', 'false'),
('0ad126a95caa39ecce69aec942ed10a8', '127.0.0.1', '2021-04-14 03:57:30', 'false'),
('3ce39c74c404e63dbd438406ee31a703', '127.0.0.1', '2021-04-14 03:58:42', 'false'),
('361bb2efd4ec2f2f36602b0babfa2916', '127.0.0.1', '2021-04-14 03:58:47', 'false'),
('ea63c29af5009cb4bf78916265e4c266', '127.0.0.1', '2021-04-14 05:22:01', 'false'),
('f257cbd99e312f70f91d213d2224cac5', '127.0.0.1', '2021-04-14 05:23:17', 'false'),
('f968ca0e03ddb383e45a550e4aa80e41', '127.0.0.1', '2021-04-14 05:24:22', 'false'),
('5fc72302aef1e6ce29ad200337f367e8', '127.0.0.1', '2021-04-14 05:24:28', 'false'),
('9bf92b010299b9123a37a66a71279238', '127.0.0.1', '2021-04-14 05:25:11', 'false'),
('5b4aef057529799f68a78aaa73f6fc36', '127.0.0.1', '2021-04-14 05:25:14', 'false'),
('e982d0c4cd140ac9059a6d1ab7461c14', '127.0.0.1', '2021-04-14 05:25:25', 'false'),
('cbab4e7495901376988218c5ce77fcf0', '127.0.0.1', '2021-04-14 05:25:30', 'false'),
('586f2a6d0dac34e37fa3756b506d6e06', '127.0.0.1', '2021-04-14 05:25:50', 'false'),
('a3fa76b113cb119e2b4301705271f547', '127.0.0.1', '2021-04-14 05:25:52', 'false'),
('497ce0eee5cae7eebc110f1ae42a9e88', '127.0.0.1', '2021-04-14 05:25:53', 'false'),
('14d2d078f514391dcf5a2c615e2d2db6', '127.0.0.1', '2021-04-14 05:25:53', 'false'),
('f96f82c1b3b20e608a87eb6f1c3b573f', '127.0.0.1', '2021-04-14 05:25:53', 'false'),
('d93aa4d51a435c0783ef13ea8a767953', '127.0.0.1', '2021-04-14 05:25:54', 'false'),
('28817bbc06acf3e9f2a424c06c30684a', '127.0.0.1', '2021-04-14 05:25:58', 'false'),
('7cf9dbb9b75b538574b6cff7b42afced', '127.0.0.1', '2021-04-14 05:26:19', 'false'),
('d4cc46f0b17717f1a87901524564780c', '127.0.0.1', '2021-04-14 05:26:20', 'false'),
('e09cf365432001756ffd6fbbba5f5459', '127.0.0.1', '2021-04-14 05:26:20', 'false'),
('d92d24c225c31074ebd31b8ef8365e37', '127.0.0.1', '2021-04-14 05:26:21', 'false'),
('bad0e28e110432fac1e38212c8636751', '127.0.0.1', '2021-04-14 05:26:46', 'false'),
('a1b7ff010e8a540e1257a1c0bf1a205a', '127.0.0.1', '2021-04-14 05:26:47', 'false'),
('593f0efed8a7dee224d5170c35df80ac', '127.0.0.1', '2021-04-14 05:27:09', 'false'),
('939297314732a9bc56a2cca15be9f6cd', '127.0.0.1', '2021-04-14 05:27:11', 'false'),
('2f82e10ebe7a2c0a508fbbf0fd83c12b', '127.0.0.1', '2021-04-14 05:27:14', 'false'),
('49fdb619ca0dd980836b9b0fd2783c9e', '127.0.0.1', '2021-04-14 05:27:41', 'false'),
('f299223c7c4e27c3cc4f1062673f39e4', '127.0.0.1', '2021-04-14 05:28:43', 'false'),
('9337a2a13b0489ad9791ebdef576467c', '127.0.0.1', '2021-04-14 05:28:55', 'false'),
('2b9c0834e1326518620401758463bd33', '127.0.0.1', '2021-04-14 05:29:14', 'false'),
('50d72bf485199fb1bd2485f3cfffe958', '127.0.0.1', '2021-04-14 05:30:48', 'false'),
('86dcdd29386e60f8f25ccfb5901a4719', '127.0.0.1', '2021-04-14 05:30:57', 'false'),
('abc4ae825ac7695b142aca65b915f971', '127.0.0.1', '2021-04-14 05:31:35', 'false'),
('9f1eeb7b951040bf18a2c2f05c210b32', '127.0.0.1', '2021-04-14 05:31:38', 'false'),
('d3571c0d167498680e54ae46532fae9e', '127.0.0.1', '2021-04-14 05:31:43', 'false'),
('b6d60118b233b10d004164a195796e53', '127.0.0.1', '2021-04-14 05:31:48', 'false'),
('4c57ee6ef5b3abff3e68cbb935fd3421', '127.0.0.1', '2021-04-14 05:31:58', 'false'),
('17c6ee2e1ab9c133c748397a672ff62a', '127.0.0.1', '2021-04-14 05:34:05', 'false'),
('4f6d3d147cf52829f68a8873cb9cce9b', '127.0.0.1', '2021-04-14 05:34:47', 'false'),
('d9e87e6baceacd705a2311370ad9f883', '127.0.0.1', '2021-04-14 05:35:30', 'false'),
('9394861c1126f785c65239f486ef254b', '127.0.0.1', '2021-04-14 05:36:29', 'false'),
('9438a8c09b6b34c176fadcda22771df2', '127.0.0.1', '2021-04-14 08:30:17', 'false'),
('b08d4dc5cfe354284d53df417a00152e', '127.0.0.1', '2021-04-14 09:18:07', 'true'),
('a4506809fe8347d3131661768647fe62', '127.0.0.1', '2021-04-22 05:17:28', 'true'),
('3102241f1ff3d88e7c7a8df1971c2154', '127.0.0.1', '2021-05-17 05:11:31', 'true'),
('ccc39caec6c112c241668160ba72e3e6', '127.0.0.1', '2021-07-16 02:23:12', 'true'),
('d80ed18b38e9975dd15b16e2011433cd', '127.0.0.1', '2021-07-16 02:23:31', 'true'),
('7446fbe7aa2dcb67ecffdff2252d51b5', '127.0.0.1', '2021-07-16 02:27:29', 'true');

-- --------------------------------------------------------

--
-- Struktur dari tabel `info_dari_cabang`
--

CREATE TABLE `info_dari_cabang` (
  `kd` varchar(50) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `judul` longtext DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `info_dari_majelis`
--

CREATE TABLE `info_dari_majelis` (
  `kd` varchar(50) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `judul` longtext DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `info_dari_majelis`
--

INSERT INTO `info_dari_majelis` (`kd`, `kategori`, `judul`, `isi`, `postdate`) VALUES
('06d4952b51df9cf79f41a02d0bac44db', 'Berita', 'Uji Coba Entri Data', 'xkkirixpxkkananxSilahkan Para Operator dan Bendahara Sekolah, bisa melakukan entri data.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxMinimal tiap hari atau beberapa hari sekali, masuk login ke web Smart Office dan melakukan entri data.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxAgar data semakin lengkap.xkkirixxgmringxpxkkananx\r\n\r\nxkkirixpxkkananxTerima kasih.xkkirixxgmringxpxkkananx', '2021-08-27 08:57:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_ketua`
--

CREATE TABLE `majelis_ketua` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `ijazah` varchar(100) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_ketua`
--

INSERT INTO `majelis_ketua` (`kd`, `nama`, `kode`, `ijazah`, `tgl_awal`, `tgl_akhir`, `postdate`) VALUES
('52296a0b20d7f13966413097fae15478', '1234', '1234', '1234', '2021-08-17', '2021-08-26', '2021-08-25 11:08:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_pegawai`
--

CREATE TABLE `majelis_pegawai` (
  `kd` varchar(50) NOT NULL,
  `nbm` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kelamin` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `no_karpeg` varchar(100) DEFAULT NULL,
  `nuptk` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `pangkat` varchar(100) DEFAULT NULL,
  `pangkat_sk` varchar(100) DEFAULT NULL,
  `pangkat_sk_no` varchar(100) DEFAULT NULL,
  `pangkat_sk_tgl` varchar(100) DEFAULT NULL,
  `tmt` varchar(100) DEFAULT NULL,
  `kualifikasi` varchar(100) DEFAULT NULL,
  `mk_gol` varchar(100) DEFAULT NULL,
  `mk_total` varchar(100) DEFAULT NULL,
  `pensiun` varchar(100) DEFAULT NULL,
  `pensiun_tgl` date DEFAULT NULL,
  `jenis_ptk` varchar(100) DEFAULT NULL,
  `lahir_tmp` varchar(100) DEFAULT NULL,
  `lahir_tgl` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `bekerja_sejak_disini` varchar(100) DEFAULT NULL,
  `bekerja_sejak_dimuh` varchar(100) DEFAULT NULL,
  `ijazah` varchar(100) DEFAULT NULL,
  `ijazah_pddkn` varchar(100) DEFAULT NULL,
  `mengajar` varchar(100) DEFAULT NULL,
  `sertifikasi` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `sumber_gaji` varchar(100) DEFAULT NULL,
  `jml_anak` varchar(5) DEFAULT NULL,
  `status_sertifikasi` varchar(100) DEFAULT NULL,
  `jml_gaji_pokok` varchar(100) DEFAULT NULL,
  `jml_tunjangan_pasangan` varchar(100) DEFAULT NULL,
  `jml_tunjangan_bpjs` varchar(100) DEFAULT NULL,
  `jml_honor` varchar(100) DEFAULT NULL,
  `jml_tunjangan_profesi` varchar(100) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `usernamex` varchar(100) DEFAULT NULL,
  `passwordx` varchar(100) DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `postdate_user` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_pegawai`
--

INSERT INTO `majelis_pegawai` (`kd`, `nbm`, `nama`, `kelamin`, `alamat`, `telp`, `kode`, `no_karpeg`, `nuptk`, `jabatan`, `pangkat`, `pangkat_sk`, `pangkat_sk_no`, `pangkat_sk_tgl`, `tmt`, `kualifikasi`, `mk_gol`, `mk_total`, `pensiun`, `pensiun_tgl`, `jenis_ptk`, `lahir_tmp`, `lahir_tgl`, `status`, `bekerja_sejak_disini`, `bekerja_sejak_dimuh`, `ijazah`, `ijazah_pddkn`, `mengajar`, `sertifikasi`, `postdate`, `sumber_gaji`, `jml_anak`, `status_sertifikasi`, `jml_gaji_pokok`, `jml_tunjangan_pasangan`, `jml_tunjangan_bpjs`, `jml_honor`, `jml_tunjangan_profesi`, `ket`, `usernamex`, `passwordx`, `email`, `postdate_user`) VALUES
('c81e728d9d4c2f636f067f89cc14862c', '2', '2', NULL, '2', '2', '2', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', '2021-08-02', NULL, '2', '2', '2', '2', '2', '2', '2021-08-25 11:22:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'c81e728d9d4c2f636f067f89cc14862c', '2', NULL),
('c4ca4238a0b923820dcc509a6f75849b', '1', '1', NULL, '1', '1', '1', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2021-08-02', NULL, '1', '1', '1', '1', '1', '1', '2021-08-25 11:22:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_rekap_sekolah`
--

CREATE TABLE `majelis_rekap_sekolah` (
  `kd` varchar(50) NOT NULL,
  `tahun1` varchar(4) DEFAULT NULL,
  `tahun2` varchar(4) DEFAULT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `sek_nama` varchar(100) DEFAULT NULL,
  `sek_alamat` varchar(100) DEFAULT NULL,
  `sek_telp` varchar(100) DEFAULT NULL,
  `thn_berdiri` varchar(4) DEFAULT NULL,
  `penyelenggara` varchar(100) DEFAULT NULL,
  `luas_tanah` varchar(100) DEFAULT NULL,
  `luas_bangunan` varchar(100) DEFAULT NULL,
  `akreditasi` varchar(100) DEFAULT NULL,
  `guru_depag` varchar(100) DEFAULT NULL,
  `guru_pnk` varchar(100) DEFAULT NULL,
  `guru_tetap` varchar(100) DEFAULT NULL,
  `guru_bantu` varchar(100) DEFAULT NULL,
  `guru_gtt` varchar(100) DEFAULT NULL,
  `guru_total` varchar(100) DEFAULT NULL,
  `karyawan_tetap` varchar(100) DEFAULT NULL,
  `karyawan_hr` varchar(100) DEFAULT NULL,
  `kelas_1` varchar(100) DEFAULT NULL,
  `kelas_2` varchar(100) DEFAULT NULL,
  `kelas_3` varchar(100) DEFAULT NULL,
  `kelas_4` varchar(100) DEFAULT NULL,
  `kelas_5` varchar(100) DEFAULT NULL,
  `kelas_6` varchar(100) DEFAULT NULL,
  `kelas_total` varchar(100) DEFAULT NULL,
  `siswa_1` varchar(100) DEFAULT NULL,
  `siswa_2` varchar(100) DEFAULT NULL,
  `siswa_3` varchar(100) DEFAULT NULL,
  `siswa_4` varchar(100) DEFAULT NULL,
  `siswa_5` varchar(100) DEFAULT NULL,
  `siswa_6` varchar(100) DEFAULT NULL,
  `siswa_total` varchar(100) DEFAULT NULL,
  `spp_rata` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_sekolah_kib_a`
--

CREATE TABLE `majelis_sekolah_kib_a` (
  `kd` varchar(100) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `luas` varchar(100) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_sekolah_kib_a`
--

INSERT INTO `majelis_sekolah_kib_a` (`kd`, `cabang`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `tahun`, `luas`, `harga`, `postdate`) VALUES
('16b00c0fb29f486620c652042512a31d', NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2022', '11', '11', '2022-05-12 22:55:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_sekolah_kib_b`
--

CREATE TABLE `majelis_sekolah_kib_b` (
  `kd` varchar(100) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_sekolah_kib_b`
--

INSERT INTO `majelis_sekolah_kib_b` (`kd`, `cabang`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `tahun`, `harga`, `postdate`) VALUES
('16b00c0fb29f486620c652042512a31d', NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2022', '23', '2022-05-12 22:55:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_sekolah_kib_c`
--

CREATE TABLE `majelis_sekolah_kib_c` (
  `kd` varchar(100) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `luas_lantai` varchar(100) DEFAULT NULL,
  `luas_tanah` varchar(100) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_sekolah_kib_c`
--

INSERT INTO `majelis_sekolah_kib_c` (`kd`, `cabang`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `tahun`, `luas_lantai`, `luas_tanah`, `harga`, `postdate`) VALUES
('16b00c0fb29f486620c652042512a31d', NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2022', '15', '17', '15', '2022-05-12 22:55:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_sekolah_kib_d`
--

CREATE TABLE `majelis_sekolah_kib_d` (
  `kd` varchar(100) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `luas` varchar(100) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_sekolah_kib_d`
--

INSERT INTO `majelis_sekolah_kib_d` (`kd`, `cabang`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `tahun`, `luas`, `harga`, `postdate`) VALUES
('16b00c0fb29f486620c652042512a31d', NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2022', '21', '18', '2022-05-12 22:55:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_sekolah_kib_e`
--

CREATE TABLE `majelis_sekolah_kib_e` (
  `kd` varchar(100) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_sekolah_kib_e`
--

INSERT INTO `majelis_sekolah_kib_e` (`kd`, `cabang`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `tahun`, `harga`, `postdate`) VALUES
('16b00c0fb29f486620c652042512a31d', NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2022', '17', '2022-05-12 22:55:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_sekolah_kib_f`
--

CREATE TABLE `majelis_sekolah_kib_f` (
  `kd` varchar(100) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_sekolah_kib_f`
--

INSERT INTO `majelis_sekolah_kib_f` (`kd`, `cabang`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `tahun`, `harga`, `postdate`) VALUES
('16b00c0fb29f486620c652042512a31d', NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2022', '18', '2022-05-12 22:55:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_sekolah_uang_keluar`
--

CREATE TABLE `majelis_sekolah_uang_keluar` (
  `kd` varchar(50) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `total` varchar(50) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_sekolah_uang_keluar`
--

INSERT INTO `majelis_sekolah_uang_keluar` (`kd`, `cabang`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `tahun`, `total`, `postdate`) VALUES
('e13d08961bd260fcd475654905330d4f', NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2022', '2680000', '2022-05-12 22:54:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_sekolah_uang_masuk`
--

CREATE TABLE `majelis_sekolah_uang_masuk` (
  `kd` varchar(50) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `total` varchar(50) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_sekolah_uang_masuk`
--

INSERT INTO `majelis_sekolah_uang_masuk` (`kd`, `cabang`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `tahun`, `total`, `postdate`) VALUES
('faf2a41fec5b48af306460b725244a23', NULL, '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2022', '2329000', '2022-05-12 22:55:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `majelis_struktur_organisasi`
--

CREATE TABLE `majelis_struktur_organisasi` (
  `kd` varchar(50) NOT NULL,
  `nourut` varchar(10) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `majelis_struktur_organisasi`
--

INSERT INTO `majelis_struktur_organisasi` (`kd`, `nourut`, `kode`, `nama`, `jabatan`, `tgl_awal`, `tgl_akhir`, `postdate`) VALUES
('9b5c94af6ee6d5e22729ed637833943d', '1', '122222', '122222', '122222', '2021-08-03', '2021-08-26', '2021-08-25 11:11:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_cabang`
--

CREATE TABLE `m_cabang` (
  `kd` varchar(50) NOT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `usernamex` varchar(100) DEFAULT NULL,
  `passwordx` varchar(100) DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `alamat_googlemap` longtext DEFAULT NULL,
  `lat_x` varchar(100) DEFAULT NULL,
  `lat_y` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `postdate_update` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_cabang`
--

INSERT INTO `m_cabang` (`kd`, `kode`, `nama`, `usernamex`, `passwordx`, `alamat`, `telp`, `email`, `alamat_googlemap`, `lat_x`, `lat_y`, `postdate`, `postdate_update`) VALUES
('b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', '1', 'Jl. Raya Semarang xstrix Batang No.199,  Karanggeneng,  Pegulon,  Kec. Kendal,  Kabupaten Kendal,  Jawa Tengah 51313,  Indonesia', '-6.921697100933536', '110.20407308771199', '2021-08-04 17:55:08', '2021-08-28 16:17:24'),
('390203695bc9abaabe182857568b9dbe', '2', 'CABANG 2', '2', 'c81e728d9d4c2f636f067f89cc14862c', '2', '2', '2', NULL, NULL, NULL, '2021-08-04 17:55:15', NULL),
('cac8cabf7194a8ba0cb33aa79062af99', '3', 'CABANG 3', '3', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', '3', NULL, NULL, NULL, '2021-08-04 17:56:12', NULL),
('05931de9faa31c0a0aef0adf3f4a37db', '4', 'CABANG 4', '4', 'a87ff679a2f3e71d9181a67b7542122c', '4', '4', '4', NULL, NULL, NULL, '2021-08-04 17:56:26', NULL),
('0a3a3d982dc7f051c5f25de40bc619b2', '5', 'CABANG 5', '5', 'e4da3b7fbbce2345d7772b0674a318d5', '5', '5', '5', NULL, NULL, NULL, '2021-08-04 17:56:42', NULL),
('bade8132bcc6797faffd9d2441bda30b', '6', 'CABANG 6', '6', '1679091c5a880faf6fb5e6087eb1b2dc', '6', '6', '6', NULL, NULL, NULL, '2021-08-04 17:56:55', NULL),
('ce3ebbb847d688a6395a490480c33580', '7', 'CABANG 7', '7', '8f14e45fceea167a5a36dedd4bea2543', '7', '7', '7', NULL, NULL, NULL, '2021-08-04 17:57:09', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_keadaan_karyawan`
--

CREATE TABLE `m_keadaan_karyawan` (
  `kd` varchar(50) NOT NULL,
  `nourut` varchar(5) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_keadaan_karyawan`
--

INSERT INTO `m_keadaan_karyawan` (`kd`, `nourut`, `nama`, `postdate`) VALUES
('c4ca4238a0b923820dcc509a6f75849b', '1', 'GT Negeri P dan K', '2021-03-26 09:30:04'),
('c81e728d9d4c2f636f067f89cc14862c', '2', 'GT Negeri Depag', '2021-03-26 09:30:29'),
('eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', 'Guru Bantu/Kontrak', '2021-03-26 09:30:45'),
('a87ff679a2f3e71d9181a67b7542122c', '4', 'GT Persyarikatan', '2021-03-26 09:31:07'),
('e4da3b7fbbce2345d7772b0674a318d5', '5', 'GTT/ Honorer', '2021-03-26 09:31:23'),
('1679091c5a880faf6fb5e6087eb1b2dc', '6', 'KT Persyarikatan', '2021-03-26 09:31:47'),
('8f14e45fceea167a5a36dedd4bea2543', '7', 'Kary. Tidak Tetap', '2021-03-26 09:32:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kib_jenis`
--

CREATE TABLE `m_kib_jenis` (
  `kd` varchar(50) NOT NULL,
  `nourut` varchar(2) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_kib_jenis`
--

INSERT INTO `m_kib_jenis` (`kd`, `nourut`, `nama`) VALUES
('A', '01', 'TANAH'),
('B', '02', 'PERALATAN DAN MESIN'),
('C', '03', 'GEDUNG DAN BANGUNAN'),
('D', '04', 'JALAN, IRIGASI DAN JARINGAN'),
('E', '05', 'ASET TETAP LAINNYA'),
('F', '06', 'KONTRUKSI DALAM PENYELESAIAN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_kib_kode`
--

CREATE TABLE `m_kib_kode` (
  `kd` varchar(50) NOT NULL,
  `golongan` varchar(10) DEFAULT NULL,
  `bidang` varchar(10) DEFAULT NULL,
  `kelompok` varchar(10) DEFAULT NULL,
  `kelompok_sub` varchar(10) DEFAULT NULL,
  `kelompok_sub_sub` varchar(10) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_kib_kode`
--

INSERT INTO `m_kib_kode` (`kd`, `golongan`, `bidang`, `kelompok`, `kelompok_sub`, `kelompok_sub_sub`, `kode`, `nama`, `postdate`) VALUES
('587648f378ccc3297a13d35542ef6a58', '01', '*', '*', '*', '*', '01.*.*.*.*', 'GOLONGAN TANAH', '2021-08-06 04:24:43'),
('545263db002ff781fd01d479254c3b7f', '01', '01', '*', '*', '*', '01.01.*.*.*', 'TANAH', '2021-08-06 04:24:43'),
('6d0fb7637c91d717b8f11953c674c404', '01', '01', '01', '*', '*', '01.01.01.*.*', 'PERKAMPUNGAN', '2021-08-06 04:24:43'),
('a30629fe981dbe002d36cbfbab8a6e39', '01', '01', '01', '01', '*', '01.01.01.01.*', 'Kampung', '2021-08-06 04:24:43'),
('13f00488e0c58c98196fd93e33f76750', '01', '01', '01', '01', '01', '01.01.01.01.01', 'Kampung', '2021-08-06 04:24:43'),
('dd49350633bc926c6906b93e9656fcfd', '01', '01', '01', '01', '02', '01.01.01.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('66fba31c8398c78ecb471868e1f74b99', '01', '01', '01', '02', '*', '01.01.01.02.*', 'Emplasmen', '2021-08-06 04:24:43'),
('9d120e8600dafeb135b9c314402eeba5', '01', '01', '01', '02', '01', '01.01.01.02.01', 'Emplasmen', '2021-08-06 04:24:43'),
('7ce89290b2412ed6f55c6fa72e08b181', '01', '01', '01', '02', '02', '01.01.01.02.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('f886678928a5e627e9fde00b8caff950', '01', '01', '01', '03', '*', '01.01.01.03.*', 'Kuburan', '2021-08-06 04:24:43'),
('f229440ebbeb8564ec08fed5591a4f3f', '01', '01', '01', '03', '01', '01.01.01.03.01', 'Islam', '2021-08-06 04:24:43'),
('adaea173a454a054c91b3955bac1f613', '01', '01', '01', '03', '02', '01.01.01.03.02', 'Kristen', '2021-08-06 04:24:43'),
('c5d3db10aaee3eb45de5be163953ad03', '01', '01', '01', '03', '03', '01.01.01.03.03', 'Cina', '2021-08-06 04:24:43'),
('27dc2c80afdfb26822804b9576399ea1', '01', '01', '01', '03', '04', '01.01.01.03.04', 'Hindu', '2021-08-06 04:24:43'),
('acc2b0f0db3ff6f445ae2af4e3040539', '01', '01', '01', '03', '05', '01.01.01.03.05', 'Budha', '2021-08-06 04:24:43'),
('a7ea52e6836f9e87dcb37df1b764bbdb', '01', '01', '01', '03', '06', '01.01.01.03.06', 'Makam Pahlawan', '2021-08-06 04:24:43'),
('ab3f1d4e9bffba667129e3b72f924fe9', '01', '01', '01', '03', '07', '01.01.01.03.07', 'Tempat Benda Bersejarah', '2021-08-06 04:24:43'),
('ff747ee0f1af0940df94d2feae208b7a', '01', '01', '01', '03', '08', '01.01.01.03.08', 'Makam UmumxgmringxKuburan Umum', '2021-08-06 04:24:43'),
('497788ea708348d181330288f0934fbc', '01', '01', '01', '03', '09', '01.01.01.03.09', 'lainxstrixlain', '2021-08-06 04:24:43'),
('cb58184ea28a14431d379621666b17d8', '01', '01', '02', '*', '*', '01.01.02.*.*', 'TANAH PERTANIAN', '2021-08-06 04:24:43'),
('7eea9d1230808a439bd64805ec75c6d1', '01', '01', '02', '01', '*', '01.01.02.01.*', 'Sawah Satu Tahun Ditanami', '2021-08-06 04:24:43'),
('07632f6ea70486b9f9fd3c57f17fdf09', '01', '01', '02', '01', '01', '01.01.02.01.01', 'Padi', '2021-08-06 04:24:43'),
('f5f6b0f36db80c897cb830e945023a4f', '01', '01', '02', '01', '02', '01.01.02.01.02', 'Palawija', '2021-08-06 04:24:43'),
('2c0efb9a8fd7c2d8bb83a445a8c9d8f4', '01', '01', '02', '01', '03', '01.01.02.01.03', 'Sawah Ditanami Tebu', '2021-08-06 04:24:43'),
('454ddcfb9cd75a453e976285e3dadc84', '01', '01', '02', '01', '04', '01.01.02.01.04', 'Sawah Ditanami Sayuran', '2021-08-06 04:24:43'),
('6c7527050232771a7ba7857e40fcd70b', '01', '01', '02', '01', '05', '01.01.02.01.05', 'Sawah Ditanami Tembakau', '2021-08-06 04:24:43'),
('699df0ca9bdd89705caf24ac415610e1', '01', '01', '02', '01', '06', '01.01.02.01.06', 'Sawah Ditanami Rosella', '2021-08-06 04:24:43'),
('9f684ffec2e38ef6e69671fb5b1997d9', '01', '01', '02', '01', '07', '01.01.02.01.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('2dd02da23c6359a5b21b48f6e61f3897', '01', '01', '02', '02', '*', '01.01.02.02.*', 'Tegalan', '2021-08-06 04:24:43'),
('68fd74ba5c889755f0c00c5bf4dd35ee', '01', '01', '02', '02', '01', '01.01.02.02.01', 'Buahxstrixbuahan', '2021-08-06 04:24:43'),
('38a1c9c7d0dbfd0eb3d1238c7e50a4d4', '01', '01', '02', '02', '02', '01.01.02.02.02', 'Tembakau', '2021-08-06 04:24:43'),
('18b60fd481c2210ec832106212faae80', '01', '01', '02', '02', '03', '01.01.02.02.03', 'Jagung', '2021-08-06 04:24:43'),
('87bbc72e24e4024773ec01409da90ed5', '01', '01', '02', '02', '04', '01.01.02.02.04', 'Ketela Pohon', '2021-08-06 04:24:43'),
('54a922c2ddd98ec3e0f7ceb87d50a531', '01', '01', '02', '02', '05', '01.01.02.02.05', 'Kacang Tanah', '2021-08-06 04:24:43'),
('567a082b37909f8a35d9cd38269f4952', '01', '01', '02', '02', '06', '01.01.02.02.06', 'Kacang Hijau', '2021-08-06 04:24:43'),
('19dc1aba96166852b1950f8b85428e39', '01', '01', '02', '02', '07', '01.01.02.02.07', 'Kedelai', '2021-08-06 04:24:43'),
('a06a2c0cca9a678ad592491c2bb5b4a8', '01', '01', '02', '02', '08', '01.01.02.02.08', 'Ubi Jalar', '2021-08-06 04:24:43'),
('ff902066c21f8384e5fe8d4f727be2d5', '01', '01', '02', '02', '09', '01.01.02.02.09', 'Keladi', '2021-08-06 04:24:43'),
('713ba8899b6ef5c9989b76eedf78db18', '01', '01', '02', '02', '10', '01.01.02.02.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('9eba1200356b2594f15873b7f27b02a0', '01', '01', '02', '03', '*', '01.01.02.03.*', 'Ladang', '2021-08-06 04:24:43'),
('a73ff5403113c6f9c2847f2ff03a9a72', '01', '01', '02', '03', '01', '01.01.02.03.01', 'Padi', '2021-08-06 04:24:43'),
('616847f6501d9bcf0f74a9d359c74810', '01', '01', '02', '03', '02', '01.01.02.03.02', 'Jagung', '2021-08-06 04:24:43'),
('d3b84ed699705f4c3b20582da2fface9', '01', '01', '02', '03', '03', '01.01.02.03.03', 'Ketela Pohon', '2021-08-06 04:24:43'),
('24f53e6952f70033b4aa5ca4c9d679cd', '01', '01', '02', '03', '04', '01.01.02.03.04', 'Kacang Tanah', '2021-08-06 04:24:43'),
('3f17b39ecb2a75de8b622b391871be46', '01', '01', '02', '03', '05', '01.01.02.03.05', 'Kacang Hijau', '2021-08-06 04:24:43'),
('1140508280f7e60137245747790b40e2', '01', '01', '02', '03', '06', '01.01.02.03.06', 'Kedelai', '2021-08-06 04:24:43'),
('8f7d2f2d82a30a424b61ac106ae71439', '01', '01', '02', '03', '07', '01.01.02.03.07', 'Ubi Jalar', '2021-08-06 04:24:43'),
('3adbc001d99cd8210d2d13120e03d995', '01', '01', '02', '03', '08', '01.01.02.03.08', 'Keladi', '2021-08-06 04:24:43'),
('21bc46a3373fcd72617ffa2c79c09df6', '01', '01', '02', '03', '09', '01.01.02.03.09', 'Bengkuang', '2021-08-06 04:24:43'),
('c3d7191ddbb3e9f6cbc48f6ca426b642', '01', '01', '02', '03', '10', '01.01.02.03.10', 'Appel', '2021-08-06 04:24:43'),
('42d095d1ea572f913cdf6de63828b8b1', '01', '01', '02', '03', '11', '01.01.02.03.11', 'Kentang', '2021-08-06 04:24:43'),
('c94e26f7f6ed6155fd4437b31cdc7bcf', '01', '01', '02', '03', '12', '01.01.02.03.12', 'Jeruk', '2021-08-06 04:24:43'),
('ab9d5167b658a783ba84b8812fafdcbb', '01', '01', '02', '03', '13', '01.01.02.03.13', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('5593d1796139162c9a977accbdb4cc24', '01', '01', '03', '*', '*', '01.01.03.*.*', 'TANAH PERKEBUNAN', '2021-08-06 04:24:43'),
('552f5262b28e5000a0ee8678a4fdeadc', '01', '01', '03', '01', '*', '01.01.03.01.*', 'Perkebunan', '2021-08-06 04:24:43'),
('d6b288fd477a1e63d086e6e3e405b64b', '01', '01', '03', '01', '01', '01.01.03.01.01', 'Karet', '2021-08-06 04:24:43'),
('b6038c6e1fcd5306a4e80513f1730b30', '01', '01', '03', '01', '02', '01.01.03.01.02', 'Kopi', '2021-08-06 04:24:43'),
('e9a3be3b04ef3f6393a8d158bf297854', '01', '01', '03', '01', '03', '01.01.03.01.03', 'Kelapa', '2021-08-06 04:24:43'),
('cbb7e00a64c90759f54633634e02b809', '01', '01', '03', '01', '04', '01.01.03.01.04', 'Randu', '2021-08-06 04:24:43'),
('916475cccc8285923a3c35e4579cf6bd', '01', '01', '03', '01', '05', '01.01.03.01.05', 'Lada', '2021-08-06 04:24:43'),
('3ea36bc43adde63fd19fc96de5a8c2d4', '01', '01', '03', '01', '06', '01.01.03.01.06', 'Teh', '2021-08-06 04:24:43'),
('37ff110fbad681d85fbb9540ad8c4d67', '01', '01', '03', '01', '07', '01.01.03.01.07', 'Kina', '2021-08-06 04:24:43'),
('9877a8124bf2b2cd0276a149761c148e', '01', '01', '03', '01', '08', '01.01.03.01.08', 'Coklat', '2021-08-06 04:24:43'),
('35d8f3fdf372a24cfb587a2906aa2c33', '01', '01', '03', '01', '09', '01.01.03.01.09', 'Kelapa Sawit', '2021-08-06 04:24:43'),
('d8bf650453a751e7b54308dd3a339fd6', '01', '01', '03', '01', '10', '01.01.03.01.10', 'Sereh', '2021-08-06 04:24:43'),
('bbebb1fad512ee0957dc86033e03ca9e', '01', '01', '03', '01', '11', '01.01.03.01.11', 'Cengkeh', '2021-08-06 04:24:43'),
('31d76b65807d75f982b1ed2d212cb796', '01', '01', '03', '01', '12', '01.01.03.01.12', 'Pala', '2021-08-06 04:24:43'),
('fa47e3202ef34cd6f7370dc9426d3e7f', '01', '01', '03', '01', '13', '01.01.03.01.13', 'Sagu', '2021-08-06 04:24:43'),
('27e386c5ef8aec337331bcb4647f2735', '01', '01', '03', '01', '14', '01.01.03.01.14', 'Jambu Mente', '2021-08-06 04:24:43'),
('e4688905623562045759efeb36113b29', '01', '01', '03', '01', '15', '01.01.03.01.15', 'Tengkawang', '2021-08-06 04:24:43'),
('9722741c3e8fa3eeede0f94386d5eaa6', '01', '01', '03', '01', '16', '01.01.03.01.16', 'Minyak Kayu Putih', '2021-08-06 04:24:43'),
('0efc911ec208852e7d8c0498e137c068', '01', '01', '03', '01', '17', '01.01.03.01.17', 'Kayu Manis', '2021-08-06 04:24:43'),
('f00ce8eb917c052f4d688c19872565e8', '01', '01', '03', '01', '18', '01.01.03.01.18', 'Petai', '2021-08-06 04:24:43'),
('a8d715bc2725df686a9297c07df1ef8e', '01', '01', '03', '01', '19', '01.01.03.01.19', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('69c9d0aced869d99210870953e866a1e', '01', '01', '04', '*', '*', '01.01.04.*.*', 'KEBUN CAMPURAN', '2021-08-06 04:24:43'),
('6946bea56f2cf9c142b3f59c82dabdba', '01', '01', '04', '01', '*', '01.01.04.01.*', 'Bidang Tanah Yang Tidak Ada Jaringan Pengairan', '2021-08-06 04:24:43'),
('e6e47f810891329b212e355118bbdf42', '01', '01', '04', '01', '01', '01.01.04.01.01', 'Tanaman Rupaxstrixrupa', '2021-08-06 04:24:43'),
('28a30c36b7a9e3edc944e1b845fb3537', '01', '01', '04', '01', '02', '01.01.04.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('057432981fccd20371d45f675a6f7251', '01', '01', '04', '02', '*', '01.01.04.02.*', 'Tumbuh Liar Bercampur Jenis Lain', '2021-08-06 04:24:43'),
('96e79ae8dc07ab772591290a3fc86e95', '01', '01', '04', '02', '01', '01.01.04.02.01', 'Jenis Tanaman rupaxstrixrupa &ampxkommaxampxkommax tidak jelas mana yang menonjol', '2021-08-06 04:24:43'),
('17047e3e52180a6b175f68eb671833ed', '01', '01', '04', '02', '02', '01.01.04.02.02', 'Tanaman Luar Perkarangan', '2021-08-06 04:24:43'),
('290bf6a01c1655f2b86cb28bb43eb022', '01', '01', '04', '02', '03', '01.01.04.02.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('d7abaa1960a55fa438533cd011e1e4f9', '01', '01', '05', '*', '*', '01.01.05.*.*', 'HUTAN', '2021-08-06 04:24:43'),
('0f09d4a1cd3a9d6b051aead93936de63', '01', '01', '05', '01', '*', '01.01.05.01.*', 'Hutan Lebat xkkurixjenis kayu utamaxkkurnanx', '2021-08-06 04:24:43'),
('e18ba106195383ea67445bfc8659eea9', '01', '01', '05', '01', '01', '01.01.05.01.01', 'Meranti', '2021-08-06 04:24:43'),
('99a23dd418704a56a663c5152e82d18d', '01', '01', '05', '01', '02', '01.01.05.01.02', 'Rasamala', '2021-08-06 04:24:43'),
('104039608db205cfb0c489c095d46425', '01', '01', '05', '01', '09', '01.01.05.01.09', 'Albenia', '2021-08-06 04:24:43'),
('e3a16c536ecb4cfff5a556c8c8ea1d92', '01', '01', '05', '01', '10', '01.01.05.01.10', 'Kayu BesixgmringxUlin', '2021-08-06 04:24:43'),
('f952b716d85a81bd2276eec884e6f8ba', '01', '01', '05', '01', '11', '01.01.05.01.11', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('060c8cdab3bf09460c0b97e91b77082b', '01', '01', '05', '02', '*', '01.01.05.02.*', 'Hutan Belukar', '2021-08-06 04:24:43'),
('9269b1b6414905187da87c7ecdeb95c7', '01', '01', '05', '02', '01', '01.01.05.02.01', 'Semakxstrixsemak', '2021-08-06 04:24:43'),
('c2f4203e6a7c1051a01d576dce240361', '01', '01', '05', '02', '02', '01.01.05.02.02', 'Hutan belukar', '2021-08-06 04:24:43'),
('b72b8f18ab790fdada9eb29369b89ec0', '01', '01', '05', '02', '03', '01.01.05.02.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('cf66b606711902a5f3d7179208125f25', '01', '01', '05', '03', '*', '01.01.05.03.*', 'Hutan Tanaman Jenis', '2021-08-06 04:24:43'),
('943d7f2028f1c454d7029cc1d8f16965', '01', '01', '05', '03', '01', '01.01.05.03.01', 'Jati', '2021-08-06 04:24:43'),
('2ebf37f6ab21940707aa4476e28320ee', '01', '01', '05', '03', '02', '01.01.05.03.02', 'Pinus', '2021-08-06 04:24:43'),
('ad0bfe03764b8d9f90715ff1eedb78c4', '01', '01', '05', '03', '03', '01.01.05.03.03', 'Rotan', '2021-08-06 04:24:43'),
('5f8361985a4b356a9f5a87ca8bc81c7e', '01', '01', '05', '03', '04', '01.01.05.03.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1edc6a5d7c96eca89cd1eaf15b09aacf', '01', '01', '05', '04', '*', '01.01.05.04.*', 'Hutan Alam SejenisxgmringxHutan Rawa', '2021-08-06 04:24:43'),
('788a44c67897d155a34a99e670d2d4db', '01', '01', '05', '04', '01', '01.01.05.04.01', 'Bakau', '2021-08-06 04:24:43'),
('496459e95a748d992b4a25a6be65ab89', '01', '01', '05', '04', '02', '01.01.05.04.02', 'Cemara xkkurixyang tidak ditanamanxkkurnanx', '2021-08-06 04:24:43'),
('e43bcde0082810945aaa7407c4bf596b', '01', '01', '05', '04', '03', '01.01.05.04.03', 'Galam', '2021-08-06 04:24:43'),
('1d071e538460c6447ff69635fa20759c', '01', '01', '05', '04', '04', '01.01.05.04.04', 'Nipah', '2021-08-06 04:24:43'),
('ab7424dc090eb4ed2a8c4c5b36eac38b', '01', '01', '05', '04', '05', '01.01.05.04.05', 'Bambu', '2021-08-06 04:24:43'),
('fde7a3565c9b95674bd385c4f07b72de', '01', '01', '05', '04', '06', '01.01.05.04.06', 'Rotan', '2021-08-06 04:24:43'),
('e41d5797208bafe762cca675fb6e6d87', '01', '01', '05', '04', '07', '01.01.05.04.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('9ea7e9755d1ee9112f2df67687aa6fcf', '01', '01', '05', '05', '*', '01.01.05.05.*', 'Hutan Untuk Penggunaan  Khusus', '2021-08-06 04:24:43'),
('82b902cc3eeb51e153c37da8a7f6cd3f', '01', '01', '05', '05', '01', '01.01.05.05.01', 'Hutan Cadangan', '2021-08-06 04:24:43'),
('bc2f999687f6327ff9d6f2ed1659426b', '01', '01', '05', '05', '02', '01.01.05.05.02', 'Hutan Lindung', '2021-08-06 04:24:43'),
('56a84be43b024eee69bc340f3f8f2814', '01', '01', '05', '05', '08', '01.01.05.05.08', 'Hutan Produksi', '2021-08-06 04:24:43'),
('5f60949611566afa87a0d52f840380f9', '01', '01', '05', '05', '09', '01.01.05.05.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('d4911d66149baa15a18815aa9f156254', '01', '01', '06', '*', '*', '01.01.06.*.*', 'KOLAM IKAN', '2021-08-06 04:24:43'),
('498763564db2cde5b58e7ae348f429e3', '01', '01', '06', '01', '*', '01.01.06.01.*', 'Tambak', '2021-08-06 04:24:43'),
('45de85244005584e6526a3d0e70b5620', '01', '01', '06', '01', '01', '01.01.06.01.01', 'Tambak', '2021-08-06 04:24:43'),
('35a4770a74e6a06bc9d1445ba6f2318b', '01', '01', '06', '01', '02', '01.01.06.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('25964367f17a9392470eaaa0a519262f', '01', '01', '06', '02', '*', '01.01.06.02.*', 'Air Tawar', '2021-08-06 04:24:43'),
('dca30a570e19935a287e7637f72f8249', '01', '01', '06', '02', '01', '01.01.06.02.01', 'Kolam Air Tawar', '2021-08-06 04:24:43'),
('fd7963bd65b19250559014f1d814aa05', '01', '01', '06', '02', '02', '01.01.06.02.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('f4e2c736aca34878cfff0ad8a6ae1633', '01', '01', '07', '*', '*', '01.01.07.*.*', 'DANAUxgmringxRAWA', '2021-08-06 04:24:43'),
('3470b8139acc3a188dbc16e8bcaac6b4', '01', '01', '07', '01', '*', '01.01.07.01.*', 'Rawa', '2021-08-06 04:24:43'),
('c3492db8196c880fae0a804f45df0d93', '01', '01', '07', '01', '01', '01.01.07.01.01', 'Rawa', '2021-08-06 04:24:43'),
('e002e36a5d327aa24522d5ee4822d70f', '01', '01', '07', '01', '02', '01.01.07.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('45b696c622572751687de4a1dc30c381', '01', '01', '07', '02', '*', '01.01.07.02.*', 'Danau', '2021-08-06 04:24:43'),
('ac0d46ba48a69fd5d8922a2253be2e99', '01', '01', '07', '02', '01', '01.01.07.02.01', 'SanauxgmringxSitu', '2021-08-06 04:24:43'),
('ab676cb7a5c683e62b6724914c2f5d0f', '01', '01', '07', '02', '02', '01.01.07.02.02', 'Waduk', '2021-08-06 04:24:43'),
('062790f93f5e8c5ae829988b95145c8b', '01', '01', '07', '02', '03', '01.01.07.02.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3748102a40e593955c696fe6f59adc3f', '01', '01', '08', '*', '*', '01.01.08.*.*', 'TANAH TANDUSxgmringxRUSAK', '2021-08-06 04:24:43'),
('6005d604eed56d23711fa559b3877e52', '01', '01', '08', '01', '*', '01.01.08.01.*', 'Tanah Tandus', '2021-08-06 04:24:43'),
('d147befa21ec7404ac08ab6f23785bbf', '01', '01', '08', '01', '01', '01.01.08.01.01', 'Berbatuxstrixbatu', '2021-08-06 04:24:43'),
('2467178f33c12ff35b55bc142703387f', '01', '01', '08', '01', '02', '01.01.08.01.02', 'Longsor', '2021-08-06 04:24:43'),
('e5b84d51ed2857a0e4b81300a48ed53b', '01', '01', '08', '01', '03', '01.01.08.01.03', 'Tanah Lahar', '2021-08-06 04:24:43'),
('58f79b3a821df18745a0b7780afd5fa4', '01', '01', '08', '01', '04', '01.01.08.01.04', 'Tanah BerpasirxgmringxPasir', '2021-08-06 04:24:43'),
('4ee9ceaa86b0c21728f7732025e015d7', '01', '01', '08', '01', '05', '01.01.08.01.05', 'Tanah PengambilanxgmringxKuasi', '2021-08-06 04:24:43'),
('ca95ae4ad2897476bb601588b150f23b', '01', '01', '08', '01', '06', '01.01.08.01.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('c5f9df4c3335337c70b637da22653205', '01', '01', '08', '02', '*', '01.01.08.02.*', 'Tanah Rusak', '2021-08-06 04:24:43'),
('4e38f625289789b1e586ed053b98e978', '01', '01', '08', '02', '01', '01.01.08.02.01', 'Tanah yang tererosixgmringxLongsor', '2021-08-06 04:24:43'),
('36329510711699846f6f9bd106dbe344', '01', '01', '08', '02', '02', '01.01.08.02.02', 'Bekas TambangxgmringxGalian', '2021-08-06 04:24:43'),
('f3206b449b8180622a02a843848b84fb', '01', '01', '08', '02', '03', '01.01.08.02.03', 'Bekas SawahxgmringxRawa', '2021-08-06 04:24:43'),
('c2e23b0b1291a5607222fd0e4ab9d0ed', '01', '01', '08', '02', '04', '01.01.08.02.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('d9d3f985f0b7a9ef7e24b8a1b45b1ffa', '01', '01', '09', '*', '*', '01.01.09.*.*', 'ALANGxstrixALANG DAN PADANG RUMPUT', '2021-08-06 04:24:43'),
('34eb2d26a813dfce6399e5d8464a7e63', '01', '01', '09', '01', '*', '01.01.09.01.*', 'Alangxstrixalang', '2021-08-06 04:24:43'),
('f7902c290d30dac082a2f1f214b667e9', '01', '01', '09', '01', '01', '01.01.09.01.01', 'Alangxstrixalang', '2021-08-06 04:24:43'),
('8b1dcc0683a23d831a800db6da0385e6', '01', '01', '09', '01', '02', '01.01.09.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('671fe902f81edd9a915b4a4a09b07bb8', '01', '01', '09', '02', '*', '01.01.09.02.*', 'Padang Rumput', '2021-08-06 04:24:43'),
('35be854f1630870747b5a1d61f495172', '01', '01', '09', '02', '01', '01.01.09.02.01', 'Semak Belukar', '2021-08-06 04:24:43'),
('83fa70a53baf1f3899025dfbad66d671', '01', '01', '09', '02', '02', '01.01.09.02.02', 'Padang Rumput', '2021-08-06 04:24:43'),
('781331c887a8d9677a85e0ba1141804e', '01', '01', '09', '02', '03', '01.01.09.02.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('8e2c0e791197c2ec30807b75793fdf7b', '01', '01', '10', '*', '*', '01.01.10.*.*', 'TANAH PENGGUNA LAIN', '2021-08-06 04:24:43'),
('fcf6e45da99f58cfa7ad549472be2f21', '01', '01', '10', '01', '*', '01.01.10.01.*', 'Penggalian', '2021-08-06 04:24:43'),
('3c7f474ec6f30f68ec53314b95d9e003', '01', '01', '10', '01', '07', '01.01.10.01.07', 'Penggalian', '2021-08-06 04:24:43'),
('3c2828617f5cc216caaa299a200f46df', '01', '01', '10', '01', '08', '01.01.10.01.08', 'Tempat Air Hangat', '2021-08-06 04:24:43'),
('ee9d973b2d704cfeb3838473b2c3bc50', '01', '01', '10', '01', '09', '01.01.10.01.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('09ba23fb1f274ded05655c707ede2096', '01', '01', '11', '*', '*', '01.01.11.*.*', 'TANAH UNTUK BANGUNAN GEDUNG', '2021-08-06 04:24:43'),
('c765d49a16b53e707b1231a03971e784', '01', '01', '11', '01', '*', '01.01.11.01.*', 'Tanah Bangunan PerumahanxgmringxGedung Tempat Tinggal', '2021-08-06 04:24:43'),
('eb0d1192050f39d8915feaabd41203a5', '01', '01', '11', '01', '01', '01.01.11.01.01', 'Tanah Bangunan Rumah Negara Gol. I', '2021-08-06 04:24:43'),
('e69337b20db5c8ccfb25224ead056992', '01', '01', '11', '01', '02', '01.01.11.01.02', 'Tanah Bangunan Rumah Negara Gol. II', '2021-08-06 04:24:43'),
('5f4e0085ccee44ef21a8d23c2cdda7be', '01', '01', '11', '01', '03', '01.01.11.01.03', 'Tanah Bangunan Rumah Negara Gol. III', '2021-08-06 04:24:43'),
('e49fe19c4c4e05ad06250ace2b0c118e', '01', '01', '11', '01', '04', '01.01.11.01.04', 'Tanah Bangunan Rumah Negara Tanpa Golongan', '2021-08-06 04:24:43'),
('1ce313f47fdc42c228c8289816e1b29f', '01', '01', '11', '01', '05', '01.01.11.01.05', 'Tanah Bangunan MessxgmringxWismaxgmringxAsrama', '2021-08-06 04:24:43'),
('350bbd126f1f34827f137e54abdd330f', '01', '01', '11', '01', '06', '01.01.11.01.06', 'Tanah Bangunan PeristirahatanxgmringxBungalaowxgmringxCottage', '2021-08-06 04:24:43'),
('a7a62cf9b652b73b823aba78b3b495c0', '01', '01', '11', '01', '07', '01.01.11.01.07', 'Tanah Bangunan Rumah Penjaga', '2021-08-06 04:24:43'),
('417aafb8e50839adfb1d9579deab30d0', '01', '01', '11', '01', '08', '01.01.11.01.08', 'Tanah Bangunan Rumah LP', '2021-08-06 04:24:43'),
('247f5ded53120a55644f1021a75a7ecf', '01', '01', '11', '01', '09', '01.01.11.01.09', 'Tanah Bangunan Rumah TahananxgmringxRutan', '2021-08-06 04:24:43'),
('5ad15961c87099cbd55e8dc458659854', '01', '01', '11', '01', '10', '01.01.11.01.10', 'Tanah Bangunan Rumah Fasilitas Tempat Tinggal Lainnya', '2021-08-06 04:24:43'),
('83b0b26ddd5f59fad31e780a3216670d', '01', '01', '11', '01', '11', '01.01.11.01.11', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('17e0cbfc0531de2d6c9874cb8f8d5617', '01', '01', '11', '02', '*', '01.01.11.02.*', 'Tanah Untuk Bangunan Gedung PerdaganganxgmringxPerusahaan', '2021-08-06 04:24:43'),
('45f598f8f189b5b702968c8797feff6f', '01', '01', '11', '02', '01', '01.01.11.02.01', 'Tanah Bangunan Pasar', '2021-08-06 04:24:43'),
('8e75c7ac2a349228ffeebe73c694481c', '01', '01', '11', '02', '02', '01.01.11.02.02', 'Tanah Bangunan PertokoanxgmringxRumah Toko', '2021-08-06 04:24:43'),
('626dd4b74eb450b66be03b93cb600217', '01', '01', '11', '02', '03', '01.01.11.02.03', 'Tanah Bangunan Gudang', '2021-08-06 04:24:43'),
('dcc5e07d55fabdc72afaff36de71753f', '01', '01', '11', '02', '10', '01.01.11.02.10', 'Tanah Bangunan Gedung Kesenian', '2021-08-06 04:24:43'),
('482da26b92e2eb25bef93d4f89217c32', '01', '01', '11', '02', '11', '01.01.11.02.11', 'Tanah Bangunan Gedung Pameran', '2021-08-06 04:24:43'),
('b9d6607572bd67ef176f228085b092c8', '01', '01', '11', '02', '15', '01.01.11.02.15', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('24ada4669e550b42813c0191e3f17a60', '01', '01', '11', '03', '*', '01.01.11.03.*', 'Tanah Untuk Bangunan  Industri', '2021-08-06 04:24:43'),
('669f3b8c4fe4ed5e23c660d1dab1b7c7', '01', '01', '11', '04', '*', '01.01.11.04.*', 'Tanah Untuk Bangunan Tempat KerjaxgmringxJasa', '2021-08-06 04:24:43'),
('95fe189cbe2d8c63fea05bf909fbfad2', '01', '01', '11', '04', '01', '01.01.11.04.01', 'Tanah Bangunan Kantor Pemerintah', '2021-08-06 04:24:43'),
('cd19bae7d7d83cbc5afa6419bd9c5596', '01', '01', '11', '04', '02', '01.01.11.04.02', 'Tanah Bangunan Pendidikan dan  Latihan xkkurixSekolahxkkurnanx', '2021-08-06 04:24:43'),
('4742471d2d39928b321c4a8d4e83870a', '01', '01', '11', '04', '03', '01.01.11.04.03', 'Tanah Bangunan Rumah Sakit', '2021-08-06 04:24:43'),
('8207df2ec32765050924e5a00adfae29', '01', '01', '11', '04', '04', '01.01.11.04.04', 'Tanah Bangunan Apotik', '2021-08-06 04:24:43'),
('d0792ab403081a837e3431386fe7c4d8', '01', '01', '11', '04', '05', '01.01.11.04.05', 'Tanah Bangunan Tempat Ibadah', '2021-08-06 04:24:43'),
('0fbef6ea61d8fd7f1357610bcacd8fe3', '01', '01', '11', '04', '06', '01.01.11.04.06', 'Tanah Bangunan Dermaga', '2021-08-06 04:24:43'),
('d81437840e4d6f3d6cb33fa2fa5d71bd', '01', '01', '11', '04', '07', '01.01.11.04.07', 'Tanah Bangunan Pelabuhan Udara', '2021-08-06 04:24:43'),
('f184429f883c291e0fb7d565c0bb02de', '01', '01', '11', '04', '08', '01.01.11.04.08', 'Tanah Bangunan Olah Raga', '2021-08-06 04:24:43'),
('143060af6161463db034bcddfaaf44c3', '01', '01', '11', '04', '09', '01.01.11.04.09', 'Tanah Bangunan TamanxgmringxWisataxgmringxRekreasi', '2021-08-06 04:24:43'),
('a196c6dad828e3ba0a43a6ed7f5fcce9', '01', '01', '11', '04', '10', '01.01.11.04.10', 'Tanah Bangunan Balai SidangxgmringxPertemuan', '2021-08-06 04:24:43'),
('df7686244a392c09e8ae02f20cb2d704', '01', '01', '11', '04', '11', '01.01.11.04.11', 'Tanah Bangunan Balai Nikah', '2021-08-06 04:24:43'),
('73a1f147afc810dbd5d9b3ab8d43c8d1', '01', '01', '11', '04', '12', '01.01.11.04.12', 'Tanah Bangunan PuskesmasxgmringxPosyandu', '2021-08-06 04:24:43'),
('87524730e7f0113b54e799dd8afdbbe5', '01', '01', '11', '04', '13', '01.01.11.04.13', 'Tanah Bangunan Poliklinik', '2021-08-06 04:24:43'),
('807e7654ce525aa8e477c38ace88a762', '01', '01', '11', '04', '14', '01.01.11.04.14', 'Tanah Bangunan Laboraturium', '2021-08-06 04:24:43'),
('013140e34ac4b3eba6f20948eb255aed', '01', '01', '11', '04', '15', '01.01.11.04.15', 'Tanah Bangunan FumigasixgmringxSterlisasi', '2021-08-06 04:24:43'),
('a83a1a814e2510128f59abfded982fd5', '01', '01', '11', '04', '16', '01.01.11.04.16', 'Tanah Bangunan Karantina', '2021-08-06 04:24:43'),
('f80405dc580797c25537bdbe006b9b6c', '01', '01', '11', '04', '17', '01.01.11.04.17', 'Tanah Bangunan Bangsal Pengolahan  Pondon Kerja', '2021-08-06 04:24:43'),
('cedda390d8db6d713920606a49ed7982', '01', '01', '11', '04', '18', '01.01.11.04.18', 'Tanah Bangunan Kandang Hewan', '2021-08-06 04:24:43'),
('409897a05f7b6f0f7adab2cfbdc6ff1e', '01', '01', '11', '04', '19', '01.01.11.04.19', 'Tanah Bangunanxstrixbangunan Pembibitan', '2021-08-06 04:24:43'),
('79d11c44d081ebc7c4010c1c5b0ea669', '01', '01', '11', '04', '20', '01.01.11.04.20', 'Tanah Bangunan Rumah Pendingin', '2021-08-06 04:24:43'),
('104219cb0a16e8f1cb34b92b3880bf6e', '01', '01', '11', '04', '21', '01.01.11.04.21', 'Tanah Bangunan Rumah Pengering', '2021-08-06 04:24:43'),
('8f5b26b817cf6f6cdf84abb7f795e486', '01', '01', '11', '04', '22', '01.01.11.04.22', 'Tanah Bangunan Stasiun Penelitian', '2021-08-06 04:24:43'),
('081fac97bb7380b11a63f4c5eeb2ed5b', '01', '01', '11', '04', '23', '01.01.11.04.23', 'Tanah Bangunan Gedung Pelelangan Ikan', '2021-08-06 04:24:43'),
('3f46a7beaece7df51d749c89949d4a93', '01', '01', '11', '04', '24', '01.01.11.04.24', 'Tanah Bangunan Pos JagaxgmringxMenara Jaga', '2021-08-06 04:24:43'),
('51c707ec19fa35ebe64c4b8ec648e713', '01', '01', '11', '04', '25', '01.01.11.04.25', 'Tanah Bangunan Tempat Kerja Lainnya', '2021-08-06 04:24:43'),
('76da699e071ad6c071b9ae4c398d9bac', '01', '01', '11', '04', '26', '01.01.11.04.26', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('708f05f4b316e0d859078317d5da2c17', '01', '01', '11', '05', '*', '01.01.11.05.*', 'Tanah Kosong', '2021-08-06 04:24:43'),
('e6a44fe89acf419b1377410287214f64', '01', '01', '11', '05', '01', '01.01.11.05.01', 'Tanah kosong yang tidak diusahakan', '2021-08-06 04:24:43'),
('68d001196fd8206c3cbe232860a6be3a', '01', '01', '11', '05', '02', '01.01.11.05.02', 'Tanah Sawah', '2021-08-06 04:24:43'),
('9b390326a141f6f762d8c3c7fecd5c68', '01', '01', '11', '05', '03', '01.01.11.05.03', 'Tanah Tegalan', '2021-08-06 04:24:43'),
('12c0d5d8812dac72115a7149d7aac77c', '01', '01', '11', '05', '04', '01.01.11.05.04', 'Tanah Kebun', '2021-08-06 04:24:43'),
('975c26758ec825393702f0497666f8e0', '01', '01', '11', '05', '05', '01.01.11.05.05', 'Tanah kosong yang sudah diperuntukan', '2021-08-06 04:24:43'),
('dd973cabc716a73b8e0103480a50573b', '01', '01', '11', '05', '06', '01.01.11.05.06', 'Kebun Pembibitan', '2021-08-06 04:24:43'),
('a6ca73ebf78c3ea2d91456ca02ff893f', '01', '01', '11', '05', '07', '01.01.11.05.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3c4fd57c399013a189811bf9791e9a5c', '01', '01', '11', '06', '*', '01.01.11.06.*', 'Tanah Peternakan', '2021-08-06 04:24:43'),
('af3b402573af5a68f2f9ccebd1f7ad42', '01', '01', '11', '06', '01', '01.01.11.06.01', 'Tanah Peternakan', '2021-08-06 04:24:43'),
('0acfe65432447dd45ef9f90a243fcac0', '01', '01', '11', '06', '02', '01.01.11.06.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b156acc13bb3c5526795e9ea7490db52', '01', '01', '11', '07', '*', '01.01.11.07.*', 'Tanah Bangunan Pengairan', '2021-08-06 04:24:43'),
('69ac0f4c8e3b3120ae69bd63069c7e68', '01', '01', '11', '07', '01', '01.01.11.07.01', 'Tanah Waduk', '2021-08-06 04:24:43'),
('5f1adcaaf9fbb130e486573215f58f99', '01', '01', '11', '07', '02', '01.01.11.07.02', ']Tanah Komplek Bendungan', '2021-08-06 04:24:43'),
('bf024ea67332e9f2c1175b0a2c5072b2', '01', '01', '11', '07', '03', '01.01.11.07.03', 'Tanah JaringanxgmringxSaluran', '2021-08-06 04:24:43'),
('1bfe1b8ec4cd1137801c5b8bf63ed58a', '01', '01', '11', '07', '04', '01.01.11.07.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('33b6c99bc315e22b0d39530e2fca68ed', '01', '01', '11', '08', '*', '01.01.11.08.*', 'Tanah Bangunan Jalan dan Jembatan', '2021-08-06 04:24:43'),
('e530d80e5336af04a859d86ae867a8d7', '01', '01', '11', '08', '01', '01.01.11.08.01', 'Tanah Jalan', '2021-08-06 04:24:43'),
('da0df9eeb596bb82567151270b0a79eb', '01', '01', '11', '08', '02', '01.01.11.08.02', 'Tanah Jembatan', '2021-08-06 04:24:43'),
('45fa8b2af99f0a3d9f52ee0645f1532c', '01', '01', '11', '08', '03', '01.01.11.08.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('123c3a40b0fc79c6442067e21571f9cc', '01', '01', '11', '09', '*', '01.01.11.09.*', 'Tanah LembiranxgmringxBantaranxgmringxLepexstrixlepexgmringxSetren dst', '2021-08-06 04:24:43'),
('20efc9d9f32797e8a0d585fa47ffd6a9', '01', '01', '11', '09', '01', '01.01.11.09.01', 'Tanah Lembiran Pengairan', '2021-08-06 04:24:43'),
('0c454c3867687ca576a30d2ba428736a', '01', '01', '11', '09', '02', '01.01.11.09.02', 'Tanah Lembiran Jalan dan Jembatan', '2021-08-06 04:24:43'),
('5b1865e362d8d1d750a572d96fe5e51f', '01', '01', '11', '09', '03', '01.01.11.09.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('c0417764ae0b1293275e4c7e71f0cdcf', '01', '01', '12', '*', '*', '01.01.12.*.*', 'TANAH PERTAMBANGAN', '2021-08-06 04:24:43'),
('9bde0e6f94490e13922595210c331a29', '01', '01', '12', '01', '*', '01.01.12.01.*', 'Pertambangan', '2021-08-06 04:24:43'),
('15d480963ed5f2719ee3b403b5aad6a2', '01', '01', '13', '*', '*', '01.01.13.*.*', 'TANAH UNTUK BANGUNAN BUKAN GEDUNG', '2021-08-06 04:24:43'),
('5608bc1f9b6009e7e9901e68da8b5e8d', '01', '01', '13', '01', '*', '01.01.13.01.*', 'Tanah Lapangan Olah Raga', '2021-08-06 04:24:43'),
('75838e47185f50181793ad3567c7fdea', '01', '01', '13', '01', '01', '01.01.13.01.01', 'Tanah Lapangan Tenis', '2021-08-06 04:24:43'),
('74e66f88fbcdfdcd88ee6b2ec5ebdd0d', '01', '01', '13', '01', '02', '01.01.13.01.02', 'Tanah Lapangan Basket', '2021-08-06 04:24:43'),
('348b06d2c1ffd00b8ec36dbd7c083d6b', '01', '01', '13', '01', '03', '01.01.13.01.03', 'Tanah Lapangan BadmintonxgmringxBulutangkis', '2021-08-06 04:24:43'),
('d769b525242703f1a099458d079eada4', '01', '01', '13', '01', '04', '01.01.13.01.04', 'Tanah Lapangan Golf', '2021-08-06 04:24:43'),
('54da8c4494051d2e7d66670381a1537f', '01', '01', '13', '01', '05', '01.01.13.01.05', 'Tanah Lapangan Sepak Bola', '2021-08-06 04:24:43'),
('b41f7cf9a67658b80678241d89171604', '01', '01', '13', '01', '06', '01.01.13.01.06', 'Tanah Lapangan Bola Volly', '2021-08-06 04:24:43'),
('addbb40381e1fb42ff147548dec5aa1c', '01', '01', '13', '01', '07', '01.01.13.01.07', 'Tanah Lapangan Sepak Takraw', '2021-08-06 04:24:43'),
('1f3192fbc4224553595ca98170aaa7d0', '01', '01', '13', '01', '08', '01.01.13.01.08', 'Taanh Lapangan Pacuan Kuda', '2021-08-06 04:24:43'),
('b33525a9db261ad6e3e615dec0f41694', '01', '01', '13', '01', '09', '01.01.13.01.09', 'Tanah Lapangan Balap Sepeda', '2021-08-06 04:24:43'),
('84f48dcbffc56393f03dc8b0a6727523', '01', '01', '13', '01', '10', '01.01.13.01.10', 'Tanah Lapangan Atletik', '2021-08-06 04:24:43'),
('847e6a0a933b7dec19a245f658775676', '01', '01', '13', '01', '11', '01.01.13.01.11', 'Tanah Lapangan Softball', '2021-08-06 04:24:43'),
('8aa64d1422c5206f805e77efd5ee7985', '01', '01', '13', '01', '12', '01.01.13.01.12', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('03c396b01654a6a246103d399e92c5d0', '01', '01', '13', '02', '*', '01.01.13.02.*', 'Tanah Lapangan Parkir', '2021-08-06 04:24:43'),
('0c97296dda4609534d24fbdbad58732f', '01', '01', '13', '03', '*', '01.01.13.03.*', 'Tanah Lapangan Penimbun Barang', '2021-08-06 04:24:43'),
('a8af9ca1c1885573baa0a2677fda1f19', '01', '01', '13', '04', '*', '01.01.13.04.*', 'Tanah Lapangan Pemancar Dan Studio Alam', '2021-08-06 04:24:43'),
('39176125c81e9af669ea26b9268b748d', '01', '01', '13', '04', '01', '01.01.13.04.01', 'Tanah Lapangan Pemancar TVxgmringxRadioxgmringxRadar', '2021-08-06 04:24:43'),
('bf101b679b7613f124fe3e1b800254dc', '01', '01', '13', '04', '02', '01.01.13.04.02', 'Tanah Lapangan Studio Alam', '2021-08-06 04:24:43'),
('9b57be9ac5a7e79e08fbc35c367b1116', '01', '01', '13', '04', '03', '01.01.13.04.03', 'Tanah Lapangan Pemancar Lainnya', '2021-08-06 04:24:43'),
('dec3eaa03bee4cab06ce35bf647c2027', '01', '01', '13', '04', '04', '01.01.13.04.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('83a68c7ed08f1eb2d1f5c1924328ee2d', '01', '01', '13', '05', '*', '01.01.13.05.*', 'Tanah Lapangan PengujianxgmringxPengolahan', '2021-08-06 04:24:43'),
('803fb79df8de92d9de7f421c971d5908', '01', '01', '13', '06', '*', '01.01.13.06.*', 'Tanah Lapangan Terbang', '2021-08-06 04:24:43'),
('b22c9c188526744e89edea05ab395b50', '01', '01', '13', '07', '*', '01.01.13.07. *', 'Tanah Untuk Bangunan Jalan', '2021-08-06 04:24:43'),
('d9f958c07c57c0a787f92bcbbdaf2bd7', '01', '01', '13', '08', '*', '01.01.13.08. *', 'Tanah Untuk Bangunan Air', '2021-08-06 04:24:43'),
('13edeb2557b774ceaad1735d7630d156', '01', '01', '13', '09', '*', '01.01.13.09.*', 'Tanah Untuk Bangunan Instalasi', '2021-08-06 04:24:43'),
('514416dae3149c747bd6cd2a7105e32c', '01', '01', '13', '10', '*', '01.01.13.10. *', 'Tanah Untuk Bangunan Jaringan', '2021-08-06 04:24:43'),
('3a32ced85e3eee81e0f5cff982c6bd9b', '01', '01', '13', '11', '*', '01.01.13.11.*', 'Tanah Untuk Bangunan Bersejarah', '2021-08-06 04:24:43'),
('d862996729c3aed419e1403513cd3e3a', '01', '01', '13', '11', '01', '01.01.13.11.01', 'Tanah Untuk Monumen', '2021-08-06 04:24:43'),
('fae8237ca31aa9c8ef2b05970e784c1d', '01', '01', '13', '11', '02', '01.01.13.11.02', 'Tanah Untuk Tugu Peringatan', '2021-08-06 04:24:43'),
('9b4d8037201876a161c99e0eeda53363', '01', '01', '13', '11', '03', '01.01.13.11.03', 'Tanah Untuk Tugu Batas Wilayah', '2021-08-06 04:24:43'),
('b5311e29ab672eb1f4ee60a6b284e7ff', '01', '01', '13', '11', '04', '01.01.13.11.04', 'Tanah Untuk Candi', '2021-08-06 04:24:43'),
('7d1b820a1a1c0d7f75b862deb2b22e14', '01', '01', '13', '11', '05', '01.01.13.11.05', 'Tanah Untuk Bangunan Moseum', '2021-08-06 04:24:43'),
('40789fc579e1ad343414655639a0fe63', '01', '01', '13', '11', '06', '01.01.13.11.06', 'Tanah Untuk Bangunan Bersejarah', '2021-08-06 04:24:43'),
('372fe35727ea766dfbe5c90355a55acb', '01', '01', '13', '11', '07', '01.01.13.11.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('c05b28bf7d3d63dd09a53bf35f80fef9', '01', '01', '13', '12', '*', '01.01.13.12.*', 'Tanah Untuk Bangunan Gedung Olah Raga', '2021-08-06 04:24:43'),
('168098dbdf684e2088d39a229dfecd5b', '01', '01', '13', '12', '01', '01.01.13.12.01', 'Tanah Bangunan Sarana Olaoh Raga Terbatas', '2021-08-06 04:24:43'),
('884d4ce192f5a5f4e65569e2c059c6fd', '01', '01', '13', '12', '02', '01.01.13.12.02', 'Tanah Bangunan Sarana Olah Raga Terbuka', '2021-08-06 04:24:43'),
('e396caed9d7831ad31594d7469873fba', '01', '01', '13', '12', '03', '01.01.13.12.03', 'Tanah Bangunan Sarana Olah Raga Lainnya', '2021-08-06 04:24:43'),
('848a4b64842824718aa9e7b1dd5a2d83', '01', '01', '13', '12', '04', '01.01.13.12.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('edade4167a037378a9dc5e44803faf50', '01', '01', '13', '13', '*', '01.01.13.13. *', 'Tanah Untuk Bangunan Tampat Ibadah', '2021-08-06 04:24:43'),
('954d5119c54b3e3ecd58fd9ec1e76490', '01', '01', '13', '13', '01', '01.01.13.13.01', 'Tanah Untuk Bangunan Mesjid', '2021-08-06 04:24:43'),
('5dd52a4dbe451020e252eff5f49f95f4', '01', '01', '13', '13', '02', '01.01.13.13.02', 'Tanah Untuk Bangunan Gereja', '2021-08-06 04:24:43'),
('38f8c49cd032340aa25e79bfe732a865', '01', '01', '13', '13', '03', '01.01.13.13.03', 'Tanah Untuk Bangunan Pura', '2021-08-06 04:24:43'),
('d14c6f25258aaac58767143c4d28b00b', '01', '01', '13', '13', '04', '01.01.13.13.04', 'Tanah Untuk Bangunan Vihara', '2021-08-06 04:24:43'),
('c6f5396804ac639635968cb24fadf53a', '01', '01', '13', '13', '05', '01.01.13.13.05', 'Tanah Untuk Bangunan KlentengxgmringxKuil', '2021-08-06 04:24:43'),
('a0a75736a0cf8c965b6965298cf034b7', '01', '01', '13', '13', '06', '01.01.13.13.06', 'Tanah Untuk Bangunan Krematorium', '2021-08-06 04:24:43'),
('03e15577e369a01aea4409069d214b4e', '01', '01', '13', '13', '07', '01.01.13.13.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('d96d07c2436278bd94dc328b3d1c8176', '02', '*', '*', '*', '*', '02.*.*.*.*', 'KELOMPOK PERALATAN DAN MESIN', '2021-08-06 04:24:43'),
('f83c7c92c5acd51cff85747d116f102b', '02', '02', '*', '*', '*', '02.02.*.*.*', 'ALAT xstrixALAT BESAR', '2021-08-06 04:24:43'),
('357cef59eef4e4ff8a6a6d91f3988726', '02', '02', '01', '*', '*', '02.02.01.*.*', 'ALAT xstrixALAT BESAR DARAT', '2021-08-06 04:24:43'),
('7d7ed4958b5422006da6775e4a2c77eb', '02', '02', '01', '01', '*', '02.02.01.01.*', 'Tractor', '2021-08-06 04:24:43'),
('418b4f6144e92d4ff9591a90b2d57ff0', '02', '02', '01', '02', '*', '02.02.01.02.*', 'Grader', '2021-08-06 04:24:43'),
('56466aa995f2002667dc135bc8d3e246', '02', '02', '01', '03', '*', '02.02.01.03.*', 'Excavator', '2021-08-06 04:24:43'),
('a82c14fa0ac680608007b91104cefec3', '02', '02', '01', '04', '*', '02.02.01.04.*', 'Pile Driver', '2021-08-06 04:24:43'),
('d5cf70c6813510445e15b7aa829a1e01', '02', '02', '01', '05', '*', '02.02.01.05.*', 'Hauler', '2021-08-06 04:24:43'),
('5b905a2913191c44bf951fb43c31cffe', '02', '02', '01', '06', '*', '02.02.01.06.*', 'Asphal Equipment', '2021-08-06 04:24:43'),
('27277e75469d71221c10c4b7d46fa04e', '02', '02', '01', '07', '*', '02.02.01.07.*', 'Compacting Equipment', '2021-08-06 04:24:43'),
('e4a8598f4a3db2ef2f8f5635a4f05e98', '02', '02', '01', '08', '*', '02.02.01.08.*', 'Aggragate $ Concrete Equipment', '2021-08-06 04:24:43'),
('e0ec55aecd8077305cd724f73c9fca9b', '02', '02', '01', '09', '*', '02.02.01.09.*', 'Loader', '2021-08-06 04:24:43'),
('ac31e0593001bdba45829553f1b713bb', '02', '02', '01', '10', '*', '02.02.01.10.*', 'Alat Pengangkat', '2021-08-06 04:24:43'),
('56cacc6a66d77eb36111d4cdd7be037c', '02', '02', '01', '11', '*', '02.02.01.11.*', 'Mesin Proses', '2021-08-06 04:24:43'),
('83a5a834f7611cd6a993f784b4f9f27f', '02', '02', '02', '*', '*', '02.02.02.*.*', 'Alatxstrixalat Besar Apung', '2021-08-06 04:24:43'),
('29788ee34dd01bb4efef4f42de4d8ddc', '02', '02', '02', '01', '*', '02.02.02.01.*', 'Dredger', '2021-08-06 04:24:43'),
('f43c4e04d2f11d20f0ccd849b4194029', '02', '02', '02', '02', '*', '02.02.02.02.*', 'Floating Exavator', '2021-08-06 04:24:43'),
('f1aa036177718c3e51def6ca73e70118', '02', '02', '02', '03', '*', '02.02.02.03.*', 'Amphibi Dredger', '2021-08-06 04:24:43'),
('c98a49b8c7d2eb016f1e38eeb00cd078', '02', '02', '02', '04', '*', '02.02.02.04.*', 'Kapal Tarik', '2021-08-06 04:24:43'),
('69cb0421e71223458a030ae2c60819af', '02', '02', '02', '05', '*', '02.02.02.05.*', 'Mesin Proses Apung', '2021-08-06 04:24:43'),
('2d4aee071a8c0b4dde72937d90101499', '02', '02', '03', '*', '*', '02.02.03.*.*', 'Alatxstrixalat Bantu', '2021-08-06 04:24:43'),
('ced4351d2cc45526553192120afa387d', '02', '02', '03', '01', '*', '02.02.03.01.*', 'Alat Penarik', '2021-08-06 04:24:43'),
('3cf7f17b374484e713717fc3d5228a23', '02', '02', '03', '02', '*', '02.02.03.02.*', 'Feeder', '2021-08-06 04:24:43'),
('425657e8f0ea5998a204e7be49f7c6c4', '02', '02', '03', '02', '01', '02.02.03.02.01', 'Elevator', '2021-08-06 04:24:43'),
('d1182dcd7411caccc15cd21c60ed2157', '02', '02', '03', '03', '*', '02.02.03.03.*', 'Compressor', '2021-08-06 04:24:43'),
('740dd5f9fc02ec77c2573b450fb3c718', '02', '02', '03', '03', '01', '02.02.03.03.01', 'Transportable Compresor', '2021-08-06 04:24:43'),
('ae3cbf9b590aefae98151689800fb3db', '02', '02', '03', '03', '02', '02.02.03.03.02', 'Portable Compresor', '2021-08-06 04:24:43'),
('212d3aaa7654ff03ad98168f284f1fe2', '02', '02', '03', '03', '03', '02.02.03.03.03', 'Stationary Compresor', '2021-08-06 04:24:43'),
('9446ce7106db0f99346ec778af8c2f89', '02', '02', '03', '03', '04', '02.02.03.03.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('be5622a6f5753c16c881f304c996d05b', '02', '02', '03', '04', '*', '02.02.03.04.*', 'Electric Generating Set', '2021-08-06 04:24:43'),
('0b05d3e1bc7eea877dc5d12b69ce42e0', '02', '02', '03', '04', '01', '02.02.03.04.01', 'Transportable Generating Set', '2021-08-06 04:24:43'),
('1c3f2f6f8ecbe1eaf7c46ecbaff601e5', '02', '02', '03', '04', '02', '02.02.03.04.02', 'Portable Generating Set', '2021-08-06 04:24:43'),
('7e3685cd067f64af6b43f1b1c33d5017', '02', '02', '03', '04', '03', '02.02.03.04.03', 'Stationary Generating Set', '2021-08-06 04:24:43'),
('81116d16443024f2201faaa67b5f2143', '02', '02', '03', '04', '04', '02.02.03.04.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1c6537828bbb5012da7d0374ab7e5c46', '02', '02', '03', '05', '*', '02.02.03.05.*', 'Pompa', '2021-08-06 04:24:43'),
('4f84b74b98542b4f89f9372c76bb9f1b', '02', '02', '03', '05', '01', '02.02.03.05.01', 'Transportable Water Pomp', '2021-08-06 04:24:43'),
('fa263be809da292957879f9e83b7ec6b', '02', '02', '03', '05', '02', '02.02.03.05.02', 'Portable Water Pump', '2021-08-06 04:24:43'),
('678633b257700a7a60a18b21c823b1a2', '02', '02', '03', '05', '03', '02.02.03.05.03', 'Stationary Water Pump', '2021-08-06 04:24:43'),
('45d0828c4e0f3e2defc4b106780b1d23', '02', '02', '03', '05', '04', '02.02.03.05.04', 'Poppa Lumpur', '2021-08-06 04:24:43'),
('299029212e8c0852baca86ad2bc3de10', '02', '02', '03', '05', '05', '02.02.03.05.05', 'Sumersible Pump', '2021-08-06 04:24:43'),
('87b8e30c7ce9d64eb44060ec8c3a3bb3', '02', '02', '03', '05', '06', '02.02.03.05.06', 'Pompa Tangan', '2021-08-06 04:24:43'),
('2392d1b55252e8ba2fc06da09bd81b07', '02', '02', '03', '05', '07', '02.02.03.05.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1cf1724986d4aa6877ebaf21993cac2b', '02', '02', '08', '06', '*', '02.02.08.06.*', 'Mesin Bor', '2021-08-06 04:24:43'),
('9867a313bef47446a71a159bf6bf2f57', '02', '02', '03', '07', '*', '02.02.03.07.*', 'Unit Pemeliharaan Lapangan', '2021-08-06 04:24:43'),
('57bb0399011ec95bd446c8f59aaf42d0', '02', '02', '03', '08', '*', '02.02.03.08.*', 'Alat Pengolahan Air Kotor', '2021-08-06 04:24:43'),
('3a8d7152b178834901d597607c4f1608', '02', '02', '03', '08', '01', '02.02.03.08.01', 'Unit Pengolahan Air Kotor', '2021-08-06 04:24:43'),
('722ce3c175f063e497a8655336348d23', '02', '02', '03', '08', '02', '02.02.03.08.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3923083d94df4b9e663af3ae58c9b025', '02', '02', '03', '09', '*', '02.02.03.09.*', 'Pembangkit Uap Air Panasxgmringx Sistem Generator', '2021-08-06 04:24:43'),
('4b5d01acd155f6bcd5c7a5a4819440f2', '02', '02', '03', '09', '01', '02.02.03.09.01', 'Unit Pembangkit Uap Air Panas', '2021-08-06 04:24:43'),
('f9197605ae84f0ea4001592f7c9eb377', '02', '02', '03', '09', '02', '02.02.03.09.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3ec9412f14997c1d110656663038900b', '02', '03', '*', '*', '*', '02.03.*.*.*', 'ALATxstrix ALAT ANGKUTAN', '2021-08-06 04:24:43'),
('64e24e76ea4bf6a7e63f1729d3fa7716', '02', '03', '01', '*', '*', '02.03.01.*.*', 'Alat Angkutan Darat Bermotor', '2021-08-06 04:24:43'),
('364b5872de367ed80d8b5c5b0f4d2204', '02', '03', '01', '01', '*', '02.03.01.01.*', 'Kendaraan Dinas Bermotor Perorangan', '2021-08-06 04:24:43'),
('1c4aa537c2e68cd3f709c30106219c3f', '02', '03', '01', '01', '01', '02.03.01.01.01', 'Sedan', '2021-08-06 04:24:43'),
('f3aee1438397c1c146da402047e6071c', '02', '03', '01', '01', '02', '02.03.01.01.02', 'Jeep', '2021-08-06 04:24:43'),
('10634477bdb86e1a3142fadeaeaeb30e', '02', '03', '01', '01', '03', '02.03.01.01.03', 'Station Wagon', '2021-08-06 04:24:43'),
('a734fa46caf2c648ddfdc5fc8bb7da26', '02', '03', '01', '01', '04', '02.03.01.01.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('d54530b87990a1fd49f60a28bd0c24a9', '02', '03', '01', '02', '*', '02.03.01.02.*', 'Kendaraan Bermotor Penumpang', '2021-08-06 04:24:43'),
('c5f1a5a1d640fd7dcb781af499e8c7a6', '02', '03', '01', '02', '01', '02.03.01.02.01', 'Bus xkkurixPenumpang 30 Orang keatasxkkurnanx', '2021-08-06 04:24:43'),
('f2522b8ea483a7ae56218672d186f19a', '02', '03', '01', '02', '02', '02.03.01.02.02', 'Bicro Bus xkkurixPenumpang 15 xstrix 30 orangxkkurnanx', '2021-08-06 04:24:43'),
('cc92821c56ee18c8a5d0179fd8a3e6cd', '02', '03', '01', '02', '04', '02.03.01.02.04', 'Mini Bus xkkurixPenumpang 14 Orang kebawahxkkurnanx', '2021-08-06 04:24:43'),
('86344e1ef1ee43f75c0ce3a8cc6aec01', '02', '03', '01', '03', '*', '02.03.01.03.*', 'Kendaraan Bermotor Angkutan Barang', '2021-08-06 04:24:43'),
('679d9029e83d34fce741d91355f82c9e', '02', '03', '01', '03', '01', '02.03.01.03.01', 'Truck + Attachhment', '2021-08-06 04:24:43'),
('70a01dafa25d91a1aa3f5f63007a2223', '02', '03', '01', '03', '02', '02.03.01.03.02', 'Pick Up', '2021-08-06 04:24:43'),
('228dc3256e9c82514b5064c0a005474f', '02', '03', '01', '03', '03', '02.03.01.03.03', 'Trailer', '2021-08-06 04:24:43'),
('99a8190b524d39b6c2e02963e23a25e0', '02', '03', '01', '03', '04', '02.03.01.03.04', 'Semi Trailer', '2021-08-06 04:24:43'),
('dd0a307c77ae69c19a701193c354acc0', '02', '03', '01', '03', '05', '02.03.01.03.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('de53e25ba9aba18f115909dcb3d3695b', '02', '03', '01', '04', '*', '02.03.01.04.*', 'Kendaraan Bermotor Khusus', '2021-08-06 04:24:43'),
('61e35ba71bc8d5bd708079ed004cc252', '02', '03', '01', '04', '01', '02.03.01.04.01', 'Mobil Ambulance', '2021-08-06 04:24:43'),
('e500d77b29095959178d8e6d46107d68', '02', '03', '01', '04', '02', '02.03.01.04.02', 'Mobil Jenazah', '2021-08-06 04:24:43'),
('45161c9b45784b625082af13ac13cd7c', '02', '03', '01', '04', '03', '02.03.01.04.03', 'Mobil Unit Penerangan', '2021-08-06 04:24:43'),
('0904d0a76045dbc223158bb347afb114', '02', '03', '01', '04', '04', '02.03.01.04.04', 'Mobil Pemadam Kebakaran', '2021-08-06 04:24:43'),
('b675eafea87fb21661dacafef64090c8', '02', '03', '01', '04', '22', '02.03.01.04.22', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('cd7c996c7b9c2c9e7cb7f65d7f4147d0', '02', '03', '01', '05', '*', '02.03.01.05.*', 'Kendaraan Bermotor Beroda Dua', '2021-08-06 04:24:43'),
('526ba7ddb2a8fc800a0eaeb0b120e129', '02', '03', '01', '05', '01', '02.03.01.05.01', 'Sepeda Motor', '2021-08-06 04:24:43'),
('ed19bf1a06b606561e467fd43641411c', '02', '03', '01', '05', '02', '02.03.01.05.02', 'Scooter', '2021-08-06 04:24:43'),
('2577c87b6de5f69ac3b14a14a7198d35', '02', '03', '01', '05', '03', '02.03.01.05.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('fac8531cfb192c7619475b2ada8f95a5', '02', '03', '01', '06', '*', '02.03.01.06.*', 'Kendaraan Bermotor Beroda Toga', '2021-08-06 04:24:43'),
('6e6dd30184d3fa2ef492b3ebab76ce75', '02', '03', '01', '06', '01', '02.03.01.06.01', 'Bemo', '2021-08-06 04:24:43'),
('0a85cda6a42fa3b409d6bc591c6913b4', '02', '03', '01', '06', '02', '02.03.01.06.02', 'HelicakxgmringxBajaj', '2021-08-06 04:24:43'),
('432dfc5059a21fa41fe696073bf064c6', '02', '03', '01', '06', '03', '02.03.01.06.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('313369d82b59cc63c30cd1032210623f', '02', '03', '02', '01', '*', '02.03.02.01.*', 'Kendaraan Bermotor Angkutan Barang', '2021-08-06 04:24:43'),
('bebd77af636a182a1d0cb2eb47a95eba', '02', '03', '02', '01', '01', '02.03.02.01.01', 'Gerobak Tarik', '2021-08-06 04:24:43'),
('cd5495a3e5f3ace84caa4d561137c3fe', '02', '03', '02', '01', '02', '02.03.02.01.02', 'Gerobak Dorong', '2021-08-06 04:24:43'),
('d5be25ea77f4211c667b7e3c5cbb6d51', '02', '03', '02', '01', '03', '02.03.02.01.03', 'Caravan', '2021-08-06 04:24:43'),
('0709c388bc8c68e9dc7e45644b0861df', '02', '03', '02', '01', '04', '02.03.02.01.04', 'Gerobak Lori', '2021-08-06 04:24:43'),
('d25b55248b83de19fd05510c21aca36f', '02', '03', '02', '01', '05', '02.03.02.01.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('f2cdee4edb84ab86f3ad6cebcf3fa974', '02', '03', '02', '02', '*', '02.03.02.02.*', 'Kendaraan Tak Bermomor Berpenumpang', '2021-08-06 04:24:43'),
('dfc05dd1bcb2165b540b0de54956a54a', '02', '03', '02', '02', '01', '02.03.02.02.01', 'Sepeda', '2021-08-06 04:24:43'),
('d29bb7233752b58fdb97509847e222fd', '02', '03', '02', '02', '02', '02.03.02.02.02', 'Dokar', '2021-08-06 04:24:43'),
('383c76b340f44910139702177cf3ad1c', '02', '03', '02', '02', '03', '02.03.02.02.03', 'Tandu Dorong', '2021-08-06 04:24:43'),
('1b7d118369a0d6ec650b60a1ebed69d5', '02', '03', '02', '02', '04', '02.03.02.02.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('9b3ffc57a8eaf72ee31fec454c0ca778', '02', '03', '03', '*', '*', '02.03.03.*.*', 'Alat Angkut Apung Bermotor', '2021-08-06 04:24:43'),
('71f6491c53397460d79d817c2245a98f', '02', '03', '03', '01', '*', '02.03.03.01.*', 'Alat Angkut Apung Bermotor Barang', '2021-08-06 04:24:43'),
('3c37236d988466277e966cb0428ee6aa', '02', '03', '03', '01', '01', '02.03.03.01.01', 'Kapal Minyak', '2021-08-06 04:24:43'),
('4ad29d1d5603d078c8c344b0f9d6f7fe', '02', '03', '03', '01', '07', '02.03.03.01.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1367824dd35c0e5e07978474422153a5', '02', '03', '03', '02', '*', '02.03.03.02.*', 'Alat Angkutan Apung Bermotor Penumpang', '2021-08-06 04:24:43'),
('9e2a39eeafa2ab6ae3449803f15eccf9', '02', '03', '03', '02', '01', '02.03.03.02.01', 'Speed Boat', '2021-08-06 04:24:43'),
('cb4be5fca0cc2ce64fd554ffc3c67731', '02', '03', '03', '02', '02', '02.03.03.02.02', 'Motor  Boat', '2021-08-06 04:24:43'),
('27412173b9fc8dfc25d9ea61285d6e7d', '02', '03', '03', '02', '03', '02.03.03.02.03', 'Klotok', '2021-08-06 04:24:43'),
('fca4177339719d4420e9212d32f2a16e', '02', '03', '03', '02', '04', '02.03.03.02.04', 'Ferry', '2021-08-06 04:24:43'),
('5b03043e6d6cf8041b0819efa0c549ed', '02', '03', '03', '02', '05', '02.03.03.02.05', 'Hidrofoil', '2021-08-06 04:24:43'),
('59aa9fd4cacf8e941c1a931ec6d32724', '02', '03', '03', '02', '06', '02.03.03.02.06', 'Jetfoil', '2021-08-06 04:24:43'),
('e7171d6c506b9aa0275ef91815905334', '02', '03', '03', '02', '07', '02.03.03.02.07', 'Long Boat', '2021-08-06 04:24:43'),
('34c2cabce63325c41f6e1e8324381df4', '02', '03', '03', '02', '08', '02.03.03.02.08', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('046c3a8ebdbe279f5318979ca8e42466', '02', '03', '03', '03', '*', '02.03.03.03.*', 'Alat Angkut Apung Bermotor Khusus', '2021-08-06 04:24:43'),
('878444ae62e9cf201abc8920620e65ad', '02', '03', '03', '03', '01', '02.03.03.03.01', 'Surver Boat', '2021-08-06 04:24:43'),
('4dc58fff199cf7d95da6e441bcbd4f32', '02', '03', '03', '03', '11', '02.03.03.03.11', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('43699635ede8e76d11bac3bf367eb8b4', '02', '03', '04', '*', '*', '02.03.04.*.*', 'Alat Angkut Apung Tak Bermotor', '2021-08-06 04:24:43'),
('00d6b75b3a6c6b847d20a0b6efa4c318', '02', '03', '04', '01', '*', '02.03.04.01.*', 'Alat Angkut Tak Bermotor Untuk Barang', '2021-08-06 04:24:43');
INSERT INTO `m_kib_kode` (`kd`, `golongan`, `bidang`, `kelompok`, `kelompok_sub`, `kelompok_sub_sub`, `kode`, `nama`, `postdate`) VALUES
('69c61780f6c7aeb9b8a72eaa9a30738d', '02', '03', '04', '01', '01', '02.03.04.01.01', 'Tongkang', '2021-08-06 04:24:43'),
('c0758fca84957c143322c1d284870284', '02', '03', '04', '01', '02', '02.03.04.01.02', 'Perahu Barang', '2021-08-06 04:24:43'),
('08a57feffc874942b949e1eeddb3eaff', '02', '03', '04', '01', '03', '02.03.04.01.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('eaf7717c31175da8403d13b2ac07758b', '02', '03', '04', '02', '*', '02.03.04.02.*', 'Alat Angkut Apung Tak Bermotor Penumpang', '2021-08-06 04:24:43'),
('94967899fcb46dccf20db8be4d751588', '02', '03', '04', '02', '01', '02.03.04.02.01', 'Perahu Penumbang', '2021-08-06 04:24:43'),
('2a82799e75840f0c8c9defc5aeb17978', '02', '03', '04', '02', '02', '02.03.04.02.02', 'Perahu Penyeberangan', '2021-08-06 04:24:43'),
('c120c046f56a9001c9c9adfa72c9d329', '02', '03', '04', '02', '03', '02.03.04.02.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('c893ddda2b84508f6abf39c514db3ebc', '02', '03', '04', '03', '*', '02.03.04.03.*', 'Alat Angkutan Apung Tak Bermotor Khusus', '2021-08-06 04:24:43'),
('46e77c91732e8bd4e0af73b23c845e71', '02', '03', '04', '03', '01', '02.03.04.03.01', 'Ponton', '2021-08-06 04:24:43'),
('c604e0a54703b58c6aa4abc8ba4002d2', '02', '03', '04', '03', '02', '02.03.04.03.02', 'Perahu Karet', '2021-08-06 04:24:43'),
('d32b6b2700e5c58984994d2460f1fb39', '02', '03', '04', '03', '03', '02.03.04.03.03', 'Ponton Rumah', '2021-08-06 04:24:43'),
('26a89613eb67d1670010b416a2c46781', '02', '03', '04', '03', '04', '02.03.04.03.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('2dbdcb827f0656b2ddb1431b595194cb', '02', '03', '05', '*', '*', '02.03.05.*.*', 'Alat Angkut Bermotor Udara', '2021-08-06 04:24:43'),
('9d4443aa6e0931dc8ea6200c85bb5269', '02', '03', '05', '01', '*', '02.03.05.01.*', 'Kapal Terbang', '2021-08-06 04:24:43'),
('b4ae878b86fd53896fa2da9d48d6741a', '02', '03', '05', '01', '06', '02.03.05.01.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('5424b1e628c8d8575936d0c76de78eac', '02', '04', '*', '*', '*', '02.04.*.*.*', 'ALAT BENGKEL DAN ALAT UKUR', '2021-08-06 04:24:43'),
('b257f8c082adb5c1ea81a15423867947', '02', '04', '01', '*', '*', '02.04.01.*.*', 'Alat Bengkel Bermesin', '2021-08-06 04:24:43'),
('6429739ebb47d8fb1529e246ed613cac', '02', '04', '01', '01', '*', '02.04.01.01.*', 'Perkakas Kontruksi Logam Terpasang pada Pondasi', '2021-08-06 04:24:43'),
('82f1acd6002dd87ebd34c0f454a2c6ee', '02', '04', '01', '02', '*', '02.04.01.02.*', 'Perkakas Konstruksi Logam yang Berpindah', '2021-08-06 04:24:43'),
('bffca74049749773b57384148ef5b898', '02', '04', '01', '03', '*', '02.04.01.03.*', 'Perkakas Bengkel Listrik', '2021-08-06 04:24:43'),
('32456f668be3cd08e0a76734cab220a3', '02', '04', '01', '04', '*', '02.04.01.04.*', 'Perkakas Bengkel Service', '2021-08-06 04:24:43'),
('7fd5419522dbf90c5d39cc76b2ca785b', '02', '04', '01', '05', '*', '02.04.01.05.*', 'Perkakas Pengangkat Bermesin', '2021-08-06 04:24:43'),
('f85f0adbc92642e363ce4ba607d13d43', '02', '04', '01', '05', '01', '02.04.01.05.01', 'Overhead Grane', '2021-08-06 04:24:43'),
('e7b220abd7530c2866c7e1ddaf3ec299', '02', '04', '01', '05', '02', '02.04.01.05.02', 'Auto Hoist', '2021-08-06 04:24:43'),
('47e81132f99ebeadae1f28baed72bced', '02', '04', '01', '05', '03', '02.04.01.05.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('04e073630e0e46fd81b0ae2f9879a6c2', '02', '04', '01', '06', '*', '02.04.01.06.*', 'Perkakas Bengkel Kayu', '2021-08-06 04:24:43'),
('440c0282ab1df79fb37d8013ef43f9ee', '02', '04', '01', '06', '01', '02.04.01.06.01', 'Mesin Gergaji', '2021-08-06 04:24:43'),
('3cef5447064c42dc8f5b8a03b7fec84d', '02', '04', '01', '06', '02', '02.04.01.06.02', 'Mesin Ketam', '2021-08-06 04:24:43'),
('f7d613bd8841c7d07584310f22521ad4', '02', '04', '01', '06', '03', '02.04.01.06.03', 'Mesin Bor', '2021-08-06 04:24:43'),
('68bc839405dee36147b576034b1ef199', '02', '04', '01', '06', '04', '02.04.01.06.04', 'Mesin Penghalus', '2021-08-06 04:24:43'),
('d3a08d487f2c7ee6e439c41d670bee6a', '02', '04', '01', '06', '05', '02.04.01.06.05', 'Mesin Penyambung Papan', '2021-08-06 04:24:43'),
('6ce31f50263993ae7593fdb78545c4bc', '02', '04', '01', '06', '06', '02.04.01.06.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('72c07e908f10d30f7d4a5ea36140740b', '02', '04', '01', '07', '*', '02.04.01.07.*', 'Perkakas Bengkel Khusus', '2021-08-06 04:24:43'),
('8d31400873b104be0a022edb7236d2d0', '02', '04', '01', '07', '01', '02.04.01.07.01', 'Mesin Jahit Terpal', '2021-08-06 04:24:43'),
('ef538a0c76afb0b30fb6482732986729', '02', '04', '01', '07', '06', '02.04.01.07.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('cfb63e9f9f7491a4f6706430d0278374', '02', '04', '01', '08', '*', '02.04.01.08.*', 'Peralatan Las', '2021-08-06 04:24:43'),
('c71f46016362bd2cd7298ffbffc0cf56', '02', '04', '01', '08', '01', '02.04.01.08.01', 'Peralatan Las Listrik', '2021-08-06 04:24:43'),
('6996e368cc7cc5067c2491295da00ffd', '02', '04', '01', '08', '02', '02.04.01.08.02', 'Peralatan Las Karbit', '2021-08-06 04:24:43'),
('a7001a49484c27a5a355b54be0883c25', '02', '04', '01', '08', '03', '02.04.01.08.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('0d2be262106d6ea43a37e4db8f289209', '02', '04', '01', '09', '*', '02.04.01.09.*', 'Perkakas Pabrik Es', '2021-08-06 04:24:43'),
('3a1e73294656f458653084bd904417ad', '02', '04', '02', '*', '*', '02.04.02.*.*', 'Alat Bengkel Tak Bermesin', '2021-08-06 04:24:43'),
('c38c51650e3c3e2ddbf9b1d5d15ce073', '02', '04', '02', '02', '*', '02.04.02.02.*', 'Perkakas Bengkel Listrik', '2021-08-06 04:24:43'),
('07312fcb51fea480243d658e3edf5f8e', '02', '04', '02', '03', '*', '02.04.02.03.*', 'Perkakas Bengkel Service', '2021-08-06 04:24:43'),
('d6cb4febd4fcb41e5b19feeca522138e', '02', '04', '02', '04', '*', '02.04.02.04.*', 'Perkakas Pengangkat', '2021-08-06 04:24:43'),
('5cd36c623c23475ada921e28adbe5627', '02', '04', '02', '04', '01', '02.04.02.04.01', 'Dongkrak Mekanik', '2021-08-06 04:24:43'),
('30e3bed5e1251cf23c2c4e2c2a16025f', '02', '04', '02', '04', '02', '02.04.02.04.02', 'Dongkrak Hidrolik', '2021-08-06 04:24:43'),
('bbe9dba16a8e001d0fbef130f1aee723', '02', '04', '02', '04', '03', '02.04.02.04.03', 'Takel', '2021-08-06 04:24:43'),
('a7739fba09071e5c57278e87444f1da1', '02', '04', '02', '04', '04', '02.04.02.04.04', 'Gantry', '2021-08-06 04:24:43'),
('08b94865e65697505abbfa8b2982d76f', '02', '04', '02', '04', '05', '02.04.02.04.05', 'Tripot', '2021-08-06 04:24:43'),
('fcda5e04075b8573312b08ccd3833dbf', '02', '04', '02', '04', '06', '02.04.02.04.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('79141928a8d8ca49f1358e14bc739ba8', '02', '04', '02', '05', '*', '02.04.02.05.*', 'Perkakas Standar xkkurixStandart Toolxkkurnanx', '2021-08-06 04:24:43'),
('1c2e17701c35a46286c03b4e5eef7c53', '02', '04', '02', '05', '01', '02.04.02.05.01', 'Tool Kit Set', '2021-08-06 04:24:43'),
('896a5e1ce081e1f9d3d23b03ae83203e', '02', '04', '02', '05', '02', '02.04.02.05.02', 'Tool Kit Boks', '2021-08-06 04:24:43'),
('d6dd4fc7daab4a132cf88cffc10f2f57', '02', '04', '02', '05', '03', '02.04.02.05.03', 'Tool Cabinet Set', '2021-08-06 04:24:43'),
('3a76f1e8e89422f5587b27cf3cacec77', '02', '04', '02', '05', '04', '02.04.02.05.04', 'Kunci Pipa', '2021-08-06 04:24:43'),
('2848ce8519b61b211884c296d3f9d535', '02', '04', '02', '05', '05', '02.04.02.05.05', 'Fuller Set', '2021-08-06 04:24:43'),
('40caf4e09c8df822cc773ed7b9666572', '02', '04', '02', '05', '06', '02.04.02.05.06', 'TapDies', '2021-08-06 04:24:43'),
('57c8fb1f873b37bafd70629db0ba91fa', '02', '04', '02', '05', '07', '02.04.02.05.07', 'Groeper', '2021-08-06 04:24:43'),
('b1f29b77a04bcc192156efa39d963bfd', '02', '04', '02', '05', '08', '02.04.02.05.08', 'Engine Stand', '2021-08-06 04:24:43'),
('6133c05ace0f9dfefb7b289e6820a841', '02', '04', '02', '05', '09', '02.04.02.05.09', 'Kunci Momet', '2021-08-06 04:24:43'),
('cf4b8d86ac1f892c8f8bab8802cc8de4', '02', '04', '02', '05', '10', '02.04.02.05.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b3d481e8152414f3af2d883c9566a04e', '02', '04', '02', '07', '*', '02.04.02.07.*', 'Perkakas Bengkel Kerja', '2021-08-06 04:24:43'),
('1f5d27b94a7ab66dda8934a530b69ef8', '02', '04', '02', '07', '01', '02.04.02.07.01', 'Gergaji', '2021-08-06 04:24:43'),
('1ef30761ba17b06c0b547a66ae7c7674', '02', '04', '02', '07', '02', '02.04.02.07.02', 'Ketam', '2021-08-06 04:24:43'),
('caade05ff134f89b94705f8e101a5f07', '02', '04', '02', '07', '03', '02.04.02.07.03', 'Bor', '2021-08-06 04:24:43'),
('04f597e31f79369dd6bcdcd493d5cdd4', '02', '04', '02', '07', '04', '02.04.02.07.04', 'Pahat', '2021-08-06 04:24:43'),
('8c336c53f037e26081dcfc0aa771234b', '02', '04', '02', '07', '05', '02.04.02.07.05', 'Kaka Tua', '2021-08-06 04:24:43'),
('7c0a6264eedf6f951166ef6fc4dc3df9', '02', '04', '02', '07', '06', '02.04.02.07.06', 'Water Pas', '2021-08-06 04:24:43'),
('f42b1400bf4dca17b39a0fded6954910', '02', '04', '02', '07', '07', '02.04.02.07.07', 'Siku', '2021-08-06 04:24:43'),
('2e6d2dcea5cf8d856b9b5af1da98ee9a', '02', '04', '02', '07', '08', '02.04.02.07.08', 'Palu', '2021-08-06 04:24:43'),
('e380a61dba6f05daf63539b4c36135c2', '02', '04', '02', '07', '09', '02.04.02.07.09', 'Tang', '2021-08-06 04:24:43'),
('aa720cd9cde92c3b4ada970f15145171', '02', '04', '02', '07', '10', '02.04.02.07.10', 'Ayakan Pasir', '2021-08-06 04:24:43'),
('810fd32143a2279f27a3f8a365c383ae', '02', '04', '02', '07', '11', '02.04.02.07.11', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('806b8582ccbeb39ed37a99b8c0657f1e', '02', '04', '02', '08', '*', '02.04.02.08.*', 'Peralatan TukangxstrixTukang Besi', '2021-08-06 04:24:43'),
('a3b5398b877625ee324e25cbd949493b', '02', '04', '02', '09', '*', '02.04.02.09.*', 'Peralatan Tukang Kayu', '2021-08-06 04:24:43'),
('5e1dc02041c84461c0e13b136d8efca9', '02', '04', '02', '09', '01', '02.04.02.09.01', 'Tatah Biasa Satu Set', '2021-08-06 04:24:43'),
('bd20ec32aae9ac8185b4ba229c08fcf3', '02', '04', '02', '09', '02', '02.04.02.09.02', 'Tatah Lengkung Satu Set', '2021-08-06 04:24:43'),
('654b9968c71c956dd3fee3bcf66aaf7a', '02', '04', '02', '09', '03', '02.04.02.09.03', 'Kaota', '2021-08-06 04:24:43'),
('ff0c1a04e08a443a3b1c4a41c32d1aa2', '02', '04', '02', '09', '04', '02.04.02.09.04', 'Petel', '2021-08-06 04:24:43'),
('00ba658eb9d99c90d77e519ab639e043', '02', '04', '02', '09', '05', '02.04.02.09.05', 'Patar', '2021-08-06 04:24:43'),
('d319545146b87d06212b1e95cd5aae1c', '02', '04', '02', '09', '06', '02.04.02.09.06', 'Boor Engkol', '2021-08-06 04:24:43'),
('578ebba3115cd61ac8f2cfb8ad33ea38', '02', '04', '02', '09', '07', '02.04.02.09.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('86dc3fd6d11cccca79b08bd152b39931', '02', '04', '02', '10', '*', '02.04.02.10.*', 'Peralatan Tukang Kulit', '2021-08-06 04:24:43'),
('a732a29cc3fc1ae2b6f705cd84c6db43', '02', '04', '02', '10', '01', '02.04.02.10.01', 'Pisau Kulit', '2021-08-06 04:24:43'),
('6a294989d62db7fb11600d2b4519d961', '02', '04', '02', '10', '02', '02.04.02.10.02', 'Pandokan Sepatu', '2021-08-06 04:24:43'),
('b85494706e9e3bc9e6e81e383fec9e0b', '02', '04', '02', '10', '03', '02.04.02.10.03', 'Lis Sepatu Satu Set', '2021-08-06 04:24:43'),
('b54b2c84866041f5b7ae56a40f36e5dd', '02', '04', '02', '10', '04', '02.04.02.10.04', 'Cokro', '2021-08-06 04:24:43'),
('69b7d5886c7120584cace920be3fde7c', '02', '04', '02', '10', '05', '02.04.02.10.05', 'Plong Kulit Satu Set', '2021-08-06 04:24:43'),
('2d78d37feccd59231365e72c288b3abe', '02', '04', '02', '10', '06', '02.04.02.10.06', 'Catut', '2021-08-06 04:24:43'),
('119a44461c5c6a23fe5bb15ff7e6095d', '02', '04', '02', '10', '07', '02.04.02.10.07', 'Pukul Sepatu', '2021-08-06 04:24:43'),
('311d8a404ee77de1c9909d71a9b628c3', '02', '04', '02', '10', '08', '02.04.02.10.08', 'Gunting Kulit', '2021-08-06 04:24:43'),
('04f7f14996205819b3b7c1c4451b3749', '02', '04', '02', '10', '09', '02.04.02.10.09', 'Gunting Kain', '2021-08-06 04:24:43'),
('2ca92de6155d78578ef24e3ca02f85ad', '02', '04', '02', '10', '10', '02.04.02.10.10', 'Drek Mata Ayam', '2021-08-06 04:24:43'),
('a4ae783f898d57daec47e9300df6ae4d', '02', '04', '02', '10', '11', '02.04.02.10.11', 'Jarum Kulit Satu Set', '2021-08-06 04:24:43'),
('30c3c2e62a90456e201b84634ee0aae2', '02', '04', '02', '10', '12', '02.04.02.10.12', 'Uncek', '2021-08-06 04:24:43'),
('908b308be3bb589c7248595c1d4ca5f1', '02', '04', '02', '10', '13', '02.04.02.10.13', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('f5b827397c9e5afa383171c8ecaffae5', '02', '04', '02', '11', '*', '02.04.02.11.*', 'Peralatan Ukur, Gip &ampxkommaxampxkommax Feting', '2021-08-06 04:24:43'),
('86e0ef323b1ffc8ad056ded597520e4a', '02', '04', '03', '*', '*', '02.04.03.*.*', 'ALAT UKUR', '2021-08-06 04:24:43'),
('b0ccc69df58d9084c392a88db7db3774', '02', '04', '03', '01', '*', '02.04.03.01.*', 'Alat Ukur Universal', '2021-08-06 04:24:43'),
('4f3773f56a18c20b21a3e9c834832add', '02', '04', '03', '02', '*', '02.04.03.02.*', 'Alat UkurxgmringxTest Intelegensia', '2021-08-06 04:24:43'),
('8297b856d4ef846b910476238ec5af11', '02', '04', '03', '03', '*', '02.04.03.03.*', 'Alat UkurxgmringxTest Alat Kepribadian', '2021-08-06 04:24:43'),
('e752936b4bca9c0f0fa5f84383caff39', '02', '04', '03', '04', '*', '02.04.03.04.*', 'Alat UkurxgmringxTest Klinis Lain', '2021-08-06 04:24:43'),
('57afe92731a66215908c6cb012338024', '02', '04', '03', '05', '*', '02.04.03.05.*', 'Alat Calibrasi', '2021-08-06 04:24:43'),
('8d7852d4d1d7d2312a4d6a9a582e738c', '02', '04', '03', '06', '*', '02.04.03.06.*', 'Oscilloscope', '2021-08-06 04:24:43'),
('97d13e6f0cb73fb47736d77c2580879e', '02', '04', '03', '07', '*', '02.04.03.07.*', 'Universal Tester', '2021-08-06 04:24:43'),
('b19a71d24087696a6e8ab7a907bab6c7', '02', '04', '03', '08', '*', '02.04.03.08.*', 'Alat UkurxgmringxPembanding', '2021-08-06 04:24:43'),
('a881cdd4cc3e6d9d26b06063e8b9580d', '02', '04', '03', '08', '01', '02.04.03.08.01', 'Ukuran Johanson xkkurixalat Pembanding Standar Ukur Panjangxkkurnanx', '2021-08-06 04:24:43'),
('7dafca584632dff385d92308acb2c05a', '02', '04', '03', '08', '12', '02.04.03.08.12', 'Termometer Standard', '2021-08-06 04:24:43'),
('44e317b0ca57239e583d45234030f7e8', '02', '04', '03', '08', '13', '02.04.03.08.13', 'Termometer Govermen Tester O Derajat sxgmringxd 100 Derajat Celsius', '2021-08-06 04:24:43'),
('6022318b6466b6fd8bc4ed81c1891cee', '02', '04', '03', '08', '14', '02.04.03.08.14', 'Thermostat xkkurixPenguji Pemeriksaan Termometerxkkurnanx', '2021-08-06 04:24:43'),
('29468a108bee9050ff3b8b96e30fb7c0', '02', '04', '03', '08', '15', '02.04.03.08.15', 'Jam Ukur xkkurixMeet Lockxkkurnanx', '2021-08-06 04:24:43'),
('9fa8fd14da6c2a10616d66422bebc925', '02', '04', '03', '08', '16', '02.04.03.08.16', 'Hardnes Tester', '2021-08-06 04:24:43'),
('e6761214995dff149faf144e9a47c339', '02', '04', '03', '08', '17', '02.04.03.08.17', 'Microscope', '2021-08-06 04:24:43'),
('f0e2e8dbc647fb69b758561ba09ab497', '02', '04', '03', '08', '18', '02.04.03.08.18', 'Stopwach', '2021-08-06 04:24:43'),
('a6259e2252c264fbc95837003c3501d7', '02', '04', '03', '08', '19', '02.04.03.08.19', 'Loup', '2021-08-06 04:24:43'),
('f480947860e4be496755c8f3a4ca9718', '02', '04', '03', '08', '20', '02.04.03.08.20', 'Micro Meter', '2021-08-06 04:24:43'),
('842446e6c7cf146029ae6338a33ab176', '02', '04', '03', '08', '21', '02.04.03.08.21', 'Pianimeter', '2021-08-06 04:24:43'),
('5efbdee381b33ec27056292f0ecb61ec', '02', '04', '03', '08', '22', '02.04.03.08.22', 'Metra Blok', '2021-08-06 04:24:43'),
('59e0728e7d4a269392224bdb29c25654', '02', '04', '03', '08', '30', '02.04.03.08.30', 'Avometer SU 20xstrix20 K', '2021-08-06 04:24:43'),
('7fb01ccaa7419c6393bfea335a20d361', '02', '04', '03', '08', '31', '02.04.03.08.31', 'Trappo 1.000 watt', '2021-08-06 04:24:43'),
('53b1a9b578165abd1134ea9ab74c22d8', '02', '04', '03', '08', '32', '02.04.03.08.32', 'Tool Set', '2021-08-06 04:24:43'),
('0f98adb0fbcd084137e873ebbdf8ddf4', '02', '04', '03', '08', '33', '02.04.03.08.33', 'Landasan Cap Lengkap', '2021-08-06 04:24:43'),
('ab690d9609be4a62d3068ee2b978557b', '02', '04', '03', '08', '34', '02.04.03.08.34', 'Kald Tiga Gantungan Dacin', '2021-08-06 04:24:43'),
('bac70f0c742bf538fb403a85e51b31a9', '02', '04', '03', '08', '35', '02.04.03.08.35', 'Alat Pendatar Takaran Bensin', '2021-08-06 04:24:43'),
('b330d731774db7042bdf1cbecd3c0162', '02', '04', '03', '08', '36', '02.04.03.08.36', 'Tang PlombirxgmringxSegel', '2021-08-06 04:24:43'),
('8981817a59db37b6d0f31b9baad930a4', '02', '04', '03', '08', '64', '02.04.03.08.64', 'Tool Maker Microscope Magnification 30 x', '2021-08-06 04:24:43'),
('f231aa7780d6c091d244358285ed09cf', '02', '04', '03', '08', '65', '02.04.03.08.65', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('c8f87503037d28aeb68f941787db933a', '02', '04', '03', '09', '*', '02.04.03.09.*', 'Alat Ukur Lainnya', '2021-08-06 04:24:43'),
('9ad42e526b44f78dfbc5ba42f167a521', '02', '04', '03', '09', '01', '02.04.03.09.01', 'Meter x xstrix 27 Dari Platina Tridium', '2021-08-06 04:24:43'),
('de1efa28c956f6b9fecca15704f87e97', '02', '04', '03', '09', '02', '02.04.03.09.02', 'H.xstrix Meter Dari Baja Nikel', '2021-08-06 04:24:43'),
('8583559f3e148d8face8c9ac2198df8c', '02', '04', '03', '09', '03', '02.04.03.09.03', 'Komparator', '2021-08-06 04:24:43'),
('53b1ce64dda0c33eff0ed16cf68d16db', '02', '04', '03', '09', '04', '02.04.03.09.04', 'Alat Pengukur Garis Tengah', '2021-08-06 04:24:43'),
('949a76e9077dd205f947bd125214d5dd', '02', '04', '03', '09', '05', '02.04.03.09.05', 'Ban Ukur', '2021-08-06 04:24:43'),
('11ef647e79bf7cc0a931c3811125c6e4', '02', '04', '03', '09', '06', '02.04.03.09.06', 'Diameter Tape', '2021-08-06 04:24:43'),
('f7f5dcf611560160d30d68a861f334dc', '02', '04', '03', '09', '07', '02.04.03.09.07', 'Ukuran Tinggi Orang', '2021-08-06 04:24:43'),
('e85d2a032a11f6b585b19a84c987b514', '02', '04', '03', '09', '08', '02.04.03.09.08', 'Schulfimaat xkkurixUkuran Ingsufxkkurnanx', '2021-08-06 04:24:43'),
('80f5b14e54158bb32daa8555a31e959a', '02', '04', '03', '09', '09', '02.04.03.09.09', 'Liffer Standard xkkurix1 literxkkurnanx', '2021-08-06 04:24:43'),
('aff4fd3b19c3a0bc08c5bd07def467b1', '02', '04', '03', '09', '10', '02.04.03.09.10', 'Bejana Ukur', '2021-08-06 04:24:43'),
('192d5e29d8c7dd35535b7b9b6d018421', '02', '04', '03', '09', '11', '02.04.03.09.11', 'Alat Ukur Kadar Air', '2021-08-06 04:24:43'),
('7bc1b919ad9311ff9cb82734ae5abc8f', '02', '04', '03', '09', '12', '02.04.03.09.12', 'Alat Ukur Pemecah Kulit Gabag', '2021-08-06 04:24:43'),
('ce168b37030f3586ffbd35dbe6849534', '02', '04', '03', '09', '13', '02.04.03.09.13', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('9a77f579d675513a093a5fee8749dc42', '02', '04', '03', '10', '*', '02.04.03.10.*', 'Alat TimbanganxgmringxBlora', '2021-08-06 04:24:43'),
('68ab37025d7822b7a3b403b6a0256cb3', '02', '04', '03', '10', '01', '02.04.03.10.01', 'Timbangan Jembatan Capasitas 10 Ton', '2021-08-06 04:24:43'),
('8bdc8b93f6020cc583575cc03169a411', '02', '04', '03', '10', '02', '02.04.03.10.02', 'Timbangan Meja Capasitas 10 Kg', '2021-08-06 04:24:43'),
('16bf2812f1b1ee1380340fcb61f83987', '02', '04', '03', '10', '03', '02.04.03.10.03', 'Timbangan Meja Capasitas 5 Kg', '2021-08-06 04:24:43'),
('3b9e85f496377025333651a386d72121', '02', '04', '03', '10', '04', '02.04.03.10.04', 'Timbangan BBI Capasitas 100 Kg', '2021-08-06 04:24:43'),
('5c3e573f08df147130130a8b4735adfe', '02', '04', '03', '10', '05', '02.04.03.10.05', 'Timbangan BBI Capasitas 25 Kg', '2021-08-06 04:24:43'),
('65603457f77a4372130c8b03e78b9c00', '02', '04', '03', '10', '06', '02.04.03.10.06', 'Timbangan BBI Capasitas 15 Kg xkkurixTimbangan Bayixkkurnanx', '2021-08-06 04:24:43'),
('30eaefccf0fe4db95dba8e85f1823c19', '02', '04', '03', '10', '07', '02.04.03.10.07', 'Timbangan BBI Capasitas 10 Kg', '2021-08-06 04:24:43'),
('9120bdf42c05e7b98c2a48929cf88de8', '02', '04', '03', '10', '08', '02.04.03.10.08', 'Timbangan Cepat Capasitas 10 Kg', '2021-08-06 04:24:43'),
('0f43a66be633e079b45111488fea8e12', '02', '04', '03', '10', '09', '02.04.03.10.09', 'Timbangan Cepat Capasitas 25 Kg', '2021-08-06 04:24:43'),
('87688c38b52871f9031d9650bd1023a8', '02', '04', '03', '10', '10', '02.04.03.10.10', 'Timbangan Cepat Capasitas 200 Kg', '2021-08-06 04:24:43'),
('f7749b4c7d52936378384547332df068', '02', '04', '03', '10', '11', '02.04.03.10.11', 'Timbangan Cepat Capasitas 10 Kg', '2021-08-06 04:24:43'),
('9096088125f95dc04e840aad33733475', '02', '04', '03', '10', '12', '02.04.03.10.12', 'Timbangan Pegas Capasitas 50 Kg', '2021-08-06 04:24:43'),
('ac072cfad474c377d61a3fa64c6d1dc8', '02', '04', '03', '10', '13', '02.04.03.10.13', 'Timbangan Pegas Capasitas 50 Kg', '2021-08-06 04:24:43'),
('c6e08ca8551a5d6db6f19c4840cd8a5c', '02', '04', '03', '10', '14', '02.04.03.10.14', 'Timbangan Surat Capasitas 100 Kg', '2021-08-06 04:24:43'),
('1c491141da69144033182bf9be878933', '02', '04', '03', '10', '15', '02.04.03.10.15', 'Timbangan Kwandran Capasitas 100 Kg', '2021-08-06 04:24:43'),
('83f16381fe8d253078e3ee8d8aabc836', '02', '04', '03', '10', '16', '02.04.03.10.16', 'Timbangan Sentisimal', '2021-08-06 04:24:43'),
('0473184d3ccab67e9af90d6f927267a4', '02', '04', '03', '10', '17', '02.04.03.10.17', 'Dacln Kuningan', '2021-08-06 04:24:43'),
('0136a3c6d3fc0621c9230638cb10416f', '02', '04', '03', '10', '18', '02.04.03.10.18', 'Timbangan Gula Gaveka', '2021-08-06 04:24:43'),
('6e5eb9ee0c83f67822af08def47ec6cb', '02', '04', '03', '10', '19', '02.04.03.10.19', 'Timbangan Gantung Capasitas 50 Gr', '2021-08-06 04:24:43'),
('4d8206a5a3f0cd0ed904648aecf32303', '02', '04', '03', '10', '20', '02.04.03.10.20', 'Neraca Halus + Neraca Capasitas 500 Gr', '2021-08-06 04:24:43'),
('d040c0abf7f56744a373d3cc235c116f', '02', '04', '03', '10', '21', '02.04.03.10.21', 'Neraca Parama E', '2021-08-06 04:24:43'),
('1f87e2242beb22f62b705b24955decd6', '02', '04', '03', '10', '22', '02.04.03.10.22', 'Neraca Parama D Capasitas 5 Gr', '2021-08-06 04:24:43'),
('fde40c4decf081a120104389b7cf5bf8', '02', '04', '03', '10', '23', '02.04.03.10.23', 'Neraca Percisi Elecronic Capasitas 1 Kg', '2021-08-06 04:24:43'),
('edd42a124e3218de7d975ccaaa992ecd', '02', '04', '03', '10', '24', '02.04.03.10.24', 'Neraca Percisi xkkurixSingle Panxkkurnanx Capasitas 20 Kg', '2021-08-06 04:24:43'),
('f7f32e8da04cf29050b79039f70a01eb', '02', '04', '03', '10', '25', '02.04.03.10.25', 'Necara Percisi xkkurixElectronic Vacum MExkkurnanx', '2021-08-06 04:24:43'),
('5b7a20756cc5e91f96d76bfad5366ee1', '02', '04', '03', '10', '26', '02.04.03.10.26', 'Neraca Percisi 30 Kg xkkurixMicro Balancexkkurnanx', '2021-08-06 04:24:43'),
('1d774a6063e8e11ace92866b73a6f3cd', '02', '04', '03', '10', '27', '02.04.03.10.27', 'Neraca Percisi Capasitas 50 Gr', '2021-08-06 04:24:43'),
('5d283038be559c47cf6c118facd77487', '02', '04', '03', '10', '28', '02.04.03.10.28', 'Neraca Parcisi Capasitas 1 KG', '2021-08-06 04:24:43'),
('0be12ff56a16d575a0e5339ea1f418bc', '02', '04', '03', '10', '29', '02.04.03.10.29', 'Neraca Tera E', '2021-08-06 04:24:43'),
('a6f0e1c473242e665f60e82e0744d7e1', '02', '04', '03', '10', '30', '02.04.03.10.30', 'Neraca Tera A Capasitas 75 KG', '2021-08-06 04:24:43'),
('148694177644ea83ba01bff35190c72b', '02', '04', '03', '10', '31', '02.04.03.10.31', 'Neraca Tera B Capasitas 10 KG', '2021-08-06 04:24:43'),
('b4d015813203083a21b22e80ef118cd3', '02', '04', '03', '10', '32', '02.04.03.10.32', 'Neraca Torsion Balance Capasitas 500 Gr', '2021-08-06 04:24:43'),
('f38799627a245fafcba3369849fbcc2a', '02', '04', '03', '10', '33', '02.04.03.10.33', 'Neraca Analisa Capasitas 1000 Gr', '2021-08-06 04:24:43'),
('aaa0bbcb220ef0f50225cbc2d415290e', '02', '04', '03', '10', '34', '02.04.03.10.34', 'Neraca Analisa Capasitas 20 Kg', '2021-08-06 04:24:43'),
('015a3f4d21ac326099b084d16c19845a', '02', '04', '03', '10', '35', '02.04.03.10.35', 'Neraca Capasitas 1 Kg', '2021-08-06 04:24:43'),
('ac8079abecb45cc1aac48a18ac7fa809', '02', '04', '03', '10', '36', '02.04.03.10.36', 'Neraca Capasitas 20 Kg', '2021-08-06 04:24:43'),
('64d4d490e486f7eed9818b872f99c0cc', '02', '04', '03', '10', '37', '02.04.03.10.37', 'Moister Meter', '2021-08-06 04:24:43'),
('cfcecf92a9c33e71e2b079ccd5fd2931', '02', '04', '03', '10', '38', '02.04.03.10.38', 'Neraca Dengan Digityal Display', '2021-08-06 04:24:43'),
('261ee0f8266a553479ad16bee229983b', '02', '04', '03', '10', '39', '02.04.03.10.39', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1dbad58f9a34517dd9da9fbc1378c3bb', '02', '04', '03', '11', '*', '02.04.03.11.*', 'Anak TimbanganxgmringxBiasa', '2021-08-06 04:24:43'),
('4867cf93eefc856cd8489354d2f32006', '02', '04', '03', '11', '01', '02.04.03.11.01', 'Kilogram Tembaga Nasional Platina', '2021-08-06 04:24:43'),
('0f6c48d8ddbf332ca65294ede15cd83c', '02', '04', '03', '11', '02', '02.04.03.11.02', 'Kilogram Tenmbanga Bentuk Tong', '2021-08-06 04:24:43'),
('7ae4167a165097607115a07491590816', '02', '04', '03', '11', '03', '02.04.03.11.03', 'Kilogram Sepuh Mas 1 Kg Pakai Tombol', '2021-08-06 04:24:43'),
('41c9868f8f6e83d461e8645185954cc9', '02', '04', '03', '11', '04', '02.04.03.11.04', 'Kilogram Baja Berbentuk Tong Bersandur Croom', '2021-08-06 04:24:43'),
('2b696c74ef128df97ce496882820398b', '02', '04', '03', '11', '05', '02.04.03.11.05', 'Kilogram dari Baja Berbentuk Slinder', '2021-08-06 04:24:43'),
('2e0c6f5fec929732faf3892ee62cd143', '02', '04', '03', '11', '06', '02.04.03.11.06', 'Kilogram Kerja Standar Tingkat II', '2021-08-06 04:24:43'),
('8d1185227a5799f04994e0d70c15a3a2', '02', '04', '03', '11', '07', '02.04.03.11.07', 'Kilogram Standar', '2021-08-06 04:24:43'),
('200dd6c5433f38328ea7aeeeff072042', '02', '04', '03', '11', '08', '02.04.03.11.08', 'Anak Timbangan Tembaga Kantor Tingkat III', '2021-08-06 04:24:43'),
('94b51373ac82fb3fa72898266c9145f4', '02', '04', '03', '11', '09', '02.04.03.11.09', 'Anak Timbangan Miligram', '2021-08-06 04:24:43'),
('6a953d3c9562cc7128d4006d740f5cfb', '02', '04', '03', '11', '10', '02.04.03.11.10', 'Anak Timbangan Miligram Platina', '2021-08-06 04:24:43'),
('5501822f097057d2d559e54a8e8082b3', '02', '04', '03', '11', '11', '02.04.03.11.11', 'Anak Timbangan Miligram Aluminium', '2021-08-06 04:24:43'),
('39ff5236069608524730bf1a70099c80', '02', '04', '03', '11', '12', '02.04.03.11.12', 'Anak Timbangan Gram Standar 1 Gram', '2021-08-06 04:24:43'),
('49041e8425cd04f713a0ec1b4adb45d2', '02', '04', '03', '11', '13', '02.04.03.11.13', 'Anak Timbangan Halus daro 1.000 xstrix 1 Gr', '2021-08-06 04:24:43'),
('0ae4b0156db619087f9ad9973a2973f5', '02', '04', '03', '11', '14', '02.04.03.11.14', 'Anak Timbangan Biasa dari 1000 xstrix 1 Gr', '2021-08-06 04:24:43'),
('fab82fcd6b136e3a279eb62ab4eff1b7', '02', '04', '03', '11', '15', '02.04.03.11.15', 'Anak Timbangan Bidur', '2021-08-06 04:24:43'),
('7e39d4a0196995950eb87c7276fbb480', '02', '04', '03', '11', '16', '02.04.03.11.16', 'Anak Timbangan dari Besi', '2021-08-06 04:24:43'),
('8389c34d973dd2176b35b0ea0e13af20', '02', '04', '03', '11', '17', '02.04.03.11.17', 'Anak Timbangan Keping xkkurixMulut Kecilxkkurnanx', '2021-08-06 04:24:43'),
('545b31e645d0d32bc822ff5a093adf7c', '02', '04', '03', '11', '18', '02.04.03.11.18', 'Anak Timbangan Keping xkkurixMulut Besarxkkurnanx', '2021-08-06 04:24:43'),
('1a2e604aaeb0bb8f4f2adbeb08e40797', '02', '04', '03', '11', '19', '02.04.03.11.19', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('7c33951157104a5d5e82db09c6255d78', '02', '04', '03', '12', '*', '02.04.03.12.*', 'Takaran Kering', '2021-08-06 04:24:43'),
('0900c8507d18feb93709f791fff1236d', '02', '04', '03', '12', '01', '02.04.03.12.01', 'Dari 100 xstrix 50 xstrix 20 Liter', '2021-08-06 04:24:43'),
('1449e48ee2189701a797375b5855936f', '02', '04', '03', '12', '02', '02.04.03.12.02', 'Dari 10 xstrixsxgmringxd 0,5 Liter', '2021-08-06 04:24:43'),
('7786a91acad5d41506d72d0a205bdf59', '02', '04', '03', '12', '03', '02.04.03.12.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('5e38b76c9ae3cf077033fe3fae63e851', '02', '04', '03', '13', '*', '02.04.03.13.*', 'Takaran Bahan Bangunan 2 HL', '2021-08-06 04:24:43'),
('86b3cfa7d6fcc9e3be474102cbe7a340', '02', '04', '03', '13', '01', '02.04.03.13.01', 'Takaran Bahan Bangunan 2 HL Berbentuk Tong', '2021-08-06 04:24:43'),
('91488a4c35a7f4533b95102863b19840', '02', '04', '03', '13', '02', '02.04.03.13.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('292f894a8b9db2fa7246003f34021a66', '02', '04', '03', '14', '*', '02.04.03.14.*', 'Takaran LatexxgmringxGetah Susu', '2021-08-06 04:24:43'),
('ed4c210da4e03dcff759d40e3a9284e5', '02', '04', '03', '14', '01', '02.04.03.14.01', 'Takaran Buah Kopi dari 0,5 Hl', '2021-08-06 04:24:43'),
('7760d921d91da2fdf93390eea3bbdabe', '02', '04', '03', '14', '02', '02.04.03.14.02', 'Takaran Kapuk dari Kayu 2 dan 1 Hl', '2021-08-06 04:24:43'),
('e77dcedd8ac651171133d0883d047158', '02', '04', '03', '14', '03', '02.04.03.14.03', 'Takaran Minyak dari Besi 0,5 Hl', '2021-08-06 04:24:43'),
('71ecaa3a93c30f29e6ff0b4015048b56', '02', '04', '03', '14', '04', '02.04.03.14.04', 'Takaran Gandum 0,5 Hl', '2021-08-06 04:24:43'),
('df6d612e56b732762db85cb0fc0af10c', '02', '04', '03', '14', '05', '02.04.03.14.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('84fe0d9bcf6edf8fa98b8c5c660726e1', '02', '04', '03', '15', '*', '02.04.03.15.*', 'Gelas Takar Berbagai Capasitas', '2021-08-06 04:24:43'),
('2178140575c428d9dc0785e9174f3045', '02', '04', '03', '15', '01', '02.04.03.15.01', 'Labu Takar xkkurixVolumetrikxkkurnanx berbagai Kapasitas', '2021-08-06 04:24:43'),
('bb49662ef147419bfee55f429fcfbfe4', '02', '04', '03', '15', '02', '02.04.03.15.02', 'Botol Uji Berbagai Ukuran', '2021-08-06 04:24:43'),
('3fe3a17e2f5f9e656081c077a7382358', '02', '04', '03', '15', '03', '02.04.03.15.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b8b1091456354c17a2cab758f5909ae9', '02', '05', '*', '*', '*', '02.05.*.*.*', 'ALAT PERTANIAN', '2021-08-06 04:24:43'),
('22a51b182fc4f94ab47a61f2bbbfbd3f', '02', '05', '01', '*', '*', '02.05.01.*.*', 'ALAT PENGOLAHAN', '2021-08-06 04:24:43'),
('ca366252f9ec102e1187fa0e9614b114', '02', '05', '01', '01', '*', '02.05.01.01.*', 'Alat Pengolahan Tanah dan Tanaman', '2021-08-06 04:24:43'),
('7540787347ae82ea1f4ae1c4dc43e4c2', '02', '05', '01', '01', '01', '02.05.01.01.01', 'Bajak Kayu', '2021-08-06 04:24:43'),
('c494c666f426ae99a2343fc9fa324f49', '02', '05', '01', '01', '02', '02.05.01.01.02', 'Bajak Muara', '2021-08-06 04:24:43'),
('ddf357b2eddc56d055151af1af8e3d86', '02', '05', '01', '01', '03', '02.05.01.01.03', 'Pacul', '2021-08-06 04:24:43'),
('f099eda13bba6fcc833ba37745b2c4cd', '02', '05', '01', '01', '04', '02.05.01.01.04', 'Linggis', '2021-08-06 04:24:43'),
('a9362729c83f4d84270d0cc1d1ee0bb1', '02', '05', '01', '01', '05', '02.05.01.01.05', 'Garpu Pacul', '2021-08-06 04:24:43'),
('b47d5fdbbf27622ffed2b3b9edbf4fca', '02', '05', '01', '01', '06', '02.05.01.01.06', 'Garpu Kayu', '2021-08-06 04:24:43'),
('60279c7992ba6715edbbaff301f10d0b', '02', '05', '01', '01', '07', '02.05.01.01.07', 'Garpu Besi', '2021-08-06 04:24:43'),
('a69e484060d3459efc0ca3ded14dba8d', '02', '05', '01', '01', '08', '02.05.01.01.08', 'Traktor Four Whell xkkurixLengkap Peralatannyaxkkurnanx', '2021-08-06 04:24:43'),
('f3a32f090ad43933f2edbb74c03b1a03', '02', '05', '01', '01', '09', '02.05.01.01.09', 'Tractor Tangan dengan perlengkapannya', '2021-08-06 04:24:43'),
('88f68b5012d5ff2b18ebcd8876e06682', '02', '05', '01', '01', '10', '02.05.01.01.10', 'Hewan kerbau', '2021-08-06 04:24:43'),
('8c6ea32d4f658d3f0e83da6a402ebe38', '02', '05', '01', '01', '11', '02.05.01.01.11', 'Hewan Sapi', '2021-08-06 04:24:43'),
('0ad3175b8a4e55fc9026255a063103bc', '02', '05', '01', '01', '12', '02.05.01.01.12', 'Chain Saw', '2021-08-06 04:24:43'),
('ab04afcb2982ac4280999b0a91ce17b0', '02', '05', '01', '01', '13', '02.05.01.01.13', 'Madula', '2021-08-06 04:24:43'),
('ef7d499afb8e2691a95fc126c12ed26a', '02', '05', '01', '01', '14', '02.05.01.01.14', 'Skap', '2021-08-06 04:24:43'),
('4d40036d7d2681b6d6084b7dd562675c', '02', '05', '01', '01', '15', '02.05.01.01.15', 'Garu', '2021-08-06 04:24:43'),
('67c1514b822952df4db66065cd9f930e', '02', '05', '01', '01', '16', '02.05.01.01.16', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('7b9dec9a4ef6703342a12d650fd191ec', '02', '05', '01', '02', '*', '02.05.01.02.*', 'Alat PanenxgmringxPengolahan', '2021-08-06 04:24:43'),
('8c063578560326da95e4fe75f2091241', '02', '05', '01', '02', '01', '02.05.01.02.01', 'Tugal Kayu', '2021-08-06 04:24:43'),
('2e17ec1b88dae894aada097e7b0c3fba', '02', '05', '01', '02', '02', '02.05.01.02.02', 'Tugal Besi', '2021-08-06 04:24:43'),
('2167d261f208fa169b44f666b47a258f', '02', '05', '01', '02', '03', '02.05.01.02.03', 'Ember Plastik', '2021-08-06 04:24:43'),
('13a3f03080210184b655354974c19f1d', '02', '05', '01', '02', '04', '02.05.01.02.04', 'Gayung Plastik', '2021-08-06 04:24:43'),
('fa0365efab017881191cd4b0bfd80cf0', '02', '05', '01', '02', '05', '02.05.01.02.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('00a18e7fabf034c0c2c9f385a49f4a13', '02', '05', '01', '03', '*', '02.05.01.03.*', 'Alatxstrixalat Peternakan', '2021-08-06 04:24:43'),
('9f24e20ef8c796713844456419b9ff13', '02', '05', '01', '04', '*', '02.05.01.04.*', 'Alat Penyimpan Hasil Percobaan Pertanian', '2021-08-06 04:24:43'),
('4c03fb794f7bda2e53594a22bd194bd5', '02', '05', '01', '04', '01', '02.05.01.04.01', 'Oven', '2021-08-06 04:24:43'),
('bde370231468f5afac2b13e74f333597', '02', '05', '01', '04', '02', '02.05.01.04.02', 'Cold Strorage xkkurixKamar Pendinginxkkurnanx', '2021-08-06 04:24:43'),
('99b248298695d76b1b01947159b46bde', '02', '05', '01', '04', '03', '02.05.01.04.03', 'Selo xkkurixKotak Penyimpanxkkurnanx dengan mengatur temperatur', '2021-08-06 04:24:43'),
('2866658214b9d2d1db165dee079adb19', '02', '05', '01', '04', '04', '02.05.01.04.04', 'Rakxstrixrak penyimpan', '2021-08-06 04:24:43'),
('daf5ab427a99cd2d16e290fb548bed40', '02', '05', '01', '04', '05', '02.05.01.04.05', 'Lemari Penyimpan', '2021-08-06 04:24:43'),
('6fa0c426354757643d40713a8b40b8e9', '02', '05', '01', '04', '06', '02.05.01.04.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('449dd4748cc17d9fa5068d68979929b4', '02', '05', '01', '05', '*', '02.05.01.05.*', 'Alat Laboratorium Pertanian', '2021-08-06 04:24:43'),
('20f547dbe52e4ae6a35498726e429f30', '02', '05', '01', '05', '01', '02.05.01.05.01', 'Alat Pengukur Curah Hujan', '2021-08-06 04:24:43'),
('12ddf7208049d4687ac258b2d878ac8a', '02', '05', '01', '05', '02', '02.05.01.05.02', 'Alat Pengukur Cahaya', '2021-08-06 04:24:43'),
('2c39456424409d3e6e727ca4ee905b5b', '02', '05', '01', '05', '03', '02.05.01.05.03', 'Alat Pengukur Intensitas Cahaya', '2021-08-06 04:24:43'),
('45a305ad2ddd2cf04cb0f9622b586b5a', '02', '05', '01', '05', '04', '02.05.01.05.04', 'Alat Pengukur Temperatur', '2021-08-06 04:24:43'),
('810f89a5e9a738f34608b516d087b6b3', '02', '05', '01', '05', '05', '02.05.01.05.05', 'Alat Pengukur PH Tanah xkkurixSoil Testerxkkurnanx', '2021-08-06 04:24:43'),
('fc34588211533e3a06ea234917b174b9', '02', '05', '01', '05', '06', '02.05.01.05.06', 'Alat Pengambil Sample Tanah', '2021-08-06 04:24:43'),
('16b90c71863fd1a57c18bda6bc9cbb17', '02', '05', '01', '05', '07', '02.05.01.05.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('0c30d39dcc07d1ead53b2ad08569fb93', '02', '05', '01', '06', '*', '02.05.01.06.*', 'Alat Prosesing', '2021-08-06 04:24:43'),
('2af211baaa43a41c3983a5f4bf31e3f3', '02', '05', '01', '06', '01', '02.05.01.06.01', 'Unit Pengaduk', '2021-08-06 04:24:43'),
('f3cdf0c7fdeeea28f0c2e805cd1e0918', '02', '05', '01', '06', '02', '02.05.01.06.02', 'Alat Pencabut Bulu Ayam', '2021-08-06 04:24:43'),
('e39dcae8b26482409eae6dd88cede764', '02', '05', '01', '06', '03', '02.05.01.06.03', 'Alat Pembuat PaletxgmringxMakanan Ternak', '2021-08-06 04:24:43'),
('f86a32d84002e3dea4ba0dfe29accd0a', '02', '05', '01', '06', '04', '02.05.01.06.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('5bbae6966c467c7b3712ba62f9211997', '02', '05', '01', '07', '*', '02.05.01.07.*', 'Alat Pasca Panen', '2021-08-06 04:24:43'),
('c548c9eba7d90f7b07f3f301eb9b6051', '02', '05', '01', '07', '01', '02.05.01.07.01', 'Alat Pengasapan', '2021-08-06 04:24:43'),
('5589e765ff396b5a98f07313e7d483a2', '02', '05', '01', '07', '02', '02.05.01.07.02', 'Alat Pembekuan', '2021-08-06 04:24:43'),
('c9f29d051c52531e8ec2ef054cb0a5fb', '02', '05', '01', '07', '03', '02.05.01.07.03', 'Alat Penggilingan Padi', '2021-08-06 04:24:43'),
('9b418314a9e07188945a6d827342b7da', '02', '05', '01', '07', '04', '02.05.01.07.04', 'Alat Pencacah Hijauan', '2021-08-06 04:24:43'),
('e04774b4f53eb2e929f4b8bffdc73fea', '02', '05', '01', '07', '05', '02.05.01.07.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('bf0047b996310b616f58b908f0195c3a', '02', '05', '01', '08', '*', '02.05.01.08.*', 'Alat Produksi Perikanan', '2021-08-06 04:24:43'),
('65fab7ecf43345c426929537ee876924', '02', '05', '02', '*', '*', '02.05.02.*.*', 'ALAT PEMELIHARAAN TANAMANxgmringxALAT PENYIMPANAN', '2021-08-06 04:24:43'),
('ebd4e0c370b03599768a7336d3b388fc', '02', '05', '02', '01', '*', '02.05.02.01.*', 'Alat Pemeliharaan Tanaman', '2021-08-06 04:24:43'),
('7baf47a63b53120c7215eb82770e6460', '02', '05', '02', '01', '01', '02.05.02.01.01', 'Kored', '2021-08-06 04:24:43'),
('ac6269b6b6175c39399754bb138d2775', '02', '05', '02', '01', '02', '02.05.02.01.02', 'Arit', '2021-08-06 04:24:43'),
('10d2d9bc3a704c2da847afbdba53ae8a', '02', '05', '02', '01', '03', '02.05.02.01.03', 'Babadan', '2021-08-06 04:24:43'),
('5a826d8e74159919d65cebefd0acae96', '02', '05', '02', '01', '04', '02.05.02.01.04', 'Pacul Dangir', '2021-08-06 04:24:43'),
('46553c0bb00f96d0ef6ac9956d9ba7d5', '02', '05', '02', '01', '05', '02.05.02.01.05', 'Penyemprot Otomatis xkkurixAutomatis Spayerxkkurnanx', '2021-08-06 04:24:43'),
('25de88448a514c548b10a8dcb2a6cab4', '02', '05', '02', '01', '06', '02.05.02.01.06', 'Penyemprot Mesin xkkurixPower Spayerxkkurnanx', '2021-08-06 04:24:43'),
('6cb1edee61f1c185b2cd0796bbc85d00', '02', '05', '02', '01', '07', '02.05.02.01.07', 'Penyemprot Tangan xkkurixHand Srayerxkkurnanx', '2021-08-06 04:24:43'),
('e3f900859487d058781ed505419d25bd', '02', '05', '02', '01', '08', '02.05.02.01.08', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3adb0689a532aafa228c2172497e566c', '02', '05', '02', '02', '*', '02.05.02.02.*', 'Alat Panen', '2021-08-06 04:24:43'),
('056b6bff75e7b5db511b2d8e1de9463c', '02', '05', '02', '02', '01', '02.05.02.02.01', 'Anixstrixani', '2021-08-06 04:24:43'),
('d95d115289492ed719c3924b8df0bd1b', '02', '05', '02', '02', '02', '02.05.02.02.02', 'Alat Perontok xkkurixTheresar pedalxkkurnanx', '2021-08-06 04:24:43'),
('b5b3a027f8525ff4a6f3a767fa4453f3', '02', '05', '02', '02', '03', '02.05.02.02.03', 'Alat Perontok Mesin xkkurixPower theresarxkkurnanx', '2021-08-06 04:24:43'),
('89f366e1e7de23433d87b9b7cce38f18', '02', '05', '02', '02', '04', '02.05.02.02.04', 'Alat Pemipit Jagung', '2021-08-06 04:24:43'),
('6a01b0d7ff7bbfd35e6b638bb52834b8', '02', '05', '02', '02', '05', '02.05.02.02.05', 'Karung', '2021-08-06 04:24:43'),
('7e767d00e1710c8240d10ac658d429d4', '02', '05', '02', '02', '06', '02.05.02.02.06', 'Blek', '2021-08-06 04:24:43'),
('1f5bd47cf0f9504d78ba092570b00e11', '02', '05', '02', '02', '07', '02.05.02.02.07', 'Alat Pengering xkkurixDreyerxkkurnanx', '2021-08-06 04:24:43'),
('6cabd7a72d8b8a13f26d9c6f77392c92', '02', '05', '02', '02', '08', '02.05.02.02.08', 'Alat Pengering Mesin xkkurixPower Dreyerxkkurnanx', '2021-08-06 04:24:43'),
('fb3e3381a4efb915fcb7a1e34bd8772d', '02', '05', '02', '02', '09', '02.05.02.02.09', 'Alat Pengukur Kadar Air xkkurixMeisture Terterxkkurnanx', '2021-08-06 04:24:43'),
('f21c661bf03f68dae0a9d82cc36f02f4', '02', '05', '02', '02', '10', '02.05.02.02.10', 'Honey xkkurixPenggulung Berasxkkurnanx', '2021-08-06 04:24:43'),
('ece3574f0d76a37fd282888bb5e4991a', '02', '05', '02', '02', '11', '02.05.02.02.11', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('27272c283c625d1ae9bd6424fdec95fb', '02', '05', '02', '03', '*', '02.05.02.03.*', 'Alat Penyimpanan', '2021-08-06 04:24:43'),
('8aedab56b9ea13b1fcf2b134190f05bd', '02', '05', '02', '03', '01', '02.05.02.03.01', 'Oven', '2021-08-06 04:24:43'),
('84337cb0ea79f9198313c630bcc16fa9', '02', '05', '02', '03', '02', '02.05.02.03.02', 'Cold Storage xkkurixKamar Pendinginxkkurnanx', '2021-08-06 04:24:43'),
('271391cfbb2876fe56b00ccc7ea492dc', '02', '05', '02', '03', '03', '02.05.02.03.03', 'Selo xkkurixKodak Penyimpanan xkkurnanx bisa diatur temperaturnya', '2021-08-06 04:24:43'),
('a3a22bbcf82fd3724674dbee4d7d92ef', '02', '05', '02', '03', '04', '02.05.02.03.04', 'Rakxstrixrak penyimpanan', '2021-08-06 04:24:43'),
('4c9704fae6dd07a82f6b57cf65dd035b', '02', '05', '02', '03', '05', '02.05.02.03.05', 'Lemari Penyimpanan', '2021-08-06 04:24:43'),
('a219bf92b7b96dadd66962ac8f8660c9', '02', '05', '02', '03', '06', '02.05.02.03.06', 'Gudang', '2021-08-06 04:24:43'),
('a2be98f984fd09e54d968a518f2a429f', '02', '05', '02', '03', '07', '02.05.02.03.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('7a14081c2e499c3388518eed2c86647c', '02', '05', '02', '04', '*', '02.05.02.04.*', 'Alat Laboratorium', '2021-08-06 04:24:43'),
('1bb1a7ef823895f51ea6b16e387ac9a5', '02', '05', '02', '04', '01', '02.05.02.04.01', 'Alat Pengukur Curah Hujan', '2021-08-06 04:24:43'),
('efebbd851d83cb46934c02cf0b5cbaa9', '02', '05', '02', '04', '02', '02.05.02.04.02', 'Alat Pengukur Cahaya', '2021-08-06 04:24:43'),
('cb2605b14f270ab9d38713aa360a3b25', '02', '05', '02', '04', '03', '02.05.02.04.03', 'Alat Pengukur Arah Angin', '2021-08-06 04:24:43'),
('f4658dfd33bb9e85fd856aede080f520', '02', '05', '02', '04', '04', '02.05.02.04.04', 'Alat Pengukur Intensitas Cahaya', '2021-08-06 04:24:43'),
('4c390e1b21416cb9f9f7d0d659b95d57', '02', '05', '02', '04', '05', '02.05.02.04.05', 'Alat Pengukur Temperatur', '2021-08-06 04:24:43'),
('06f9ef1ee85258c808c3a44c0852a6a1', '02', '05', '02', '04', '06', '02.05.02.04.06', 'Alat Pengukur PH Tanah xkkurixSoil Testerxkkurnanx', '2021-08-06 04:24:43'),
('979661339a2d9109350ae2496e177181', '02', '05', '02', '04', '07', '02.05.02.04.07', 'Alat Pengambil Sample Tanah', '2021-08-06 04:24:43'),
('c59a0492fb0e5efc29fd4e230de2ed76', '02', '05', '02', '04', '08', '02.05.02.04.08', 'Mesin Penetes Telur', '2021-08-06 04:24:43'),
('2dfbf3e34f1e69a00029ee0a7761a754', '02', '05', '02', '04', '09', '02.05.02.04.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('d88ae2e5d207dc95f549aa1f12ceee29', '02', '05', '02', '05', '*', '02.05.02.05.*', 'Alat Penangkap Ikan', '2021-08-06 04:24:43'),
('29850aa0c756e27f75611d8b36bd5d36', '02', '05', '02', '05', '01', '02.05.02.05.01', 'Jaring', '2021-08-06 04:24:43'),
('d0956e0538c1308857131f0190dccef1', '02', '05', '02', '05', '02', '02.05.02.05.02', 'AncoxgmringxTangkul', '2021-08-06 04:24:43'),
('382631d684c39ab6edd989c4d1aaf862', '02', '05', '02', '05', '03', '02.05.02.05.03', 'Keramba Apung', '2021-08-06 04:24:43'),
('08ec1806daf437203bb0e7bfe5527c04', '02', '05', '02', '05', '04', '02.05.02.05.04', 'RawaixgmringxLongline, Tombak', '2021-08-06 04:24:43'),
('6450e3c7102b8bb32102f3aa1c5ff6b6', '02', '05', '02', '05', '05', '02.05.02.05.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('640e07418ec0d86bb31db55c16618022', '02', '06', '*', '*', '*', '02.06.*.*.*', 'ALAT KANTOR DAN RUMAH TANGGA', '2021-08-06 04:24:43'),
('240c5e0873cb58ffa5fe352f13421988', '02', '06', '01', '*', '*', '02.06.01.*.*', 'ALAT KANTOR', '2021-08-06 04:24:43'),
('0b9eed03864a48711ef54c1f2517aaef', '02', '06', '01', '01', '*', '02.06.01.01.*', 'Mesin Tik', '2021-08-06 04:24:43'),
('0375ea93d83e3d227ffd64869bd2e172', '02', '06', '01', '01', '01', '02.06.01.01.01', 'Mesin Ketik Manual Portable xkkurix11xstrix13xkkurnanx', '2021-08-06 04:24:43'),
('2e3cd89da8c2ffce0f836ed7c38869fd', '02', '06', '01', '01', '02', '02.06.01.01.02', 'Mesin Ketik Manual Standar xkkurix14xstrix16xkkurnanx', '2021-08-06 04:24:43'),
('f1b7f123c6b59047f0f5dd34ba18532e', '02', '06', '01', '01', '03', '02.06.01.01.03', 'Mesin Ketik Manual Longewagen xkkurix18..xkkurnanx', '2021-08-06 04:24:43'),
('2a09c789f8e63117d1be5e93b1ba3545', '02', '06', '01', '01', '04', '02.06.01.01.04', 'Mesin Ketik Listrik Portable', '2021-08-06 04:24:43'),
('01e7b3c1c172e696d275ba0d64fb7f3b', '02', '06', '01', '01', '05', '02.06.01.01.05', 'Mesin Listrik Standar', '2021-08-06 04:24:43'),
('5c0ae42d050e08df7000b60f975df4d2', '02', '06', '01', '01', '06', '02.06.01.01.06', 'Mesin Ketik Listrik Longewagen', '2021-08-06 04:24:43'),
('51eeb2f7463596d66440f04369368a76', '02', '06', '01', '01', '07', '02.06.01.01.07', 'Mesin Ketik Elektronik', '2021-08-06 04:24:43'),
('b6c6260407faea95aa3a6984830508f3', '02', '06', '01', '01', '08', '02.06.01.01.08', 'Mesin Ketik ElektronikxgmringxSelektrik', '2021-08-06 04:24:43'),
('3d6f2902818993c5848c3eb2e643f8b3', '02', '06', '01', '01', '09', '02.06.01.01.09', 'Mesin Ketik Braile', '2021-08-06 04:24:43'),
('7af507d5d82dfca65d5c4c0c00a94424', '02', '06', '01', '01', '10', '02.06.01.01.10', 'Mesin Ketik Phromosons', '2021-08-06 04:24:43'),
('cdb5e3b7e67cf456578f35206d458117', '02', '06', '01', '01', '11', '02.06.01.01.11', 'Mesin Cetak Stereo Pioner xkkurixBrailexkkurnanx', '2021-08-06 04:24:43'),
('6ec086cc6d9f29b043dad96322d248c2', '02', '06', '01', '01', '12', '02.06.01.01.12', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('cc952f7a89a68a05651e3edeea0516cb', '02', '06', '01', '02', '*', '02.06.01.02.*', 'Mesin Hitung xgmringx Jumlah', '2021-08-06 04:24:43'),
('e53e4ee7cd5497d6e495b52b55cb3575', '02', '06', '01', '02', '01', '02.06.01.02.01', 'Mesin Hitung Manual', '2021-08-06 04:24:43'),
('dc8599d48efd4ba6f77d98d40f297b45', '02', '06', '01', '02', '02', '02.06.01.02.02', 'Mesin Hitung Listrik', '2021-08-06 04:24:43'),
('8764e106af2716f8724c41ba0391e5bf', '02', '06', '01', '02', '03', '02.06.01.02.03', 'Mesin Hitung Elektronik', '2021-08-06 04:24:43'),
('31e24fb8a802dd9fb232ddefbd02028a', '02', '06', '01', '02', '04', '02.06.01.02.04', 'Mesin Jumlah Manual', '2021-08-06 04:24:43'),
('5cade0ab83cff498906212aac1296fc9', '02', '06', '01', '02', '05', '02.06.01.02.05', 'Mesin Jumlah Listrik', '2021-08-06 04:24:43'),
('d25cdc4845e9ce2b5bb451b1ef6b93d9', '02', '06', '01', '02', '06', '02.06.01.02.06', 'Mesin Jumlah Eletornik', '2021-08-06 04:24:43'),
('30a90b8b2e0e46d57f95973196a1f43c', '02', '06', '01', '02', '07', '02.06.01.02.07', 'Mesin Kas Regester', '2021-08-06 04:24:43'),
('338c8ccbb55270eeac989816072ceae7', '02', '06', '01', '02', '08', '02.06.01.02.08', 'Mesin Pembukuan', '2021-08-06 04:24:43'),
('49a36c0affc40fd2b30d824be8ccb0e5', '02', '06', '01', '02', '09', '02.06.01.02.09', 'Mesin Absen xkkurixTime Recorderxkkurnanx', '2021-08-06 04:24:43'),
('fbdac0cfa4f7bf1ca269259bd1125289', '02', '06', '01', '02', '10', '02.06.01.02.10', 'Mesin KontrolxgmringxJaga', '2021-08-06 04:24:43'),
('b331e11fc19836e7cc239ff3abb7e508', '02', '06', '01', '02', '11', '02.06.01.02.11', 'Mesin calculator', '2021-08-06 04:24:43'),
('0d6147a4e476149c10b751a3382c9ca5', '02', '06', '01', '02', '12', '02.06.01.02.12', 'Mesin Penghitung Uang', '2021-08-06 04:24:43'),
('7d7b63cee36430abd3ab163413822c17', '02', '06', '01', '02', '13', '02.06.01.02.13', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('f91e73c66e71af0d9bb65825dc919ea8', '02', '06', '01', '03', '*', '02.06.01.03.*', 'Alat Reproduksi xkkurixPenggandaxkkurnanx', '2021-08-06 04:24:43'),
('38b52f44aaee7d13bc0253cedbfbf01b', '02', '06', '01', '03', '01', '02.06.01.03.01', 'Mesin Stensil Manual Folio', '2021-08-06 04:24:43'),
('7770e5ddb411309fdad84a4ecfc3c2b7', '02', '06', '01', '03', '02', '02.06.01.03.02', 'Mesin Stensil Manual Doble Folio', '2021-08-06 04:24:43'),
('70f3a509e7287ab8c2e992aa799313a1', '02', '06', '01', '03', '03', '02.06.01.03.03', 'Mesin Stensil Listrik Folio', '2021-08-06 04:24:43'),
('abf5fb07c7303abb9efd7118cb31570b', '02', '06', '01', '03', '04', '02.06.01.03.04', 'Mesin Stensil Listrik Doble Folio', '2021-08-06 04:24:43'),
('ec99c87e6ae12e0dcda77f402e724255', '02', '06', '01', '03', '05', '02.06.01.03.05', 'Mesin Stentil Spirtus Manual', '2021-08-06 04:24:43'),
('0c2e570202d81c970153c51cf6981964', '02', '06', '01', '03', '06', '02.06.01.03.06', 'Mesin Stentil Spirtus Listrik', '2021-08-06 04:24:43'),
('84758a44d2e0c63a82600b9fe68677f7', '02', '06', '01', '03', '07', '02.06.01.03.07', 'Mesin Foto Copy dengan keras Folio', '2021-08-06 04:24:43'),
('cf77e003a64c0b448f236421054ed698', '02', '06', '01', '03', '08', '02.06.01.03.08', 'Mesin Photo Copy dengan kertas doble Folio', '2021-08-06 04:24:43'),
('0a5c4e7ca012dfd46027673198f76672', '02', '06', '01', '03', '09', '02.06.01.03.09', 'Mesin Photo Copi dengan kertas biasa Folio', '2021-08-06 04:24:43'),
('48d183288d262c470630fe1b64139d06', '02', '06', '01', '03', '10', '02.06.01.03.10', 'Mesin Photo Copi dengan kertas biasa doble Folio', '2021-08-06 04:24:43'),
('4a62fbbb04a4768ae8a1ad7f453e6beb', '02', '06', '01', '03', '11', '02.06.01.03.11', 'Mesin Perekam Stentil Folio', '2021-08-06 04:24:43'),
('892606909960e51b80becdcb540ec022', '02', '06', '01', '03', '12', '02.06.01.03.12', 'Mesin Perekam Stentil doble Folio', '2021-08-06 04:24:43'),
('5fead1165f7c090f65a1a66b88014092', '02', '06', '01', '03', '13', '02.06.01.03.13', 'Mesin Plate Folio', '2021-08-06 04:24:43'),
('937ff067bb6fbf7a9346411b34705416', '02', '06', '01', '03', '14', '02.06.01.03.14', 'Mesin Plate Doble Folio', '2021-08-06 04:24:43'),
('f475d30fb3f02febd632a359dee56ad0', '02', '06', '01', '03', '15', '02.06.01.03.15', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('4b27ac03b3fc8d7642d8016dd9d14821', '02', '06', '01', '04', '*', '02.06.01.04.*', 'Alat Penyimpanan Perlengkapan Kantor', '2021-08-06 04:24:43'),
('f1b59fe0cefd781f429b06d9d47ef2cc', '02', '06', '01', '04', '01', '02.06.01.04.01', 'Lemari BesixgmringxMetal', '2021-08-06 04:24:43'),
('eb052bf2562d024810eca01bc2390f0e', '02', '06', '01', '04', '02', '02.06.01.04.02', 'Rak BesixgmringxMetal', '2021-08-06 04:24:43'),
('ef080ddc413811cabbf59155855535e9', '02', '06', '01', '04', '03', '02.06.01.04.03', 'Rak Kayu', '2021-08-06 04:24:43'),
('cab0abd1a85586d18b635ee5365a6937', '02', '06', '01', '04', '04', '02.06.01.04.04', 'Filling BesixgmringxMetal', '2021-08-06 04:24:43'),
('4ebcc994e8c41849fbdc1087ec326dba', '02', '06', '01', '04', '05', '02.06.01.04.05', 'Filling Kayu', '2021-08-06 04:24:43'),
('03b20cf8576520164aea095411d986bf', '02', '06', '01', '04', '06', '02.06.01.04.06', 'Brand Kas', '2021-08-06 04:24:43'),
('ba1dd6e2eae4a0626278bf9ed1abffdd', '02', '06', '01', '04', '07', '02.06.01.04.07', 'Kardek BesixgmringxMetal', '2021-08-06 04:24:43'),
('38fae5e6036b1b0fc12a43eed5cfa47d', '02', '06', '01', '04', '08', '02.06.01.04.08', 'Kardek Kayu', '2021-08-06 04:24:43'),
('3d5f85ab884e25b00716c02887970074', '02', '06', '01', '04', '09', '02.06.01.04.09', 'Rotary Filling', '2021-08-06 04:24:43'),
('21a443ea37828492c49c0e77d6f6a26a', '02', '06', '01', '04', '10', '02.06.01.04.10', 'Peti Uang', '2021-08-06 04:24:43');
INSERT INTO `m_kib_kode` (`kd`, `golongan`, `bidang`, `kelompok`, `kelompok_sub`, `kelompok_sub_sub`, `kode`, `nama`, `postdate`) VALUES
('a9c8d554a1e05b7ad20115503a18aeb0', '02', '06', '01', '04', '11', '02.06.01.04.11', 'Lemari Sorok', '2021-08-06 04:24:43'),
('e5151ba06333e35dcbb015cb2b1971b5', '02', '06', '01', '04', '12', '02.06.01.04.12', 'Lemari Kaca', '2021-08-06 04:24:43'),
('e6e2ee3a03fb9d3eaeeaef3c611864f8', '02', '06', '01', '04', '13', '02.06.01.04.13', 'Lemari Makan', '2021-08-06 04:24:43'),
('7ea1fd985027de75c3dfe196c168c46d', '02', '06', '01', '04', '14', '02.06.01.04.14', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('007542561f9919eb5c4d568aeae45ea5', '02', '06', '01', '05', '*', '02.06.01.05.*', 'Alat Kantor Lainnya', '2021-08-06 04:24:43'),
('55257be12c2d73f8a9ace2074cc3d5c3', '02', '06', '01', '05', '01', '02.06.01.05.01', 'Papan Visuil', '2021-08-06 04:24:43'),
('d3aaaf2ee3f311c221a0aac5ddd9491e', '02', '06', '01', '05', '02', '02.06.01.05.02', 'Perkakas Kantor', '2021-08-06 04:24:43'),
('3bf63d82cb57e2014759c3a64c1c8387', '02', '06', '01', '05', '03', '02.06.01.05.03', 'Alat PengamatanxgmringxSinyal', '2021-08-06 04:24:43'),
('3c410ecceddf2d3e74cec768ae887c20', '02', '06', '01', '05', '04', '02.06.01.05.04', 'Alat Dektektor Uang Palsu', '2021-08-06 04:24:43'),
('15d63f69785ccb27ef15a247df310a1d', '02', '06', '01', '05', '05', '02.06.01.05.05', 'Alat Penghancur Kertas', '2021-08-06 04:24:43'),
('46e83bb2af3eaa123fda6371e05e0406', '02', '06', '01', '05', '06', '02.06.01.05.06', 'Papan Nama Instansi', '2021-08-06 04:24:43'),
('1bc746a045bcb1d0d98e2d7605cd9d00', '02', '06', '01', '05', '07', '02.06.01.05.07', 'Papan Pengumuman', '2021-08-06 04:24:43'),
('fb8d670f3774ba1059e33d08b733e226', '02', '06', '01', '05', '08', '02.06.01.05.08', 'Papan Tulis', '2021-08-06 04:24:43'),
('979698bc05f984cd35e5e42903602d83', '02', '06', '01', '05', '09', '02.06.01.05.09', 'Papan Absen', '2021-08-06 04:24:43'),
('54725d33f9f82dbf486bddd08b975f3a', '02', '06', '01', '05', '10', '02.06.01.05.10', 'White Board', '2021-08-06 04:24:43'),
('a05de992f5d37d2a4e52f4418316a078', '02', '06', '01', '05', '11', '02.06.01.05.11', 'Alat Detektor Uang Palsu', '2021-08-06 04:24:43'),
('4efc6f25095ccfda54ae516b99809c95', '02', '06', '01', '05', '12', '02.06.01.05.12', 'Alat Detektor Barang TerlarangxgmringxX Ray', '2021-08-06 04:24:43'),
('2b9260dbe0eed6f034c3dba25adf2bd2', '02', '06', '01', '05', '13', '02.06.01.05.13', 'Copy BoardxgmringxElektrik White Board', '2021-08-06 04:24:43'),
('36b08559a19e3b2123c8374e6255291d', '02', '06', '01', '05', '14', '02.06.01.05.14', 'Peta', '2021-08-06 04:24:43'),
('4c5553c485a5f8cc9fb8ace7403cd148', '02', '06', '01', '05', '15', '02.06.01.05.15', 'Alat Penghancur Kertas Globe', '2021-08-06 04:24:43'),
('73d4a4680071ab41dd77582946e91090', '02', '06', '01', '05', '16', '02.06.01.05.16', 'Globe', '2021-08-06 04:24:43'),
('bc257550b91e45d1d72bc6517f783cb2', '02', '06', '01', '05', '17', '02.06.01.05.17', 'Mesin Absensi', '2021-08-06 04:24:43'),
('2a41675e0b0ef32469fb8299fb5ce4b6', '02', '06', '01', '05', '18', '02.06.01.05.18', 'Dry Seal', '2021-08-06 04:24:43'),
('13545267f874f2601bd8c6f4d3e2f209', '02', '06', '01', '05', '19', '02.06.01.05.19', 'Fergulator', '2021-08-06 04:24:43'),
('bf95744bb577ef240147bf73afdd979b', '02', '06', '01', '05', '20', '02.06.01.05.20', 'Crelm Folisher', '2021-08-06 04:24:43'),
('ec9d88d490efce776921b209059b55b8', '02', '06', '01', '05', '21', '02.06.01.05.21', 'Mesin Perangko', '2021-08-06 04:24:43'),
('628ab30a94ba3861fab969f79a2c27ef', '02', '06', '01', '05', '22', '02.06.01.05.22', 'Check Writer', '2021-08-06 04:24:43'),
('2b19690476a4b06f55b193c6db05bc8b', '02', '06', '01', '05', '23', '02.06.01.05.23', 'Numirator', '2021-08-06 04:24:43'),
('a544f8d077bc22d5f611876f0df74759', '02', '06', '01', '05', '24', '02.06.01.05.24', 'Alat Pemotong Kertas', '2021-08-06 04:24:43'),
('25d6cf11c027e32e3f3c25fe0d54558b', '02', '06', '01', '05', '25', '02.06.01.05.25', 'Hecmaching Besar', '2021-08-06 04:24:43'),
('f0b49d8a672e9915f6c719556dc162f7', '02', '06', '01', '05', '26', '02.06.01.05.26', 'Perforator Besar', '2021-08-06 04:24:43'),
('5b6db0007baed2fd89b15df5c5b4140b', '02', '06', '01', '05', '27', '02.06.01.05.27', 'Alat Pencetak Label', '2021-08-06 04:24:43'),
('acc9842164d1e6a3b9b533f5c7e449b3', '02', '06', '01', '05', '28', '02.06.01.05.28', 'Overhead Projektor', '2021-08-06 04:24:43'),
('f6b536701a393269c774e531fcc6d210', '02', '06', '01', '05', '30', '02.06.01.05.30', 'Walkman Detector', '2021-08-06 04:24:43'),
('1c58db9f97b82582cd566584f46d54f4', '02', '06', '01', '05', '31', '02.06.01.05.31', 'Panel Pameran', '2021-08-06 04:24:43'),
('4ad04dd247f57ad88adc6a546d096bab', '02', '06', '01', '05', '32', '02.06.01.05.32', 'Alat Pengaman xkkurixSinyalxkkurnanx', '2021-08-06 04:24:43'),
('bba9398078509872c1e653066caff0a8', '02', '06', '01', '05', '33', '02.06.01.05.33', 'Board Modulux', '2021-08-06 04:24:43'),
('1f609d6ae11940598ac473645469b909', '02', '06', '01', '05', '34', '02.06.01.05.34', 'Porto Safe Travel Cose', '2021-08-06 04:24:43'),
('d1eead0bc92a6be5234274a27e0694d3', '02', '06', '01', '05', '35', '02.06.01.05.35', 'Disk Prime', '2021-08-06 04:24:43'),
('87c2c744253a66cd9e028a16346a9bc6', '02', '06', '01', '05', '36', '02.06.01.05.36', 'Megashow', '2021-08-06 04:24:43'),
('7a41aec2a77263779814538366aea253', '02', '06', '01', '05', '37', '02.06.01.05.37', 'White Board Elektronic', '2021-08-06 04:24:43'),
('ef3912c08af0e11cc92ef26c92612b8c', '02', '06', '01', '05', '38', '02.06.01.05.38', 'Laser Pionter', '2021-08-06 04:24:43'),
('09d2d729f60b047b8be29a90d8286877', '02', '06', '01', '05', '39', '02.06.01.05.39', 'Display', '2021-08-06 04:24:43'),
('98292c94b104bb25ecf5d636c97ed96f', '02', '06', '01', '05', '40', '02.06.01.05.40', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('c7e382966c116a4e41b9b1a462da50f7', '02', '06', '02', '*', '*', '02.06.02.*.*', 'Alat Rumah Tangga', '2021-08-06 04:24:43'),
('540d15e925fa215bd9ebca0fcebfbe9c', '02', '06', '02', '01', '*', '02.06.02.01.*', 'Meubelair', '2021-08-06 04:24:43'),
('9043ba92ddfc404de6d80777d5fe5c91', '02', '06', '02', '01', '01', '02.06.02.01.01', 'Lemari Kayu', '2021-08-06 04:24:43'),
('8a0c66a67750e06bc53da05f04953c4c', '02', '06', '02', '01', '02', '02.06.02.01.02', 'Rak Kayu', '2021-08-06 04:24:43'),
('285042bfa883d36b1bdce327d9ec5bd9', '02', '06', '02', '01', '03', '02.06.02.01.03', 'Meja BesixgmringxMetal', '2021-08-06 04:24:43'),
('fec7bbfac375c60f481a295bc66cb4f0', '02', '06', '02', '01', '04', '02.06.02.01.04', 'Meja KayuxgmringxRotan', '2021-08-06 04:24:43'),
('1c8f396b32efda90c07cbf21dc2290c7', '02', '06', '02', '01', '05', '02.06.02.01.05', 'Kursi Besi xgmringx Metal', '2021-08-06 04:24:43'),
('c69d361d98e73443e93c94eaffd08fd5', '02', '06', '02', '01', '06', '02.06.02.01.06', 'Kursi kayuxgmringxRotanxgmringxBambu', '2021-08-06 04:24:43'),
('28c42eaf993a90cc7bf9e8ba723a59a5', '02', '06', '02', '01', '07', '02.06.02.01.07', 'Zice', '2021-08-06 04:24:43'),
('d08dd35b5378415d58f6f5a9088fc2e5', '02', '06', '02', '01', '08', '02.06.02.01.08', 'Tempat Tidur BesixgmringxMetal xkkurixLengkapxkkurnanx', '2021-08-06 04:24:43'),
('5ccd4db398b2532f9d514ef110c7d98d', '02', '06', '02', '01', '09', '02.06.02.01.09', 'Tempat Tidur Kayu xkkurixlengkapxkkurnanx', '2021-08-06 04:24:43'),
('457df2439dad1e2c3e994b44c72a533f', '02', '06', '02', '01', '10', '02.06.02.01.10', 'Meja Rapat', '2021-08-06 04:24:43'),
('4de341d47f088d6d96b0720be2049eab', '02', '06', '02', '01', '11', '02.06.02.01.11', 'Meja Tulis', '2021-08-06 04:24:43'),
('65e0f64047306d57c7f8ad271720d827', '02', '06', '02', '01', '12', '02.06.02.01.12', 'Meja Makan', '2021-08-06 04:24:43'),
('af7ffacfc3f9c9496a03ea6d039c7a2d', '02', '06', '02', '01', '13', '02.06.02.01.13', 'Meja Telpon', '2021-08-06 04:24:43'),
('083df1fbdf3c177c3a97ebee045226fd', '02', '06', '02', '01', '14', '02.06.02.01.14', 'Meja Lelang', '2021-08-06 04:24:43'),
('e80c3c180cb014b251623b6426633989', '02', '06', '02', '01', '15', '02.06.02.01.15', 'Meja Podium', '2021-08-06 04:24:43'),
('884178b9e52bcef1a65ea120137ef6cb', '02', '06', '02', '01', '16', '02.06.02.01.16', 'Meja Tik', '2021-08-06 04:24:43'),
('c4e87060923d6603302d7a44036c6732', '02', '06', '02', '01', '17', '02.06.02.01.17', 'Meja Resepsion', '2021-08-06 04:24:43'),
('beb3fa8b114fa828a239812818511463', '02', '06', '02', '01', '18', '02.06.02.01.18', 'Meja Tambahan', '2021-08-06 04:24:43'),
('7eff7b4fcb5653014db8a9d18d756c67', '02', '06', '02', '01', '19', '02.06.02.01.19', 'Meja Panjang', '2021-08-06 04:24:43'),
('dd9c99f6ec0d7f73cfa45bb547f8e6ab', '02', '06', '02', '01', '20', '02.06.02.01.20', 'Meja Bundar', '2021-08-06 04:24:43'),
('be203095992c15c5274b689d80bdb358', '02', '06', '02', '01', '21', '02.06.02.01.21', 'Meja Periksa Pasien', '2021-08-06 04:24:43'),
('fef9a346c5446c3b053e8f7d28d9ae02', '02', '06', '02', '01', '22', '02.06.02.01.22', 'Meja Obat', '2021-08-06 04:24:43'),
('54b6be0752a377cd30abb337792ccbbc', '02', '06', '02', '01', '23', '02.06.02.01.23', 'Meja Kartu', '2021-08-06 04:24:43'),
('54d93330a794abd96e9cd2aad766c074', '02', '06', '02', '01', '24', '02.06.02.01.24', 'Meja Suntik', '2021-08-06 04:24:43'),
('49008edd788eccbac4ccf337b852e8e0', '02', '06', '02', '01', '25', '02.06.02.01.25', 'Meja Bayi', '2021-08-06 04:24:43'),
('77e3c66d293c0aabc010383db32752f2', '02', '06', '02', '01', '26', '02.06.02.01.26', 'Meja Sekolah', '2021-08-06 04:24:43'),
('106f02e89743f40e66ebe24e1c9b3457', '02', '06', '02', '01', '27', '02.06.02.01.27', 'Kursi Rapat', '2021-08-06 04:24:43'),
('c119ca00bccf5e35e1c82cd973375f3c', '02', '06', '02', '01', '28', '02.06.02.01.28', 'Kursi Tamu', '2021-08-06 04:24:43'),
('38a03072b16dde2de96c89418b1682d0', '02', '06', '02', '01', '29', '02.06.02.01.29', 'Kursi Tangan', '2021-08-06 04:24:43'),
('68e894cd53f5bc4e04b72ddcd0ccc4f6', '02', '06', '02', '01', '30', '02.06.02.01.30', 'Kursi Putar', '2021-08-06 04:24:43'),
('195b6e06aad01e882388d17eeaa31c3e', '02', '06', '02', '01', '31', '02.06.02.01.31', 'Kursi Biasa', '2021-08-06 04:24:43'),
('9de8c76c9f4a9961fb11e0190793f25f', '02', '06', '02', '01', '32', '02.06.02.01.32', 'Bangku Sekolah', '2021-08-06 04:24:43'),
('7651c5320e3d81fbbdf9461a930ca250', '02', '06', '02', '01', '33', '02.06.02.01.33', 'Bangku Tunggu', '2021-08-06 04:24:43'),
('91cc31877779153cb5474d6948422681', '02', '06', '02', '01', '34', '02.06.02.01.34', 'Kursi Lipat', '2021-08-06 04:24:43'),
('4d9a53dffd7be5f0f12cbe716662a042', '02', '06', '02', '01', '35', '02.06.02.01.35', 'Bangku Injak', '2021-08-06 04:24:43'),
('caa4d2c59833f1e608c4614411f1e884', '02', '06', '02', '01', '36', '02.06.02.01.36', 'Meja Cetakan', '2021-08-06 04:24:43'),
('c01de517bad147e1e500648d2812c79d', '02', '06', '02', '01', '37', '02.06.02.01.37', 'Meja Komputer', '2021-08-06 04:24:43'),
('ab04406ccf71fc98a7ded813efdf006e', '02', '06', '02', '01', '38', '02.06.02.01.38', 'Kasur', '2021-08-06 04:24:43'),
('05d8351f4e185803a23c9afde2761379', '02', '06', '02', '01', '39', '02.06.02.01.39', 'Bantal', '2021-08-06 04:24:43'),
('46113b928ed3a23c67d020d54c8e7767', '02', '06', '02', '01', '40', '02.06.02.01.40', 'Guling', '2021-08-06 04:24:43'),
('c2b6ecbb8568daa92d0b84eb055457ca', '02', '06', '02', '01', '41', '02.06.02.01.41', 'Locker katun', '2021-08-06 04:24:43'),
('87a2aac235b7ab31e5b924e3acb30975', '02', '06', '02', '01', '42', '02.06.02.01.42', 'Selimut Wool', '2021-08-06 04:24:43'),
('0ad3b6731dc5906174fe5c7f44e80fe1', '02', '06', '02', '01', '43', '02.06.02.01.43', 'Waslap', '2021-08-06 04:24:43'),
('5d588da88f6fa23bf66ca98ce8871a33', '02', '06', '02', '01', '44', '02.06.02.01.44', 'Meja Piket', '2021-08-06 04:24:43'),
('f141f7d12f257b7fe0405bd162f00747', '02', '06', '02', '01', '45', '02.06.02.01.45', 'Sepre', '2021-08-06 04:24:43'),
('1ac2a0b1530b790d406dc4500fdcfd91', '02', '06', '02', '01', '46', '02.06.02.01.46', 'Tikar', '2021-08-06 04:24:43'),
('6d80370c33ff4501a3e24d34fb48b897', '02', '06', '02', '01', '47', '02.06.02.01.47', 'Tenda', '2021-08-06 04:24:43'),
('a1c2bcff01f4e74d332d9a06a7ff29c1', '02', '06', '02', '01', '48', '02.06.02.01.48', 'Meja 1xgmringx2 Biro', '2021-08-06 04:24:43'),
('a6d8b9d51080bba73a4569bd9d57d951', '02', '06', '02', '01', '49', '02.06.02.01.49', 'Sofa', '2021-08-06 04:24:43'),
('dda9b2076270bf8774169a6a15383e31', '02', '06', '02', '01', '50', '02.06.02.01.50', 'Daun Pintu Alumunium', '2021-08-06 04:24:43'),
('138236096a55bbb84505b7b1222688c9', '02', '06', '02', '01', '51', '02.06.02.01.51', 'Kaca Bening', '2021-08-06 04:24:43'),
('09fef430d301fc034f0013d8174f512f', '02', '06', '02', '01', '52', '02.06.02.01.52', 'Kaca Riben', '2021-08-06 04:24:43'),
('09473eb3a10b0fc8e472203d88064da5', '02', '06', '02', '01', '53', '02.06.02.01.53', 'Kasur Alumunium', '2021-08-06 04:24:43'),
('783dbb5e0e3aa85ea4ccc62e0f4fa721', '02', '06', '02', '01', '54', '02.06.02.01.54', 'Lemari Pakaian', '2021-08-06 04:24:43'),
('aec8db47f3e607e45073e53a8c722689', '02', '06', '02', '01', '55', '02.06.02.01.55', 'Lemari Rias', '2021-08-06 04:24:43'),
('9233b60f42305eb3107dfb8cf163fc15', '02', '06', '02', '01', '56', '02.06.02.01.56', 'Ratto', '2021-08-06 04:24:43'),
('c096302b1fecab1fd10ec22482585d57', '02', '06', '02', '01', '57', '02.06.02.01.57', 'Jepano', '2021-08-06 04:24:43'),
('97dbfc40ef21b037e397cecb84070b4c', '02', '06', '02', '01', '58', '02.06.02.01.58', 'Pusiban', '2021-08-06 04:24:43'),
('9cbbf0e0aadbf58e791b808e68db579c', '02', '06', '02', '01', '59', '02.06.02.01.59', 'Panggo', '2021-08-06 04:24:43'),
('89e2d30c70ff30b8434afa0d91f82bd3', '02', '06', '02', '01', '60', '02.06.02.01.60', 'Tudung Saji', '2021-08-06 04:24:43'),
('da0110ee494fe9bc0d57504b2df6d991', '02', '06', '02', '01', '61', '02.06.02.01.61', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('acbcc5f3ae173e4a5d4881e2e209efc6', '02', '06', '02', '02', '*', '02.06.02.02.*', 'Alat Pengukur Waktu', '2021-08-06 04:24:43'),
('d42bdfc232b270954ec18bccbbd68067', '02', '06', '02', '02', '01', '02.06.02.02.01', 'Jam Mekanis', '2021-08-06 04:24:43'),
('e92996c140e948ca19ee36f34ee87862', '02', '06', '02', '02', '02', '02.06.02.02.02', 'Jam Listrik', '2021-08-06 04:24:43'),
('aba35bc80aeca438e5bd3e6ef26eda06', '02', '06', '02', '02', '03', '02.06.02.02.03', 'Jam Elektronik', '2021-08-06 04:24:43'),
('e84027a44f55f101b9585f83d17ac03f', '02', '06', '02', '02', '04', '02.06.02.02.04', 'Lampu Lalulintas xkkurixTravic Lighxkkurnanx', '2021-08-06 04:24:43'),
('5feec38e426d3e0bbd3ef5760582b851', '02', '06', '02', '02', '05', '02.06.02.02.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('64748f2755c3c7dbe8571f2e62810a63', '02', '06', '02', '03', '*', '02.06.02.03.*', 'Alat Pembersih', '2021-08-06 04:24:43'),
('ec658ef8e175080ef9eec0b232c45c90', '02', '06', '02', '03', '01', '02.06.02.03.01', 'Mesin Penghisap Debu', '2021-08-06 04:24:43'),
('3f3bf95228b0c9c38d79b82754b26db5', '02', '06', '02', '03', '02', '02.06.02.03.02', 'Mesin Pel', '2021-08-06 04:24:43'),
('b255ce29f3c645d9f2646614846742ca', '02', '06', '02', '03', '03', '02.06.02.03.03', 'Mesin Potong Rumput', '2021-08-06 04:24:43'),
('34d2b6c81d87dded2f5d09c864a06d9f', '02', '06', '02', '03', '04', '02.06.02.03.04', 'Mesin Cuci', '2021-08-06 04:24:43'),
('1e68f86cc616644107ca52d308a7b33d', '02', '06', '02', '03', '05', '02.06.02.03.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('d08edae1aca30425eb8b3405dd84627b', '02', '06', '02', '04', '*', '02.06.02.04.*', 'Alat Pendingin', '2021-08-06 04:24:43'),
('0b542e35ed1400ef4d99b4665395e7d6', '02', '06', '02', '04', '01', '02.06.02.04.01', 'Lemari Es', '2021-08-06 04:24:43'),
('46479671186d9482aab64b2263d6f315', '02', '06', '02', '04', '02', '02.06.02.04.02', 'AC Sentral', '2021-08-06 04:24:43'),
('b4689218131dbf50b5392be61f95d7d8', '02', '06', '02', '04', '03', '02.06.02.04.03', 'AC Unit', '2021-08-06 04:24:43'),
('ce9537587a9e5b66a8933689d8fe723d', '02', '06', '02', '04', '04', '02.06.02.04.04', 'AC Split', '2021-08-06 04:24:43'),
('39d29103f15ae7afcbcbc1044cd20d0a', '02', '06', '02', '04', '05', '02.06.02.04.05', 'Power Conditioner', '2021-08-06 04:24:43'),
('73ebe99af521e0de3e7883e53306e0df', '02', '06', '02', '04', '06', '02.06.02.04.06', 'Kipas Angin', '2021-08-06 04:24:43'),
('3da371b3c695773563463ceb44f202df', '02', '06', '02', '04', '07', '02.06.02.04.07', 'Exhause Fan', '2021-08-06 04:24:43'),
('2f530e1678ded64a32afb5ef5d82fe2d', '02', '06', '02', '04', '08', '02.06.02.04.08', 'Cold Storage', '2021-08-06 04:24:43'),
('f3b54adbb39af96b74d8c5a69888d433', '02', '06', '02', '04', '09', '02.06.02.04.09', 'Reach In Frezzer', '2021-08-06 04:24:43'),
('001a2b4fee7a80ef0b3f9a1e54acb9a6', '02', '06', '02', '04', '10', '02.06.02.04.10', 'Reach In Chiller', '2021-08-06 04:24:43'),
('c8e6f495ab86894166fdf1067e9e4543', '02', '06', '02', '04', '11', '02.06.02.04.11', 'Up Right ChillerxgmringxFrezzer', '2021-08-06 04:24:43'),
('0771842d83163332e93b995dac652d76', '02', '06', '02', '04', '12', '02.06.02.04.12', 'Cold Room Storage', '2021-08-06 04:24:43'),
('3aa6da2ff34fb4aeb8e44e051a98c4ed', '02', '06', '02', '04', '13', '02.06.02.04.13', 'lainxstrixlain', '2021-08-06 04:24:43'),
('dbc66145829d487038ba55ea22920565', '02', '06', '02', '05', '*', '02.06.02.05.*', 'Alat Dapur', '2021-08-06 04:24:43'),
('6e1013f0e1adcba4b61f72f039229cda', '02', '06', '02', '05', '01', '02.06.02.05.01', 'Kompor Listrik', '2021-08-06 04:24:43'),
('42f1759ba529d9f33d9e786e9f494049', '02', '06', '02', '05', '02', '02.06.02.05.02', 'Kompor Gas', '2021-08-06 04:24:43'),
('531e579f3ad6c84bea8cc1e3c08eded3', '02', '06', '02', '05', '03', '02.06.02.05.03', 'Kompor Minyak', '2021-08-06 04:24:43'),
('8618b617e54b15b1ef1775d71579cbca', '02', '06', '02', '05', '04', '02.06.02.05.04', 'Teko Listrik', '2021-08-06 04:24:43'),
('ce5c4dcb96075eb2c6b66bddf0c38a76', '02', '06', '02', '05', '05', '02.06.02.05.05', 'Alat Dapur Lainnya', '2021-08-06 04:24:43'),
('87978a9df6eaf49e547c21ad40a9b1b6', '02', '06', '02', '05', '06', '02.06.02.05.06', 'Oven Listrik', '2021-08-06 04:24:43'),
('33fdf05b7c6b71be00d72d84f4ec0033', '02', '06', '02', '05', '07', '02.06.02.05.07', 'Alat Dapur Lainnya', '2021-08-06 04:24:43'),
('87af0394da90a0e6b021d276f454b3e7', '02', '06', '02', '05', '08', '02.06.02.05.08', 'Kitchen set', '2021-08-06 04:24:43'),
('94891dc2ab9072ca1d5aa2d616863ef8', '02', '06', '02', '05', '09', '02.06.02.05.09', 'Tabung Gas', '2021-08-06 04:24:43'),
('fcf5215dc6465638296c4c4a519d59cd', '02', '06', '02', '05', '10', '02.06.02.05.10', 'Mesin Giling Bambu', '2021-08-06 04:24:43'),
('a12573e60fb883e34b512819f26b6653', '02', '06', '02', '05', '11', '02.06.02.05.11', 'Treng Air', '2021-08-06 04:24:43'),
('0bc5f8d20f46ae781ebe08565b381c14', '02', '06', '02', '05', '12', '02.06.02.05.12', 'Mesin Parutan Kelapa', '2021-08-06 04:24:43'),
('f52e15edef2bb4ddffdabc89c27cd744', '02', '06', '02', '05', '13', '02.06.02.05.13', 'Kompor Kompresor', '2021-08-06 04:24:43'),
('327ca92942d1cb0e50900537f61bcf97', '02', '06', '02', '05', '14', '02.06.02.05.14', 'lainxstrixlain', '2021-08-06 04:24:43'),
('65bd85f014075aa7f54814b88b4e5d25', '02', '06', '02', '06', '*', '02.06.02.06.*', 'Alat Rumah Tangga Lainnya xkkurixHome Usexkkurnanx', '2021-08-06 04:24:43'),
('4744cf4818e7e9400928e1f9a56bc75f', '02', '06', '02', '06', '01', '02.06.02.06.01', 'Alat Pemanas', '2021-08-06 04:24:43'),
('685111917fecff82d665d447c201c001', '02', '06', '02', '06', '02', '02.06.02.06.02', 'Radio', '2021-08-06 04:24:43'),
('adf1bd7a467a105bc7df03f7d20e8a08', '02', '06', '02', '06', '03', '02.06.02.06.03', 'Televisi', '2021-08-06 04:24:43'),
('c619abcda8af832f3f47f2da39fa2ca6', '02', '06', '02', '06', '04', '02.06.02.06.04', 'Cassette Recorder', '2021-08-06 04:24:43'),
('1f6508f398cb58becb1b99832d95b576', '02', '06', '02', '06', '05', '02.06.02.06.05', 'Amplifier', '2021-08-06 04:24:43'),
('852cfd254bca62af534f26e5d5255d61', '02', '06', '02', '06', '06', '02.06.02.06.06', 'Equalizer', '2021-08-06 04:24:43'),
('87373d719bf3f23c1df04008ee986133', '02', '06', '02', '06', '07', '02.06.02.06.07', 'Loudspeker', '2021-08-06 04:24:43'),
('4bd9e54b355042e64409863e3a3709bd', '02', '06', '02', '06', '08', '02.06.02.06.08', 'Sound System', '2021-08-06 04:24:43'),
('3605b0a6f94612523741307ada3db42e', '02', '06', '02', '06', '09', '02.06.02.06.09', 'Compact Disk', '2021-08-06 04:24:43'),
('e7f45880024ca7a2777b3a6447311823', '02', '06', '02', '06', '10', '02.06.02.06.10', 'Laser Disc', '2021-08-06 04:24:43'),
('54d5fcccd63be8583292152324c2e7d2', '02', '06', '02', '06', '11', '02.06.02.06.11', 'Karaoke', '2021-08-06 04:24:43'),
('06ee6f3fd12c164c4f3703d78f0325ea', '02', '06', '02', '06', '12', '02.06.02.06.12', 'Wireless', '2021-08-06 04:24:43'),
('a5977c3c3f97524d028f12955b6bf45d', '02', '06', '02', '06', '13', '02.06.02.06.13', 'Megaphone', '2021-08-06 04:24:43'),
('81d9e347093963dff13f6bd5fa731ddb', '02', '06', '02', '06', '14', '02.06.02.06.14', 'Microphone', '2021-08-06 04:24:43'),
('f7b57981720790ca4f2ea04e3a067061', '02', '06', '02', '06', '15', '02.06.02.06.15', 'Microphone Floor Stand', '2021-08-06 04:24:43'),
('21f75f4939dfb9dd34d7bc41f90eaca5', '02', '06', '02', '06', '16', '02.06.02.06.16', 'Microphone Table Stand', '2021-08-06 04:24:43'),
('bd9aae9dccb8e80f6f424e4114815af1', '02', '06', '02', '06', '17', '02.06.02.06.17', 'Mic Conference', '2021-08-06 04:24:43'),
('aaa70bad128d6e9d92ee0571c07ac344', '02', '06', '02', '06', '18', '02.06.02.06.18', 'Unit Power Supply', '2021-08-06 04:24:43'),
('d94289c1747f0017e7bf3f32c0404e95', '02', '06', '02', '06', '19', '02.06.02.06.19', 'Step UpxgmringxDown', '2021-08-06 04:24:43'),
('5881f95b4726984f3a7f10298c7dde3d', '02', '06', '02', '06', '20', '02.06.02.06.20', 'Stabilisator', '2021-08-06 04:24:43'),
('f6aa370b389d55f0583a47978308d36a', '02', '06', '02', '06', '21', '02.06.02.06.21', 'Camera Video', '2021-08-06 04:24:43'),
('41a50f23c9ecabbe1f8d9fe140ca7384', '02', '06', '02', '06', '22', '02.06.02.06.22', 'Camera Film', '2021-08-06 04:24:43'),
('100615cce4b6409dc385e36eff1c631a', '02', '06', '02', '06', '23', '02.06.02.06.23', 'Tustel', '2021-08-06 04:24:43'),
('8a289957fae072ea45d770b402571e04', '02', '06', '02', '06', '24', '02.06.02.06.24', 'Mesin Jahit', '2021-08-06 04:24:43'),
('7d6ede9e40b0423a34db875f8b38b2f6', '02', '06', '02', '06', '25', '02.06.02.06.25', 'Timbangan Orang', '2021-08-06 04:24:43'),
('22f4906133a85798bf46beefc1631d99', '02', '06', '02', '06', '26', '02.06.02.06.26', 'Timbangan Barang', '2021-08-06 04:24:43'),
('eca22991001da8913bf78021a16b4c5c', '02', '06', '02', '06', '27', '02.06.02.06.27', 'Alat Hiasan', '2021-08-06 04:24:43'),
('4ebf54eb2460ba00af5c4cfc2a7d00be', '02', '06', '02', '06', '28', '02.06.02.06.28', 'Lambang Garuda Pancasila', '2021-08-06 04:24:43'),
('4bf7d8d374e2ef45d60b711acb6d1dd2', '02', '06', '02', '06', '29', '02.06.02.06.29', 'Gambar Presidenxgmringxwakil Presiden', '2021-08-06 04:24:43'),
('5a0d4456efb7f0f88f285d6b491d88d9', '02', '06', '02', '06', '30', '02.06.02.06.30', 'Lambang KoprixgmringxDharma Wanita', '2021-08-06 04:24:43'),
('e7fccf55006be338929921b254a187b0', '02', '06', '02', '06', '31', '02.06.02.06.31', 'Aquarium', '2021-08-06 04:24:43'),
('f3e5b1211654958ceea77d3c03f6ca05', '02', '06', '02', '06', '32', '02.06.02.06.32', 'Tiang Bendera', '2021-08-06 04:24:43'),
('b77a5a97b6b1542d5a80964a680f2d8d', '02', '06', '02', '06', '33', '02.06.02.06.33', 'Petaka', '2021-08-06 04:24:43'),
('518b0a6cb34735fd1d732d909f967d59', '02', '06', '02', '06', '34', '02.06.02.06.34', 'Lift', '2021-08-06 04:24:43'),
('a1c5c5c24fdd17303149d29395172315', '02', '06', '02', '06', '35', '02.06.02.06.35', 'Seterika', '2021-08-06 04:24:43'),
('1fec60a3f72672873650b035cf0c8622', '02', '06', '02', '06', '36', '02.06.02.06.36', 'Water Filter', '2021-08-06 04:24:43'),
('2af08517b820c131daee37daf793e9c0', '02', '06', '02', '06', '37', '02.06.02.06.37', 'Tangga Alumunium', '2021-08-06 04:24:43'),
('a1f1edfccf397f84ff8fbe3303a22cca', '02', '06', '02', '06', '38', '02.06.02.06.38', 'Kaca Hias', '2021-08-06 04:24:43'),
('a2c8b12f132217112c50a3985794ee0f', '02', '06', '02', '06', '39', '02.06.02.06.39', 'Dispencer', '2021-08-06 04:24:43'),
('dd83252e4db49eb07c36c1e619bc60fc', '02', '06', '02', '06', '40', '02.06.02.06.40', 'MimbarxgmringxPodium', '2021-08-06 04:24:43'),
('c4834175a580371694ea0068f5579f9a', '02', '06', '02', '06', '41', '02.06.02.06.41', 'Gucci', '2021-08-06 04:24:43'),
('7a497f5684d408e7c43bd3f4a03f3070', '02', '06', '02', '06', '42', '02.06.02.06.42', 'Tangga Hidrolik', '2021-08-06 04:24:43'),
('069c055735cea4cfd8462dd6c5a271cf', '02', '06', '02', '06', '43', '02.06.02.06.43', 'Palu Sidang', '2021-08-06 04:24:43'),
('6b9318890c2d40849ab0798c3af7cdb0', '02', '06', '02', '06', '44', '02.06.02.06.44', 'Mesin Pengering Pakaian', '2021-08-06 04:24:43'),
('b359d2de8e36100bca0aac9448e5422e', '02', '06', '02', '06', '45', '02.06.02.06.45', 'Lambang Instansi', '2021-08-06 04:24:43'),
('d4ad91384156b81671b0ad3b375c2579', '02', '06', '02', '06', '46', '02.06.02.06.46', 'LoncengxgmringxGenta', '2021-08-06 04:24:43'),
('42fd74e264aa45a8b90253289646e760', '02', '06', '02', '06', '47', '02.06.02.06.47', 'Mesin Pemotong Keramik', '2021-08-06 04:24:43'),
('2bf471556464315d49d4cf7c37e4dfca', '02', '06', '02', '06', '48', '02.06.02.06.48', 'Coofie Maker', '2021-08-06 04:24:43'),
('6117cf8b018280f16add20f357127edb', '02', '06', '02', '06', '49', '02.06.02.06.49', 'Handy Cam', '2021-08-06 04:24:43'),
('186b4e010d79de3b7bb474676c9dc834', '02', '06', '02', '06', '50', '02.06.02.06.50', 'lainxstrixlain', '2021-08-06 04:24:43'),
('09b77bd0985ea278dba4a8f17084c417', '02', '06', '02', '07', '*', '02.06.02.07.*', 'Alat Pemadam Kebakaran', '2021-08-06 04:24:43'),
('e385bf0cd17ddf61f5f5642ffa3a5f1a', '02', '06', '02', '07', '01', '02.06.02.07.01', 'Alat PemadamxgmringxPortable', '2021-08-06 04:24:43'),
('3f2a356d3f8766c3b5b058f2e75c4bd0', '02', '06', '02', '07', '02', '02.06.02.07.02', 'Pompa KebakaranxgmringxPortable', '2021-08-06 04:24:43'),
('6df7a7b2859c3ab63ad65e730bf1d449', '02', '06', '02', '07', '03', '02.06.02.07.03', 'Generator Busa', '2021-08-06 04:24:43'),
('a3bb8c2cc100b78c4e8ce0efef23a7b6', '02', '06', '02', '07', '04', '02.06.02.07.04', 'Detektor Kebakaran', '2021-08-06 04:24:43'),
('92d67e6adfd6434b2fcfacce77298975', '02', '06', '02', '07', '05', '02.06.02.07.05', 'Alat Splinker Otomatis', '2021-08-06 04:24:43'),
('de1c412898a4ef608bb97460157515c2', '02', '06', '02', '07', '06', '02.06.02.07.06', 'Panel Pengontrol Kebakaran', '2021-08-06 04:24:43'),
('f3c1f124c4cb0c38232df46a27a7df9d', '02', '06', '02', '07', '07', '02.06.02.07.07', 'Tombol KebakaranxgmringxAlarm', '2021-08-06 04:24:43'),
('d967b282f31e539493d3ab83576c6be8', '02', '06', '02', '07', '08', '02.06.02.07.08', 'Hidran Kebakaran', '2021-08-06 04:24:43'),
('4a1c154d0f63931cd81c9217fac39787', '02', '06', '02', '07', '09', '02.06.02.07.09', 'Pipa Pemancar', '2021-08-06 04:24:43'),
('8a2916ae36c5fd19502b663193cc6fc0', '02', '06', '02', '07', '10', '02.06.02.07.10', 'Pakaian PanasxgmringxLengkap', '2021-08-06 04:24:43'),
('086ba0f15adf2815a8538dac1b033c1b', '02', '06', '02', '07', '11', '02.06.02.07.11', 'Topeng xkkurixMaskerxkkurnanx Oxigen', '2021-08-06 04:24:43'),
('86daba15daeeb5bda8b41770240fd1e9', '02', '06', '02', '07', '12', '02.06.02.07.12', 'Topeng xkkurixMaskerxkkurnanx Gas', '2021-08-06 04:24:43'),
('be96f0e1671596e2dfa2c776654e9a1f', '02', '06', '02', '07', '13', '02.06.02.07.13', 'Alat Peluncur', '2021-08-06 04:24:43'),
('ea17838b5b80e43d1c94c9ee9e1854fb', '02', '06', '02', '07', '14', '02.06.02.07.14', 'Lemari Slang', '2021-08-06 04:24:43'),
('9f308c47c1505fa77a24a95da131fe6d', '02', '06', '02', '07', '15', '02.06.02.07.15', 'Lonceng Kebakaran', '2021-08-06 04:24:43'),
('605cc614e7f2a5db3b14d04a8a6c1c0e', '02', '06', '02', '07', '16', '02.06.02.07.16', 'Alat Pembantu Pemadam Kebakaran', '2021-08-06 04:24:43'),
('aa466fbb636a1057176ef6ad9c81da69', '02', '06', '02', '07', '17', '02.06.02.07.17', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('9505bb273a8b46e88a221771fe5096b2', '02', '06', '03', '*', '*', '02.06.03.*.*', 'KOMPUTER', '2021-08-06 04:24:43'),
('0f160516db1ca2a9e82fc37440f040b4', '02', '06', '03', '01', '*', '02.06.03.01.*', 'Komputer UnitxgmringxJaringan', '2021-08-06 04:24:43'),
('c268f9b4b746ebb70343de4170a39302', '02', '06', '03', '01', '01', '02.06.03.01.01', 'Mainframe', '2021-08-06 04:24:43'),
('2ceda05ec9f4cc58f135df155f5769a3', '02', '06', '03', '01', '02', '02.06.03.01.02', 'Mini Komputer', '2021-08-06 04:24:43'),
('3bdc1e219a8e9c57b8ed0919d4948a38', '02', '06', '03', '01', '03', '02.06.03.01.03', 'Local Area Network xkkurixLANxkkurnanx', '2021-08-06 04:24:43'),
('dec6f3ac10f0ad23679f663a54d90563', '02', '06', '03', '01', '04', '02.06.03.01.04', 'Internet', '2021-08-06 04:24:43'),
('cfaabae30e40e552b727b6f187ffaea4', '02', '06', '03', '01', '05', '02.06.03.01.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('37f0edc5e9bbab1296823f545eb86294', '02', '06', '03', '02', '*', '02.06.03.02.*', 'Personal Komputer', '2021-08-06 04:24:43'),
('1a01b0bc83f62c767df4085f8e83f5fd', '02', '06', '03', '02', '01', '02.06.03.02.01', 'P.C. Unit', '2021-08-06 04:24:43'),
('bd168c62e3027e6b9494cee97e2e38b5', '02', '06', '03', '02', '02', '02.06.03.02.02', 'Lap Top', '2021-08-06 04:24:43'),
('a5f563b57d0ec6d50523aed2435f46e0', '02', '06', '03', '02', '03', '02.06.03.02.03', 'Note Book', '2021-08-06 04:24:43'),
('60f601cb99e0724cfd2f256097c55d4f', '02', '06', '03', '02', '04', '02.06.03.02.04', 'Palm Top', '2021-08-06 04:24:43'),
('1578cd29e14ada9a734a4905c2574445', '02', '06', '03', '02', '05', '02.06.03.02.05', 'lainxstrixlain', '2021-08-06 04:24:43'),
('c3e7d77bba0c42019604e272b2f62560', '02', '06', '03', '03', '*', '02.06.03.03.*', 'Peralatan Komputer Mainframe', '2021-08-06 04:24:43'),
('25a26f2fc2f5162b2cb398282ff883aa', '02', '06', '03', '03', '01', '02.06.03.03.01', 'Card Reader', '2021-08-06 04:24:43'),
('d9d528518c44af88c6ed9bffb47a3318', '02', '06', '03', '03', '02', '02.06.03.03.02', 'Magnetic Tape Unit', '2021-08-06 04:24:43'),
('a36c27077692f932243ec76599ab5fcf', '02', '06', '03', '03', '03', '02.06.03.03.03', 'Floppy Disk Unit', '2021-08-06 04:24:43'),
('4626551dcb4c4f8a8705305dd2d62b83', '02', '06', '03', '03', '04', '02.06.03.03.04', 'Storage Modul Disk', '2021-08-06 04:24:43'),
('c66bb9ec5966d7365830691a5de096c3', '02', '06', '03', '03', '05', '02.06.03.03.05', 'Console Unit', '2021-08-06 04:24:43'),
('33acb7653eb298a3038bf9b6a1ae4c97', '02', '06', '03', '03', '06', '02.06.03.03.06', 'CPU', '2021-08-06 04:24:43'),
('80b770a7b00099aaf438c7ce14a61335', '02', '06', '03', '03', '07', '02.06.03.03.07', 'Disk Parck', '2021-08-06 04:24:43'),
('cb87572f1fc36315cd64685cf08f9f71', '02', '06', '03', '03', '08', '02.06.03.03.08', 'Hard Copy Console', '2021-08-06 04:24:43'),
('ece7f800d15327ac6dbcdf6185105287', '02', '06', '03', '03', '09', '02.06.03.03.09', 'Serial Pointer', '2021-08-06 04:24:43'),
('5bca6d4aa5f134064546229755595842', '02', '06', '03', '03', '10', '02.06.03.03.10', 'Line Printer', '2021-08-06 04:24:43'),
('2c1816df059511ca7a86ec13eeb1066e', '02', '06', '03', '03', '11', '02.06.03.03.11', 'Ploter', '2021-08-06 04:24:43'),
('e2343be64a39f63a6dbf44b8b9bd9dc6', '02', '06', '03', '03', '12', '02.06.03.03.12', 'Hard Disk', '2021-08-06 04:24:43'),
('8c093d8e1db70a82dc81460f88346298', '02', '06', '03', '03', '13', '02.06.03.03.13', 'Keyboard', '2021-08-06 04:24:43'),
('0da5dd053de6ffd9c39c3dff63ef4ddc', '02', '06', '03', '03', '14', '02.06.03.03.14', 'lainxstrixlain', '2021-08-06 04:24:43'),
('8f7707e6188b9e839be2d39c5d26493c', '02', '06', '03', '04', '*', '02.06.03.04.*', 'Peralatan Mini Komputer', '2021-08-06 04:24:43'),
('adfa6f82f346b6514672234cc03ce627', '02', '06', '03', '04', '01', '02.06.03.04.01', 'Card Reader', '2021-08-06 04:24:43'),
('500b2f85c598c8b31c75c208eda565de', '02', '06', '03', '04', '02', '02.06.03.04.02', 'Magnetic Tape Unit', '2021-08-06 04:24:43'),
('7e1fa3277d3f405fc5f56dacd09d917f', '02', '06', '03', '04', '03', '02.06.03.04.03', 'Flopp Disk Unit', '2021-08-06 04:24:43'),
('6116a0721bd31daed657b484e75ed502', '02', '06', '03', '04', '04', '02.06.03.04.04', 'Storage Modul Disk', '2021-08-06 04:24:43'),
('a1b7b3efbc68113fba29337d3cb72cf8', '02', '06', '03', '04', '05', '02.06.03.04.05', 'Console Unit', '2021-08-06 04:24:43'),
('512769e695cd05e9ee6eceac749ed6e8', '02', '06', '03', '04', '06', '02.06.03.04.06', 'CPU', '2021-08-06 04:24:43'),
('dd83f29363bcdf91e7c3da8b6247676d', '02', '06', '03', '04', '07', '02.06.03.04.07', 'Disk Pack', '2021-08-06 04:24:43'),
('b92c4307c9680fe53c84d038c03fa41b', '02', '06', '03', '04', '08', '02.06.03.04.08', 'Printer', '2021-08-06 04:24:43'),
('1a0bcfe664314f0f3ba6e95d05f8e22e', '02', '06', '03', '04', '09', '02.06.03.04.09', 'Plotter', '2021-08-06 04:24:43'),
('40131b9a0320cc48ef15ddb6654896c9', '02', '06', '03', '04', '10', '02.06.03.04.10', 'Scanner', '2021-08-06 04:24:43'),
('0b2982eeda82e9d398cf172855d01061', '02', '06', '03', '04', '11', '02.06.03.04.11', 'Computer Compatible', '2021-08-06 04:24:43'),
('04bc557e9defb1499550a75fe3ac267e', '02', '06', '03', '04', '12', '02.06.03.04.12', 'Viewer', '2021-08-06 04:24:43'),
('cd17534609799d1fb917874f33cacabb', '02', '06', '03', '04', '13', '02.06.03.04.13', 'Digitzer', '2021-08-06 04:24:43'),
('3d37fa4c1eff745af85c058f284d9b24', '02', '06', '03', '04', '14', '02.06.03.04.14', 'Keyboard', '2021-08-06 04:24:43'),
('977f9d7a53b3882e4b63017ec86fc701', '02', '06', '03', '04', '15', '02.06.03.04.15', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('374658437aa4e646db79b97f5beeeffe', '02', '06', '03', '05', '*', '02.06.03.05.*', 'Peralatan Personal Komputer', '2021-08-06 04:24:43'),
('ba4795ad1ef364834d3004b08e79fdd0', '02', '06', '03', '05', '01', '02.06.03.05.01', 'CPU', '2021-08-06 04:24:43'),
('e62a89ff2ceb9a7ba8f201070a2c129b', '02', '06', '03', '05', '02', '02.06.03.05.02', 'Monitor', '2021-08-06 04:24:43'),
('110651acfa726b8dcab0de7e4db7762a', '02', '06', '03', '05', '03', '02.06.03.05.03', 'Printer', '2021-08-06 04:24:43'),
('dc611cbb1e24849c4a370f2ff59d4730', '02', '06', '03', '05', '04', '02.06.03.05.04', 'Scanner', '2021-08-06 04:24:43'),
('438a23183e12c552da49b0fe71a72f24', '02', '06', '03', '05', '05', '02.06.03.05.05', 'Plotter', '2021-08-06 04:24:43'),
('87b7e129565e48751d60cc62163aac52', '02', '06', '03', '05', '06', '02.06.03.05.06', 'Viewer', '2021-08-06 04:24:43'),
('650a3a822a64b7b1cfaee8c3f36b934c', '02', '06', '03', '05', '07', '02.06.03.05.07', 'Extermal', '2021-08-06 04:24:43'),
('5d655d51397b77567f2116d87ead5f45', '02', '06', '03', '05', '08', '02.06.03.05.08', 'Digitzer', '2021-08-06 04:24:43'),
('962cbdf92b0bb5d33822c70b8fc29512', '02', '06', '03', '05', '09', '02.06.03.05.09', 'Keyboard', '2021-08-06 04:24:43'),
('1e5e2a8009692b546b7967263fe168e9', '02', '06', '03', '05', '10', '02.06.03.05.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('4939a7463a54012f29e03bbadeb93d78', '02', '06', '03', '06', '*', '02.06.03.06.*', 'Peralatan Jaringan', '2021-08-06 04:24:43'),
('94477d97a6a865828f26557fe14c4674', '02', '06', '03', '06', '01', '02.06.03.06.01', 'Server', '2021-08-06 04:24:43'),
('2e3078c11d5fc73ac5817d16e93bac89', '02', '06', '03', '06', '02', '02.06.03.06.02', 'Router', '2021-08-06 04:24:43'),
('ef444b8c83be29b38fd21d927d15bf05', '02', '06', '03', '06', '03', '02.06.03.06.03', 'Hub', '2021-08-06 04:24:43'),
('01ae5ca8db969da198aae18a8fdbb490', '02', '06', '03', '06', '04', '02.06.03.06.04', 'Modem', '2021-08-06 04:24:43'),
('c5d7566e9f8755923bffc33dfb7910f7', '02', '06', '03', '06', '05', '02.06.03.06.05', 'Netware Interface External', '2021-08-06 04:24:43'),
('7ac7634d17d675ccde4567b2cd6fa640', '02', '06', '03', '06', '06', '02.06.03.06.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('7421ed5b6a97f08a6d5b98b6bc76806b', '02', '06', '04', '*', '*', '02.06.04.*.*', 'MEJA DAN KURSI KERJAxgmringxRAPAT PEJABAT', '2021-08-06 04:24:43'),
('9eebd0651d574d037f362ceea4d85113', '02', '06', '04', '01', '*', '02.06.04.01.*', 'Meja Kerja Pejabat', '2021-08-06 04:24:43'),
('6155d7a97a945dd2d501da88936008b9', '02', '06', '04', '01', '01', '02.06.04.01.01', 'Meja Kerja MenterixgmringxGubernurxgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('98a5381b155adbba2ce21ad3e6b329a7', '02', '06', '04', '01', '02', '02.06.04.01.02', 'Meja Kerja Pejabat Eselon IxgmringxWakil GubernurxgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('8b486d38b4a7830a95e8fae581d2fd14', '02', '06', '04', '01', '03', '02.06.04.01.03', 'Meja Kerja KetuaxgmringxWakil Ketua DPRD', '2021-08-06 04:24:43'),
('5c840863823ece217bd8cfbfd5ed5748', '02', '06', '04', '01', '04', '02.06.04.01.04', 'Meja Kerja Pejabat Eselon II', '2021-08-06 04:24:43'),
('db84a38c23546a2c0c7999e4af37360a', '02', '06', '04', '01', '05', '02.06.04.01.05', 'Meja Kerja Pejabat Eselon III', '2021-08-06 04:24:43'),
('d4f7faa50724c9ff63e7e0a6f08322fc', '02', '06', '04', '01', '06', '02.06.04.01.06', 'Meja Kerja Pejabat Eselon IV', '2021-08-06 04:24:43'),
('fdb46cc4ba7b96e41415c8ce8951c447', '02', '06', '04', '01', '07', '02.06.04.01.07', 'Meja Kerja Pejabat Eselon V', '2021-08-06 04:24:43'),
('68277286ad7bd6bbcf0cdb42ee8b7296', '02', '06', '04', '01', '08', '02.06.04.01.08', 'Meja Kerja Pegawai Non Struktural', '2021-08-06 04:24:43'),
('f53dadc57a77b9c37db10cc697680990', '02', '06', '04', '01', '09', '02.06.04.01.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('a45745cc59f57474995cac5b7bf47c52', '02', '06', '04', '02', '*', '02.06.04.02.*', 'Meja Rapat Pejabat', '2021-08-06 04:24:43'),
('59120882f6108e3b98ec3df375641e88', '02', '06', '04', '02', '01', '02.06.04.02.01', 'Meja Rapat MenterixgmringxGubernurxgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('5f2df88d50b1d253ac65f2bab06db192', '02', '06', '04', '02', '02', '02.06.04.02.02', 'Meja Rapat Pejabat Eselon IxgmringxWakil GubernurxgmringxBup.xgmringxWalikota', '2021-08-06 04:24:43'),
('3c1433647b559dae7e093bcfbc7ec291', '02', '06', '04', '02', '03', '02.06.04.02.03', 'Meja Rapat KetuaxgmringxWakil Ketua DPRD', '2021-08-06 04:24:43'),
('c84a5ed0fcb98e0796b11e51209ce922', '02', '06', '04', '02', '04', '02.06.04.02.04', 'Meja Rapat Pejabat Eselon II', '2021-08-06 04:24:43'),
('3eb121784ac0fa586a596a569ada83dd', '02', '06', '04', '02', '05', '02.06.04.02.05', 'Meja Rapat Pejabat Eselon III', '2021-08-06 04:24:43'),
('f0219a0c84984ecc9d1d67f7d8d91190', '02', '06', '04', '02', '06', '02.06.04.02.06', 'Meja Tamu Ruangan Tunggu MenterixgmringxGub.xgmringxBup.xgmringxWalikota', '2021-08-06 04:24:43'),
('30c1f278d6b94d55aa24f999c7f3d750', '02', '06', '04', '02', '07', '02.06.04.02.07', 'Meja Tamu Ruangan Tunggu Pejabat Eselon IxgmringxWakil Gub.xgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('01bd64627cfd08e6555265bdb56f0178', '02', '06', '04', '02', '08', '02.06.04.02.08', 'Meja Tamu Ruangan Tunggu KetuaxgmringxWakil DPRD', '2021-08-06 04:24:43'),
('96190a29c7b0851d6f13ae6310217164', '02', '06', '04', '02', '09', '02.06.04.02.09', 'Meja Tamu Ruangan Tunggu Pejabat Eselon II', '2021-08-06 04:24:43'),
('36dea11355815e4d9322578d78f80348', '02', '06', '04', '02', '10', '02.06.04.02.10', 'Mena Tamu Ruangan Tunggu Pejabat Eselon III', '2021-08-06 04:24:43'),
('b17d7db99ed961555ea959b491cad558', '02', '06', '04', '02', '11', '02.06.04.02.11', 'Meja Tamu Biasa', '2021-08-06 04:24:43'),
('0b7b614674f4b3da5f9c531912d5977e', '02', '06', '04', '02', '12', '02.06.04.02.12', 'Meja MaketxgmringxPeta', '2021-08-06 04:24:43'),
('3b1edc06d7741c13c5f0d04d7e1c92f4', '02', '06', '04', '02', '13', '02.06.04.02.13', 'Meja Operator', '2021-08-06 04:24:43'),
('04c3ba6f60f42f2ccc42ff6d5fa4c76e', '02', '06', '04', '02', '14', '02.06.04.02.14', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('bd0e1169ba6558fa08d4f2bf655a064e', '02', '06', '04', '03', '*', '02.06.04.03.*', 'Kursi Kerja Pejabat', '2021-08-06 04:24:43'),
('d451c5dc136ab70751561e05172bac62', '02', '06', '04', '03', '01', '02.06.04.03.01', 'Kursi Kerja MenterixgmringxGubernurxgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('fc727cf13d76fb702fd7ff1bf0046e32', '02', '06', '04', '03', '02', '02.06.04.03.02', 'Kursi Kerja Pejabat Eselon IxgmringxWakil GubernurxgmringxBup.xgmringxWalikota', '2021-08-06 04:24:43'),
('360ff6f02ab9fa6cfdf2e5d0261a5d7e', '02', '06', '04', '03', '03', '02.06.04.03.03', 'Kursi Kerja KetruaxgmringxWakil DPRD', '2021-08-06 04:24:43'),
('fa489e04f956c32d19c2157992102f18', '02', '06', '04', '03', '04', '02.06.04.03.04', 'Kursi Kerja Pejabat Eselon II', '2021-08-06 04:24:43'),
('540fb6410ea1db4df1efd736bad1b28f', '02', '06', '04', '03', '05', '02.06.04.03.05', 'Kursi Kerja Pejabat Eselon III', '2021-08-06 04:24:43'),
('6a0e44e889f8c085d43ffee3efdcb874', '02', '06', '04', '03', '06', '02.06.04.03.06', 'Kursi Kerja Pejabat Eselon IV', '2021-08-06 04:24:43'),
('97fd497a29d14ef0fad925b28fec9813', '02', '06', '04', '03', '07', '02.06.04.03.07', 'Kursi Kerja Pejabat Eselon IV', '2021-08-06 04:24:43'),
('b70a6a6e002607c9adabf000b24f1534', '02', '06', '04', '03', '08', '02.06.04.03.08', 'Kursi Kerja Pegawai Non Struktural', '2021-08-06 04:24:43'),
('ad9b3abede51eb4072cd036c3ba71f20', '02', '06', '04', '03', '09', '02.06.04.03.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('90c84446c32aa4caf9a54888a9c9ec67', '02', '06', '04', '04', '*', '02.06.04.04.*', 'Kursi Rapat Pejabat', '2021-08-06 04:24:43'),
('b58c853e63be7b56a8f745836b08cc88', '02', '06', '04', '04', '01', '02.06.04.04.01', 'Kursi Rapat MenterixgmringxGubernurxgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('dd6c4dcaadc2c6fcd2882d8e418468ad', '02', '06', '04', '04', '02', '02.06.04.04.02', 'Kursi Rapat Ruangan Rapat Pejabat Eselon IxgmringxWakil Gub.xgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('e509b5a8fd784ea9b6b660dd40e0fb70', '02', '06', '04', '04', '03', '02.06.04.04.03', 'Kursi Rapat Ruangan Rapat KetuaxgmringxWakil DPRD', '2021-08-06 04:24:43'),
('d69056765f6a86a7150fd1805d418d57', '02', '06', '04', '04', '04', '02.06.04.04.04', 'Kursi Rapat Ruangan Rapat Pejabat Eselon II', '2021-08-06 04:24:43'),
('b944025a184d99d5c91eefe7391e47e1', '02', '06', '04', '04', '05', '02.06.04.04.05', 'Kursi Rapat Ruangan Rapat Pejabat Eselon III', '2021-08-06 04:24:43'),
('fb92cbb57199184df31f9e92c63f209b', '02', '06', '04', '04', '06', '02.06.04.04.06', 'Kursi Rapat  Ruangan Data', '2021-08-06 04:24:43'),
('4c533c21c16fe5275bd3da4c1be1c7d1', '02', '06', '04', '04', '07', '02.06.04.04.07', 'Kursi Rapat Ruangan Rapat Staf', '2021-08-06 04:24:43'),
('be93abe6bdebbd10e6e872f57c4f0cb1', '02', '06', '04', '04', '08', '02.06.04.04.08', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3c098ad87223008944e43ce348f9110e', '02', '06', '04', '05', '*', '02.06.04.05.*', 'Kursi Hadap Depan Meja Kerja Pejabat', '2021-08-06 04:24:43'),
('56889902f425b72920f0a18876ed3c2f', '02', '06', '04', '05', '01', '02.06.04.05.01', 'Kursi Hadap Depab Meja Kerja MenterixgmringxGubxgmringxBupxgmringxWalikota', '2021-08-06 04:24:43'),
('3b2a94002f5b7573b80f0ac0e3bc4d46', '02', '06', '04', '05', '02', '02.06.04.05.02', 'Kursi Hadap Depan Meja Kerja Pejabat Eselon IxgmringxWkl GubxgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('74ed28615aa7fbb1e77012539be15cb6', '02', '06', '04', '05', '03', '02.06.04.05.03', 'Kursi Hadap Depan Meja Kerja KetuaxgmringxWakil DPRD', '2021-08-06 04:24:43'),
('158d2ececec3c0212c6d0f730540f2b3', '02', '06', '04', '05', '04', '02.06.04.05.04', 'Kursi Hadap Depan Meja Kerja Pejabat Eselon II', '2021-08-06 04:24:43'),
('f7112a2e5391e09bf67e49069fc626fe', '02', '06', '04', '05', '05', '02.06.04.05.05', 'Kursi Hadap Depan Meja Kerja Pejabat Eselon III', '2021-08-06 04:24:43'),
('a8519772369d628995b550cd072958d4', '02', '06', '04', '05', '06', '02.06.04.05.06', 'Kursi Hadap Depan Meja Kerja Pejabat Eselon IV', '2021-08-06 04:24:43'),
('38935db9c46183a0592984910c3007c2', '02', '06', '04', '05', '07', '02.06.04.05.07', 'Kursi Hadap Depan Meja Kerja Pejabat Eselon V', '2021-08-06 04:24:43'),
('b4f7b7e2d09a0f5bd76a20c65d691dec', '02', '06', '04', '05', '08', '02.06.04.05.08', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('ff26294f597d81702655efab1e645a6a', '02', '06', '04', '06', '*', '02.06.04.06.*', 'Kursi Tamu di Ruangan Pejabat', '2021-08-06 04:24:43'),
('d3ba0e2afd7a509390e69b2627273404', '02', '06', '04', '06', '01', '02.06.04.06.01', 'Kursi Tamu di Ruangan MenterixgmringxGubernurxgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('435b1d81f1c87a6cfeda1b4afb049cc7', '02', '06', '04', '06', '02', '02.06.04.06.02', 'Kursi Tamu di Ruangan Pejabat Ess. IxgmringxWkl GubxgmringxBupxgmringxWalikota', '2021-08-06 04:24:43'),
('6d88be2e8ac7df66e5a9b6ee3583201b', '02', '06', '04', '06', '03', '02.06.04.06.03', 'Kursi Tamu di Ruangan KetuaxgmringxWakil Ketua DPRD', '2021-08-06 04:24:43'),
('a1222ba8b5fb23f05a1045e182a9555b', '02', '06', '04', '06', '04', '02.06.04.06.04', 'Kursi Tamu di Ruangan Pejabat Eselon II', '2021-08-06 04:24:43'),
('538521e7e25210ab767f52cf0e663986', '02', '06', '04', '06', '05', '02.06.04.06.05', 'Kursi Tamu di Ruangan Pejabat Eselon III', '2021-08-06 04:24:43'),
('038b93b09399e7c5b1381623a32bf169', '02', '06', '04', '06', '06', '02.06.04.06.06', 'Kursi Tamu di Ruangan Tunggu MenterixgmringxGubxgmringxBupxgmringxWalikota', '2021-08-06 04:24:43'),
('1f439399774c389689ad3e703cbfd9b7', '02', '06', '04', '06', '07', '02.06.04.06.07', 'Kursi Tamu di Ruangan Tunggu Pejabat Eselon I', '2021-08-06 04:24:43'),
('08ecb2488dbcafefa66c42cbda0afaf1', '02', '06', '04', '06', '08', '02.06.04.06.08', 'Kursi Tamu di Ruangan Tunggu Pejabat Eselon II', '2021-08-06 04:24:43'),
('9cfa089f2660d9bd3606921b3fb964ab', '02', '06', '04', '06', '09', '02.06.04.06.09', 'Kursi Tamu di Depan Ajudan MenterixgmringxGubernurxgmringxBupxgmringxWalikota', '2021-08-06 04:24:43'),
('e3d86c3a271942b90194f05c07b89db3', '02', '06', '04', '06', '10', '02.06.04.06.10', 'Kursi Tamu di Depan Ajudan Pej. Eselon IxgmringxWkl GubxgmringxBupxgmringxWalikota', '2021-08-06 04:24:43'),
('71d9b28c61f6c519ad1e330250bbd285', '02', '06', '04', '06', '11', '02.06.04.06.11', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('462ed9ce27f8d08dfe919a186dccd7bf', '02', '06', '04', '07', '*', '02.06.04.07.*', 'Lemari dan Arsip Pejabat', '2021-08-06 04:24:43'),
('0bd14b8bb317309ccb7d803c3ac605d8', '02', '06', '04', '07', '01', '02.06.04.07.01', 'Lemari Buku untuk MenterixgmringxGubernurxgmringxBupatixgmringxWalikota', '2021-08-06 04:24:43'),
('29b0dac3456eaf3469b96e4b66e6cf7f', '02', '06', '04', '07', '02', '02.06.04.07.02', 'Lemari Buku untuk Pejabat Eselon I xgmringxWkl GubxgmringxBupxgmringxWalikota', '2021-08-06 04:24:43'),
('b8aaace17b964d908da01564339c9def', '02', '06', '04', '07', '03', '02.06.04.07.03', 'Lemari Buku untuk Pejabat Eselon II', '2021-08-06 04:24:43'),
('7754544156918f4a413932e6aa670c99', '02', '06', '04', '07', '04', '02.06.04.07.04', 'Lemari Buku untuk Pejabat Eselon III', '2021-08-06 04:24:43'),
('e1429f02e9ac5c983ce63cf0d4377bb3', '02', '06', '04', '07', '05', '02.06.04.07.05', 'Lemari Buku untuk untuk Perpustakaan', '2021-08-06 04:24:43'),
('597b5d582de444deb0ee20cb4a62ab59', '02', '06', '04', '07', '06', '02.06.04.07.06', 'Lemari Arsip untuk arsip Dinamis', '2021-08-06 04:24:43'),
('ca0b4cfe0680af8c346e56f12824547a', '02', '06', '04', '07', '07', '02.06.04.07.07', 'Buffet Kayu', '2021-08-06 04:24:43'),
('ced804366a70b855ff523159a5e253db', '02', '06', '04', '07', '08', '02.06.04.07.08', 'Buffet Kaca', '2021-08-06 04:24:43'),
('bb19385aa96afc28099a6ce2c9fb9805', '02', '06', '04', '07', '09', '02.06.04.07.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('d5781d552e943e82758ffba582c39f8c', '02', '07', '*', '*', '*', '02.07.*.*.*', 'ALAT STUDIO DAN ALAT KOMUNIKASI', '2021-08-06 04:24:43'),
('7b1654e1b7faeca7e628ee8a9069960b', '02', '07', '01', '*', '*', '02.07.01.*.*', 'ALAT STUDIO', '2021-08-06 04:24:43'),
('c45e0f45b188f23b16d5d0272cbd30ee', '02', '07', '01', '01', '*', '02.07.01.01.*', 'Peralatan Studio Visual', '2021-08-06 04:24:43'),
('588bf3364982d189d20139a3573f4172', '02', '07', '01', '01', '01', '02.07.01.01.01', 'Camera +Attachmen', '2021-08-06 04:24:43'),
('3db3ca27a87eceafe0febd4461898e37', '02', '07', '01', '01', '02', '02.07.01.01.02', 'Photo Processing Set', '2021-08-06 04:24:43'),
('c9b908fed8d055f6f4e589897c046fcc', '02', '07', '01', '01', '03', '02.07.01.01.03', 'Proyektor +Attachment', '2021-08-06 04:24:43'),
('5e9722245a3c66ee18af8484ddd5a23b', '02', '07', '01', '01', '04', '02.07.01.01.04', 'Mikro Film', '2021-08-06 04:24:43'),
('11b1978bad1f8a0c1fe2ff570f15673a', '02', '07', '01', '01', '05', '02.07.01.01.05', 'Audio Mbding Console', '2021-08-06 04:24:43'),
('cfe303845dd9899dc0a22f506f938f52', '02', '07', '01', '01', '06', '02.07.01.01.06', 'Audio Mbding Portable', '2021-08-06 04:24:43'),
('b85dcb0ad1e96fcce051b374a614bc7f', '02', '07', '01', '01', '07', '02.07.01.01.07', 'Audio Mbding Stationer', '2021-08-06 04:24:43'),
('3add3f2ece52f8610484fea3d9bf5ba4', '02', '07', '01', '01', '08', '02.07.01.01.08', 'Audio Attenuator', '2021-08-06 04:24:43'),
('d26ec17413a28c9cc68fe23f6f26c386', '02', '07', '01', '01', '09', '02.07.01.01.09', 'Audio Amplifier', '2021-08-06 04:24:43'),
('21ff709bd650b074eb79ac4c06dbf116', '02', '07', '01', '01', '10', '02.07.01.01.10', 'Audio Erase Unit', '2021-08-06 04:24:43'),
('74f0a85b33d1e4b2fed18ad8c772a997', '02', '07', '01', '01', '11', '02.07.01.01.11', 'Audio Vidio Selector', '2021-08-06 04:24:43'),
('eaea76b30e89a09777d3ad373005bde3', '02', '07', '01', '01', '12', '02.07.01.01.12', 'Audio Monitor Active', '2021-08-06 04:24:43'),
('40b62bdd5d2c2ca2706f4832d29a3fd8', '02', '07', '01', '01', '13', '02.07.01.01.13', 'Audio Monitor Passive', '2021-08-06 04:24:43'),
('274d4dc5613b301e7a11ca34a58d5a17', '02', '07', '01', '01', '14', '02.07.01.01.14', 'Audio Reverberation', '2021-08-06 04:24:43'),
('d225ca8bff904b480fdd8367ab3be4d7', '02', '07', '01', '01', '15', '02.07.01.01.15', 'Audio Patch Panel', '2021-08-06 04:24:43'),
('c2e2d089f11a0b8e71f9338b8f73d17b', '02', '07', '01', '01', '16', '02.07.01.01.16', 'Audio Distribution', '2021-08-06 04:24:43'),
('57912cadd22bbec4ff532498295dd599', '02', '07', '01', '01', '17', '02.07.01.01.17', 'Audio Tone Generator', '2021-08-06 04:24:43'),
('99132eaff2361f2cbbb0e0966d817126', '02', '07', '01', '01', '18', '02.07.01.01.18', 'Audio Catridge Recorder', '2021-08-06 04:24:43'),
('a0d38e04069aaea945884a7959fd134f', '02', '07', '01', '01', '19', '02.07.01.01.19', 'Audio Logging Recorder', '2021-08-06 04:24:43'),
('f6bfc2c7cf44572b29e7200ac1b001a4', '02', '07', '01', '01', '20', '02.07.01.01.20', 'Compact Disc Player', '2021-08-06 04:24:43'),
('a4bbb779e368d7e887c99e84b1c3b384', '02', '07', '01', '01', '21', '02.07.01.01.21', 'Cassette Duplicator', '2021-08-06 04:24:43'),
('125253205c45c665e66f1b8373a2e881', '02', '07', '01', '01', '22', '02.07.01.01.22', 'Disc Record Player', '2021-08-06 04:24:43');
INSERT INTO `m_kib_kode` (`kd`, `golongan`, `bidang`, `kelompok`, `kelompok_sub`, `kelompok_sub_sub`, `kode`, `nama`, `postdate`) VALUES
('75b9627b80274e3a93fb49b7d8963382', '02', '07', '01', '01', '23', '02.07.01.01.23', 'Multitrack Recorder', '2021-08-06 04:24:43'),
('c2a8eadcdec79258f721081b12d83b3c', '02', '07', '01', '01', '24', '02.07.01.01.24', 'Reel Tape Duplicator', '2021-08-06 04:24:43'),
('9e7781551547ba6bb06f512f880c226f', '02', '07', '01', '01', '25', '02.07.01.01.25', 'Compact Disc Juke Box System', '2021-08-06 04:24:43'),
('93ec23bb618a07cc7c6157d672ed67f6', '02', '07', '01', '01', '26', '02.07.01.01.26', 'Telephone Hybrid', '2021-08-06 04:24:43'),
('c099bccaa4da2524452c06d3ace4f829', '02', '07', '01', '01', '27', '02.07.01.01.27', 'Audio Phone In', '2021-08-06 04:24:43'),
('d6cb1e4ff81f70898c6dbeec247bf83e', '02', '07', '01', '01', '28', '02.07.01.01.28', 'Porfanity Delay System', '2021-08-06 04:24:43'),
('37f373e2917f6d7afc360eca78f197da', '02', '07', '01', '01', '29', '02.07.01.01.29', 'Equalizer', '2021-08-06 04:24:43'),
('e483c9e38e1c67ccd6dae95be1d9c60c', '02', '07', '01', '01', '30', '02.07.01.01.30', 'Audio Filter', '2021-08-06 04:24:43'),
('75fe0577b81c003035bea76eadf3dc06', '02', '07', '01', '01', '31', '02.07.01.01.31', 'Audio Limiter', '2021-08-06 04:24:43'),
('f166501940151a3df862802c352a12c0', '02', '07', '01', '01', '32', '02.07.01.01.32', 'Audio Compresor', '2021-08-06 04:24:43'),
('495681355acf17503d6a3bfd6165c347', '02', '07', '01', '01', '33', '02.07.01.01.33', 'Tum Table', '2021-08-06 04:24:43'),
('229b4e6ccd5bb64371d8bfd6f0a45e5c', '02', '07', '01', '01', '34', '02.07.01.01.34', 'Talk Back Unit', '2021-08-06 04:24:43'),
('f52fe275b6e7efe7c18a68be9f650464', '02', '07', '01', '01', '35', '02.07.01.01.35', 'Intercom Unit', '2021-08-06 04:24:43'),
('153d6faed44d1db707b19e6b6f3d463b', '02', '07', '01', '01', '36', '02.07.01.01.36', 'Buzzer', '2021-08-06 04:24:43'),
('b5073752bf582a1d065d5e183844b256', '02', '07', '01', '01', '37', '02.07.01.01.37', 'Set Studio Light Signal', '2021-08-06 04:24:43'),
('7cc7bca1be6d43677b82516808f9c97a', '02', '07', '01', '01', '38', '02.07.01.01.38', 'Dolby Nois Reduction', '2021-08-06 04:24:43'),
('3f4b8ed865530143000ee492f6bc4590', '02', '07', '01', '01', '39', '02.07.01.01.39', 'Headphone', '2021-08-06 04:24:43'),
('c4c4ffd3e38050ed8effaecec86f8e28', '02', '07', '01', '01', '40', '02.07.01.01.40', 'MicrophonexgmringxWireless Mic', '2021-08-06 04:24:43'),
('fc54b819134c37343d444db8b87599f2', '02', '07', '01', '01', '41', '02.07.01.01.41', 'MicrophonxgmringxBoom Stand', '2021-08-06 04:24:43'),
('e12864f9853ce9efa38af60a26250eec', '02', '07', '01', '01', '42', '02.07.01.01.42', 'Microphone Connector Box', '2021-08-06 04:24:43'),
('a2bd579c340128ee30fc86a455d0369a', '02', '07', '01', '01', '43', '02.07.01.01.43', 'Microphone Floor Stand', '2021-08-06 04:24:43'),
('0a40810e0f96c44b4ef31b6a2ded5d44', '02', '07', '01', '01', '44', '02.07.01.01.44', 'Power Supply Microphone', '2021-08-06 04:24:43'),
('a13144bac16fa6ca46d1cff995565736', '02', '07', '01', '01', '45', '02.07.01.01.45', 'Professional Soun System', '2021-08-06 04:24:43'),
('95a1d6c0fb4050c6691e24714bacc7da', '02', '07', '01', '01', '46', '02.07.01.01.46', 'Audio Master Control Unit', '2021-08-06 04:24:43'),
('f77cde4ab87f572bd329a83ae6aad693', '02', '07', '01', '01', '47', '02.07.01.01.47', 'Time Indetification Unit', '2021-08-06 04:24:43'),
('10af83ce5ae67ad7835bfc182cc435aa', '02', '07', '01', '01', '48', '02.07.01.01.48', 'Audio Anouncer Desk', '2021-08-06 04:24:43'),
('b79db57e0b7be3bcf406ec810e5a9ccf', '02', '07', '01', '01', '49', '02.07.01.01.49', 'Master Clock', '2021-08-06 04:24:43'),
('975dc5bf7dd916e225e16d71d5401d96', '02', '07', '01', '01', '50', '02.07.01.01.50', 'Slave Clock', '2021-08-06 04:24:43'),
('0e4ee68dfcac3f0ca6ed23d9abf05cb0', '02', '07', '01', '01', '51', '02.07.01.01.51', 'Audio Command Desk', '2021-08-06 04:24:43'),
('7e15efc91b32e37d7531f508032c52b1', '02', '07', '01', '01', '52', '02.07.01.01.52', 'Unintemuptible Power Supply xkkurixUPSxkkurnanx', '2021-08-06 04:24:43'),
('cef265bd14622f2fb58ed04e4b346966', '02', '07', '01', '01', '53', '02.07.01.01.53', 'Master Control Desk', '2021-08-06 04:24:43'),
('686880ac1046f48bb6d3c45b87281e0b', '02', '07', '01', '01', '54', '02.07.01.01.54', 'Head Compensator', '2021-08-06 04:24:43'),
('cedadb984a97a0e1a68e321530941b3d', '02', '07', '01', '01', '55', '02.07.01.01.55', 'Autometic Voltage Regulator xkkurixAVRxkkurnanx', '2021-08-06 04:24:43'),
('ddf5df1530d23045e5b662250953b584', '02', '07', '01', '01', '56', '02.07.01.01.56', 'Audio Vidio Selector', '2021-08-06 04:24:43'),
('788489ca228dc5b4653488f8a02c303b', '02', '07', '01', '01', '57', '02.07.01.01.57', 'HumxgmringxCable Conpensator', '2021-08-06 04:24:43'),
('2a91f7166de740b10fbf83a0e5b619bc', '02', '07', '01', '01', '58', '02.07.01.01.58', 'Editing &ampxkommaxampxkommax Dubbing System', '2021-08-06 04:24:43'),
('57a4b2c3926d5d5d0d9c1e790480a6aa', '02', '07', '01', '01', '59', '02.07.01.01.59', 'Analog Delay', '2021-08-06 04:24:43'),
('67990fd750108bb22e413327661f6ea6', '02', '07', '01', '01', '60', '02.07.01.01.60', 'Battery Charger', '2021-08-06 04:24:43'),
('c7ae53553785cc7990dfe305f2e0a035', '02', '07', '01', '01', '61', '02.07.01.01.61', 'Blank Panel', '2021-08-06 04:24:43'),
('41a841f0d7dd7c701ad41c3844dffd5b', '02', '07', '01', '01', '62', '02.07.01.01.62', 'Control Unit HF', '2021-08-06 04:24:43'),
('6f24643f28beeb01997f337419a3ef32', '02', '07', '01', '01', '63', '02.07.01.01.63', 'Delay Unit', '2021-08-06 04:24:43'),
('880c210e31a82855610787c0761f2644', '02', '07', '01', '01', '64', '02.07.01.01.64', 'Power Amplifier', '2021-08-06 04:24:43'),
('083695353c190ba63ea0ee952fd62b72', '02', '07', '01', '01', '65', '02.07.01.01.65', 'Paging Mic', '2021-08-06 04:24:43'),
('8833c5adf39429209959788c39e77945', '02', '07', '01', '01', '66', '02.07.01.01.66', 'Compact Monitor Panel For Streo', '2021-08-06 04:24:43'),
('136a2224f4311e0bd0c679b7b7a2de51', '02', '07', '01', '01', '67', '02.07.01.01.67', 'Pistol Grip', '2021-08-06 04:24:43'),
('89d9083c0a5ef97e7b79a95da2606c1f', '02', '07', '01', '01', '68', '02.07.01.01.68', 'Mounting Breaken', '2021-08-06 04:24:43'),
('9c82b85c3c84b1e8f502bf8c9f29631c', '02', '07', '01', '01', '69', '02.07.01.01.69', 'ChairmanxgmringxAudio Comference', '2021-08-06 04:24:43'),
('3b5d3e509fda051948ac59a55fd1dcb9', '02', '07', '01', '01', '70', '02.07.01.01.70', 'Time Switching', '2021-08-06 04:24:43'),
('8167b58e09793b43fa569b7ca353b32a', '02', '07', '01', '01', '71', '02.07.01.01.71', 'Terminal Board', '2021-08-06 04:24:43'),
('783f2b9beb7790facddafa6500997980', '02', '07', '01', '01', '72', '02.07.01.01.72', 'EnconderxgmringxDecoder', '2021-08-06 04:24:43'),
('07ca7da3aefa8f83de1b5eff1b9b54ff', '02', '07', '01', '01', '73', '02.07.01.01.73', 'Wind Shild', '2021-08-06 04:24:43'),
('0fd7a63b0375d81d60a471f53e0c3295', '02', '07', '01', '01', '74', '02.07.01.01.74', 'Recalver HFxgmringxLF', '2021-08-06 04:24:43'),
('5da25f2fa116a268510b6d423540ccff', '02', '07', '01', '01', '75', '02.07.01.01.75', 'Recalver VHFxgmringxFM', '2021-08-06 04:24:43'),
('83758c9aca196fca60f3c732cc10ccd7', '02', '07', '01', '01', '76', '02.07.01.01.76', 'Audio Tape Reel Recorder', '2021-08-06 04:24:43'),
('ac9cbd4833b8a4cf37b157a669c2eac5', '02', '07', '01', '01', '77', '02.07.01.01.77', 'Audio Cassette Recorder', '2021-08-06 04:24:43'),
('e1f4c822790376f18fe5ad125334943c', '02', '07', '01', '01', '78', '02.07.01.01.78', 'Compact Disc Recorder', '2021-08-06 04:24:43'),
('85aafb252c855d5df144eb5eef562157', '02', '07', '01', '01', '79', '02.07.01.01.79', 'Digital Audio Storage System', '2021-08-06 04:24:43'),
('aaa45f4b2e9f335019a5bd44d71364fa', '02', '07', '01', '01', '80', '02.07.01.01.80', 'Digital Audio Taperecorder', '2021-08-06 04:24:43'),
('f05db4f9b570ea3691cf694503e69fa4', '02', '07', '01', '01', '81', '02.07.01.01.81', 'Microphone Table Stand', '2021-08-06 04:24:43'),
('19565e2bccfdf55ca96f2e1ce2bd6051', '02', '07', '01', '01', '82', '02.07.01.01.82', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b67606cdb6cfaf30f5989abb157499eb', '02', '07', '01', '02', '*', '02.07.01.02. *', 'Peralatan Studio Video dan Film', '2021-08-06 04:24:43'),
('6d9f6cc53b962e95d2befcac6e6d127b', '02', '07', '01', '02', '01', '02.07.01.02.01', 'Assigmment Switcher', '2021-08-06 04:24:43'),
('d7f39df652e58959ca7448bf67e0b960', '02', '07', '01', '02', '02', '02.07.01.02.02', 'Off Air TV Monitor', '2021-08-06 04:24:43'),
('ab34a600f4d05a3d21c34e35ef807a55', '02', '07', '01', '02', '03', '02.07.01.02.03', 'Camera Electonic', '2021-08-06 04:24:43'),
('015dccf98ce2869a3a4fe75cb9d7ed37', '02', '07', '01', '02', '04', '02.07.01.02.04', 'Pulse Generator', '2021-08-06 04:24:43'),
('22391a728bdf720c98bbc3c76f1199d7', '02', '07', '01', '02', '05', '02.07.01.02.05', 'Pulse Distributor Amplifier', '2021-08-06 04:24:43'),
('5fae695625811c4454fa9019c6b4821e', '02', '07', '01', '02', '06', '02.07.01.02.06', 'Pulse Awitdher', '2021-08-06 04:24:43'),
('fa9240a3437a6d4a1ef61cf453859d6f', '02', '07', '01', '02', '07', '02.07.01.02.07', 'Pulse Delay Line', '2021-08-06 04:24:43'),
('7b3bfc68cebc3784310115ebfd72f37a', '02', '07', '01', '02', '08', '02.07.01.02.08', 'Character Generator', '2021-08-06 04:24:43'),
('29b7f60c0f7a45d21abe007e2974a2ff', '02', '07', '01', '02', '09', '02.07.01.02.09', 'Caption Generator', '2021-08-06 04:24:43'),
('2498aa1b897b8d111bebde79e48476e7', '02', '07', '01', '02', '10', '02.07.01.02.10', 'Caption Generator', '2021-08-06 04:24:43'),
('c9aafcea4e3d4f21f3226f55971d700c', '02', '07', '01', '02', '11', '02.07.01.02.11', 'Telecine', '2021-08-06 04:24:43'),
('93cfacbfbbe293b468b7d5dbbd1f0097', '02', '07', '01', '02', '44', '02.07.01.02.44', 'Stabilizing Amplifier', '2021-08-06 04:24:43'),
('f91a185cf90d0efbbc0fa4acbfd1b995', '02', '07', '01', '02', '45', '02.07.01.02.45', 'Digital', '2021-08-06 04:24:43'),
('29d447b85345d0f8032874c6949066e9', '02', '07', '01', '02', '46', '02.07.01.02.46', 'Video Effect', '2021-08-06 04:24:43'),
('2abaecc7f60b9192f20ab0433bb7235f', '02', '07', '01', '02', '47', '02.07.01.02.47', 'Tripod Camera', '2021-08-06 04:24:43'),
('e78ef619eb8f7fbb088bcc5777115544', '02', '07', '01', '02', '48', '02.07.01.02.48', 'Dimmer', '2021-08-06 04:24:43'),
('3a1004e4582c06dceea936c8e754d501', '02', '07', '01', '02', '49', '02.07.01.02.49', 'Chiller', '2021-08-06 04:24:43'),
('1b131fd65b74aea9eb1a84f3fa868b7f', '02', '07', '01', '02', '50', '02.07.01.02.50', 'Slave Clock', '2021-08-06 04:24:43'),
('a91915c7f95b30194fa22dd5fe380b7f', '02', '07', '01', '02', '51', '02.07.01.02.51', 'Master Clock', '2021-08-06 04:24:43'),
('e71e82e317e6ab5783ec0f51233a6b72', '02', '07', '01', '02', '52', '02.07.01.02.52', 'Teledyne', '2021-08-06 04:24:43'),
('46812d9998a76c400fd59328c3e4a109', '02', '07', '01', '02', '53', '02.07.01.02.53', 'Slying Spot Scanner', '2021-08-06 04:24:43'),
('7a989381163f038ba41aad16077df5e8', '02', '07', '01', '02', '54', '02.07.01.02.54', 'Synchronizing Pulse Generator', '2021-08-06 04:24:43'),
('db41b730b014ac5d498ce335ca960779', '02', '07', '01', '02', '55', '02.07.01.02.55', 'DC Corverter', '2021-08-06 04:24:43'),
('9f1d8d0092038ef074c38ce889e9967e', '02', '07', '01', '02', '56', '02.07.01.02.56', 'Black Burst Generator', '2021-08-06 04:24:43'),
('26c455e1d60e44c19050674771dac914', '02', '07', '01', '02', '57', '02.07.01.02.57', 'Ligjting Stand Tripod', '2021-08-06 04:24:43'),
('a4522b6f4b7bc821bcd1e81126ee00e9', '02', '07', '01', '02', '58', '02.07.01.02.58', 'Film Projector', '2021-08-06 04:24:43'),
('f5bc556c79874298e078b34b2fd3f664', '02', '07', '01', '02', '59', '02.07.01.02.59', 'Silde Projector', '2021-08-06 04:24:43'),
('6cdfc72dd5cd81208ca7812ba40ba653', '02', '07', '01', '02', '60', '02.07.01.02.60', 'Command Desk', '2021-08-06 04:24:43'),
('3e0b69f618dd22eb867a37908216013c', '02', '07', '01', '02', '61', '02.07.01.02.61', 'Announcer Desk', '2021-08-06 04:24:43'),
('c4257e6027a837d7440e9e6140698c8d', '02', '07', '01', '02', '62', '02.07.01.02.62', 'Camera Film', '2021-08-06 04:24:43'),
('8d3ff8c742a66d7a25b503dcbed13b55', '02', '07', '01', '02', '63', '02.07.01.02.63', 'Lensa kamera', '2021-08-06 04:24:43'),
('f9b78c92e292450eec389dec2e89a5dc', '02', '07', '01', '02', '64', '02.07.01.02.64', 'Film Magazine', '2021-08-06 04:24:43'),
('d2ce7c20349d864001e23ed0b6b7bbe0', '02', '07', '01', '02', '65', '02.07.01.02.65', 'Claver', '2021-08-06 04:24:43'),
('077c8f569361d3ad9816c356a78a1470', '02', '07', '01', '02', '66', '02.07.01.02.66', 'Changing Bag', '2021-08-06 04:24:43'),
('5eea63a7a879db817404230b6a27c3c1', '02', '07', '01', '02', '67', '02.07.01.02.67', 'Conditioner', '2021-08-06 04:24:43'),
('40b3cee615ed912966b9838148550a38', '02', '07', '01', '02', '68', '02.07.01.02.68', 'Color Film Anaiyzer', '2021-08-06 04:24:43'),
('314b4d0990e1a64cdcc16666bac99c73', '02', '07', '01', '02', '69', '02.07.01.02.69', 'Printer', '2021-08-06 04:24:43'),
('4db9076295aa14c2634a8f01f742aa6d', '02', '07', '01', '02', '70', '02.07.01.02.70', 'Film Sound Recorder', '2021-08-06 04:24:43'),
('1e25ab200a32e0f11700df0a5e932fa1', '02', '07', '01', '02', '71', '02.07.01.02.71', 'Tele Recorder', '2021-08-06 04:24:43'),
('0c8b1cf8a2397e9d0ff3cf708ad293ad', '02', '07', '01', '02', '72', '02.07.01.02.72', 'Camera View Finder', '2021-08-06 04:24:43'),
('a316b13a693f855336a876626cdd1dd5', '02', '07', '01', '02', '73', '02.07.01.02.73', 'Servo Zoom Lens', '2021-08-06 04:24:43'),
('e752c62b0816635645913976c8f1b9ff', '02', '07', '01', '02', '74', '02.07.01.02.74', 'Camera Adaptor', '2021-08-06 04:24:43'),
('2966becf7045e8b30899f13b37af7545', '02', '07', '01', '02', '75', '02.07.01.02.75', 'Photo Processing Set', '2021-08-06 04:24:43'),
('52c5446643e977cc41347e071a368466', '02', '07', '01', '02', '76', '02.07.01.02.76', 'Micro Film', '2021-08-06 04:24:43'),
('ed0adecd0f0e928164ef8ab08340f34f', '02', '07', '01', '02', '77', '02.07.01.02.77', 'Mixer PVC', '2021-08-06 04:24:43'),
('5b9c8f0818ec65d7092665e9e5d82298', '02', '07', '01', '02', '99', '02.07.01.02.99', 'Video Audio Jack Panel', '2021-08-06 04:24:43'),
('1512b25d8bfe47980bae1ff54bf81652', '02', '07', '01', '02', '00', '02.07.01.02.00', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('a8c47a420cc1d107c8b846567f9c06bf', '02', '07', '02', '*', '*', '02.07.02.*.*', 'ALAT KOMUNIKASI', '2021-08-06 04:24:43'),
('604bae8b5613e0d625cf3d8ccc91894b', '02', '07', '02', '01', '*', '02.07.02.01.*', 'Alat Komunikasi Telephone', '2021-08-06 04:24:43'),
('a5f0a2faca97c0b5c0e098631fb42fd0', '02', '07', '02', '01', '01', '02.07.02.01.01', 'Unit TranscarverxgmringxTransmiter UHF', '2021-08-06 04:24:43'),
('e009c89053e5ecc2cc65fc106de2876b', '02', '07', '02', '01', '02', '02.07.02.01.02', 'Unit TranscarverxgmringxTransmiter VHF', '2021-08-06 04:24:43'),
('ab8b93542bfba75040c537394cb5305c', '02', '07', '02', '01', '03', '02.07.02.01.03', 'Unit TranscarverxgmringxTransmiter HF', '2021-08-06 04:24:43'),
('9fd38b3da9b540e9cb0efd8c2f5c58bb', '02', '07', '02', '01', '04', '02.07.02.01.04', 'Amplifier', '2021-08-06 04:24:43'),
('bc35c5e6a45ff55df74c8ae090c5ac02', '02', '07', '02', '01', '05', '02.07.02.01.05', 'Microphone', '2021-08-06 04:24:43'),
('15951bdeeefb377aeca7a6031a6a87c5', '02', '07', '02', '01', '06', '02.07.02.01.06', 'Loudspeker', '2021-08-06 04:24:43'),
('a3beb201801e96905c37de7ee0f4975f', '02', '07', '02', '01', '07', '02.07.02.01.07', 'Megaphone', '2021-08-06 04:24:43'),
('3463437af5c31ce45998c1767344a70f', '02', '07', '02', '01', '08', '02.07.02.01.08', 'Sound System', '2021-08-06 04:24:43'),
('df288a8de6fe9aaf96ecf9d79327d8c1', '02', '07', '02', '01', '09', '02.07.02.01.09', 'Telephone xkkurixPABXxkkurnanx', '2021-08-06 04:24:43'),
('089f8fe2ecbd037fbefd7d7f3eaf0d83', '02', '07', '02', '01', '10', '02.07.02.01.10', 'Intermadiate TelephonexgmringxKey Telephone', '2021-08-06 04:24:43'),
('99e6f5eeb16e27ae73f285c1cb71ea69', '02', '07', '02', '01', '11', '02.07.02.01.11', 'Pesawat Telephone', '2021-08-06 04:24:43'),
('8ab93d3b1c19d3e6492b1df01acaf896', '02', '07', '02', '01', '12', '02.07.02.01.12', 'Telephone Mobile', '2021-08-06 04:24:43'),
('e5d7419b6782509a7f6bad631e8b4574', '02', '07', '02', '01', '13', '02.07.02.01.13', 'Pager', '2021-08-06 04:24:43'),
('f6b1833f99779c063aa1fa0574e1a75b', '02', '07', '02', '01', '14', '02.07.02.01.14', 'Handy Talky xkkurixHTxkkurnanx', '2021-08-06 04:24:43'),
('cfc5a54d88b74c6cf498e0359589dfc4', '02', '07', '02', '01', '15', '02.07.02.01.15', 'Telex', '2021-08-06 04:24:43'),
('cde7e5923483abda1f7c526609086b04', '02', '07', '02', '01', '16', '02.07.02.01.16', 'Intercom', '2021-08-06 04:24:43'),
('693645d685b84dcc3ca0d6bc7ef5b55e', '02', '07', '02', '01', '17', '02.07.02.01.17', 'Talk Back', '2021-08-06 04:24:43'),
('64dcf872c96d625a0204c3414d441e05', '02', '07', '02', '01', '18', '02.07.02.01.18', 'Selective Colling', '2021-08-06 04:24:43'),
('112a27a3b44359736991dc512bdfcdbe', '02', '07', '02', '01', '19', '02.07.02.01.19', 'Peralatan Spech Plas', '2021-08-06 04:24:43'),
('62a2dfc64bc72f394131a0fde38ca582', '02', '07', '02', '01', '20', '02.07.02.01.20', 'Facsimile', '2021-08-06 04:24:43'),
('7b67130e509cf09ed682a8b4cb824b94', '02', '07', '02', '01', '21', '02.07.02.01.21', 'Handphone', '2021-08-06 04:24:43'),
('bb457a02722c4a69934ca1d8b89cba95', '02', '07', '02', '01', '22', '02.07.02.01.22', 'Local Battery Telephone', '2021-08-06 04:24:43'),
('ce69da27fd9b2dba614c7bab52ce87c0', '02', '07', '02', '01', '23', '02.07.02.01.23', 'Hand Phone', '2021-08-06 04:24:43'),
('71ebe228438bb118c2bd82d150e1befb', '02', '07', '02', '01', '24', '02.07.02.01.24', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('6cb9c7d46d82abd970e927a94c98c3b8', '02', '07', '02', '06', '*', '02.07.02.06.*', 'Alat Komunikasi Sosial', '2021-08-06 04:24:43'),
('65556b7843cc329eaf3ecbcfea0605e5', '02', '07', '02', '06', '01', '02.07.02.06.01', 'Publik Address xkkurixLapanganxkkurnanx', '2021-08-06 04:24:43'),
('59731fbb54722add3a02913e49b8ec4d', '02', '07', '02', '06', '02', '02.07.02.06.02', 'Wireless Amplifier', '2021-08-06 04:24:43'),
('79deb7792d976141beb7f829c93b6d76', '02', '07', '02', '06', '03', '02.07.02.06.03', 'Slide Projector xkkurixLapanganxkkurnanx', '2021-08-06 04:24:43'),
('21f90b937145adc130d7f3acb8460cb9', '02', '07', '02', '06', '04', '02.07.02.06.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('ecfdd0269937b70a3849d440b79a7bed', '02', '07', '02', '07', '*', '02.07.02.07.*', 'Alatxstrixalat Sandi', '2021-08-06 04:24:43'),
('125928e475e0061d0f9804e487394504', '02', '07', '02', '07', '01', '02.07.02.07.01', 'Morse Keyer', '2021-08-06 04:24:43'),
('36b223274a2127bbe3ad43a44dbfa5df', '02', '07', '02', '07', '02', '02.07.02.07.02', 'Automatic Morse Keyer', '2021-08-06 04:24:43'),
('c0392dd7b9f8ed44480e82f41942e3f5', '02', '07', '02', '07', '03', '02.07.02.07.03', 'Alat Semboyan', '2021-08-06 04:24:43'),
('9562a9f843be2ab714b0ac700659a926', '02', '07', '02', '07', '04', '02.07.02.07.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('87ab841fe39b21b907ba93e1f730b5c6', '02', '07', '03', '*', '*', '02.07.03.*.*', 'PERALATAN PEMANCAR', '2021-08-06 04:24:43'),
('b5e9cc51c900f7706bf9526c4a163ad7', '02', '09', '02', '*', '*', '02.09.02.*.*', 'ALAT PERAGA xgmringx PRAKTEK SEKOLAH', '2021-08-06 04:24:43'),
('1f5478ee00882ce9cf294a62eb0e3c04', '02', '09', '02', '01', '*', '02.09.02.01.*', 'bidang Studi : Bahasa Indonesia', '2021-08-06 04:24:43'),
('4eb483d653b3bd357ccf611e5884c2a3', '02', '09', '02', '01', '01', '02.09.02.01.01', 'Kit Bahasa A', '2021-08-06 04:24:43'),
('f932486dde7defdbe42a3026a3d8b089', '02', '09', '02', '01', '02', '02.09.02.01.02', 'Papan Panel', '2021-08-06 04:24:43'),
('a489a3ebee0d08b04ad1ed30d3d9ab0e', '02', '09', '02', '01', '03', '02.09.02.01.03', 'Kit SAS individual', '2021-08-06 04:24:43'),
('0ea86bfdd175d07ff18abe096826b2db', '02', '09', '02', '01', '04', '02.09.02.01.04', 'Kotak Alatxstrixalat Peraga Metode SAS', '2021-08-06 04:24:43'),
('5c16b056abbf564b74e76cce51ddebb4', '02', '09', '02', '01', '05', '02.09.02.01.05', 'Gambar Total', '2021-08-06 04:24:43'),
('4587cd0764b6ee3558107c57acd57ca4', '02', '09', '02', '01', '06', '02.09.02.01.06', 'Gambar Analitik', '2021-08-06 04:24:43'),
('781b60f4df548dacb962ad78b470236a', '02', '09', '02', '01', '07', '02.09.02.01.07', 'Kotak Bahasa Untuk Kartu Kalimat Huruf Cetak dan Kartu', '2021-08-06 04:24:43'),
('0ff67d6969421e65bee29bd0b906b37d', '02', '09', '02', '01', '08', '02.09.02.01.08', 'Kartu Kalimat Huruf Cetak', '2021-08-06 04:24:43'),
('5e579bcea16fda29ef0f182b351f4610', '02', '09', '02', '01', '09', '02.09.02.01.09', 'Kartu Kalimat Dengan Huruf Cetak', '2021-08-06 04:24:43'),
('66d84d363743fda886b5993ae80f5d9b', '02', '09', '02', '01', '10', '02.09.02.01.10', 'Kotak Bahasa Untuk Kartu Kata Dan Kartu', '2021-08-06 04:24:43'),
('dc3a9f238f3b6a0f680081f924d3d2c3', '02', '09', '02', '01', '11', '02.09.02.01.11', 'Kartu kata dengan Huruf Cetak', '2021-08-06 04:24:43'),
('6c5f1d85f1d22154f441b4aa6d37ec04', '02', '09', '02', '01', '12', '02.09.02.01.12', 'Kartu Suku Kata Dengan Huruf Cetak', '2021-08-06 04:24:43'),
('4c5ed1ff7397333753dceccb70c69e8c', '02', '09', '02', '01', '13', '02.09.02.01.13', 'Kartu Huruf Degan Huruf Cetak', '2021-08-06 04:24:43'),
('a419ab439cbf0bbc3432edb185fc5653', '02', '09', '02', '01', '14', '02.09.02.01.14', 'Papan Alfabet', '2021-08-06 04:24:43'),
('fc18ec4a7658cf13c6f89b5e4a9e2449', '02', '09', '02', '01', '15', '02.09.02.01.15', 'Kain Panel', '2021-08-06 04:24:43'),
('89d66785f97fe83c04fb58082570eab3', '02', '09', '02', '01', '16', '02.09.02.01.16', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('c85762847e4246bb0840edc398b8b188', '02', '09', '02', '02', '*', '02.09.02.02.*', 'Bidang Studi : Matematika', '2021-08-06 04:24:43'),
('7193230f2378305ec8a663c9514a230b', '02', '09', '02', '02', '01', '02.09.02.02.01', 'Kit Matematika', '2021-08-06 04:24:43'),
('0a364319b02a7aa2241c94e0bd5fff18', '02', '09', '02', '02', '02', '02.09.02.02.02', 'Roda Motor', '2021-08-06 04:24:43'),
('ac51f6d68ac84684860a841bd4a769ce', '02', '09', '02', '02', '03', '02.09.02.02.03', 'Banpaku', '2021-08-06 04:24:43'),
('f6c396defac012d99d157b9ae9bfbbb3', '02', '09', '02', '02', '04', '02.09.02.02.04', 'Muka Jam', '2021-08-06 04:24:43'),
('a6ea8a46691ad9a6c17f4f5804fcb3e2', '02', '09', '02', '02', '05', '02.09.02.02.05', 'Rak Bilangan Dua Ruang', '2021-08-06 04:24:43'),
('d39bccb8ab814506e52a85f096281a06', '02', '09', '02', '02', '06', '02.09.02.02.06', 'Rak Bilangan Tiga Ruang', '2021-08-06 04:24:43'),
('ecb893289f6da7e8ad3e828f42980501', '02', '09', '02', '02', '07', '02.09.02.02.07', 'Papan Planel', '2021-08-06 04:24:43'),
('1cde440243685e70fe84fe70755835a0', '02', '09', '02', '02', '08', '02.09.02.02.08', 'Papan Bergerak', '2021-08-06 04:24:43'),
('09f357912b8a72e71a4cfaa0d791d272', '02', '09', '02', '02', '09', '02.09.02.02.09', 'Rak Bilangan Tiga Ruang', '2021-08-06 04:24:43'),
('8762a4c607a129ac655f5c77bd3e08bf', '02', '09', '02', '02', '10', '02.09.02.02.10', 'Papan Berpaku', '2021-08-06 04:24:43'),
('c30d0159085a6facec2fc119f93af2f7', '02', '09', '02', '02', '11', '02.09.02.02.11', 'Papan Pasak', '2021-08-06 04:24:43'),
('78dd484a7d7d34d4a071a5dbfa6cd8f9', '02', '09', '02', '02', '12', '02.09.02.02.12', 'Kubus Untuk Bilangan Berbaris Sepuluh', '2021-08-06 04:24:43'),
('6fc7de120b0e4d3c2fcb01a943822787', '02', '09', '02', '02', '13', '02.09.02.02.13', 'Abakus untuk Bilangan Berbaris', '2021-08-06 04:24:43'),
('8e53b2589b84df0d320cbcc2662b512d', '02', '09', '02', '02', '14', '02.09.02.02.14', 'Pengukur Luas', '2021-08-06 04:24:43'),
('06500bbfc2488d8d8139376852f87dfb', '02', '09', '02', '02', '15', '02.09.02.02.15', 'Blok Untuk Bilangan', '2021-08-06 04:24:43'),
('bb47c16acc857801cc152b4597e2ce36', '02', '09', '02', '02', '16', '02.09.02.02.16', 'Bata', '2021-08-06 04:24:43'),
('123396bd8b1bc5e26392a246e63d68cb', '02', '09', '02', '02', '17', '02.09.02.02.17', 'Mistar Geser C', '2021-08-06 04:24:43'),
('8a539cd3b4450b800419c44e97d7ac79', '02', '09', '02', '02', '18', '02.09.02.02.18', 'Bangunxstrixbangun Ruang', '2021-08-06 04:24:43'),
('b21c8d806538d2bb8b2d2d1af4eb0743', '02', '09', '02', '02', '19', '02.09.02.02.19', 'Pola Bangun Ruang', '2021-08-06 04:24:43'),
('9385914eb5038f8b6573dc46a96b3bb2', '02', '09', '02', '02', '20', '02.09.02.02.20', 'Kerangka Benda Ruang', '2021-08-06 04:24:43'),
('1721e9a03bdb72ff7a7138241b738455', '02', '09', '02', '02', '21', '02.09.02.02.21', 'Aritmatika Jam', '2021-08-06 04:24:43'),
('d4960e2dfa5a9b1555af8414d0d63fa4', '02', '09', '02', '02', '22', '02.09.02.02.22', 'Garis dan Bagnun Ruang', '2021-08-06 04:24:43'),
('3f37ab856bce616d2adf6c6092282d48', '02', '09', '02', '02', '23', '02.09.02.02.23', 'Pengukur Panjang Kurva', '2021-08-06 04:24:43'),
('5fb005cc37852f9f738849cd1de7f6ba', '02', '09', '02', '02', '24', '02.09.02.02.24', 'Penggaris Plastik', '2021-08-06 04:24:43'),
('db94c32542864f0e8399c72977a2478b', '02', '09', '02', '02', '25', '02.09.02.02.25', 'Pipa Plastik', '2021-08-06 04:24:43'),
('003b0dd1d896e70187857706004b2ac4', '02', '09', '02', '02', '26', '02.09.02.02.26', 'Simetri Cermin', '2021-08-06 04:24:43'),
('fc2e5b0691200e1b0dd95a1a116f420d', '02', '09', '02', '02', '27', '02.09.02.02.27', 'Blok Untuk Bilangan Berbaris', '2021-08-06 04:24:43'),
('7c5044cd70b07cf27657729e41381387', '02', '09', '02', '02', '28', '02.09.02.02.28', 'Blok Simetri Putar', '2021-08-06 04:24:43'),
('cb8f1a62f5de58543e5e4b60d625c66c', '02', '09', '02', '02', '29', '02.09.02.02.29', 'Blok Untuk Bilangan Berbaris dua', '2021-08-06 04:24:43'),
('05b1b4651ce605716532ca4f0df4f4ec', '02', '09', '02', '02', '30', '02.09.02.02.30', 'Blok Untuk Bilangan berbaris lima', '2021-08-06 04:24:43'),
('b6f52ea4d696762567f945a9116bf260', '02', '09', '02', '02', '31', '02.09.02.02.31', 'Bangunan dan Daerah bangun Datar', '2021-08-06 04:24:43'),
('ba8c65886b8d112dd1b6e5e7a945a2f9', '02', '09', '02', '02', '32', '02.09.02.02.32', 'Kubus Satuan', '2021-08-06 04:24:43'),
('f234f344c4283b981754d7b06520bc8b', '02', '09', '02', '02', '33', '02.09.02.02.33', 'Busur Derajat', '2021-08-06 04:24:43'),
('9cc26544a6c8fb147db93007a373ea4f', '02', '09', '02', '02', '34', '02.09.02.02.34', 'Miter Ceser B', '2021-08-06 04:24:43'),
('b71c4c56b4db791f298b31ee664ae6b7', '02', '09', '02', '02', '35', '02.09.02.02.35', 'Penggaris Sikuxstrixsiku', '2021-08-06 04:24:43'),
('f19859112b8b09bc59e6d8919c7a014e', '02', '09', '02', '02', '36', '02.09.02.02.36', 'Jangka', '2021-08-06 04:24:43'),
('899573902c2d8dc9a480f6ad9e6c9f8e', '02', '09', '02', '02', '37', '02.09.02.02.37', 'Coin', '2021-08-06 04:24:43'),
('d0e25c24ade2536412f6e8178975d218', '02', '09', '02', '02', '38', '02.09.02.02.38', 'Dadu Matematika', '2021-08-06 04:24:43'),
('93ddce1deb3c0b6f936ca399b56785e1', '02', '09', '02', '02', '39', '02.09.02.02.39', 'Pusingan', '2021-08-06 04:24:43'),
('1f09ddd7e4609dcb74811b3c5c84c8fb', '02', '09', '02', '02', '40', '02.09.02.02.40', 'Blok Pytheagroras', '2021-08-06 04:24:43'),
('94382f8fff404f943dddc599e344a106', '02', '09', '02', '02', '41', '02.09.02.02.41', 'Blok Logica', '2021-08-06 04:24:43'),
('981f00b2ef8b10fc607105c7060696ec', '02', '09', '02', '02', '42', '02.09.02.02.42', 'Blok Segitiga ABC', '2021-08-06 04:24:43'),
('ac27804cb871678fd3f64f1d48e13e99', '02', '09', '02', '02', '43', '02.09.02.02.43', 'Pengukur Sudut Elevansi', '2021-08-06 04:24:43'),
('af44111612863ca4abd53d7bafb30a5c', '02', '09', '02', '02', '44', '02.09.02.02.44', 'Model Kubus', '2021-08-06 04:24:43'),
('71d7702d7fe549f32ff9c5417ea2b438', '02', '09', '02', '02', '45', '02.09.02.02.45', 'Model Balok', '2021-08-06 04:24:43'),
('655d35f1e0a66d420a906ebee40486e5', '02', '09', '02', '02', '46', '02.09.02.02.46', 'Model Prisma Segitiga Sikuxstrixsiku', '2021-08-06 04:24:43'),
('e92f47d5cd4d8599cd11528fe11950b3', '02', '09', '02', '02', '47', '02.09.02.02.47', 'Model Prima Tegak Segi Tiga', '2021-08-06 04:24:43'),
('7881ff32c4868899d201fb8c91edfb2e', '02', '09', '02', '02', '48', '02.09.02.02.48', 'Bidang Delapan Beraturn', '2021-08-06 04:24:43'),
('5a0187236acf4c165f666679f5df250a', '02', '09', '02', '02', '49', '02.09.02.02.49', 'Model Bidang Dua Belas Beraturan', '2021-08-06 04:24:43'),
('8be36bbd397bcbf6c205d0fd2b119814', '02', '09', '02', '02', '50', '02.09.02.02.50', 'Model Tabung Jaringxstrixjaring', '2021-08-06 04:24:43'),
('172dc07a606c4bedde90ce5082e23dbb', '02', '09', '02', '02', '51', '02.09.02.02.51', 'Model Bola Dan Setengah Bola', '2021-08-06 04:24:43'),
('efd05e5ad110ae83a0af7fd7e3463b24', '02', '09', '02', '02', '52', '02.09.02.02.52', 'Kit Matematika SD', '2021-08-06 04:24:43'),
('cc87957ee82040b3d476d17242142d88', '02', '09', '02', '02', '53', '02.09.02.02.53', 'Rak Bilangan Dua Ruang', '2021-08-06 04:24:43'),
('a14946b89fb99f39a1467e57b3d7f26f', '02', '09', '02', '02', '54', '02.09.02.02.54', 'Papan paku Kecil', '2021-08-06 04:24:43'),
('eb41f787fc7ff5962a1d5522f13a62fa', '02', '09', '02', '02', '55', '02.09.02.02.55', 'Rak Bilangan Tiga Ruang', '2021-08-06 04:24:43'),
('fdfd00b001e21d93a5e8cabc090a504f', '02', '09', '02', '02', '56', '02.09.02.02.56', 'Kartu Gambar', '2021-08-06 04:24:43'),
('d86a3e07c7f35ea65b63bbb1a117fa4a', '02', '09', '02', '02', '57', '02.09.02.02.57', 'Satuan Sosok', '2021-08-06 04:24:43'),
('45f80b4b34d15f0f7db71d687018693f', '02', '09', '02', '02', '58', '02.09.02.02.58', 'Gawang Penghitung', '2021-08-06 04:24:43'),
('86d2decdf55480c039c08c2d8b17cfba', '02', '09', '02', '02', '59', '02.09.02.02.59', 'Gawang Angka', '2021-08-06 04:24:43'),
('cd886eb7ff3eceb7ee2bc8ae60f8564b', '02', '09', '02', '02', '60', '02.09.02.02.60', 'Model Jam Bentuk Dasar', '2021-08-06 04:24:43'),
('0c50cc7fa3d5b37ef52cae4efd50bb77', '02', '09', '02', '02', '61', '02.09.02.02.61', 'Bola Gelinding', '2021-08-06 04:24:43'),
('dbeed85f4be1104bc03127de93553ae9', '02', '09', '02', '02', '62', '02.09.02.02.62', 'Lempar Galang', '2021-08-06 04:24:43'),
('c4894329c8695c07f240cf42fa3e8b35', '02', '09', '02', '02', '63', '02.09.02.02.63', 'Lempar Galang', '2021-08-06 04:24:43'),
('1205e34f6b78d2c77117a59db7725340', '02', '09', '02', '02', '64', '02.09.02.02.64', 'Detakxstrixdetak Tiang', '2021-08-06 04:24:43'),
('2cc8a5794affaafb416eb69d85b3249b', '02', '09', '02', '02', '65', '02.09.02.02.65', 'Mistra Papan Tulis', '2021-08-06 04:24:43'),
('ea82671b61bec67fb0a68f4242d90bb2', '02', '09', '02', '02', '66', '02.09.02.02.66', 'Papan Tulis Berkotak', '2021-08-06 04:24:43'),
('f4a63230a04371c79ca977ea52038362', '02', '09', '02', '02', '67', '02.09.02.02.67', 'Kain Planel', '2021-08-06 04:24:43'),
('c68ae50c96f38edb435387b2043c3466', '02', '09', '02', '02', '68', '02.09.02.02.68', 'Model Bangun Ruang', '2021-08-06 04:24:43'),
('9b4a506805207c9beaf53b9c2932cc5d', '02', '09', '02', '02', '69', '02.09.02.02.69', 'Alat Peraga Matematika', '2021-08-06 04:24:43'),
('c76678cd5dc61db3199ab87230dd8d62', '02', '09', '02', '02', '70', '02.09.02.02.70', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('7828db54f09f09c8fcd3dfb71c141eb2', '02', '09', '02', '03', '*', '02.09.02.03.*', 'Bidang Studi : IPA Dasar', '2021-08-06 04:24:43'),
('99eb15d40ed8316ed5bda0f782cd9ccd', '02', '09', '02', '03', '01', '02.09.02.03.01', 'Kotak Peti Lengkap', '2021-08-06 04:24:43'),
('e2d80160288e8c1aaa770aaf316fd689', '02', '09', '02', '03', '02', '02.09.02.03.02', 'Bingkai Plastik', '2021-08-06 04:24:43'),
('47d015cb027501028e11d92c695c5066', '02', '09', '02', '03', '03', '02.09.02.03.03', 'Baut', '2021-08-06 04:24:43'),
('7096a8be1fd99131a8e8c7a801bf8591', '02', '09', '02', '03', '04', '02.09.02.03.04', 'Pasak', '2021-08-06 04:24:43'),
('42de972cb06c7e9291f3d6541aa34c50', '02', '09', '02', '03', '05', '02.09.02.03.05', 'Pemegang Batera', '2021-08-06 04:24:43'),
('703ada4a7efba1c82ee6043047e13b13', '02', '09', '02', '03', '06', '02.09.02.03.06', 'Sklar', '2021-08-06 04:24:43'),
('161c98caf40a1928358569c2dff8d2b5', '02', '09', '02', '03', '07', '02.09.02.03.07', 'Piting', '2021-08-06 04:24:43'),
('d9a6baebcb7c25f477db65753cebd66a', '02', '09', '02', '03', '08', '02.09.02.03.08', 'Kabel listrik', '2021-08-06 04:24:43'),
('5fff77f6a745472f3dac7b1293aec0b4', '02', '09', '02', '03', '09', '02.09.02.03.09', 'Pembakar Spirtus', '2021-08-06 04:24:43'),
('934af9f211e7a0094e848503351e822d', '02', '09', '02', '03', '10', '02.09.02.03.10', 'Gelas Kimia', '2021-08-06 04:24:43'),
('1bd98a0825e0399ee23560044c41661c', '02', '09', '02', '03', '11', '02.09.02.03.11', 'Pemegang', '2021-08-06 04:24:43'),
('4ba312bc4dc22cbf2e71773442a458a8', '02', '09', '02', '03', '12', '02.09.02.03.12', 'KacaxgmringxSkala', '2021-08-06 04:24:43'),
('c07b2859ce3aa5df34d1c92b1cc9fa90', '02', '09', '02', '03', '13', '02.09.02.03.13', 'Sumbat Karet xkkurixuntuk 01xstrix02xstrix79xstrix0110xkkurnanx', '2021-08-06 04:24:43'),
('51136cb4bd220cd2f2bf729ad16e4f78', '02', '09', '02', '03', '14', '02.09.02.03.14', 'Batang Muai', '2021-08-06 04:24:43'),
('025d1827a6e3f72cdff6c94d51561e52', '02', '09', '02', '03', '15', '02.09.02.03.15', 'Langan Neraca', '2021-08-06 04:24:43'),
('bcb3a8ab129b4aac4cdbce74bef400f8', '02', '09', '02', '03', '16', '02.09.02.03.16', 'Poros Neraca', '2021-08-06 04:24:43'),
('e28afb7c3aff07c58dae179d7c84b35a', '02', '09', '02', '03', '17', '02.09.02.03.17', 'Mangkok Neraca', '2021-08-06 04:24:43'),
('d80c784fa1a978d827a71c34f84c4274', '02', '09', '02', '03', '18', '02.09.02.03.18', 'Jarum Penunjuk', '2021-08-06 04:24:43'),
('438bd75c55d1f4b5a10e7f87e2b1bcf3', '02', '09', '02', '03', '19', '02.09.02.03.19', 'Kubus Plastik', '2021-08-06 04:24:43'),
('3fc3bd34eedbf0bd40b826edcb7b14e3', '02', '09', '02', '03', '20', '02.09.02.03.20', 'Kubus Kayu', '2021-08-06 04:24:43'),
('74bd2eb3558c32123dadc555b73897f9', '02', '09', '02', '03', '21', '02.09.02.03.21', 'Bola Baja', '2021-08-06 04:24:43'),
('5259e10f982c0a5af167499d5dbfa165', '02', '09', '02', '03', '22', '02.09.02.03.22', 'Pipa Intai Bias', '2021-08-06 04:24:43'),
('6453ddb49af71232376f6d1a740c62ae', '02', '09', '02', '03', '23', '02.09.02.03.23', 'Kontrol Tunggal', '2021-08-06 04:24:43'),
('98fd826639d059abda7c209e67108b83', '02', '09', '02', '03', '24', '02.09.02.03.24', 'Kontrol Ganda', '2021-08-06 04:24:43'),
('55d2ae10dd66301e5ca9503e3db130d3', '02', '09', '02', '03', '25', '02.09.02.03.25', 'Pegas', '2021-08-06 04:24:43'),
('5074c40c026928dd97c62349812ec090', '02', '09', '02', '03', '26', '02.09.02.03.26', 'Volume Konstan', '2021-08-06 04:24:43'),
('176e9ee6d9cbd0f7b76ebf55084cb671', '02', '09', '02', '03', '27', '02.09.02.03.27', 'Kawat damai', '2021-08-06 04:24:43'),
('5957f21ba9490a1943cf5fcbed100301', '02', '09', '02', '03', '28', '02.09.02.03.28', 'Turbin Air', '2021-08-06 04:24:43'),
('8644fd6732d3a205c94bc25a4007b64f', '02', '09', '02', '03', '29', '02.09.02.03.29', 'Klem Plastik', '2021-08-06 04:24:43'),
('680f672af9ef5640155e1c6bd507584d', '02', '09', '02', '03', '30', '02.09.02.03.30', 'Tabung Reaksi', '2021-08-06 04:24:43'),
('eda3715e71a7c69c7b8688491e934d01', '02', '09', '02', '03', '31', '02.09.02.03.31', 'Magnet', '2021-08-06 04:24:43'),
('8b30162eae0a51ed76f923dc644c8951', '02', '09', '02', '03', '32', '02.09.02.03.32', 'Sistem Optik', '2021-08-06 04:24:43'),
('7436cc5b460727f24d5b32776111481e', '02', '09', '02', '03', '33', '02.09.02.03.33', 'Isi kelompok Penyimpanan 1', '2021-08-06 04:24:43'),
('750fd7b519137f939f038ed315278e0c', '02', '09', '02', '03', '34', '02.09.02.03.34', 'Isi kelompok Penyimpanan 2', '2021-08-06 04:24:43'),
('2c7249390bac3b1d0c0aeb7fa87bcdee', '02', '09', '02', '03', '35', '02.09.02.03.35', 'Isi kelompok Penyimpanan 3', '2021-08-06 04:24:43'),
('6f9c61a294d53efbb263242ff496b2d8', '02', '09', '02', '03', '36', '02.09.02.03.36', 'Isi kelompok Penyimpanan 4', '2021-08-06 04:24:43'),
('30f3be47f20e283f05f7e7c4a314b0aa', '02', '09', '02', '03', '37', '02.09.02.03.37', 'Isi kelompok Penyimpanan 5', '2021-08-06 04:24:43'),
('6693e5c36a20eaf9d7c5c84653865f33', '02', '09', '02', '03', '38', '02.09.02.03.38', 'Isi kelompok Penyimpanan 6', '2021-08-06 04:24:43'),
('5db5fb5dad32c3e8ae4871075797842c', '02', '09', '02', '03', '39', '02.09.02.03.39', 'Isi kelompok Penyimpanan 7', '2021-08-06 04:24:43'),
('703b6a95cc5d851ec84941dd2dc75908', '02', '09', '02', '03', '40', '02.09.02.03.40', 'Tutup Penyimpanan 21415', '2021-08-06 04:24:43'),
('640301246e07fee6f36cabc2d417b995', '02', '09', '02', '03', '41', '02.09.02.03.41', 'Kelompopk PenyimpananxgmringxLaci 8', '2021-08-06 04:24:43'),
('aa75186e97d114424d20b2e0b67fcd72', '02', '09', '02', '03', '42', '02.09.02.03.42', 'Tutup Penyimpanan 3', '2021-08-06 04:24:43'),
('dcd676478493b7b88843067dc983b527', '02', '09', '02', '03', '43', '02.09.02.03.43', 'Tutup Penyimpanan 7', '2021-08-06 04:24:43'),
('7f0e75dd3bf9217eff6a942e1ed5e2f0', '02', '09', '02', '03', '44', '02.09.02.03.44', 'Pedoman Guru', '2021-08-06 04:24:43'),
('39031d39020210642df802f5aa5b5b05', '02', '09', '02', '03', '45', '02.09.02.03.45', 'Petunjuk Percobaan', '2021-08-06 04:24:43'),
('ab7e8f4183838aaeab1127899feff192', '02', '09', '02', '03', '46', '02.09.02.03.46', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('c7c70a8986319eaa30218ae05a89fbac', '02', '09', '02', '04', '*', '02.09.02.04.*', 'Bidang Studi : IPA Lanjutan', '2021-08-06 04:24:43'),
('188dd2ad99fa4de4e76550b5f50bc0a4', '02', '09', '02', '04', '01', '02.09.02.04.01', 'Kerangka Karet', '2021-08-06 04:24:43'),
('9258138a51aec0ae820519caaf519345', '02', '09', '02', '04', '02', '02.09.02.04.02', 'Model Tenkorang', '2021-08-06 04:24:43'),
('ec6cb16e6c11d126978ecc0379a8f5c7', '02', '09', '02', '04', '03', '02.09.02.04.03', 'Model Lidah', '2021-08-06 04:24:43'),
('ff22ebdaf9d42a965dfe5a5d998e34cb', '02', '09', '02', '04', '04', '02.09.02.04.04', 'Model Torso Wanita', '2021-08-06 04:24:43'),
('dc656254f82ee4009b3fec18c1eea455', '02', '09', '02', '04', '05', '02.09.02.04.05', 'Model Jantung', '2021-08-06 04:24:43'),
('1f26ce4fe43a373f76d6f45f0a8ca4b6', '02', '09', '02', '04', '06', '02.09.02.04.06', 'Model Rahang Gigi', '2021-08-06 04:24:43'),
('5df5e5a219f213cd56d922812aa61477', '02', '09', '02', '04', '07', '02.09.02.04.07', 'Model Pencernaan Makanan', '2021-08-06 04:24:43'),
('5659643fda4964b292223225af6153db', '02', '09', '02', '04', '08', '02.09.02.04.08', 'Model Mata', '2021-08-06 04:24:43'),
('80721fd1e86926444ef1b092febb3e4a', '02', '09', '02', '04', '09', '02.09.02.04.09', 'Model kuda', '2021-08-06 04:24:43'),
('90a02b37dad4ce902c9f9d028bf80257', '02', '09', '02', '04', '10', '02.09.02.04.10', 'Model Lembu', '2021-08-06 04:24:43'),
('563a2670d3ed90ee5e63b9a57e7c9624', '02', '09', '02', '04', '11', '02.09.02.04.11', 'Model Torso Mini', '2021-08-06 04:24:43'),
('59af4f8516ac0e48a1a9b9c3e05fb21c', '02', '09', '02', '04', '12', '02.09.02.04.12', 'Mistar', '2021-08-06 04:24:43'),
('42b9f3d191da1fb6426101782e12aa6b', '02', '09', '02', '04', '13', '02.09.02.04.13', 'Bujur Sangkar 1 Cm2', '2021-08-06 04:24:43'),
('b28415485200a880d6408bc47bb89b95', '02', '09', '02', '04', '14', '02.09.02.04.14', 'Bujur Sangkar 1 Dm2', '2021-08-06 04:24:43'),
('24b5db82001f9f248707db14eb7331d5', '02', '09', '02', '04', '15', '02.09.02.04.15', 'Kubus 1 Cm3', '2021-08-06 04:24:43'),
('f12a3777622cf24bfc48b96715ec2435', '02', '09', '02', '04', '16', '02.09.02.04.16', 'Gelas Ukur', '2021-08-06 04:24:43'),
('f7e94012c83495473545bc03d3c85554', '02', '09', '02', '04', '17', '02.09.02.04.17', 'Pipet Tetes', '2021-08-06 04:24:43'),
('36621f60f9e31864a1e1515013aab60d', '02', '09', '02', '04', '18', '02.09.02.04.18', 'Botol Plastik', '2021-08-06 04:24:43'),
('a5e0bd6c70247386309e24cf2c03aed2', '02', '09', '02', '04', '19', '02.09.02.04.19', 'Statip', '2021-08-06 04:24:43'),
('19dc0eccaeea8fa51443a084d2ccc86b', '02', '09', '02', '04', '20', '02.09.02.04.20', 'Klem Rangkap', '2021-08-06 04:24:43'),
('cccc884f3cda5a546f9860a3833a3d14', '02', '09', '02', '04', '21', '02.09.02.04.21', 'Batu Timbangan', '2021-08-06 04:24:43'),
('8d404c96b4bed5b3e6a0f698c30d7062', '02', '09', '02', '04', '22', '02.09.02.04.22', 'Gelas Minum', '2021-08-06 04:24:43'),
('f05e29e5fbcbc678256709dcb064f603', '02', '09', '02', '04', '23', '02.09.02.04.23', 'Labu Erlen Mayer', '2021-08-06 04:24:43'),
('53824b0286c3168e47abb114adc5de9e', '02', '09', '02', '04', '24', '02.09.02.04.24', 'Corong', '2021-08-06 04:24:43'),
('30d617bda1d888459564dcef28b12bca', '02', '09', '02', '04', '25', '02.09.02.04.25', 'Pipa Plastik', '2021-08-06 04:24:43'),
('e47675a68b71f2a8cf9bfb13070168c8', '02', '09', '02', '04', '26', '02.09.02.04.26', 'Bak air', '2021-08-06 04:24:43'),
('238a1fe0b862076b60981368d2459f81', '02', '09', '02', '04', '27', '02.09.02.04.27', 'Sumbat Erlemenyer', '2021-08-06 04:24:43'),
('b6fc2e046af4229df51f13e49fc50a64', '02', '09', '02', '04', '28', '02.09.02.04.28', 'Bendera dan Gabus', '2021-08-06 04:24:43'),
('fb537d244e4c40f4148a6e9f10d42571', '02', '09', '02', '04', '29', '02.09.02.04.29', 'Lilin', '2021-08-06 04:24:43'),
('30f14e1cf21610566009b9644163395a', '02', '09', '02', '04', '30', '02.09.02.04.30', 'Balon Karet', '2021-08-06 04:24:43'),
('72ddf2320f17d54247f89f7392ab0d9f', '02', '09', '02', '04', '31', '02.09.02.04.31', 'Buku', '2021-08-06 04:24:43'),
('b9af77e6b4c0caccedc1ae408e4e0314', '02', '09', '02', '04', '32', '02.09.02.04.32', 'Cawan Alumunium', '2021-08-06 04:24:43'),
('879e2dabbe68ebc488a636a0559d063b', '02', '09', '02', '04', '33', '02.09.02.04.33', 'Ember xkkurixpotxkkurnanx', '2021-08-06 04:24:43'),
('b229764a9810566a71ca0e8ff8bac1cb', '02', '09', '02', '04', '34', '02.09.02.04.34', 'Thermometer Kamar', '2021-08-06 04:24:43'),
('88d45621003a7a9babb6d35d0478e07e', '02', '09', '02', '04', '35', '02.09.02.04.35', 'Thermometer 0.0 xstrix 100.0 C', '2021-08-06 04:24:43'),
('e9841bc384d9b10dd9d97cc58d65a70d', '02', '09', '02', '04', '36', '02.09.02.04.36', 'Pengaduk', '2021-08-06 04:24:43'),
('52ed900355a099edbe4fc46c442034d3', '02', '09', '02', '04', '37', '02.09.02.04.37', 'Thermometer Badan', '2021-08-06 04:24:43'),
('744a76f046de00afdd3365b4d2552c7b', '02', '09', '02', '04', '38', '02.09.02.04.38', 'Balok Berkait', '2021-08-06 04:24:43'),
('44ccb4b4d09d1c77801391236090b69c', '02', '09', '02', '04', '39', '02.09.02.04.39', 'Silinder Berkait', '2021-08-06 04:24:43'),
('9e04c4bfee8e1f4fd2bb00e5b84d56fc', '02', '09', '02', '04', '40', '02.09.02.04.40', 'TalixgmringxBenang', '2021-08-06 04:24:43'),
('ebde4b1baa2a50e4af38236dc68d9438', '02', '09', '02', '04', '41', '02.09.02.04.41', 'Lembaran Plastik', '2021-08-06 04:24:43'),
('2dec60b49b305abe3a14269ca53c67d6', '02', '09', '02', '04', '42', '02.09.02.04.42', 'Sumbat Pipa Runcing', '2021-08-06 04:24:43'),
('c0e5bd7cacbe216e0eba1d87528ec38f', '02', '09', '02', '04', '43', '02.09.02.04.43', 'Pipet Isap', '2021-08-06 04:24:43'),
('caf9e756632cd64d12c5444ee1c830a5', '02', '09', '02', '04', '44', '02.09.02.04.44', 'Kaki Tiga', '2021-08-06 04:24:43'),
('ba6ebe1d8629bb91ca0993976031d287', '02', '09', '02', '04', '45', '02.09.02.04.45', 'SumbatxstrixSumbat Pipa Gelas', '2021-08-06 04:24:43'),
('6b03be8cf3e949d606b5733df837e0e2', '02', '09', '02', '04', '46', '02.09.02.04.46', 'Penjepit Tabung Reaksi Jembatan', '2021-08-06 04:24:43'),
('b8c501007d4bec05bdc90ad4a6c1ec69', '02', '09', '02', '04', '47', '02.09.02.04.47', 'Jembatan', '2021-08-06 04:24:43'),
('b71877d29e37f427c2c736350bf590e5', '02', '09', '02', '04', '48', '02.09.02.04.48', 'Batang BambuxgmringxBesi', '2021-08-06 04:24:43'),
('7f8ba4e74bab65e116b2369dace13ad8', '02', '09', '02', '04', '49', '02.09.02.04.49', 'Lampu Spirtus', '2021-08-06 04:24:43'),
('018198bd8d247ab3f402d0298f367a2f', '02', '09', '02', '04', '50', '02.09.02.04.50', 'Batang Logam', '2021-08-06 04:24:43'),
('eb74bce67de08f323b9d6562e2d1e206', '02', '09', '02', '04', '51', '02.09.02.04.51', 'Batang Kuningan', '2021-08-06 04:24:43'),
('f0b714a4a242374cfb45dd4a6b20c640', '02', '09', '02', '04', '52', '02.09.02.04.52', 'Batang Gelas', '2021-08-06 04:24:43'),
('ba0fcb3d6862a4a0419e2af607739e58', '02', '09', '02', '04', '53', '02.09.02.04.53', 'Sapu tangan', '2021-08-06 04:24:43'),
('57e97929f0acb2e4ac5b697e1c16b88a', '02', '09', '02', '04', '54', '02.09.02.04.54', 'Kertas Karton', '2021-08-06 04:24:43'),
('f84683cacb7f8505722bb6664a4dc622', '02', '09', '02', '04', '55', '02.09.02.04.55', 'Landasan Segi tiga', '2021-08-06 04:24:43'),
('3134f56cf013521a151d64b32368070b', '02', '09', '02', '04', '56', '02.09.02.04.56', 'Pemberat', '2021-08-06 04:24:43'),
('ca1e912852793b0809f7fc6737fd342c', '02', '09', '02', '04', '57', '02.09.02.04.57', 'Statip Lilin', '2021-08-06 04:24:43'),
('bce73c5c73e3fccc03b00484a6c0cb23', '02', '09', '02', '04', '58', '02.09.02.04.58', 'Gelas Horizontal', '2021-08-06 04:24:43'),
('ac27c4688feff571703ae0ca10930ae6', '02', '09', '02', '04', '59', '02.09.02.04.59', 'Layar', '2021-08-06 04:24:43'),
('f41b711fb3cf06d3348c40b3f755ff2d', '02', '09', '02', '04', '60', '02.09.02.04.60', 'Lensa', '2021-08-06 04:24:43'),
('575491d7ba1da8e7d7140c68bf213ace', '02', '09', '02', '04', '61', '02.09.02.04.61', 'KapasxgmringxKertas Yang Kering', '2021-08-06 04:24:43'),
('9c6131a4902266ad178f68c9788154e3', '02', '09', '02', '04', '62', '02.09.02.04.62', 'Stetip Film', '2021-08-06 04:24:43'),
('093c495ade384ac1578384bb708fd9aa', '02', '09', '02', '04', '63', '02.09.02.04.63', 'Baterai', '2021-08-06 04:24:43'),
('1bcffaf6f189ccc46f787ce4f1894d96', '02', '09', '02', '04', '64', '02.09.02.04.64', 'Lampu Pijar', '2021-08-06 04:24:43'),
('bab6c70b3e89534f50fb1e008cbe1cb2', '02', '09', '02', '04', '65', '02.09.02.04.65', 'Kawat Penghubung', '2021-08-06 04:24:43'),
('bb1726a8829151026ca82eba1dd929df', '02', '09', '02', '04', '66', '02.09.02.04.66', 'Paku', '2021-08-06 04:24:43'),
('d249028c2dbddb0c5c7208bb9060d6af', '02', '09', '02', '04', '67', '02.09.02.04.67', 'Tabung Reaksi', '2021-08-06 04:24:43'),
('5d7029ec2b218fb423e7ff64bd830179', '02', '09', '02', '04', '68', '02.09.02.04.68', 'Kompas', '2021-08-06 04:24:43'),
('d0ac777453db56d458ea22b631410a37', '02', '09', '02', '04', '69', '02.09.02.04.69', 'Kunci Sinyal', '2021-08-06 04:24:43'),
('8d673d5a50f9110b8373cc00826e4cef', '02', '09', '02', '04', '70', '02.09.02.04.70', 'Bel Listrik', '2021-08-06 04:24:43'),
('3234255cbace9ac8f6cfc9a3a0218af5', '02', '09', '02', '04', '71', '02.09.02.04.71', 'Alas Dengan Lampu + Kontak', '2021-08-06 04:24:43'),
('b08d706fdbec9855e9d1197a447f67e5', '02', '09', '02', '04', '72', '02.09.02.04.72', 'Pasangan Batu baterai seri', '2021-08-06 04:24:43'),
('78f15fc351cf7cea55f527fa54395f3e', '02', '09', '02', '04', '73', '02.09.02.04.73', 'Pasangan Batu baterai Paralel', '2021-08-06 04:24:43'),
('8f99b484b415cbdfcbc83124b4d93eae', '02', '09', '02', '04', '74', '02.09.02.04.74', 'Cawan Patri', '2021-08-06 04:24:43'),
('630258eb133233dc1815552ded27a597', '02', '09', '02', '04', '75', '02.09.02.04.75', 'Pensil Kaca', '2021-08-06 04:24:43'),
('466af10506a01345c052d98c660aa37b', '02', '09', '02', '04', '76', '02.09.02.04.76', 'Pot Plastik', '2021-08-06 04:24:43'),
('6f8e2f32b89e0340798b5c4458c9845c', '02', '09', '02', '04', '77', '02.09.02.04.77', 'Pisau', '2021-08-06 04:24:43'),
('c2d4502497afec2abeda0c72c1c911f5', '02', '09', '02', '04', '78', '02.09.02.04.78', 'Pipa Karet', '2021-08-06 04:24:43'),
('5349eb2bb9dca0fc73b97e37157876c6', '02', '09', '02', '04', '79', '02.09.02.04.79', 'Pipa Kaca Lurus', '2021-08-06 04:24:43'),
('df2b2a3cbcda5473a15b323b7bd0d4be', '02', '09', '02', '04', '80', '02.09.02.04.80', 'Statip Tabung Reaksi', '2021-08-06 04:24:43'),
('7bd4fb7a55340f43dee4bf8bbfa7ad09', '02', '09', '02', '04', '81', '02.09.02.04.81', 'Gelas Piala', '2021-08-06 04:24:43'),
('3cd9eb777e4e969038790d2c84ae5b58', '02', '09', '02', '04', '82', '02.09.02.04.82', 'Larutan Garam NaCi', '2021-08-06 04:24:43'),
('4255a90f28a668fc046d2e57ae155082', '02', '09', '02', '04', '83', '02.09.02.04.83', 'Kertas Selopatan', '2021-08-06 04:24:43'),
('d85d1de2c21fc37d9a91e30eab90af36', '02', '09', '02', '04', '84', '02.09.02.04.84', 'Pinset + Pensil', '2021-08-06 04:24:43'),
('6c2887ea1e0e536c7db09ca6e1b61cb5', '02', '09', '02', '04', '85', '02.09.02.04.85', 'Lilin Mainan', '2021-08-06 04:24:43'),
('098ce77822e987a932fe514a7b7e4fb1', '02', '09', '02', '04', '86', '02.09.02.04.86', 'Jarum Pentul', '2021-08-06 04:24:43'),
('eeb44d4e54f538356066a0f11955fe2a', '02', '09', '02', '04', '87', '02.09.02.04.87', 'Balok Gabus', '2021-08-06 04:24:43'),
('150446956687c7f3c3cc76c7421e9e55', '02', '09', '02', '04', '88', '02.09.02.04.88', 'Sepitan', '2021-08-06 04:24:43'),
('03c99f8a68d443c34267dd490669dd5d', '02', '09', '02', '04', '89', '02.09.02.04.89', 'Penjepit Kertas', '2021-08-06 04:24:43'),
('c71ccda90280fec16ff21123b2551ff6', '02', '09', '02', '04', '90', '02.09.02.04.90', 'Botol Spesiman', '2021-08-06 04:24:43'),
('d4e666b17f85381f4508db4634251885', '02', '09', '02', '04', '91', '02.09.02.04.91', 'Sumbat Karet', '2021-08-06 04:24:43'),
('c7dc38d34c263efef86001ba88753aaa', '02', '09', '02', '04', '92', '02.09.02.04.92', 'Lilin dan Pemegangnya', '2021-08-06 04:24:43'),
('e66b36ad20e0aa1c856f5379559cbf9a', '02', '09', '02', '04', '93', '02.09.02.04.93', 'Sumbat Berlubang 2 xkkurixduaxkkurnanx', '2021-08-06 04:24:43'),
('a6988417e9e0d78ce09714152fcafd3f', '02', '09', '02', '04', '94', '02.09.02.04.94', 'Pinset Bengkok', '2021-08-06 04:24:43'),
('8cfc49df4655a63a0de84d9756451b27', '02', '09', '02', '04', '95', '02.09.02.04.95', 'Alat Demonstrasi Pernapasan', '2021-08-06 04:24:43'),
('eea939529bc94053d7dd194c1c593f20', '02', '09', '02', '04', '96', '02.09.02.04.96', 'Pipa Bentuk Y', '2021-08-06 04:24:43'),
('9a462461deaa6b26fc8f9ddea5d24bee', '02', '09', '02', '04', '97', '02.09.02.04.97', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('e86d2a2410ffe53973b9dc205e39350b', '02', '09', '02', '05', '*', '02.09.02.05.*', 'Bidang Studi : IPA Menengah', '2021-08-06 04:24:43'),
('9f300cba74871014807e86d704042615', '02', '09', '02', '05', '01', '02.09.02.05.01', 'Bejana Berhubungan', '2021-08-06 04:24:43'),
('a010cb7b1293b69abaa76bf0200455b8', '02', '09', '02', '05', '02', '02.09.02.05.02', 'Pipa Kapiter', '2021-08-06 04:24:43'),
('4d8a0c78127bcaf63c6a7978f0998674', '02', '09', '02', '05', '03', '02.09.02.05.03', 'Ember SxpsijixGravesandre', '2021-08-06 04:24:43'),
('e96b8f9564903c75764f259f741a446b', '02', '09', '02', '05', '04', '02.09.02.05.04', 'Galangan Kapal', '2021-08-06 04:24:43'),
('cd4b4fd6fe6ecf04fdba56b2cdc94307', '02', '09', '02', '05', '05', '02.09.02.05.05', 'Manometer Terbuka', '2021-08-06 04:24:43'),
('8d804e7cc7dec0a326f04293fab1b9e5', '02', '09', '02', '05', '06', '02.09.02.05.06', 'Manometer Tertutup', '2021-08-06 04:24:43'),
('2ff35badcb1f5fa41bb251eabc5c7f2d', '02', '09', '02', '05', '07', '02.09.02.05.07', 'Pompa Karet', '2021-08-06 04:24:43'),
('5f19ff6e9494380fee1537a5d02b72e8', '02', '09', '02', '05', '08', '02.09.02.05.08', '2 Pipa Gelas + Karet Penyumbat', '2021-08-06 04:24:43'),
('e60ac5af0e406a9ccf706d31c1f960e3', '02', '09', '02', '05', '09', '02.09.02.05.09', 'Pipa Bengkok + Sumbat', '2021-08-06 04:24:43');
INSERT INTO `m_kib_kode` (`kd`, `golongan`, `bidang`, `kelompok`, `kelompok_sub`, `kelompok_sub_sub`, `kode`, `nama`, `postdate`) VALUES
('4bb96d4eeee2594a8dd3bb17dc367e70', '02', '09', '02', '05', '10', '02.09.02.05.10', 'Klem Rangkap', '2021-08-06 04:24:43'),
('f7ea65e051526de76224185f4f4c854a', '02', '09', '02', '05', '11', '02.09.02.05.11', 'Batu Timbangan Berkait', '2021-08-06 04:24:43'),
('aca683ed347c699c174cbfe09b0590ed', '02', '09', '02', '05', '12', '02.09.02.05.12', 'Alas jungkitan', '2021-08-06 04:24:43'),
('f9a69dc8eaa4dd26c31502c8e1483b8e', '02', '09', '02', '05', '13', '02.09.02.05.13', 'Katrol', '2021-08-06 04:24:43'),
('42fc5c686015b63fdaac4b3543a80711', '02', '09', '02', '05', '14', '02.09.02.05.14', 'Penahan Ban', '2021-08-06 04:24:43'),
('5ee710f691c753263e963008e03d87c3', '02', '09', '02', '05', '15', '02.09.02.05.15', 'Desimeter', '2021-08-06 04:24:43'),
('f0904d3df03175942469bfe2122d267e', '02', '09', '02', '05', '16', '02.09.02.05.16', 'Alat Untuk Menunjukan Tekanan Dalam kelema', '2021-08-06 04:24:43'),
('4fcab39e371c9f7da30a9071236311d0', '02', '09', '02', '05', '17', '02.09.02.05.17', 'Tabung Resonansi', '2021-08-06 04:24:43'),
('a57d9a859dd5b7fc561cf7f7a0818768', '02', '09', '02', '05', '18', '02.09.02.05.18', 'Garpu Penala', '2021-08-06 04:24:43'),
('c614bd4389978177eb96935f675455cb', '02', '09', '02', '05', '19', '02.09.02.05.19', 'Lempeng Gelas', '2021-08-06 04:24:43'),
('d5005e460a3ac3fe268b2502fbabf12a', '02', '09', '02', '05', '20', '02.09.02.05.20', 'Galang SandaranxgmringxRing', '2021-08-06 04:24:43'),
('ed0234ff5daaf26c2111cf6287e9eb60', '02', '09', '02', '05', '21', '02.09.02.05.21', 'Alat Menunjukan Aliran zat Cair', '2021-08-06 04:24:43'),
('43e7fc8a5d58e73b0f149550f40f846b', '02', '09', '02', '05', '22', '02.09.02.05.22', 'Kertas Isap', '2021-08-06 04:24:43'),
('f08a7eea168df8de5833e8bf18a99ff3', '02', '09', '02', '05', '23', '02.09.02.05.23', 'Pipa PendinginxgmringxPenyuling', '2021-08-06 04:24:43'),
('da80cb55b4b625c0869939f51f6af676', '02', '09', '02', '05', '24', '02.09.02.05.24', 'Rol Optik', '2021-08-06 04:24:43'),
('ee7ea4bdb2a947efdc4b716e7dec2dab', '02', '09', '02', '05', '25', '02.09.02.05.25', 'Sumber Cahaya', '2021-08-06 04:24:43'),
('90b1b3e261ddfbde76b0cb68b2fece82', '02', '09', '02', '05', '26', '02.09.02.05.26', 'Rangka Penjepit', '2021-08-06 04:24:43'),
('2215b31c5a3f90b7054ad96cfb589820', '02', '09', '02', '05', '27', '02.09.02.05.27', 'Celah Satu Horisontal', '2021-08-06 04:24:43'),
('cb1c34fca4093106109eb89c7d43bcb9', '02', '09', '02', '05', '28', '02.09.02.05.28', 'Layar', '2021-08-06 04:24:43'),
('53b551d53a782f012a50662ab1fa1704', '02', '09', '02', '05', '29', '02.09.02.05.29', 'Cermin Datar Dan Skala', '2021-08-06 04:24:43'),
('36da0fee44492d11c46254a16dd7f8db', '02', '09', '02', '05', '30', '02.09.02.05.30', 'Klem Pegas', '2021-08-06 04:24:43'),
('b609014684651e46269aae7fdb08561f', '02', '09', '02', '05', '31', '02.09.02.05.31', 'Lempeng Perpeks 1xgmringx2 Lingkaran', '2021-08-06 04:24:43'),
('1db3cc47319dd2932c2cbec8a8bb6eac', '02', '09', '02', '05', '32', '02.09.02.05.32', 'Prisma xkkurixLempengxkkurnanx', '2021-08-06 04:24:43'),
('790f04d432dae43f793ab221aa9e2387', '02', '09', '02', '05', '33', '02.09.02.05.33', 'Lempeng Plane paralel', '2021-08-06 04:24:43'),
('72c4c59a7152b376f2bd45720a0b591d', '02', '09', '02', '05', '34', '02.09.02.05.34', 'Celah Lima Horisontal', '2021-08-06 04:24:43'),
('80b8d95d3f0e82e507f95e3cfb45231b', '02', '09', '02', '05', '35', '02.09.02.05.35', 'Lensa Pepeku Bikonfeks', '2021-08-06 04:24:43'),
('8666ca78ef51bd42c990a3d70ced4f41', '02', '09', '02', '05', '36', '02.09.02.05.36', 'Lensa PerPeku Konkaf', '2021-08-06 04:24:43'),
('4fe7b64067dcf20de494b79c3d78b129', '02', '09', '02', '05', '37', '02.09.02.05.37', 'Lensa F+15', '2021-08-06 04:24:43'),
('8515cd0eda585d2fe15ef5bcd4ee9e24', '02', '09', '02', '05', '38', '02.09.02.05.38', 'Lensa F+10', '2021-08-06 04:24:43'),
('2aed90182f3621cd52feb039a21bf2f6', '02', '09', '02', '05', '39', '02.09.02.05.39', 'Lensa F+5', '2021-08-06 04:24:43'),
('6e5af76526d4af74687ad7697dbfc55f', '02', '09', '02', '05', '40', '02.09.02.05.40', 'Lensa F+30', '2021-08-06 04:24:43'),
('2cc971aaaa3475e2ea178328c3cf5eac', '02', '09', '02', '05', '41', '02.09.02.05.41', 'Lensa F+15', '2021-08-06 04:24:43'),
('5cc4e0bd83ea0f9a491dcff6996819de', '02', '09', '02', '05', '42', '02.09.02.05.42', 'Lensa F+12', '2021-08-06 04:24:43'),
('4945c64d068866b5c3e65f9f46e2f277', '02', '09', '02', '05', '43', '02.09.02.05.43', 'Magnit Batang', '2021-08-06 04:24:43'),
('108f68fa55db84fa68110e6899f1b0bd', '02', '09', '02', '05', '44', '02.09.02.05.44', 'Sebuk Besi', '2021-08-06 04:24:43'),
('98a65a496026cb73679dd7ceb3b76581', '02', '09', '02', '05', '45', '02.09.02.05.45', 'Tombok Tekan', '2021-08-06 04:24:43'),
('439767f3fdeafbc992450c1817d4e904', '02', '09', '02', '05', '46', '02.09.02.05.46', 'Kumparan 300', '2021-08-06 04:24:43'),
('d22a85a67cbbd14eac37336b07e600a3', '02', '09', '02', '05', '47', '02.09.02.05.47', 'Inti Kumparan 300 Lilitan', '2021-08-06 04:24:43'),
('4f53d5043e2452008b79441fb0b508a6', '02', '09', '02', '05', '48', '02.09.02.05.48', 'Ampermeter', '2021-08-06 04:24:43'),
('5893919341529bfaa81c976020bd28de', '02', '09', '02', '05', '49', '02.09.02.05.49', 'Batang GelasxgmringxPlastik', '2021-08-06 04:24:43'),
('bc2305dc84ca562a1e64da8c53bc6793', '02', '09', '02', '05', '50', '02.09.02.05.50', 'Voltmeter', '2021-08-06 04:24:43'),
('1fe2e14bba2d9a51f7146865039591c7', '02', '09', '02', '05', '51', '02.09.02.05.51', 'Beberapa Tahanan', '2021-08-06 04:24:43'),
('b178cd9f931c59c882f717649dc3fdc8', '02', '09', '02', '05', '52', '02.09.02.05.52', 'Jarum Panjang', '2021-08-06 04:24:43'),
('a46cd2343c566dd6b5e57a7f814dd462', '02', '09', '02', '05', '53', '02.09.02.05.53', 'Lensa Tangan', '2021-08-06 04:24:43'),
('fb196b087740d891ea4b52efc7a782ac', '02', '09', '02', '05', '54', '02.09.02.05.54', 'Papan Pengempres', '2021-08-06 04:24:43'),
('7e8829c9fe31f2e0f1fd5cff973b2840', '02', '09', '02', '05', '55', '02.09.02.05.55', 'Papan Perentang', '2021-08-06 04:24:43'),
('1ad44928263b0fd08333a0fa35259321', '02', '09', '02', '05', '56', '02.09.02.05.56', 'Kertas Minyak', '2021-08-06 04:24:43'),
('d39ddc2fe7f8f41a7f4c42dae250e9a9', '02', '09', '02', '05', '57', '02.09.02.05.57', 'Gelas Obyek', '2021-08-06 04:24:43'),
('ce62de1b9ec04ac9973864061b2b8773', '02', '09', '02', '05', '58', '02.09.02.05.58', 'Gelas Penutup', '2021-08-06 04:24:43'),
('b3c7b6a43ddbc88b2375ec719fddff54', '02', '09', '02', '05', '59', '02.09.02.05.59', 'Mikroskop', '2021-08-06 04:24:43'),
('e36389f115b00744473fc4b69257327e', '02', '09', '02', '05', '60', '02.09.02.05.60', 'Bronthymol Biru', '2021-08-06 04:24:43'),
('b8ea81a47da28af57301e73248ba2f9f', '02', '09', '02', '05', '61', '02.09.02.05.61', 'Belyar', '2021-08-06 04:24:43'),
('182053d2ba9ea5e77f8f42ca571a0f85', '02', '09', '02', '05', '62', '02.09.02.05.62', 'Gunting', '2021-08-06 04:24:43'),
('0ea408f147a7c89274c252d18aca71fb', '02', '09', '02', '05', '63', '02.09.02.05.63', 'Glukosa', '2021-08-06 04:24:43'),
('0f6555bf1369946847bca5530fbc4e58', '02', '09', '02', '05', '64', '02.09.02.05.64', 'Pipa Manometer', '2021-08-06 04:24:43'),
('7e9bff04062bfa102e718e2a214b186b', '02', '09', '02', '05', '65', '02.09.02.05.65', 'Pipa Gelas', '2021-08-06 04:24:43'),
('0f7a2266497c5d17965b95ff68e570c6', '02', '09', '02', '05', '66', '02.09.02.05.66', 'Sepit Kayu', '2021-08-06 04:24:43'),
('cee279cb9ac99fa6f483cd299221c75c', '02', '09', '02', '05', '67', '02.09.02.05.67', 'Reagen Biuret', '2021-08-06 04:24:43'),
('db4d8478524eb435946595f3de1f234b', '02', '09', '02', '05', '68', '02.09.02.05.68', 'MortirxgmringxAlu', '2021-08-06 04:24:43'),
('1fd28accdbdb8f2efb8a75ac2fa15ac5', '02', '09', '02', '05', '69', '02.09.02.05.69', 'Rak Tabunga Reaksi', '2021-08-06 04:24:43'),
('000d57d9a6b671d836f1214ea7f03d3a', '02', '09', '02', '05', '70', '02.09.02.05.70', 'Sumbat Karet Berpipa', '2021-08-06 04:24:43'),
('36e0b7038392b9247c9e76bc3f1914d0', '02', '09', '02', '05', '71', '02.09.02.05.71', 'Sumbat ERlemeyer Berpipa', '2021-08-06 04:24:43'),
('325414abd63c14b581a25e8838f094b5', '02', '09', '02', '05', '72', '02.09.02.05.72', 'Sumbat Tabung Reaksi + Pipa Lurus', '2021-08-06 04:24:43'),
('137d0170f1d6d559db06f0710dce1284', '02', '09', '02', '05', '73', '02.09.02.05.73', 'Kawat Kasa', '2021-08-06 04:24:43'),
('036a424c5b4bcc531e120761939cd3ba', '02', '09', '02', '05', '74', '02.09.02.05.74', 'Thermos Kecil', '2021-08-06 04:24:43'),
('30d32c655028ed37f0b0c308171358f8', '02', '09', '02', '05', '75', '02.09.02.05.75', 'Pot Besar', '2021-08-06 04:24:43'),
('9dcd83cd18614c8eb9e1f2f50ccb7dde', '02', '09', '02', '05', '76', '02.09.02.05.76', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('cf7700da76670c079cee7705bc221edc', '02', '09', '02', '06', '*', '02.09.02.06.*', 'Bidang Studi : IPA Atas', '2021-08-06 04:24:43'),
('bff4191d6ac112950a37336fbd187d12', '02', '09', '02', '06', '01', '02.09.02.06.01', 'Model Kepala dan Otak', '2021-08-06 04:24:43'),
('5d0aff480593f5f137f632d68aa73db6', '02', '09', '02', '06', '02', '02.09.02.06.02', 'Model Kulit', '2021-08-06 04:24:43'),
('169b1bbc3ca9c5cf92faf5b1fe0015a6', '02', '09', '02', '06', '03', '02.09.02.06.03', 'Model Mata', '2021-08-06 04:24:43'),
('b0aab8a507d44756b8777223913e2aed', '02', '09', '02', '06', '04', '02.09.02.06.04', 'Model Telinga', '2021-08-06 04:24:43'),
('fbd3b6268cce81d6e886bc315608b4d6', '02', '09', '02', '06', '05', '02.09.02.06.05', 'Model Hati dan Ginjal', '2021-08-06 04:24:43'),
('63a03072298cc4b9a2f6cd49805aca9a', '02', '09', '02', '06', '06', '02.09.02.06.06', 'Model Gigi', '2021-08-06 04:24:43'),
('560b173060769eb94725c6f2821e4a2d', '02', '09', '02', '06', '07', '02.09.02.06.07', 'Model Lambung', '2021-08-06 04:24:43'),
('c176a0e00cab6cc3175d51aa38d14620', '02', '09', '02', '06', '08', '02.09.02.06.08', 'Model Ginjal', '2021-08-06 04:24:43'),
('9dfdaf19c076c9ffbd8d7c17447c5b90', '02', '09', '02', '06', '09', '02.09.02.06.09', 'Slotted Weight dan Hanger 250 gram', '2021-08-06 04:24:43'),
('0e6adc56c7ff424812f508ef34c04b7b', '02', '09', '02', '06', '10', '02.09.02.06.10', 'Slotted Weight dan Hanger 10 xstrix 00 gram', '2021-08-06 04:24:43'),
('8d5223ddfbb435e0a3918037336e34a6', '02', '09', '02', '06', '11', '02.09.02.06.11', 'Bel Listrik', '2021-08-06 04:24:43'),
('41c1f31f32ce58f63dfd627f0a3b4797', '02', '09', '02', '06', '12', '02.09.02.06.12', 'Elektroda Tembaga xkkurixVoltameterxkkurnanx', '2021-08-06 04:24:43'),
('86e4f5472ea018dc67f16abc9eca9730', '02', '09', '02', '06', '13', '02.09.02.06.13', 'Alat Hartle', '2021-08-06 04:24:43'),
('e1c54f2b51ce8766e42cdedd9014f4e0', '02', '09', '02', '06', '14', '02.09.02.06.14', 'Hygrometer Basah dan Kering', '2021-08-06 04:24:43'),
('e8735fe1d4951242add379c381d1cfc6', '02', '09', '02', '06', '15', '02.09.02.06.15', 'Motor Listrik', '2021-08-06 04:24:43'),
('609f07a78799752c82a412551d9a991e', '02', '09', '02', '06', '16', '02.09.02.06.16', 'Ticker Limer', '2021-08-06 04:24:43'),
('8ecd062b532e8d2b42d28e56734daa5f', '02', '09', '02', '06', '17', '02.09.02.06.17', 'Conductivity App', '2021-08-06 04:24:43'),
('b9403203ed2b6c82e309f1d54f4a77fc', '02', '09', '02', '06', '18', '02.09.02.06.18', 'Katrol Ganda', '2021-08-06 04:24:43'),
('8c4e29594a59b839b968960898746027', '02', '09', '02', '06', '19', '02.09.02.06.19', 'Silender Materi', '2021-08-06 04:24:43'),
('52dad39b1a056371e60fa70f3fee1524', '02', '09', '02', '06', '20', '02.09.02.06.20', 'Pascal Syirine', '2021-08-06 04:24:43'),
('974ba5b784f2e765a5a2520d3f019371', '02', '09', '02', '06', '21', '02.09.02.06.21', 'Alat Difusi Zat Cair xkkurixliquid Diff Appxkkurnanx', '2021-08-06 04:24:43'),
('9e379a6a6cbffcab959aa4f921cce0a9', '02', '09', '02', '06', '22', '02.09.02.06.22', 'Kotak Cacing Tanah', '2021-08-06 04:24:43'),
('e773722e52ac8068779f92083f05912f', '02', '09', '02', '06', '23', '02.09.02.06.23', 'Alat Ukur Takanan Air xkkurixPoot Presure Appxkkurnanx', '2021-08-06 04:24:43'),
('dcd8f639e3438b0caac3e856c1f86282', '02', '09', '02', '06', '24', '02.09.02.06.24', 'Foto meter', '2021-08-06 04:24:43'),
('b2e17d9a331f6d2f197e34bca6d3ddc3', '02', '09', '02', '06', '25', '02.09.02.06.25', 'Kotak Kaca Obyek', '2021-08-06 04:24:43'),
('dc953e77ca1fefcdfbed7db32846c2c4', '02', '09', '02', '06', '26', '02.09.02.06.26', 'Snaper For Crok Berer', '2021-08-06 04:24:43'),
('b602969574885bc9218cd8e7bc25d02b', '02', '09', '02', '06', '27', '02.09.02.06.27', 'Tabung Penyuling', '2021-08-06 04:24:43'),
('943497a80c1815e19ab867a83ca10cb1', '02', '09', '02', '06', '28', '02.09.02.06.28', 'Lampu spirtus', '2021-08-06 04:24:43'),
('cd5a08a4cc29ee02cb899bfc14bcfa05', '02', '09', '02', '06', '29', '02.09.02.06.29', 'Segitiga Porselin', '2021-08-06 04:24:43'),
('f546a97253a3ec6b703fbb74365bf746', '02', '09', '02', '06', '30', '02.09.02.06.30', 'Jepitan Tabung reaksi', '2021-08-06 04:24:43'),
('b39c60fdb990ec45d284ce18b0b06736', '02', '09', '02', '06', '31', '02.09.02.06.31', 'Spatula Tanduk', '2021-08-06 04:24:43'),
('023ca521e513ef2300e7a501f52cbfee', '02', '09', '02', '06', '32', '02.09.02.06.32', 'Spatula Stenles Steel', '2021-08-06 04:24:43'),
('2c60de56cac3a83d48f2c4e7c33f2241', '02', '09', '02', '06', '33', '02.09.02.06.33', 'Sikat Tabung Reaksi Besar Kecil', '2021-08-06 04:24:43'),
('acf535ba0152be51ee423e5af4c33e2c', '02', '09', '02', '06', '34', '02.09.02.06.34', 'Pipa T', '2021-08-06 04:24:43'),
('02c87a95d892704907e636fa887da9c1', '02', '09', '02', '06', '35', '02.09.02.06.35', 'Pipa Y dari Kaca', '2021-08-06 04:24:43'),
('ab2a7b4a591118cbb9316d116ac64b32', '02', '09', '02', '06', '36', '02.09.02.06.36', 'Pipa Ukuran 5 ml', '2021-08-06 04:24:43'),
('8bd2467a5cf0fd278dd7f7eee62dfd5f', '02', '09', '02', '06', '37', '02.09.02.06.37', 'Pipa Ukuran 10 ml', '2021-08-06 04:24:43'),
('841f1d0608d78adc33092180b0606dfe', '02', '09', '02', '06', '38', '02.09.02.06.38', 'Pipa Ukuran 25 ml', '2021-08-06 04:24:43'),
('ea23f8aa551b39d932421e8820e1c6c7', '02', '09', '02', '06', '39', '02.09.02.06.39', 'Model Jantung', '2021-08-06 04:24:43'),
('7724bbbaa78a3d97811e73d92c29975b', '02', '09', '02', '06', '40', '02.09.02.06.40', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('18117be4efb916b4fe1d1848c72c54cc', '02', '09', '02', '07', '*', '02.09.02.07.*', 'Bidang Studi : IPS', '2021-08-06 04:24:43'),
('009c4572c66f3c7284da4b0d8b9482c9', '02', '09', '02', '07', '01', '02.09.02.07.01', 'Peta', '2021-08-06 04:24:43'),
('dec0cedaa6cf8e99586178f36431fbff', '02', '09', '02', '07', '02', '02.09.02.07.02', 'Laixstrixlain', '2021-08-06 04:24:43'),
('466e4e8213e9a5f9e8d7b9afa2213725', '02', '09', '02', '08', '*', '02.09.02.08.*', 'Bidang Studi : Agama Islam', '2021-08-06 04:24:43'),
('308c5797f04626ec0c37fc4ce666d546', '02', '09', '02', '08', '01', '02.09.02.08.01', 'Alat Peraga Membaca Dan Menulis Al Quran', '2021-08-06 04:24:43'),
('7c357f4563cc92bda7f08d701bde32d7', '02', '09', '02', '08', '02', '02.09.02.08.02', 'Papan Peraga', '2021-08-06 04:24:43'),
('f39e858bfef27e6738446dffd4859aa0', '02', '09', '02', '08', '03', '02.09.02.08.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('70241240cadf029e44669d2a5d5e88a5', '02', '09', '02', '09', '*', '02.09.02.09.*', 'Bidang Studi : Ketrampilan', '2021-08-06 04:24:43'),
('927c61d6ee2a0afb251e721786443996', '02', '09', '02', '09', '01', '02.09.02.09.01', 'Pisau Okulasi', '2021-08-06 04:24:43'),
('ce66353606143c07e42eb26495807303', '02', '09', '02', '09', '02', '02.09.02.09.02', 'Garpu', '2021-08-06 04:24:43'),
('4a3a947706c8e557ce5d1f59ebbe1e5c', '02', '09', '02', '09', '03', '02.09.02.09.03', 'Gunting Stek', '2021-08-06 04:24:43'),
('9b154ef9f43cfc8d8b96da8521c9a9ec', '02', '09', '02', '09', '04', '02.09.02.09.04', 'Gunting Pemangkas', '2021-08-06 04:24:43'),
('c9a70a1c4d69dcca15116cf756ae86b6', '02', '09', '02', '09', '05', '02.09.02.09.05', 'Spryer Kecil', '2021-08-06 04:24:43'),
('116593cf82af95ba1100a437fcffae4d', '02', '09', '02', '09', '06', '02.09.02.09.06', 'Sekop', '2021-08-06 04:24:43'),
('fe37ff50a8895bb0fd4d8b49ab513471', '02', '09', '02', '09', '07', '02.09.02.09.07', 'Panci', '2021-08-06 04:24:43'),
('f3937dc1dad6ab7554a9328194b3ec2a', '02', '09', '02', '09', '08', '02.09.02.09.08', 'Botol', '2021-08-06 04:24:43'),
('bf583fcc5763b9af7f25eea910a945f3', '02', '09', '02', '09', '09', '02.09.02.09.09', 'Botol Jam xkkurixBotol Selatxkkurnanx', '2021-08-06 04:24:43'),
('f89690a8524890ddbfb08b0669a84aee', '02', '09', '02', '09', '10', '02.09.02.09.10', 'Penutup Botol', '2021-08-06 04:24:43'),
('035247f73b45b04975cbb35a10fda478', '02', '09', '02', '09', '11', '02.09.02.09.11', 'Pisau Buah', '2021-08-06 04:24:43'),
('96785f486052567890129e548923ce27', '02', '09', '02', '09', '12', '02.09.02.09.12', 'Spruit xkkurixAlat Penyuntikxkkurnanx', '2021-08-06 04:24:43'),
('a9641237f398fafe269eda427cea7e2d', '02', '09', '02', '09', '13', '02.09.02.09.13', 'Tempat Minum Ayam', '2021-08-06 04:24:43'),
('72923eb1e98cdfa37d398550319f37ef', '02', '09', '02', '09', '14', '02.09.02.09.14', 'Lumpang Besi', '2021-08-06 04:24:43'),
('b1cb6116d0b80486ad8348fa40be2079', '02', '09', '02', '09', '15', '02.09.02.09.15', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('42b445ecb2d6cb49430539e765418cdc', '02', '09', '02', '10', '*', '02.09.02.10.*', 'Bidang Studi : Kesenian', '2021-08-06 04:24:43'),
('f8a10aaec01a0b498d80fb1f8209287d', '02', '09', '02', '10', '01', '02.09.02.10.01', 'Gitar Sppanish', '2021-08-06 04:24:43'),
('20b794f488b531b2f298ac1bc2be6107', '02', '09', '02', '10', '02', '02.09.02.10.02', 'Gitar Elastrik', '2021-08-06 04:24:43'),
('6e6699e8f48d569f9c8d749395fe9094', '02', '09', '02', '10', '03', '02.09.02.10.03', 'Piano', '2021-08-06 04:24:43'),
('0508c2bfb9dc93b72860dcc314d6ccae', '02', '09', '02', '10', '04', '02.09.02.10.04', 'OrgenxgmringxElectrone', '2021-08-06 04:24:43'),
('38d0e70e7aa8a855e18466f7e1da221e', '02', '09', '02', '10', '05', '02.09.02.10.05', 'Recorder', '2021-08-06 04:24:43'),
('7b51638ee2017667135f2fcc11a620c2', '02', '09', '02', '10', '06', '02.09.02.10.06', 'Stem Fluid', '2021-08-06 04:24:43'),
('0fae0058614d89233c1084e6698827e8', '02', '09', '02', '10', '07', '02.09.02.10.07', 'Gambar Didinding Notasi Musik', '2021-08-06 04:24:43'),
('db9718d5ce092303286c2babd97ac1d2', '02', '09', '02', '10', '08', '02.09.02.10.08', 'Pianika', '2021-08-06 04:24:43'),
('46d3f2952dd62355a075e9d96e7f48a6', '02', '09', '02', '10', '09', '02.09.02.10.09', 'Harmonika', '2021-08-06 04:24:43'),
('e994205728273f744e6ceb6da97e21ae', '02', '09', '02', '10', '10', '02.09.02.10.10', 'Gamelan', '2021-08-06 04:24:43'),
('db715ad63357c42fb03615552be55a62', '02', '09', '02', '10', '11', '02.09.02.10.11', 'Angklung', '2021-08-06 04:24:43'),
('4f141258974068db33e87471b661f14d', '02', '09', '02', '10', '12', '02.09.02.10.12', 'SulingxgmringxSeruling', '2021-08-06 04:24:43'),
('93a85412a574504627997b53b1102940', '02', '09', '02', '10', '13', '02.09.02.10.13', 'Kecapi', '2021-08-06 04:24:43'),
('65235775278d526a64e683ee497ffa21', '02', '09', '02', '10', '14', '02.09.02.10.14', 'Rebab', '2021-08-06 04:24:43'),
('0ea6da9dcaef3db61682ae8a3e43e281', '02', '09', '02', '10', '15', '02.09.02.10.15', 'Garpu tala', '2021-08-06 04:24:43'),
('c77bd6169b16a2f5df4200e97e164ef3', '02', '09', '02', '10', '16', '02.09.02.10.16', 'Gendang', '2021-08-06 04:24:43'),
('e22589740df4ffc1e57223944581292c', '02', '09', '02', '10', '17', '02.09.02.10.17', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('5bcfa8d1b465372ed6c22b2372d94322', '02', '09', '02', '11', '*', '02.09.02.11.*', 'Bidang Studi : Olah Raga', '2021-08-06 04:24:43'),
('02b030d26dc72556328d21fd87e6ec54', '02', '09', '02', '11', '01', '02.09.02.11.01', 'Nama Nada', '2021-08-06 04:24:43'),
('c370d2f95d76e208f9231a9610fc7025', '02', '09', '02', '11', '02', '02.09.02.11.02', 'Bola Kasti', '2021-08-06 04:24:43'),
('8ab47b87bd2d24838930885d237c71b4', '02', '09', '02', '11', '03', '02.09.02.11.03', 'Kayu Pemukul', '2021-08-06 04:24:43'),
('f14bbc09c306875e0450d3093266fd89', '02', '09', '02', '11', '04', '02.09.02.11.04', 'Start Blok', '2021-08-06 04:24:43'),
('1ee36275ecc466f4102ad734f0998827', '02', '09', '02', '11', '05', '02.09.02.11.05', 'Bendera Start', '2021-08-06 04:24:43'),
('3f538e9055d17fac0108dd713ea146c2', '02', '09', '02', '11', '06', '02.09.02.11.06', 'Bola Pasil', '2021-08-06 04:24:43'),
('bb0ef6778988c1a90f2735b94a2168d2', '02', '09', '02', '11', '07', '02.09.02.11.07', 'Pita ukuran xkkurixmeteranxkkurnanx', '2021-08-06 04:24:43'),
('40ccf0503bdb21b07b2b6a3bc0923c6f', '02', '09', '02', '11', '08', '02.09.02.11.08', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('5e9fc00334219c9eb228ac99159a20b3', '02', '09', '02', '12', '*', '02.09.02.12.*', 'Bidang Studi : PMP', '2021-08-06 04:24:43'),
('19054efc77a20accd0eaa0189fdad14e', '02', '09', '02', '12', '01', '02.09.02.12.01', 'Lambang NegaraxkkurixGarudaxkkurnanx', '2021-08-06 04:24:43'),
('fd5ae7e59665481bf0875ce4423e8e0a', '02', '09', '02', '12', '02', '02.09.02.12.02', 'Bendera Merah Putih', '2021-08-06 04:24:43'),
('9fd964edee4999c254bd149ff86d3f85', '02', '09', '02', '12', '03', '02.09.02.12.03', 'Gambar Presiden', '2021-08-06 04:24:43'),
('b0062f77d614fb486cbdf211079915bd', '02', '09', '02', '12', '04', '02.09.02.12.04', 'Gambar Wakil Presiden', '2021-08-06 04:24:43'),
('2f3e5bd484e569bcb21a680279ca4b3b', '02', '09', '02', '12', '05', '02.09.02.12.05', 'Gambar TokohxstrixTokoh Nasional', '2021-08-06 04:24:43'),
('12285d2becb9c914846275d5f45ddb1b', '02', '09', '02', '12', '06', '02.09.02.12.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('60b7121b67d132002c2f2f7f37472c0b', '02', '10', '*', '*', '*', '02.10.*.*.*', 'ALATxstrixALAT PERSENJATAANxgmringxKEAMANAN', '2021-08-06 04:24:43'),
('966589594faaea9aa8cf857f9748ba32', '02', '10', '01', '*', '*', '02.10.01.*.*', 'SENJATA API', '2021-08-06 04:24:43'),
('3afb43940b6773479cbea999c71fc983', '02', '10', '02', '*', '*', '02.10.02.*.*', 'PERSENJATAAN NON SENJATA API', '2021-08-06 04:24:43'),
('1233de404550d1afaef66ff3494e79b3', '02', '10', '02', '01', '*', '02.10.02.01.*', 'Alat Keamanan', '2021-08-06 04:24:43'),
('92c1c3a922499b69f866befa4d40e904', '02', '10', '02', '01', '01', '02.10.02.01.01', 'Alat Khusus Kepolisian', '2021-08-06 04:24:43'),
('7a372c6f9182b362e94fc1d0e42f443b', '02', '10', '02', '01', '10', '02.10.02.01.10', 'Alat Khusus Keamanan Lainnya', '2021-08-06 04:24:43'),
('88bbea84e910efd1cf05af7bfd5900cc', '02', '10', '02', '01', '11', '02.10.02.01.11', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('7a6929d43ba0d1690d6df4b935e064a7', '02', '10', '02', '02', '*', '02.10.02.02.*', 'Non Senjata Api', '2021-08-06 04:24:43'),
('0015c566c8be13520076013c9a3a3580', '02', '10', '02', '02', '01', '02.10.02.02.01', 'Celurit', '2021-08-06 04:24:43'),
('85390871b226e01dafae2a375b110e77', '02', '10', '02', '02', '02', '02.10.02.02.02', 'Keris', '2021-08-06 04:24:43'),
('cea44a8216423303fc95cea6efc67513', '02', '10', '02', '02', '03', '02.10.02.02.03', 'Rencong', '2021-08-06 04:24:43'),
('bf1b202833a8f6ff291a64e4c2c77889', '02', '10', '02', '02', '04', '02.10.02.02.04', 'Kelewang', '2021-08-06 04:24:43'),
('ad997579bd3a43ea47a297f21e46ac83', '02', '10', '02', '02', '05', '02.10.02.02.05', 'Golok', '2021-08-06 04:24:43'),
('a527530c0b57cb3d2e15ea8825e74879', '02', '10', '02', '02', '06', '02.10.02.02.06', 'Samurai', '2021-08-06 04:24:43'),
('a4b97c12bb71a26534fef94602a784c3', '02', '10', '02', '02', '07', '02.10.02.02.07', 'Sangkur', '2021-08-06 04:24:43'),
('db0e9b0a38a8562667399f6b82f99951', '02', '10', '02', '02', '08', '02.10.02.02.08', 'Pentung', '2021-08-06 04:24:43'),
('8929417e38fa90fd106a0202cc6bbe98', '02', '10', '02', '02', '09', '02.10.02.02.09', 'Bumerang', '2021-08-06 04:24:43'),
('ec9f2688bd7f686f0d4e3b3f0f1aa7c3', '02', '10', '02', '02', '10', '02.10.02.02.10', 'Pisau Belati', '2021-08-06 04:24:43'),
('c15a7d919f05f0fdec2fc8ae0d2d09d2', '02', '10', '02', '02', '11', '02.10.02.02.11', 'Tongkat Kejut', '2021-08-06 04:24:43'),
('3d75c0bc8700c582d3992f57194137dd', '02', '10', '02', '02', '12', '02.10.02.02.12', 'Gas Air MataxgmringxStick Gas', '2021-08-06 04:24:43'),
('1d0b6b204c275119755479fc7fb45ed2', '02', '10', '02', '02', '13', '02.10.02.02.13', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('877affe9e6e0aa67c1a296755c2aa6de', '02', '10', '03', '*', '*', '02.10.03.*.*', 'AMUNIISI', '2021-08-06 04:24:43'),
('3a0509fd250da57a5eb54c7398fece68', '03', '*', '*', '*', '*', '03.*.*.*.*', 'GOLONGAN GEDUNG DAN BANGUNAN', '2021-08-06 04:24:43'),
('9bdde023a42ae7a21171b06318e4f2e4', '03', '11', '*', '*', '*', '03.11.*.*.*', 'BANGUNAN GEDUNG', '2021-08-06 04:24:43'),
('11c4142460492d6fa8e7d1072bf2da19', '03', '11', '01', '*', '*', '03.11.01.*.*', 'BANGUNAN GEDUNG TEMPAT KERJA', '2021-08-06 04:24:43'),
('1f2951ba7fe38c12d3a3a6c332c28947', '03', '11', '01', '01', '*', '03.11.01.01.*', 'BANGUNAN GEDUNG KANTOR', '2021-08-06 04:24:43'),
('1fd6db22086c49822100871a3d9dd60d', '03', '11', '01', '01', '01', '03.11.01.01.01', 'Bangunan Gedung Kantor Permanen', '2021-08-06 04:24:43'),
('eb1775ff23d5005e85368ea9868c9a1d', '03', '11', '01', '01', '02', '03.11.01.01.02', 'Bangunan Gedung Kantor Semi Permanen', '2021-08-06 04:24:43'),
('b1d621c5f453f59f1fab852819da4674', '03', '11', '01', '01', '03', '03.11.01.01.03', 'Bangunan Gedung Kantor Darurat', '2021-08-06 04:24:43'),
('4d8b7709f1f3c79f943947d9d95079ec', '03', '11', '01', '01', '04', '03.11.01.01.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('60954d13d3e00dda5b070b198ccc1c04', '03', '11', '01', '02', '*', '03.11.01.02.*', 'BANGUNAN GUDANG', '2021-08-06 04:24:43'),
('bffc6dfc0763828607337f07bcb9da30', '03', '11', '01', '02', '01', '03.11.01.02.01', 'Bangunan Gudang tertutup Permanen', '2021-08-06 04:24:43'),
('961971b494fc425abe9283a0f750e6d2', '03', '11', '01', '02', '02', '03.11.01.02.02', 'Bangunan Gudang tertutup Semi Permanen', '2021-08-06 04:24:43'),
('a552192af0800b42b0674663f1de284f', '03', '11', '01', '02', '03', '03.11.01.02.03', 'Bangunan Gudang tertutup Darurat', '2021-08-06 04:24:43'),
('fc4642c1d9a480c36bb7f39b16babba9', '03', '11', '01', '02', '04', '03.11.01.02.04', 'Bangunan Gudang terbuka Permanen', '2021-08-06 04:24:43'),
('e3e904a879045ee50a5486cf16d97d95', '03', '11', '01', '02', '05', '03.11.01.02.05', 'Bangunan Gudang terbuka Semi Permanen', '2021-08-06 04:24:43'),
('5f060422d1b943cd0e67120d7bcb295d', '03', '11', '01', '02', '06', '03.11.01.02.06', 'Bangunan Gudang terbuka Darurat', '2021-08-06 04:24:43'),
('f4078569603fe771dd8b6794f9bfc3ce', '03', '11', '01', '02', '07', '03.11.01.02.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1d3c9e428e8fe5be4035cf0747398c4c', '03', '11', '01', '05', '*', '03.11.01.05.*', 'BAGUNNAN GEDUNG LABORATORIUM', '2021-08-06 04:24:43'),
('fe544c8b8a866eec3d79f5c925c8482f', '03', '11', '01', '05', '01', '03.11.01.05.01', 'Bangunan Gedung Laboratorium Permanen', '2021-08-06 04:24:43'),
('a6ebad53afefabcee64712365df42dcf', '03', '11', '01', '05', '02', '03.11.01.05.02', 'Bangunan Gedung Laboratorium Semi Permanen', '2021-08-06 04:24:43'),
('b1287655ad7a9281572682e56476db2b', '03', '11', '01', '05', '03', '03.11.01.05.03', 'Bangunan Gedung Laboratorium Darurat', '2021-08-06 04:24:43'),
('f4e57f413cd56ee74d06bb9021e40e49', '03', '11', '01', '05', '04', '03.11.01.05.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('8e1c6a78bcf51aa7e2e040fb603d0174', '03', '11', '01', '08', '*', '03.11.01.08.*', 'BANGUNAN GEDUNG TEMPAT IBADAH', '2021-08-06 04:24:43'),
('f7c4e44434774a625a5148bc0d7da51d', '03', '11', '01', '08', '01', '03.11.01.08.01', 'Bangunan Tempat Ibadah Permanen', '2021-08-06 04:24:43'),
('b23ca7853c9353e9411c4cca2ed06d55', '03', '11', '01', '08', '02', '03.11.01.08.02', 'Bangunan Tempat Ibadah Semi Permanen', '2021-08-06 04:24:43'),
('4f0b6b1de049a5a591d7ee93abfbd032', '03', '11', '01', '08', '03', '03.11.01.08.03', 'Bangunan Tempat Ibadah Darurat', '2021-08-06 04:24:43'),
('f75cee6c2c9e15a58c01d970906ecf75', '03', '11', '01', '08', '04', '03.11.01.08.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('519dda1d4d81b887a48d23c68f3b22f5', '03', '11', '01', '09', '*', '03.11.01.09.*', 'BANGUNAN GEDUNG TEMPAT PERTEMUAN', '2021-08-06 04:24:43'),
('539adb0fa0a642b11eb731c1dd959e40', '03', '11', '01', '09', '01', '03.11.01.09.01', 'Bangunan Gedung Pertemuan Permanen', '2021-08-06 04:24:43'),
('ecf8ed591c8655f737f6e5975e932d9e', '03', '11', '01', '09', '02', '03.11.01.09.02', 'Bangunan Gedung Pertemuan Semi Permanen', '2021-08-06 04:24:43'),
('48934440a6d6903c160719ff5718b6f4', '03', '11', '01', '09', '03', '03.11.01.09.03', 'Bangunan Gedung Pertemuan Darurat', '2021-08-06 04:24:43'),
('9244f405f3de3a5a80359ca83eed4bbe', '03', '11', '01', '09', '04', '03.11.01.09.04', 'Bangunan Gedung HiburanxgmringxKesenian Permanen', '2021-08-06 04:24:43'),
('1b4ed4369410f6e2f3cece6849c4a61b', '03', '11', '01', '09', '05', '03.11.01.09.05', 'Bangunan Gedung HiburanxgmringxKesenian Semi Permanen', '2021-08-06 04:24:43'),
('1a5d2467edb5d23276913e58f53d60f3', '03', '11', '01', '09', '06', '03.11.01.09.06', 'Bangunan Gedung HiburanxgmringxKesenian Darurat', '2021-08-06 04:24:43'),
('37d37fe91be5a7a93fd0d5a63aefe4b8', '03', '11', '01', '09', '07', '03.11.01.09.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('973eee87d7214072de95d0de1a1a88f8', '03', '11', '01', '10', '*', '03.11.01.10.*', 'BANGUNAN GEDUNG TEMPAT PENDIDIKAN', '2021-08-06 04:24:43'),
('5f9f9b090696f1d415f2b7c14371048e', '03', '11', '01', '10', '01', '03.11.01.10.01', 'Bangunan Gedung Pendidikan Permanen', '2021-08-06 04:24:43'),
('729e94dc34693b166801bf3f9dd520cc', '03', '11', '01', '10', '02', '03.11.01.10.02', 'Bangunan Gedung Pendidikan Semi Permanen', '2021-08-06 04:24:43'),
('a35dfd0a37c79f900ed4775d0103759e', '03', '11', '01', '10', '03', '03.11.01.10.03', 'Bangunan Gedung Pendidikan Darurat', '2021-08-06 04:24:43'),
('416d2cf04865e9889567ec5b3d2b23a9', '03', '11', '01', '10', '04', '03.11.01.10.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('617177ff368b928a46f08019fdf152bc', '03', '11', '01', '11', '*', '03.11.01.11.*', 'BANGUNAN GEDUNG TEMPAT OLAH RAGA', '2021-08-06 04:24:43'),
('b7783dc3f0123eacb4375999430597db', '03', '11', '01', '11', '01', '03.11.01.11.01', 'Bangunan Olah Raga Tertutup Permanen', '2021-08-06 04:24:43'),
('207be80868b12fa5f753d4746187602d', '03', '11', '01', '11', '02', '03.11.01.11.02', 'Bangunan Olah Raga Tertutup Semi Permanen', '2021-08-06 04:24:43'),
('38f1446eff895afe6962a243b9f8660c', '03', '11', '01', '11', '03', '03.11.01.11.03', 'Bangunan Olah Raga Tertutup Darurat', '2021-08-06 04:24:43'),
('85e88684a3136061661867f1f226602c', '03', '11', '01', '11', '04', '03.11.01.11.04', 'Bangunan Olah Raga Terbuka Permanen', '2021-08-06 04:24:43'),
('1ecafd3212365df1814e6f9a14aabf77', '03', '11', '01', '11', '05', '03.11.01.11.05', 'Bangunan Olah Raga Terbuka Semi Permanen', '2021-08-06 04:24:43'),
('3554a8add9416f2f14f7cf466247e867', '03', '11', '01', '11', '06', '03.11.01.11.06', 'Bangunan Olah Raga Terbuka Darurat', '2021-08-06 04:24:43'),
('7ab0ac3aec0c46334eb492b25d181479', '03', '11', '01', '11', '07', '03.11.01.11.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('246cc77b330e63e133301789f1b4c1f2', '03', '11', '01', '12', '*', '03.11.01.12.*', 'BANGUNAN GEDUNG PERTOKOANxgmringxKOPERASI PASAR', '2021-08-06 04:24:43'),
('b49dd8cbbe059426136ea3d280f0f171', '03', '11', '01', '12', '01', '03.11.01.12.01', 'Gedung PertokoanxgmringxKoperasi Pasar Permanen', '2021-08-06 04:24:43'),
('f8034baa20178e3009205d37e4cbd15d', '03', '11', '01', '12', '02', '03.11.01.12.02', 'Gedung PertokoanxgmringxKoperasi Pasar Semi Permanen', '2021-08-06 04:24:43'),
('4a9fd166775f9a43131a717459bfd546', '03', '11', '01', '12', '03', '03.11.01.12.03', 'Gedung PertokoanxgmringxKoperasi Pasar Darurat', '2021-08-06 04:24:43'),
('d478c438736412b9f04cab4222d954ec', '03', '11', '01', '12', '04', '03.11.01.12.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('8c7b2c0d5847d41f1b240a94b07563d4', '03', '11', '01', '13', '*', '03.11.01.13.*', 'BANGUNAN GEDUNG UNTUK POS JAGA', '2021-08-06 04:24:43'),
('31d72dadc35223e4b58d091b050276da', '03', '11', '01', '13', '01', '03.11.01.13.01', 'Gedung Pos Jaga Permanen', '2021-08-06 04:24:43'),
('7be0d69965bb03e1be4d4b831834d7b4', '03', '11', '01', '13', '02', '03.11.01.13.02', 'Gedung Pos Jaga Semi Permanen', '2021-08-06 04:24:43'),
('66f392077235a8bfa3a71a9a7ef2ebac', '03', '11', '01', '13', '03', '03.11.01.13.03', 'Gedung Pos Jaga Darurat', '2021-08-06 04:24:43'),
('cb262e50c76bbcd1ad8658ee904d0c84', '03', '11', '01', '13', '04', '03.11.01.13.04', 'Gedung Menara Peninjau Permanen', '2021-08-06 04:24:43'),
('d3d9efb53759250afb308991f81981c2', '03', '11', '01', '13', '05', '03.11.01.13.05', 'Gedung Menara Peninjau Semi Permanen', '2021-08-06 04:24:43'),
('720becb63d263a357ccd566bc68dfb94', '03', '11', '01', '13', '06', '03.11.01.13.06', 'Gedung Menara Peninjau Darurat', '2021-08-06 04:24:43'),
('74d5f4f6767b8bfe6b7d7e2ca815bb3d', '03', '11', '01', '13', '07', '03.11.01.13.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('09c8f6deea9411e904a0a56961ca3d07', '03', '11', '01', '14', '*', '03.11.01.14.*', 'BANGUNAN GEDUNG GARASIxgmringx POOL', '2021-08-06 04:24:43'),
('66b747ad234dea74867211cf3236f5c8', '03', '11', '01', '14', '01', '03.11.01.14.01', 'Gedung GarasixgmringxPool Permanen', '2021-08-06 04:24:43'),
('78fb4998c312399dead01e1317e24b54', '03', '11', '01', '14', '02', '03.11.01.14.02', 'Gedung GarasixgmringxPool Semi Permanen', '2021-08-06 04:24:43'),
('f490a9f35cc1d3d3da88bf4805984f52', '03', '11', '01', '14', '03', '03.11.01.14.03', 'Gedung GarasixgmringxPool Darurat', '2021-08-06 04:24:43'),
('460e0b234d2cadb502f24a5374ec4f52', '03', '11', '01', '14', '04', '03.11.01.14.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('ee4f71fdb9dfe281d9a5aee7ef5308da', '03', '11', '01', '18', '*', '03.11.01.18.*', 'BANGUNAN KANDANG HEWANxgmringxTERNAK', '2021-08-06 04:24:43'),
('36190990b937035cc9a52ceef55d45cb', '03', '11', '01', '18', '01', '03.11.01.18.01', 'Bangunan Kandang HewanxgmringxTernak Permanen', '2021-08-06 04:24:43'),
('ce716dfa54e7850685ea7bdac17f4159', '03', '11', '01', '18', '02', '03.11.01.18.02', 'Bangunan Kandang HewanxgmringxTernak Semi Permanen', '2021-08-06 04:24:43'),
('ef5033b8fb492a39b4be5eac1782ece5', '03', '11', '01', '18', '03', '03.11.01.18.03', 'Bangunan Kandang HewanxgmringxTernak Darurat', '2021-08-06 04:24:43'),
('3520f56f3990d3923cddc0c74a1d198e', '03', '11', '01', '18', '04', '03.11.01.18.04', 'Bangunan Kandang Observasi Permanen', '2021-08-06 04:24:43'),
('7852af2e6847fedc0daf2686c7402935', '03', '11', '01', '18', '05', '03.11.01.18.05', 'Bangunan Kandang Observasi Semi Permanen', '2021-08-06 04:24:43'),
('f2595253b9e1533301143a173fbf366c', '03', '11', '01', '18', '06', '03.11.01.18.06', 'Bangunan Kandang Observasi Darurat', '2021-08-06 04:24:43'),
('e133f92ed67b3b17be337946518b3b3d', '03', '11', '01', '18', '07', '03.11.01.18.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('acf88c3afed5d49dee1d6c3e5d8a670b', '03', '11', '01', '19', '*', '03.11.01.19.*', 'BANGUNAN GEDUNG PERPUSTAKAAN', '2021-08-06 04:24:43'),
('365f0fb2931eb6f6e21c09a2e01b13c8', '03', '11', '01', '19', '01', '03.11.01.19.01', 'Bangunan Gedung Perpustakaan Permanen', '2021-08-06 04:24:43'),
('551008ebdca6eded595f97e060c635e0', '03', '11', '01', '19', '02', '03.11.01.19.02', 'Bangunan Gedung Perpustakaan Semi Permanen', '2021-08-06 04:24:43'),
('03be4064ecc8ab2cdae8c7869d943bb5', '03', '11', '01', '19', '03', '03.11.01.19.03', 'Bangunan Gedung Perpustakaan Darurat', '2021-08-06 04:24:43'),
('89d07748089f528aa19ba17421a21d79', '03', '11', '01', '19', '04', '03.11.01.19.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('2ad4c3bb328f9490f7d773691fd22a15', '03', '11', '01', '22', '*', '03.11.01.22.*', 'BANGUNAN PENGUJIAN KELAIKAN', '2021-08-06 04:24:43'),
('e4c13854bafef092c625500f3aff2f82', '03', '11', '01', '22', '01', '03.11.01.22.01', 'Bangunan Pengujian Kelaikan Permanen', '2021-08-06 04:24:43'),
('c6420c6540684156856abbf28a637299', '03', '11', '01', '22', '02', '03.11.01.22.02', 'Bangunan Pengujian Kelaikan Semi Permanen', '2021-08-06 04:24:43'),
('c7eabe21d2bba7de0f25eb5844479b39', '03', '11', '01', '22', '03', '03.11.01.22.03', 'Bangunan Pengujian Kelaikan Darurat', '2021-08-06 04:24:43'),
('dbaeb3e8da346cce6e561647ebf7a4ff', '03', '11', '01', '22', '04', '03.11.01.22.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('a86c673c96c79da80d09082eccc9faf3', '03', '11', '01', '27', '*', '03.11.01.27.*', 'Bangunan Ged. Tempat Kerja Lainnya', '2021-08-06 04:24:43'),
('5adc36460006693b744ac4128afb079e', '03', '11', '01', '27', '01', '03.11.01.27.01', 'Bangunan Gedung Tempat Kerja Lainnya Permanen', '2021-08-06 04:24:43'),
('25c4324ab5c603adff37fa4726d25739', '03', '11', '01', '27', '02', '03.11.01.27.02', 'Bangunan Gedung Tempat Kerja Lainnya Semi Permanen', '2021-08-06 04:24:43'),
('e2e134a2afa8e5e1c9cb0ca2231a438d', '03', '11', '01', '27', '03', '03.11.01.27.03', 'Bangunan Gedung Tempat Kerja Lainnya Darurat', '2021-08-06 04:24:43'),
('b3c49b000a6eb4d39dbd36164a3cbc0e', '03', '11', '01', '27', '04', '03.11.01.27.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('e9c4327ef1097735dfb9039f3771df03', '03', '11', '02', '01', '*', '03.11.02.01.*', 'BANGUNAN GEDUNG TEMPAT TINGGAL', '2021-08-06 04:24:43'),
('deca7592e468a721743066bea624b639', '03', '11', '02', '01', '01', '03.11.02.01.01', 'Rumah Negara Golongan I Type A Permanen', '2021-08-06 04:24:43'),
('daa197b7c968b76a5a4f8dc945d3df3d', '03', '11', '02', '01', '02', '03.11.02.01.02', 'Rumah Negara Golongan I Type A Semi Permanen', '2021-08-06 04:24:43'),
('5e797fe11b958aac91c06ac443baf387', '03', '11', '02', '01', '15', '03.11.02.01.15', 'Rumah Negara Golongan I Type E Darurat', '2021-08-06 04:24:43'),
('8b0ac9f19a03c042131bddff1a75c8a4', '03', '11', '02', '01', '16', '03.11.02.01.16', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('68b1bb953c96e800d87f65e6013030e4', '03', '11', '02', '02', '*', '03.11.02.02.*', 'Rumah Negara Golongan II', '2021-08-06 04:24:43'),
('616f924a31aca117e0aa325a68cedebc', '03', '11', '02', '02', '01', '03.11.02.02.01', 'Rumah Negara Golongan II Type A Permanen', '2021-08-06 04:24:43'),
('b1038371b36771ac271f6037130596d5', '03', '11', '02', '02', '14', '03.11.02.02.14', 'Rumah Negara Golongan II Type E Semi Permanen', '2021-08-06 04:24:43'),
('8ef1730efec4ad5d0a43f4b39241ebcd', '03', '11', '02', '02', '15', '03.11.02.02.15', 'Rumah Negara Golongan II Type E Darurat', '2021-08-06 04:24:43'),
('5c9649389d0533b7579432e0acfec356', '03', '11', '02', '02', '16', '03.11.02.02.16', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('97f846af68eb8ebb1527442c423c9315', '03', '11', '02', '03', '*', '03.11.02.03.*', 'Rumah Negara Golongan III', '2021-08-06 04:24:43'),
('c7d84ea240e5592b3cfb7b819a59aa3e', '03', '11', '02', '03', '01', '03.11.02.03.01', 'Rumah Negara Golongan III Type A Permanen', '2021-08-06 04:24:43'),
('e584fb48ea9064872f7256609636eed3', '03', '11', '02', '03', '15', '03.11.02.03.15', 'Rumah Negara Golongan III Type E Semi Permanen', '2021-08-06 04:24:43'),
('9bc7e944dd845fe68e18fb797e3fa070', '03', '11', '02', '03', '16', '03.11.02.03.16', 'Rumah Negara Golongan III Type E Darurat', '2021-08-06 04:24:43'),
('34a0518b45ef5a409bbced19c512d1ce', '03', '11', '02', '03', '17', '03.11.02.03.17', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b70547228299daf894b74eee001d629f', '03', '11', '03', '*', '*', '03.11.03.*.*', 'BANGUNAN MENARA', '2021-08-06 04:24:43'),
('245aaf4f7723ee65e7b9a2a86a0d0f63', '03', '12', '*', '*', '*', '03.12.*.*.*', 'MONUMEN', '2021-08-06 04:24:43'),
('5c355d082bdeb54485e2d151328e13c9', '03', '12', '01', '*', '*', '03.12.01.*.*', 'Bangunan Bersejarah', '2021-08-06 04:24:43'),
('19e284158a09713ef8fdc489bcaa8afc', '03', '12', '02', '*', '*', '03.12.02.*.*', 'TUGU PERINGATAN', '2021-08-06 04:24:43'),
('c2494f5d553883ad889092ded342c1b8', '03', '12', '02', '01', '*', '03.12.02.01.*', 'Tugu Kemerdekaan', '2021-08-06 04:24:43'),
('125326d5ec7ee0bb87fd9a2a5c30a513', '03', '12', '02', '01', '01', '03.12.02.01.01', 'Tugu Kemerdekaan', '2021-08-06 04:24:43'),
('d3d201cd59b5cc091eb918d78e28f77b', '03', '12', '02', '01', '02', '03.12.02.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('7e89268a9777a05157cb481a9dfd4eec', '03', '12', '0', '0', '0', '03.12.0 .0 .0', '', '2021-08-06 04:24:43'),
('542844c24a07bd0291b880455544945b', '03', '12', '02', '02', '*', '03.12.02.02.*', 'Tugu Pembangunan', '2021-08-06 04:24:43'),
('c56050b0ce725ef131ee266f709e8a4b', '03', '12', '02', '02', '01', '03.12.02.02.01', 'Tugu Pembangunan', '2021-08-06 04:24:43'),
('7af9c041a44e9230879444f988a1bcd8', '03', '12', '02', '02', '02', '03.12.02.02.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('877f9d8350bc05444c4efdc0b5055a75', '03', '12', '02', '03', '*', '03.12.02.03.*', 'Tugu Peringatan Lainnya', '2021-08-06 04:24:43'),
('8f2f56d512799339106417563038d99e', '03', '12', '02', '03', '01', '03.12.02.03.01', 'Tugu Peringatan Lainnya', '2021-08-06 04:24:43'),
('f388bf2e6b109d36690a52c869f735ff', '03', '12', '02', '03', '02', '03.12.02.03.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('eaec9ba8e821bf8d9a4d8a7a0b186af6', '03', '12', '03', '01', '*', '03.12.03.01.*', 'CANDI', '2021-08-06 04:24:43'),
('d9c4ad2d2fec1a9ac123b2475aaf2f26', '03', '12', '04', '*', '*', '03.12.04.*.*', 'MONUMENxgmringxBANGUNAN BERSEJARAH', '2021-08-06 04:24:43'),
('98a01f5e8eef81b2a12ed349b98e60b8', '03', '12', '04', '01', '*', '03.12.04.01.*', 'Bangunan Bersejarah Lainnya', '2021-08-06 04:24:43'),
('06babf46a8ea9380ab14e12b604aa3c9', '03', '12', '04', '01', '01', '03.12.04.01.01', 'Bangunan Bersejarah Lainnya', '2021-08-06 04:24:43'),
('d8901389e644151ba05d13cc6c129836', '03', '12', '04', '01', '02', '03.12.04.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('79e2e291cd8e13092d380a36d60cce32', '03', '12', '05', '01', '*', '03.12.05.01.*', 'TUGU PERINGATAN', '2021-08-06 04:24:43'),
('92edf2f0c53ee892c81bba27f607d23f', '03', '12', '05', '01', '01', '03.12.05.01.01', 'TUGU PERINGATAN', '2021-08-06 04:24:43'),
('c64ff852ea1e918480bb4b1d57afc455', '03', '12', '05', '01', '02', '03.12.05.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('4a9981b370d77b5b6febbb624ff6bb52', '03', '12', '06', '01', '*', '03.12.06.01.*', 'TUGU TITIK KONTROLxgmringxPASTI', '2021-08-06 04:24:43'),
('bf45e43ba07e651d6a1863c4f701a7ca', '03', '12', '06', '01', '01', '03.12.06.01.01', 'Tuguxgmringx tanda batas Administrasi Negara', '2021-08-06 04:24:43'),
('ea3e867788435a7376322a540041b4d1', '03', '12', '06', '01', '09', '03.12.06.01.09', 'Tuguxgmringx Tanda Batas Administrasi Kepemilikan', '2021-08-06 04:24:43'),
('e7a01f4f8b88c9ce92e2ced46f060e2b', '03', '12', '06', '01', '10', '03.12.06.01.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('0bc60987712602d83255a4921b52f027', '03', '12', '07', '*', '*', '03.12.07. *. *', 'RAMBUxstrixRAMBU', '2021-08-06 04:24:43'),
('091ac50653203b4808b6567f34a43b6c', '03', '12', '07', '01', '*', '03.12.07.01. *', 'Rambu Bersuar Lalu Lintas Darat', '2021-08-06 04:24:43'),
('e1677abc584b585189a529048e51b7b7', '03', '12', '07', '02', '*', '03.12.07.02. *', 'Rambu Tidak Bersuar', '2021-08-06 04:24:43'),
('3623d4e663ebba1e21a48086d8175c7f', '04', '*', '*', '*', '*', '04.*.*.*.*', 'GOLONGAN JALAN, IRIGASI DAN JARINGAN', '2021-08-06 04:24:43'),
('404d0423449c265de25c357d2a2b70b5', '04', '13', '*', '*', '*', '04.13.*.*.*', 'JALAN DAN JEMBATAN', '2021-08-06 04:24:43'),
('1b3625a9643875bd958874a1def1cd9c', '04', '13', '01', '05', '*', '04.13.01.05.*', 'Jalan Khusus', '2021-08-06 04:24:43'),
('ed96e7208f14b3eb631a5d0bd42ea3de', '04', '13', '01', '05', '01', '04.13.01.05.01', 'Jalan Khusus', '2021-08-06 04:24:43'),
('58f8b30d2d3e331a80507b6ec99183c2', '04', '13', '01', '05', '08', '04.13.01.05.08', 'Jalan Khusus Perorangan', '2021-08-06 04:24:43'),
('a016ae0d883eabe87faa622fd3593f52', '04', '13', '01', '05', '09', '04.13.01.05.09', 'Jalan Khusus Under Pass', '2021-08-06 04:24:43'),
('859e0dc99e5301fe47cd9e5dd5ab96c3', '04', '13', '01', '05', '10', '04.13.01.05.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('09e2cb88cd9c1deff6128076cf131ec9', '04', '13', '02', '*', '*', '04.13.02.*.*', 'JEMBATAN', '2021-08-06 04:24:43'),
('b64def49c15597f0a2e14e7cf0dfef3a', '04', '13', '02', '05', '*', '04.13.02.05.*', 'Jembatan Khusus', '2021-08-06 04:24:43'),
('11e42581ffd52b9e18afc05e9b33ff5c', '04', '13', '02', '05', '01', '04.13.02.05.01', 'Jembatan Beton', '2021-08-06 04:24:43'),
('f880fa4cbd07eaa0a8eef41f93255738', '04', '13', '02', '05', '02', '04.13.02.05.02', 'Jembatan Baja', '2021-08-06 04:24:43'),
('b106c50d84919cd4874faf78fa782d88', '04', '13', '02', '05', '03', '04.13.02.05.03', 'Jembatan Kayu', '2021-08-06 04:24:43'),
('3001359839f4094fcdc37fb3a97b9771', '04', '13', '02', '05', '04', '04.13.02.05.04', 'Jembatan Baliy', '2021-08-06 04:24:43'),
('8ab3ba59947c2614648b753b572a3951', '04', '13', '02', '05', '05', '04.13.02.05.05', 'Jembatan Pas Batu', '2021-08-06 04:24:43'),
('9438dc610f8060910d4517fd9e5c09db', '04', '13', '02', '05', '14', '04.13.02.05.14', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('efec906dde33bfcb3c8d64c77d8d7c7c', '04', '14', '*', '*', '*', '04.14.*.*.*', 'BANGUNAN AIRxgmringxIRIGASI', '2021-08-06 04:24:43'),
('375b271448f4a8b30b66d69829b3f0db', '04', '14', '01', '*', '*', '04.14.01.*.*', 'Bangunan Air Irigasi', '2021-08-06 04:24:43'),
('add737d38c4b591e15151fa7e1f41087', '04', '14', '01', '01', '*', '04.14.01.01.*', 'Bangunan Waduk', '2021-08-06 04:24:43'),
('7caea2ace0e0be2f456d0dd5c7e0050b', '04', '14', '02', '01', '*', '04.14.02.01.*', 'BANGUNAN AIR PASANG SURUT', '2021-08-06 04:24:43'),
('615d302fb0bb9af71e7bd778bdd0e658', '04', '14', '06', '01', '*', '04.14.06.01.*', 'BANGUNAN AIR BERSIHxgmringxBAKU', '2021-08-06 04:24:43'),
('099f34004593fef10f35c61b94a7d1d0', '04', '14', '06', '01', '01', '04.14.06.01.01', 'Waduk Penyimpanan Air Baku', '2021-08-06 04:24:43'),
('df4541f8702a6222da1ca2b21d225642', '04', '14', '06', '01', '02', '04.14.06.01.02', 'Waduk Penyimpanan Air Hujan', '2021-08-06 04:24:43'),
('64af046b8eb6046820220b0a6fe252f6', '04', '14', '06', '01', '03', '04.14.06.01.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('88a4936b7f9d4431507a65b64c6be415', '04', '14', '06', '02', '*', '04.14.06.02.*', 'Bangunan Pengambilan Air BersihxgmringxBaku', '2021-08-06 04:24:43'),
('58c68ec7f5bcab95e099ccd9e47af9ff', '04', '14', '07', '05', '*', '04.14.07.05.*', 'Bangunan Pelengkap Air Kotor', '2021-08-06 04:24:43'),
('0e02b48970f022b8bc1b9c5412f0fc00', '04', '14', '07', '05', '01', '04.14.07.05.01', 'Bangunan Talang', '2021-08-06 04:24:43'),
('030c45cb139b1cc1db9e032f747f016c', '04', '14', '07', '05', '02', '04.14.07.05.02', 'Bangunan Syphon', '2021-08-06 04:24:43'),
('4d56a487acc410f26b669552f7bc4ca6', '04', '14', '07', '05', '03', '04.14.07.05.03', 'Bangunan Gorongxstrixgorong', '2021-08-06 04:24:43'),
('6faedb213545cd97ad6e3fffad908b61', '04', '14', '07', '05', '04', '04.14.07.05.04', 'Bangunan Jembatan', '2021-08-06 04:24:43'),
('89897853ae0dbcaed11bb08a6c0c5af6', '04', '14', '07', '05', '05', '04.14.07.05.05', 'Bangunan Air Kotor Saluran Dari Rumah', '2021-08-06 04:24:43'),
('dd736663ebaa3f1675bfa4d8c97de110', '04', '14', '07', '05', '06', '04.14.07.05.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('f051fc4d6a0b534e7e3b377834ac96ce', '04', '14', '08', '*', '*', '04.14.08.*.*', 'BANGUNAN AIR', '2021-08-06 04:24:43'),
('5a0788954c93a5d3e48fede50408e6f5', '04', '14', '08', '01', '*', '04.14.08.01.*', 'Bangunan Air Laut', '2021-08-06 04:24:43'),
('e3273754aebcf379df90968b0aaf6092', '04', '14', '08', '01', '01', '04.14.08.01.01', 'Pelabuhan', '2021-08-06 04:24:43'),
('8d57f191d993810f973900d2f79d46f6', '04', '14', '08', '01', '02', '04.14.08.01.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1720880a21b7227b4a8726ccd5ac1335', '04', '14', '08', '02', '*', '04.14.08.02.*', 'Bangunan Air Tawar', '2021-08-06 04:24:43'),
('81121b0023ac9e83960ef2e7ee929966', '04', '14', '08', '02', '01', '04.14.08.02.01', 'Dermaga', '2021-08-06 04:24:43'),
('513b5d3f12d6b0c61ed54c096d88a90b', '04', '14', '08', '02', '02', '04.14.08.02.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3854d2bf63bba36902cd3427b6408929', '04', '15', '*', '*', '*', '04.15.*.*.*', 'INSTALASI', '2021-08-06 04:24:43'),
('b329fe7d972fb1e08221268a7ab28a1d', '04', '15', '01', '*', '*', '04.15.01.*.*', 'INSTALASI AIR MINUMxgmringxBERSIH', '2021-08-06 04:24:43'),
('bbe5a6cbceda0ce2faee45ea849f62ff', '04', '15', '01', '01', '*', '04.15.01.01.*', 'Air Muka Tanah', '2021-08-06 04:24:43'),
('2e335a62102e8bc801996fd95872699d', '04', '15', '01', '01', '01', '04.15.01.01.01', 'Air Muka Tanah Kapasitas Kecil', '2021-08-06 04:24:43'),
('a4cd8428b40bdd5d9815f6296ae0861f', '04', '15', '01', '01', '02', '04.15.01.01.02', 'Air Muka Tanah Kapasitas Sedang', '2021-08-06 04:24:43'),
('38f16f91a6de1bfd72770161d6408ffa', '04', '15', '01', '01', '03', '04.15.01.01.03', 'Air Muka Tanah Kapasitas Besar', '2021-08-06 04:24:43'),
('a559fb74d170b07cf5a4a75dc46673fb', '04', '15', '01', '01', '04', '04.15.01.01.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('4380b95188f8adc1454ec5674e30e0d5', '04', '15', '01', '02', '*', '04.15.01.02.*', 'Air SumberxgmringxMata Air', '2021-08-06 04:24:43'),
('c60806c98eb7e12c7c218564f41c9c5b', '04', '15', '01', '02', '01', '04.15.01.02.01', 'Air Sumber Kapasitas Kecil', '2021-08-06 04:24:43'),
('0f8dc463d918cb8cb11b05acbac4e02a', '04', '15', '01', '02', '02', '04.15.01.02.02', 'Air Sumber Kapasitas Sedang', '2021-08-06 04:24:43'),
('8ca9aa14e5a2ef58097a58328fd71ca6', '04', '15', '01', '02', '03', '04.15.01.02.03', 'Air Sumber Kapasitas Besar', '2021-08-06 04:24:43'),
('b4ae7bbd5dc4577f71aa63c218028739', '04', '15', '01', '02', '04', '04.15.01.02.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('cf9693f7781d3270a4fa92b19e80c232', '04', '15', '01', '03', '*', '04.15.01.03.*', 'Air Tanah Dalam', '2021-08-06 04:24:43'),
('3c7bdc65ab08664f37aa3506d9be9005', '04', '15', '01', '03', '01', '04.15.01.03.01', 'Air Tanah Dalam Kapasitas Kecil', '2021-08-06 04:24:43'),
('6f71fa6c99fc5054f13d5896f5281a30', '04', '15', '01', '03', '02', '04.15.01.03.02', 'Air Tanah Dalam Kapasitas Sedang', '2021-08-06 04:24:43'),
('0a42808d6164f16a2b1c98f5d8e53472', '04', '15', '01', '03', '03', '04.15.01.03.03', 'Air Tanah Dalam Kapasitas Besar', '2021-08-06 04:24:43'),
('ece2fbeff24fd169dc89b95c0a54e504', '04', '15', '01', '03', '04', '04.15.01.03.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('bc723902e72b4ddf6068622624a6eecc', '04', '15', '01', '04', '*', '04.15.01.04. *', 'Instalasi Air Tanah Dangkal', '2021-08-06 04:24:43'),
('49c355d43a4946c4e852fe6cfdd2ddec', '04', '15', '01', '04', '01', '04.15.01.04.01', 'Instalasi Air Tanah  Dangkal Kpasitas Kecil', '2021-08-06 04:24:43'),
('60790b154a5c68bc6f01cb67906e665d', '04', '15', '01', '04', '02', '04.15.01.04.02', 'Instalasi Air Tanah Dangkal Kapasitas Sedang', '2021-08-06 04:24:43');
INSERT INTO `m_kib_kode` (`kd`, `golongan`, `bidang`, `kelompok`, `kelompok_sub`, `kelompok_sub_sub`, `kode`, `nama`, `postdate`) VALUES
('017cf17c9edb55a018c4116177ceca99', '04', '15', '01', '04', '03', '04.15.01.04.03', 'Instalasi Air Tanah Dangkal Kapasitas Besar', '2021-08-06 04:24:43'),
('a3f5d188ca0afd611344885c89238abc', '04', '15', '01', '04', '04', '04.15.01.04.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('83064367903bca67f822e7508be3e6cd', '04', '15', '01', '05', '*', '04.15.01.05.*', 'Instalasi Air BersihxgmringxAir Baku Lainnya', '2021-08-06 04:24:43'),
('832e442851d2daf204589784a5d59c97', '04', '15', '01', '05', '01', '04.15.01.05.01', 'Sistim Pengolahan Air Sederhana xkkurixSipasxkkurnanx', '2021-08-06 04:24:43'),
('d39ac5cbd9582c1be8ca894039a01ee4', '04', '15', '01', '05', '02', '04.15.01.05.02', 'Jaringan Rumah Tangga xkkurixJarutxkkurnanx', '2021-08-06 04:24:43'),
('6a1188e57121b55956fca15481d39ef5', '04', '15', '01', '05', '03', '04.15.01.05.03', 'Penampungan Air Hujan xkkurixPAMxkkurnanx', '2021-08-06 04:24:43'),
('4d2c4b9cf85000077d7eed9779ca2727', '04', '15', '01', '05', '04', '04.15.01.05.04', 'Sumur Gali xkkurixSGLxkkurnanx', '2021-08-06 04:24:43'),
('72a80e77d80816fe42ede2601ef21960', '04', '15', '01', '05', '05', '04.15.01.05.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('29ea4cc69c047f6e62658881d276e0fe', '04', '15', '05', '01', '*', '04.15.05.01.*', 'INSTALASI PEMBANGKIT LISTRIK', '2021-08-06 04:24:43'),
('2cd52756a908f21387f1d51cdba2c645', '04', '15', '05', '01', '01', '04.15.05.01.01', 'PLTA Kapasitas Kecil', '2021-08-06 04:24:43'),
('c30f9bcf7726f8ae0feaf24f6a4d52e8', '04', '15', '05', '01', '02', '04.15.05.01.02', 'PLTA Kapasitas Sedang', '2021-08-06 04:24:43'),
('aa420186f4ff740664301c1c2281f4b0', '04', '15', '05', '01', '03', '04.15.05.01.03', 'PLTA Kapasitas Besar', '2021-08-06 04:24:43'),
('3d941b646ce9312a41fbee949b6753ba', '04', '15', '05', '01', '04', '04.15.05.01.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('56007f1187c890366dd41dd6cab33a3a', '04', '15', '05', '09', '*', '04.15.05.09.*', 'Instalansi Pembangkit Litrik Tenaga Surya xkkurixPLTSxkkurnanx', '2021-08-06 04:24:43'),
('e2903f9bc7cdf5812b582c3d8a274306', '04', '15', '05', '09', '01', '04.15.05.09.01', 'Instalasi PLTS Kapasitas Kecil', '2021-08-06 04:24:43'),
('0347586253c3ad15f07b739a2c069f27', '04', '15', '05', '09', '02', '04.15.05.09.02', 'Instalasi PLTS Kapasitas Sedang', '2021-08-06 04:24:43'),
('a87cf9bb897a9e89c43f8fe2f918c64a', '04', '15', '05', '09', '03', '04.15.05.09.03', 'Instalasi PLTS Kapasitas Besaar', '2021-08-06 04:24:43'),
('f11b4ed9d479ef16f78469388c0b2a34', '04', '15', '05', '09', '04', '04.15.05.09.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('49337738fae205edcb049901715a4686', '04', '15', '05', '10', '*', '04.15.05.10.*', 'Instalasi Pembangkit Listrik Tenaga Biogas xkkurixPLTBxkkurnanx', '2021-08-06 04:24:43'),
('4e193b5b8b0385929f7593e8636a9b17', '04', '15', '05', '10', '01', '04.15.05.10.01', 'Instalasi PLTB Kapasitas Kecil', '2021-08-06 04:24:43'),
('a04e15d4c4491d32ac1b0b81246dd1c6', '04', '15', '05', '10', '02', '04.15.05.10.02', 'Instalasi PLTB Kapasitas Sedang', '2021-08-06 04:24:43'),
('61c5b148d702e78240bd7386fb46966e', '04', '15', '05', '10', '03', '04.15.05.10.03', 'Instalasi PLTB Kapasitas Besaar', '2021-08-06 04:24:43'),
('7ac8459c869a75a51a54b4bad0b11295', '04', '15', '05', '10', '04', '04.15.05.10.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('a985cd0be4a92f3900148fa8a1a8f42a', '04', '15', '06', '*', '*', '04.15.06.*.*', 'INSTALASI GARDU LISTRIK', '2021-08-06 04:24:43'),
('a6cec4ac6e496a3237988ff4196af5b0', '04', '15', '06', '01', '*', '04.15.06.01.*', 'Instalasi Gardu Listrik Induk', '2021-08-06 04:24:43'),
('0e109a0a30817053d0c8e30a19658b01', '04', '15', '06', '02', '*', '04.15.06.02.*', 'Instalasi Gardu Listrik Distribusi', '2021-08-06 04:24:43'),
('9210679ee20c7ffadc5188093ee35a29', '04', '15', '06', '03', '*', '04.15.06.03.*', 'Instalasi Pusat Pengatur Listrik', '2021-08-06 04:24:43'),
('a37a9fdd8d5fa721ffdc778e38dd46a5', '04', '15', '06', '03', '01', '04.15.06.03.01', 'Instalasi Pusat Pengatur Listrik Kapasitas Kecil', '2021-08-06 04:24:43'),
('c86c1207ca5bdc743343e2efc8aee0f3', '04', '15', '06', '03', '02', '04.15.06.03.02', 'Instalasi Pusat Pengatur Listrik Kapasitas Sedang', '2021-08-06 04:24:43'),
('40145934fd20c2a3cd7083fb9964f2e7', '04', '15', '06', '03', '03', '04.15.06.03.03', 'Instalasi Pusat Pengatur Listrik Kapasitas Besar', '2021-08-06 04:24:43'),
('01f17bd31dabf31661f30bc6672533ab', '04', '15', '06', '03', '04', '04.15.06.03.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('5df8a087120de70945f3262a97be9076', '04', '15', '09', '*', '*', '04.15.09.*.*', 'INSTALASI PENGAMAN', '2021-08-06 04:24:43'),
('0d1f18929bdc8dcc00ad54a015c52cdd', '04', '15', '09', '01', '*', '04.15.09.01.*', 'Instalasi Pengaman Penangkal Petir', '2021-08-06 04:24:43'),
('e58a429acecc57f2fb41694899d3be9a', '04', '15', '09', '01', '01', '04.15.09.01.01', 'Instalasi Penangkal Petir Manual', '2021-08-06 04:24:43'),
('a27cae2235090c18046084ea570fbf07', '04', '15', '09', '01', '02', '04.15.09.01.02', 'Instalasi Penangkal Petir Komputerisasi', '2021-08-06 04:24:43'),
('0345b63aedcac9e1ec68814423023b05', '04', '15', '09', '01', '03', '04.15.09.01.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('27acd41fba56c3f69c2be83b46c16d2d', '04', '16', '*', '*', '*', '04.16.*.*.*', 'JARINGAN', '2021-08-06 04:24:43'),
('d9bcbec12bc269c4130a4a0775097a8b', '04', '16', '01', '01', '*', '04.16.01.01.*', 'JARINGAN AIR MINUM', '2021-08-06 04:24:43'),
('7283ef5391960980a8393254c731169f', '04', '16', '01', '01', '01', '04.16.01.01.01', 'Jaringan Pembawa', '2021-08-06 04:24:43'),
('a643ff21697234772a0bfc57eca00c62', '04', '16', '01', '02', '*', '04.16.01.02.*', 'Jaringan Induk Distribusi', '2021-08-06 04:24:43'),
('d108109cd13e571ad2de8470caf56588', '04', '16', '01', '03', '*', '04.16.01.03.*', 'Jaringan Cabang Distribusi', '2021-08-06 04:24:43'),
('6dafc0dc5a4f847a3654281cb71d569a', '04', '16', '01', '04', '*', '04.16.01.04.*', 'Jaringan Sambungan Kerumah', '2021-08-06 04:24:43'),
('9b5bee635e06848e2c24143a5e0a3cda', '04', '16', '01', '04', '01', '04.16.01.04.01', 'Jaringan Sambungan Kerumah Kapasitas Kecil', '2021-08-06 04:24:43'),
('5e9dda8ee16fe7b41b3d15ae6d36ff51', '04', '16', '01', '04', '02', '04.16.01.04.02', 'Jaringan Sambungan Kerumah Kapasitas Sedang', '2021-08-06 04:24:43'),
('84be7d22a56a9bbc84b9fc916baa962b', '04', '16', '01', '04', '03', '04.16.01.04.03', 'Jaringan Sambungan Kerumah Kapaitas Besar', '2021-08-06 04:24:43'),
('6a158a7efb3a94e2a3c7835d99659cc4', '04', '16', '01', '04', '04', '04.16.01.04.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('bfdf33037304c258fa5ee7cbe4304428', '04', '16', '02', '*', '*', '04.16.02.*.*', 'JARINGAN LISTRIK', '2021-08-06 04:24:43'),
('3615860a78ed96949a45a26c900cccac', '04', '16', '02', '01', '*', '04.16.02.01.*', 'Jaringan Tranmisi', '2021-08-06 04:24:43'),
('efa8651d84a305106908cc2f743cd7a5', '04', '16', '02', '02', '*', '04.16.02.02.*', 'Jaringan Distribusi', '2021-08-06 04:24:43'),
('92a5513b312a6f3f207dec05a622569c', '05', '17', '*', '*', '*', '05.17.*.*.*', 'BUKU DAN PERPUSTAKAAN', '2021-08-06 04:24:43'),
('2eb90a3c1fbe5e9e7b4a8b6886db2580', '05', '17', '01', '*', '*', '05.17.01.*.*', 'BUKU', '2021-08-06 04:24:43'),
('9b0d42bcbbb3acf9b305d3422e110aba', '05', '17', '01', '01', '*', '05.17.01.01.*', 'Umum', '2021-08-06 04:24:43'),
('4b4a95b90af559494f5d981c9182eb39', '05', '17', '01', '01', '01', '05.17.01.01.01', 'Ilmu Pengetahuan Umum', '2021-08-06 04:24:43'),
('846d8e42e5d6ab399a9d5779862f5241', '05', '17', '01', '01', '02', '05.17.01.01.02', 'Bibliografi, Katalog', '2021-08-06 04:24:43'),
('bd2ab040d65c037c1a637409d70f4eed', '05', '17', '01', '01', '03', '05.17.01.01.03', 'Ilmu Perpustakaan', '2021-08-06 04:24:43'),
('ae0c90f8f7d87d963c21b2dbfc203dee', '05', '17', '01', '01', '04', '05.17.01.01.04', 'Ensyclopedia, Kamus, Buku Referensi', '2021-08-06 04:24:43'),
('df98ec1be6a534359de32643769b9f3c', '05', '17', '01', '01', '05', '05.17.01.01.05', 'Essay, Pamflet', '2021-08-06 04:24:43'),
('0be4543435aff5ecf9cfd42233bb718f', '05', '17', '01', '01', '06', '05.17.01.01.06', 'Berkala', '2021-08-06 04:24:43'),
('f416d664fbb87dc8ddc2d46c83c3cd48', '05', '17', '01', '01', '07', '05.17.01.01.07', 'Institut, Assosiasi, Musium', '2021-08-06 04:24:43'),
('a8123ffaa26913eeec680a089552dca9', '05', '17', '01', '01', '08', '05.17.01.01.08', 'Harian', '2021-08-06 04:24:43'),
('eb1bcf65e0d3d485a35ff54f399eab6f', '05', '17', '01', '01', '09', '05.17.01.01.09', 'Manuskrip', '2021-08-06 04:24:43'),
('07b2fddd0f971ee3d5dd5d172cdcd315', '05', '17', '01', '01', '10', '05.17.01.01.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b80211898db22b54d3fb906b4973d7c6', '05', '17', '01', '02', '*', '05.17.01.02.*', 'Filsafat', '2021-08-06 04:24:43'),
('873cd5777517f61d0ef5011eb7476580', '05', '17', '01', '02', '01', '05.17.01.02.01', 'Metafisika', '2021-08-06 04:24:43'),
('c235495c97a940ca03b8c29a11d91536', '05', '17', '01', '02', '02', '05.17.01.02.02', 'Sistem Filsafat', '2021-08-06 04:24:43'),
('d524e4a38ccd293075194852eaa18ed0', '05', '17', '01', '02', '03', '05.17.01.02.03', 'Ilmu Jiwa', '2021-08-06 04:24:43'),
('03d55b5dc3d66ed93f8265573f532cc4', '05', '17', '01', '02', '04', '05.17.01.02.04', 'Logika', '2021-08-06 04:24:43'),
('15ae4131bcdc0cc724bce0fbea878ddc', '05', '17', '01', '02', '05', '05.17.01.02.05', 'Etika', '2021-08-06 04:24:43'),
('a554313ef51762daecd493e786ef39c1', '05', '17', '01', '02', '06', '05.17.01.02.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('620e94898e6752738b761beb34fb897e', '05', '17', '01', '03', '*', '05.17.01.03.*', 'Agama', '2021-08-06 04:24:43'),
('b7b24b00e98421c0dd77f2d388cb8d1e', '05', '17', '01', '03', '01', '05.17.01.03.01', 'Agama Islam', '2021-08-06 04:24:43'),
('762f3c4b23d5b19cec0bc3b94fbe30ff', '05', '17', '01', '03', '02', '05.17.01.03.02', 'Agama Kristen', '2021-08-06 04:24:43'),
('a4810442415cc8cce04d1650b70be1e4', '05', '17', '01', '03', '03', '05.17.01.03.03', 'Agama Budha', '2021-08-06 04:24:43'),
('2e32f342ee0fc77d8fb5efe26f6f795c', '05', '17', '01', '03', '04', '05.17.01.03.04', 'Agama Hindu', '2021-08-06 04:24:43'),
('eaf7113e325a3f8cf56ff17e4d699829', '05', '17', '01', '03', '05', '05.17.01.03.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3c01091e89f96d7d6862ec1aa7ddcd58', '05', '17', '01', '04', '*', '05.17.01.04.*', 'Ilmu Sosial', '2021-08-06 04:24:43'),
('0d40a4370d87d3216b8231544c9cbdc2', '05', '17', '01', '04', '01', '05.17.01.04.01', 'Sosiologi', '2021-08-06 04:24:43'),
('0cd6de2cc4f5e70eb1ebcc35db626355', '05', '17', '01', '04', '02', '05.17.01.04.02', 'Statistik', '2021-08-06 04:24:43'),
('cc4ec02d5756ec8455745e6a35fc05bf', '05', '17', '01', '04', '03', '05.17.01.04.03', 'Ilmu Politik', '2021-08-06 04:24:43'),
('37e3cb4b04d179eb9d2787983db6257d', '05', '17', '01', '04', '04', '05.17.01.04.04', 'Ekonomi', '2021-08-06 04:24:43'),
('33d0a295dfdbd8f624ac32d8c4dd85c4', '05', '17', '01', '04', '05', '05.17.01.04.05', 'Hukum', '2021-08-06 04:24:43'),
('2ca731ef8f5ce6c2b8653212b8aff52c', '05', '17', '01', '04', '06', '05.17.01.04.06', 'Administrasi, Pertanahan dan Keamanan', '2021-08-06 04:24:43'),
('ce535098c4111a46564e61671d833955', '05', '17', '01', '04', '07', '05.17.01.04.07', 'Service Umum Sosial', '2021-08-06 04:24:43'),
('c7c003171b7adf37fd734f97064237e7', '05', '17', '01', '04', '08', '05.17.01.04.08', 'Pendidikan', '2021-08-06 04:24:43'),
('998f04a1a684fe7e884e667fb2728f27', '05', '17', '01', '04', '09', '05.17.01.04.09', 'Perdagangan', '2021-08-06 04:24:43'),
('80983e5e6c6b0af14d70faf0d98556ba', '05', '17', '01', '04', '10', '05.17.01.04.10', 'Etnografi, Cerita Rakyat', '2021-08-06 04:24:43'),
('5b8521e3a9528035012395c8fa78607e', '05', '17', '01', '04', '11', '05.17.01.04.11', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('ac6bfdd3907c42fb52861948e147489b', '05', '17', '01', '05', '*', '05.17.01.05.*', 'Ilmu Bahasa', '2021-08-06 04:24:43'),
('d44c9c26a64c82493aa41097cbeabe82', '05', '17', '01', '05', '01', '05.17.01.05.01', 'Umum', '2021-08-06 04:24:43'),
('136db8d219ca68ad78db7f8dc62daef6', '05', '17', '01', '05', '02', '05.17.01.05.02', 'Pengetahuan Bahasa Indonesia', '2021-08-06 04:24:43'),
('79225c5e17b2e21f53f8bcd1cd0454a2', '05', '17', '01', '05', '03', '05.17.01.05.03', 'Pengetahuan Bahasa Inggris', '2021-08-06 04:24:43'),
('258e487a3a8c7a8cd3195a79f10fcc0f', '05', '17', '01', '05', '04', '05.17.01.05.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('0f7515f27d8362ba8502ddf9172d3630', '05', '17', '01', '06', '*', '05.17.01.06.*', 'Matematika &ampxkommaxampxkommax Pengetahuan Alam', '2021-08-06 04:24:43'),
('c681fcf1afa741f5b41d2e0383434ccf', '05', '17', '01', '06', '01', '05.17.01.06.01', 'Matematika', '2021-08-06 04:24:43'),
('a3a5398ef18788c1dc222e0c9770ed22', '05', '17', '01', '06', '02', '05.17.01.06.02', 'Astronomi, Geodesi', '2021-08-06 04:24:43'),
('c5b063a131f3ae065c0ef78d46734ed1', '05', '17', '01', '06', '03', '05.17.01.06.03', 'Fisika dan Mekanika', '2021-08-06 04:24:43'),
('b25a052252fdb7f551992036d333b757', '05', '17', '01', '06', '04', '05.17.01.06.04', 'Kimia', '2021-08-06 04:24:43'),
('0026b4d95d1ad5cdf14bf650a403258d', '05', '17', '01', '06', '05', '05.17.01.06.05', 'Geologi, Metrologi', '2021-08-06 04:24:43'),
('05a85f09688d495b4e89fb8398e14a89', '05', '17', '01', '06', '06', '05.17.01.06.06', 'Palaentologi', '2021-08-06 04:24:43'),
('ea232499fcd14c68e889616395ba0590', '05', '17', '01', '06', '07', '05.17.01.06.07', 'Biologi, Antropologi', '2021-08-06 04:24:43'),
('79f32d3863da768ba06c087a72017187', '05', '17', '01', '06', '08', '05.17.01.06.08', 'Bitani', '2021-08-06 04:24:43'),
('fb416d9e66eea0e656e77c3c31f520ea', '05', '17', '01', '06', '09', '05.17.01.06.09', 'Zoologi xkkurixIlmu Hewanxkkurnanx', '2021-08-06 04:24:43'),
('28a8c7c949d53728812feddf2ea6126d', '05', '17', '01', '06', '10', '05.17.01.06.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('56f256d41fd51d11d238c483be91da43', '05', '17', '01', '07', '*', '05.17.01.07.*', 'Ilmu Pengetahuan Praktis', '2021-08-06 04:24:43'),
('1eaba4b9632205a24183a2842905f2e9', '05', '17', '01', '07', '01', '05.17.01.07.01', 'Ilmu kedocteran', '2021-08-06 04:24:43'),
('b85c97dafb2636b7eade36e1b7c3a9c2', '05', '17', '01', '07', '02', '05.17.01.07.02', 'Teknologi', '2021-08-06 04:24:43'),
('8ef2ee2b2341ece2800c45412472bbc7', '05', '17', '01', '07', '03', '05.17.01.07.03', 'Pertanian, Kehutanan, Perikanan', '2021-08-06 04:24:43'),
('7967a23fcf305e56bb25ac3009b866e2', '05', '17', '01', '07', '04', '05.17.01.07.04', 'Ilmu Kerumah Tanggaan', '2021-08-06 04:24:43'),
('3dc874bc814ac7422709ef1e9bdd4431', '05', '17', '01', '07', '05', '05.17.01.07.05', 'Management dan Perkantoran', '2021-08-06 04:24:43'),
('7d29df9ed99ed5ea6b04e98cf0663cbc', '05', '17', '01', '07', '06', '05.17.01.07.06', 'Industri Kimia', '2021-08-06 04:24:43'),
('28d96598986a3f24aab61810b4f29db9', '05', '17', '01', '07', '07', '05.17.01.07.07', 'Teknik Industri &ampxkommaxampxkommax Kerajinan', '2021-08-06 04:24:43'),
('c1f9068c51bfb964c4e3a2dfabcd2785', '05', '17', '01', '07', '08', '05.17.01.07.08', 'Ilmu Perdagangan Khusus Industri', '2021-08-06 04:24:43'),
('e767f77c18616ec10a22b4881290b75d', '05', '17', '01', '07', '09', '05.17.01.07.09', 'Industri Kontruksi dan Perdagangan', '2021-08-06 04:24:43'),
('6084e5481fa27c744c5213bb35dcf8c3', '05', '17', '01', '07', '10', '05.17.01.07.10', 'Lainxstrixlain.', '2021-08-06 04:24:43'),
('eb772836b297c856bf141150e6d4a562', '05', '17', '01', '08', '*', '05.17.01.08.*', 'Arsitektur, Kesenian, Olag Raga', '2021-08-06 04:24:43'),
('c4cc6c5490d4d151848205d3a22f9af4', '05', '17', '01', '08', '01', '05.17.01.08.01', 'Perencanaan Fisik, Pertamanan dll', '2021-08-06 04:24:43'),
('bafff8d7456a76de89d03cebd5d284b0', '05', '17', '01', '08', '02', '05.17.01.08.02', 'Arsitektur', '2021-08-06 04:24:43'),
('8387b04731b3a748b1e87c63cd030bea', '05', '17', '01', '08', '03', '05.17.01.08.03', 'Seni Pahat', '2021-08-06 04:24:43'),
('0266de57feacfecde4a798a669c2f6b2', '05', '17', '01', '08', '04', '05.17.01.08.04', 'Seni Lukis, Ukir', '2021-08-06 04:24:43'),
('8fd39b561cbb29dbc788461411be7792', '05', '17', '01', '08', '05', '05.17.01.08.05', 'Seni Gambar, Grafika', '2021-08-06 04:24:43'),
('34d8a951aa05a7f0d854ddfac71ed9e1', '05', '17', '01', '08', '06', '05.17.01.08.06', 'Fotografi, Senimatografi', '2021-08-06 04:24:43'),
('42b49ecc9fc6e0f4e3299a0f015f34d1', '05', '17', '01', '08', '07', '05.17.01.08.07', 'Musik', '2021-08-06 04:24:43'),
('476ed39ab0399a89f635984150e6b623', '05', '17', '01', '08', '08', '05.17.01.08.08', 'Permainan dan Olah Raga', '2021-08-06 04:24:43'),
('1949be7a893fbb2ed05a37d10b0c2e72', '05', '17', '01', '08', '09', '05.17.01.08.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('28afe05f228b69c5dcc851b308ac7867', '05', '17', '01', '09', '*', '05.17.01.09.*', 'Georafi, Biografi, Sejarah', '2021-08-06 04:24:43'),
('144f09ecbefa534599f89a191c52bf46', '05', '17', '01', '09', '01', '05.17.01.09.01', 'Geografi, Eksplorasi', '2021-08-06 04:24:43'),
('7922da0ca7850ea9bf6b3a3e4faff50b', '05', '17', '01', '09', '02', '05.17.01.09.02', 'Biografi', '2021-08-06 04:24:43'),
('ec1080a0f4f6ef473000384fac0a96fa', '05', '17', '01', '09', '03', '05.17.01.09.03', 'Sejarah', '2021-08-06 04:24:43'),
('5501b667c8a7b9567aa60e589e54dbc6', '05', '17', '01', '09', '04', '05.17.01.09.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('021ab3d57636d96091acc0aa11d3f211', '05', '17', '02', '01', '*', '05.17.02.01.*', 'Terbitan Berkala', '2021-08-06 04:24:43'),
('d63f5c5e719902a8ba7e671a13232bba', '05', '17', '02', '01', '01', '05.17.02.01.01', 'Koran', '2021-08-06 04:24:43'),
('b41f05de48cb4b103abe2f489d46766c', '05', '17', '02', '01', '02', '05.17.02.01.02', 'Majalah', '2021-08-06 04:24:43'),
('f7a4732012523fe1545a1b79593a5fc1', '05', '17', '02', '01', '03', '05.17.02.01.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3351200b083766cf4ed2316a25b2ae86', '05', '17', '03', '*', '*', '05.17.03.*.*', 'BARANGxstrixBARANG PERPUSTAKAAN', '2021-08-06 04:24:43'),
('478cc2fb0af88ddf526efcbe70bbe0fe', '05', '17', '03', '01', '*', '05.17.03.01.*', 'Peta', '2021-08-06 04:24:43'),
('ffb155e2a516c1196e78ea55887efb58', '05', '17', '03', '01', '01', '05.17.03.01.01', 'Buku Peta xkkurixAtlasxkkurnanx', '2021-08-06 04:24:43'),
('8b259d799422d135485440ae4de1131c', '05', '17', '03', '01', '02', '05.17.03.01.02', 'Bagan, Gambar xkkurixDiagramxkkurnanx', '2021-08-06 04:24:43'),
('52c6bd71bb2a5c14a91b175f68ead1a4', '05', '17', '03', '01', '03', '05.17.03.01.03', 'Bola Dunia xkkurixGlobexkkurnanx', '2021-08-06 04:24:43'),
('4ccb7a48ef337732a53686ff11c1e6b6', '05', '17', '03', '01', '04', '05.17.03.01.04', 'Peta xkkurixMapxkkurnanx', '2021-08-06 04:24:43'),
('7df8841adb1f0b8d364d2c6d44d5a933', '05', '17', '03', '01', '05', '05.17.03.01.05', 'Peta Udara', '2021-08-06 04:24:43'),
('5edd56c214bb00530513ee131c259aba', '05', '17', '03', '01', '06', '05.17.03.01.06', 'Peta Hidrografi', '2021-08-06 04:24:43'),
('2375381ba6186c1f533bb9364a32cb41', '05', '17', '03', '01', '07', '05.17.03.01.07', 'Peta Imaginer', '2021-08-06 04:24:43'),
('f83ecac491b6371b88d04c71b86a6052', '05', '17', '03', '01', '08', '05.17.03.01.08', 'Peta Gambar Penampang', '2021-08-06 04:24:43'),
('3339ac170cceb1c9eb2d027e2fcda388', '05', '17', '03', '01', '09', '05.17.03.01.09', 'Peta Photo', '2021-08-06 04:24:43'),
('e4a5ca320bc372fc2b110b05a4f9f4fc', '05', '17', '03', '01', '10', '05.17.03.01.10', 'Peta Tipografi', '2021-08-06 04:24:43'),
('1c664d838002ed1968789090c88fb666', '05', '17', '03', '01', '11', '05.17.03.01.11', 'Peta Ruang Angkasa', '2021-08-06 04:24:43'),
('9c386c3b8e241ce1372ecbbc26fc9b93', '05', '17', '03', '01', '12', '05.17.03.01.12', 'Gambar Tipografi', '2021-08-06 04:24:43'),
('d47fe9c4429d85da2566c1ce5f581ebf', '05', '17', '03', '01', '13', '05.17.03.01.13', 'Model Relief', '2021-08-06 04:24:43'),
('cbc12119e9fac5628efab6f363173cc9', '05', '17', '03', '01', '14', '05.17.03.01.14', 'Photo Mozaik', '2021-08-06 04:24:43'),
('057504caa752ecf4c34f05f811561422', '05', '17', '03', '01', '15', '05.17.03.01.15', 'Gambar Jarak Jauh xkkurixRemote Sensing Imagexkkurnanx', '2021-08-06 04:24:43'),
('7b8539cd1371e71f608cab3906bd6dbf', '05', '17', '03', '01', '16', '05.17.03.01.16', 'View', '2021-08-06 04:24:43'),
('a75009ab6b7d18515e7fd448c96a4aac', '05', '17', '03', '01', '17', '05.17.03.01.17', 'Peta Pengamanan Tanah', '2021-08-06 04:24:43'),
('2d40aae11cd52cbe1bba2bcad43fc7c0', '05', '17', '03', '01', '18', '05.17.03.01.18', 'Peta Kemampuan Tanah', '2021-08-06 04:24:43'),
('6e2b048251e937c0f956f9b23696be8a', '05', '17', '03', '01', '19', '05.17.03.01.19', 'Peta Lokasi', '2021-08-06 04:24:43'),
('d3e02e5b16112cd68b7783fae41c1a4f', '05', '17', '03', '01', '20', '05.17.03.01.20', 'Peta Jaringan', '2021-08-06 04:24:43'),
('28d0f29f43e4b8ab99af49a268e4e7da', '05', '17', '03', '01', '21', '05.17.03.01.21', 'Peta Citra Sport', '2021-08-06 04:24:43'),
('914a50b4938de315b5762bac0b832ee5', '05', '17', '03', '01', '22', '05.17.03.01.22', 'Peta Citra Radar', '2021-08-06 04:24:43'),
('da80000d0471a9672151293d281a8504', '05', '17', '03', '01', '23', '05.17.03.01.23', 'Peta Citra Satelit', '2021-08-06 04:24:43'),
('a56a1a9581972a01578010807a43f070', '05', '17', '03', '01', '24', '05.17.03.01.24', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b98552b4f1138086bdd9b60205368371', '05', '17', '03', '03', '*', '05.17.03.03.*', 'Musik', '2021-08-06 04:24:43'),
('6d7d4df58852c711459003903c622b7a', '05', '17', '03', '03', '01', '05.17.03.03.01', 'Kumpulan Karya Musik xkkurixSkorexkkurnanx', '2021-08-06 04:24:43'),
('b99977b626f325b4c629b429c6f1184a', '05', '17', '03', '03', '02', '05.17.03.03.02', 'Kumpulan Karya Musik Singkat xkkurixCondeset Skorexkkurnanx', '2021-08-06 04:24:43'),
('688e35edfbe5e74fbe3f3a0ec57b1283', '05', '17', '03', '03', '03', '05.17.03.03.03', 'Kumpulan Karya Musik Tertutup xkkurixCloset Skorexkkurnanx', '2021-08-06 04:24:43'),
('6c788510e0eb66810097d5891c22f3be', '05', '17', '03', '03', '04', '05.17.03.03.04', 'Kumpulan Karya Musik Bentuk Mini xkkurixMiniature Skorexkkurnanx', '2021-08-06 04:24:43'),
('38114071e0ab773f9e2e3dc218fa0855', '05', '17', '03', '03', '05', '05.17.03.03.05', 'Partitur Piano', '2021-08-06 04:24:43'),
('76afb6e17317917bf1cd6cf974a861f4', '05', '17', '03', '03', '06', '05.17.03.03.06', 'Kumpulan Karya Musik Vokal', '2021-08-06 04:24:43'),
('8f2c62fdf84b3861b7697789ebb10435', '05', '17', '03', '03', '07', '05.17.03.03.07', 'Kumpulan Karya Musik Piano', '2021-08-06 04:24:43'),
('c84667c8d6cefcb2c2ac0df3a99982bd', '05', '17', '03', '03', '08', '05.17.03.03.08', 'Kumpulan Karya Musik Chord', '2021-08-06 04:24:43'),
('358ab38f6902ca675381de8f7e246b03', '05', '17', '03', '03', '09', '05.17.03.03.09', 'Partitur', '2021-08-06 04:24:43'),
('47f8d93f8fc695bc34e515bca9d02700', '05', '17', '03', '03', '10', '05.17.03.03.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1b5ca8285fad18279b13d1ba1200e0c2', '05', '17', '03', '04', '*', '05.17.03.04.*', 'Karya Grafika xkkurixGraphic Materialxkkurnanx', '2021-08-06 04:24:43'),
('c7e62397d117287702637aa4058c6617', '05', '17', '03', '04', '01', '05.17.03.04.01', 'Karya Seni Asli, Lukisan Asli xkkurixArt Originalxkkurnanx', '2021-08-06 04:24:43'),
('a9a1eec79a6b557d0d36bbcfe3965a8a', '05', '17', '03', '04', '02', '05.17.03.04.02', 'Karya Seni Cetak xgmringx Grafis xkkurixArt Pointxkkurnanx', '2021-08-06 04:24:43'),
('5c48ecf2c69ceb8559aeceec3bfebf27', '05', '17', '03', '04', '03', '05.17.03.04.03', 'Reproduksi xkkurixArts Reproductionxkkurnanx', '2021-08-06 04:24:43'),
('a76bd96bcf10c4ab4d25ec84c17cb1ac', '05', '17', '03', '04', '04', '05.17.03.04.04', 'GrafikxgmringxBagan', '2021-08-06 04:24:43'),
('b5554c84a9707d84a0ee4b3f67178215', '05', '17', '03', '04', '05', '05.17.03.04.05', 'Lembaran Film', '2021-08-06 04:24:43'),
('99b40608c7b009d44992f870642cee23', '05', '17', '03', '04', '06', '05.17.03.04.06', 'Slongsongan Film', '2021-08-06 04:24:43'),
('3b02eb7b0023174421f30ac254be9247', '05', '17', '03', '04', '07', '05.17.03.04.07', 'Kartu Pengikat', '2021-08-06 04:24:43'),
('fafdc594094a14fca601d5cfb116926f', '05', '17', '03', '04', '08', '05.17.03.04.08', 'Slongsongan Grafic', '2021-08-06 04:24:43'),
('1e7e5b0f1075d58ff4a5a354e7a87f23', '05', '17', '03', '04', '09', '05.17.03.04.09', 'Photo', '2021-08-06 04:24:43'),
('a4620678acc3cb99f9f1c7b74b09f1fc', '05', '17', '03', '04', '10', '05.17.03.04.10', 'Gambar', '2021-08-06 04:24:43'),
('6106317934f5a2706ef0830c6f820bfa', '05', '17', '03', '04', '11', '05.17.03.04.11', 'Kartu Pos', '2021-08-06 04:24:43'),
('0486c5c49c365a69e8ebe5edb94f0bbf', '05', '17', '03', '04', '12', '05.17.03.04.12', 'Koster', '2021-08-06 04:24:43'),
('28911ab7330cb1510d7f0bd05c5f7987', '05', '17', '03', '04', '13', '05.17.03.04.13', 'Radiogram', '2021-08-06 04:24:43'),
('98f2838165136da309615e9a93b5c7da', '05', '17', '03', '04', '14', '05.17.03.04.14', 'Slide', '2021-08-06 04:24:43'),
('56c2bac5bd6641ee774041c39c300020', '05', '17', '03', '04', '15', '05.17.03.04.15', 'Gambar Ruang', '2021-08-06 04:24:43'),
('099ed68a16b62aea9417d0b3c584ca03', '05', '17', '03', '04', '16', '05.17.03.04.16', 'Study Print', '2021-08-06 04:24:43'),
('1434d943d18a127b4bc87edfbaeba2e7', '05', '17', '03', '04', '17', '05.17.03.04.17', 'Gambar Teknik', '2021-08-06 04:24:43'),
('d732775979458aeb7dc9fe8f3581f33b', '05', '17', '03', '04', '18', '05.17.03.04.18', 'Transparansi', '2021-08-06 04:24:43'),
('dac45b67ce180124be5ba1f28978240e', '05', '17', '03', '04', '19', '05.17.03.04.19', 'GrafikxgmringxBagan Dinding', '2021-08-06 04:24:43'),
('c62b10e4e41622d7033130f2127604f9', '05', '17', '03', '04', '20', '05.17.03.04.20', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3c39a3ea22252ec97874db6fecce88b8', '05', '17', '03', '05', '*', '05.17.03.05.*', 'Three Dimensional Artetacs and Realita', '2021-08-06 04:24:43'),
('6f89a5687ef01b10114697638b7a6762', '05', '17', '03', '05', '01', '05.17.03.05.01', 'Karya Seni Asli', '2021-08-06 04:24:43'),
('60e2736d80917b98a0d52a3279863120', '05', '17', '03', '05', '02', '05.17.03.05.02', 'Reproduksi', '2021-08-06 04:24:43'),
('15b35639efb91eb46366d6c04bb6b516', '05', '17', '03', '05', '03', '05.17.03.05.03', 'Kaset Braile', '2021-08-06 04:24:43'),
('5826869c85ef86ec52a3e5c51e27909e', '05', '17', '03', '05', '04', '05.17.03.05.04', 'Diaroma', '2021-08-06 04:24:43'),
('da55f6bf2fd789859f0a140519086845', '05', '17', '03', '05', '05', '05.17.03.05.05', 'Pameran', '2021-08-06 04:24:43'),
('f363451fd4cd3ccbc7e86bc1d09c0ef5', '05', '17', '03', '05', '06', '05.17.03.05.06', 'Mainan', '2021-08-06 04:24:43'),
('695b456bf4d5b22c04ff1572f8283012', '05', '17', '03', '05', '07', '05.17.03.05.07', 'Slide Mikroskop', '2021-08-06 04:24:43'),
('d7d8f56fed2c6494617654005baa967f', '05', '17', '03', '05', '08', '05.17.03.05.08', 'Maket xkkurixMockxstrixupxkkurnanx', '2021-08-06 04:24:43'),
('95dd823e65774d05dc8436acf0148938', '05', '17', '03', '05', '09', '05.17.03.05.09', 'Model', '2021-08-06 04:24:43'),
('df5a08b6ef43a48e50e68fea158e2ff6', '05', '17', '03', '05', '10', '05.17.03.05.10', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('af818addf070765b0ac63f6193015e3c', '05', '17', '03', '06', '*', '05.17.03.06.*', 'Bentuk Micro xkkurixMicroformxkkurnanx', '2021-08-06 04:24:43'),
('ccf29f2ed4167a6385dd257c12fb8f0a', '05', '17', '03', '06', '01', '05.17.03.06.01', 'Kartu Micro', '2021-08-06 04:24:43'),
('1f239370fce4fc4594f362eb6661b222', '05', '17', '03', '06', '02', '05.17.03.06.02', 'Kartu Celah', '2021-08-06 04:24:43'),
('7d98f25ed478b5adbf73b6e6ee9042e8', '05', '17', '03', '06', '03', '05.17.03.06.03', 'Mikro Film', '2021-08-06 04:24:43'),
('762502b8f81c576641f1d5ae45fb3cc1', '05', '17', '03', '06', '04', '05.17.03.06.04', 'Mikrofis', '2021-08-06 04:24:43'),
('08850c0e73ff7c3b3242ff33ddce2de2', '05', '17', '03', '06', '05', '05.17.03.06.05', 'Mikrolograp', '2021-08-06 04:24:43'),
('4c6ac201160d15a52658905f09cc34c4', '05', '17', '03', '06', '06', '05.17.03.06.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1185c6c4244ccde9ff269cb653f16061', '05', '17', '03', '07', '*', '05.17.03.07.*', 'Rekaman Suara xkkurixSound Recordingxkkurnanx', '2021-08-06 04:24:43'),
('76bad1b1e72d53c806784e07d4d6e7db', '05', '17', '03', '07', '01', '05.17.03.07.01', 'Katridge Suara', '2021-08-06 04:24:43'),
('865c51db8d1dbbfee0fb4f4451953ea4', '05', '17', '03', '07', '02', '05.17.03.07.02', 'Kaset Suara', '2021-08-06 04:24:43'),
('d89846c683fa92f37b7133a1ef43ae96', '05', '17', '03', '07', '03', '05.17.03.07.03', 'Pasangan Suara', '2021-08-06 04:24:43'),
('bb19030a6a6043c7d80f5da9d1013af3', '05', '17', '03', '07', '04', '05.17.03.07.04', 'Pita Suara', '2021-08-06 04:24:43'),
('2f812cea8bb485bdb0bd94e454b73909', '05', '17', '03', '07', '05', '05.17.03.07.05', 'Runnut Suara', '2021-08-06 04:24:43'),
('60c26c36b64535634709ad49bb5a96da', '05', '17', '03', '07', '06', '05.17.03.07.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('382c9fb57c48b011721a259b09939904', '05', '17', '03', '08', '*', '05.17.03.08.*', 'Berkas Komputer xkkurixComputer Filesxkkurnanx', '2021-08-06 04:24:43'),
('b8332be1ca0a06a0ac8ffb682b03ae25', '05', '17', '03', '08', '01', '05.17.03.08.01', 'Komputer Katridge', '2021-08-06 04:24:43'),
('30a2df7d1de273b7dc6a606eccb0927f', '05', '17', '03', '08', '02', '05.17.03.08.02', 'Kaset Komputer', '2021-08-06 04:24:43'),
('18ecd76fd6b5a4ec1488f1d61dc948fc', '05', '17', '03', '08', '03', '05.17.03.08.03', 'Komputer Disk', '2021-08-06 04:24:43'),
('abf27663bc63bbb7cf9c4e9d81a6ed9c', '05', '17', '03', '08', '04', '05.17.03.08.04', 'Compack Disk', '2021-08-06 04:24:43'),
('b176c38a33328feaffffc7ee3441fd73', '05', '17', '03', '08', '05', '05.17.03.08.05', 'Gulungan Komputer', '2021-08-06 04:24:43'),
('4f0094b3ebe89a3cc9d149eb76083753', '05', '17', '03', '08', '06', '05.17.03.08.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('58cc05254361b36bf5b8f190bea3d8c8', '05', '17', '03', '09', '*', '05.17.03.09.*', 'Film Bergerak dan Rekaman Video', '2021-08-06 04:24:43'),
('4e1a11e2eb0f062b5136f29d34e2d883', '05', '17', '03', '09', '01', '05.17.03.09.01', 'Film Katridge', '2021-08-06 04:24:43'),
('040fdf1b4882c3bb2773950cb9be3310', '05', '17', '03', '09', '02', '05.17.03.09.02', 'Kaset Film', '2021-08-06 04:24:43'),
('b151a4320a96ab66ac582d563f8204e7', '05', '17', '03', '09', '03', '05.17.03.09.03', 'Slongsongan Film', '2021-08-06 04:24:43'),
('cd823a30e6d4c24314504194ae957b60', '05', '17', '03', '09', '04', '05.17.03.09.04', 'Katridge Video', '2021-08-06 04:24:43'),
('2255bc84671e8d92121eb125ce912dbc', '05', '17', '03', '09', '05', '05.17.03.09.05', 'Kaset Video', '2021-08-06 04:24:43'),
('230bf3169c235b123da1e87c6b8480be', '05', '17', '03', '09', '06', '05.17.03.09.06', 'Piringan Video', '2021-08-06 04:24:43'),
('59679f55cdb617ca9dee690113eaa09c', '05', '17', '03', '09', '07', '05.17.03.09.07', 'Gulungan Video', '2021-08-06 04:24:43'),
('235fe5bf97670537564c231febc4f6e5', '05', '17', '03', '09', '08', '05.17.03.09.08', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('3f83c0f72c1af96508306543875fcddd', '05', '17', '03', '10', '*', '05.17.03.10.*', 'Tarscalt', '2021-08-06 04:24:43'),
('bd542df94ca707495d091219598fdf5a', '05', '17', '03', '10', '01', '05.17.03.10.01', 'Tarscalt', '2021-08-06 04:24:43'),
('fa92a7515c449ae581a4613342e933d2', '05', '17', '03', '10', '02', '05.17.03.10.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('4a2d2fcf79049f99d214809c4229dd5f', '05', '18', '*', '*', '*', '05.18.*.*.*', 'BARANG BERCORAK KEBUDAYAAN', '2021-08-06 04:24:43'),
('24de30a509257ca8d2229877bdab2dfa', '05', '18', '01', '*', '*', '05.18.01.*.*', 'BARANG BERCORAK KEBUDAYAAN', '2021-08-06 04:24:43'),
('44f31530adf828b6df8f645e1227c84c', '05', '18', '01', '01', '*', '05.18.01.01.*', 'Pahatan', '2021-08-06 04:24:43'),
('3f9bf38bf5f6603adfcaa72a87d93eaa', '05', '18', '01', '01', '01', '05.18.01.01.01', 'Pahatan batuxstrixbatuan', '2021-08-06 04:24:43'),
('94f85888ba7c2d518166da1a103b2265', '05', '18', '01', '01', '02', '05.18.01.01.02', 'Pahatan Kayu', '2021-08-06 04:24:43'),
('4b577e6be00c60141bd438c6b3e4c8ab', '05', '18', '01', '01', '03', '05.18.01.01.03', 'Pahatan Logam', '2021-08-06 04:24:43'),
('e10918d524f29bac3bd8e5009804a402', '05', '18', '01', '01', '04', '05.18.01.01.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('6c41c0741cc9cd3af4b75bcd6f029baa', '05', '18', '01', '02', '*', '05.18.01.02.*', 'Lukisan', '2021-08-06 04:24:43'),
('65300e982735578754a661854b414699', '05', '18', '01', '02', '01', '05.18.01.02.01', 'Lukisan Cat Air', '2021-08-06 04:24:43'),
('09e3495fe9df27b7549914c0710db424', '05', '18', '01', '02', '02', '05.18.01.02.02', 'Lukisan SulamanxgmringxTempelan', '2021-08-06 04:24:43'),
('47303ef8dc4338a23cdbd38d645f078f', '05', '18', '01', '02', '03', '05.18.01.02.03', 'Gambar PresidenxgmringxGubernur', '2021-08-06 04:24:43'),
('f6e135d3372e51e2306472f69ff81dc9', '05', '18', '01', '02', '04', '05.18.01.02.04', 'Lambang Garuda', '2021-08-06 04:24:43'),
('8d8d4bfce09ac3b8003cb92a00f3b5fe', '05', '18', '01', '02', '05', '05.18.01.02.05', 'Lukisan Batik', '2021-08-06 04:24:43'),
('32883b7b0f838317f6ce011e879a7927', '05', '18', '01', '02', '06', '05.18.01.02.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('bea8d5da29215673d1a94536ec3ae334', '05', '18', '01', '03', '*', '05.18.01.03.*', 'Alat Kesenian', '2021-08-06 04:24:43'),
('a7baf961abd031af2e244fbc2fa2b720', '05', '18', '01', '03', '01', '05.18.01.03.01', 'Alat MusikxgmringxBand', '2021-08-06 04:24:43'),
('4911067c1ce93bc808651a922e9d72ab', '05', '18', '01', '03', '02', '05.18.01.03.02', 'Alat Musik NasionalxgmringxDaerah', '2021-08-06 04:24:43'),
('4140688d62d6ebe37aa0a0ec566b5a12', '05', '18', '01', '03', '03', '05.18.01.03.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('ca9f8d81f36f394c50b10bdc7ae8f4e7', '05', '18', '01', '04', '*', '05.18.01.04.*', 'Alat Olah Raga', '2021-08-06 04:24:43'),
('dd2d148678fc844395fac656017349ea', '05', '18', '01', '04', '01', '05.18.01.04.01', 'Alat Golf', '2021-08-06 04:24:43'),
('cc1826b70eb40c63a8f4aac36cd92b4a', '05', '18', '01', '04', '02', '05.18.01.04.02', 'Alat Volley', '2021-08-06 04:24:43'),
('786a0b7ae04cc81826e6aa2e81a38087', '05', '18', '01', '04', '03', '05.18.01.04.03', 'Alat Tenis', '2021-08-06 04:24:43'),
('5659e14590b4fdbf2baf127acaf4e765', '05', '18', '01', '04', '04', '05.18.01.04.04', 'Alat Tenis Meja', '2021-08-06 04:24:43'),
('3183a428048b305a98bdb6826ee7c483', '05', '18', '01', '04', '05', '05.18.01.04.05', 'Alat Sepak Bola', '2021-08-06 04:24:43'),
('73f6cd345bc5c008cebe32339e6ab439', '05', '18', '01', '04', '06', '05.18.01.04.06', 'Alat Badminton', '2021-08-06 04:24:43'),
('3df9261a89da394dc49245743f1ed443', '05', '18', '01', '04', '07', '05.18.01.04.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('dd00f7a68ab38294b95a1389f4842d99', '05', '18', '01', '05', '*', '05.18.01.05.*', 'Tanda Penghargaan', '2021-08-06 04:24:43'),
('12d9da0b30a0dc388778905bcf3a68c3', '05', '18', '01', '05', '01', '05.18.01.05.01', 'Piala', '2021-08-06 04:24:43'),
('84f6da12d45f260eca5b639cb10528d8', '05', '18', '01', '05', '02', '05.18.01.05.02', 'Medali', '2021-08-06 04:24:43'),
('1ad7208efe943ee1a0e3091396656998', '05', '18', '01', '05', '03', '05.18.01.05.03', 'Piagam', '2021-08-06 04:24:43'),
('be7b1c930fdae7c1b80019faa878c6ed', '05', '18', '01', '05', '04', '05.18.01.05.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('1039087f4ae05967280e8d58624c5b68', '05', '18', '01', '06', '*', '05.18.01.06.*', 'Maket dan Foto Dokumen', '2021-08-06 04:24:43'),
('388e83edbea8709c335d17d5caa266f3', '05', '18', '01', '06', '01', '05.18.01.06.01', 'Maket', '2021-08-06 04:24:43'),
('dd9da155df2b91599360b99957ba7d52', '05', '18', '01', '06', '02', '05.18.01.06.02', 'Foto Dokumen', '2021-08-06 04:24:43'),
('9afa862336e6907b41afeafa15822174', '05', '18', '01', '06', '03', '05.18.01.06.03', 'Peta Topografi', '2021-08-06 04:24:43'),
('3a2e1e2b2cbe4c6a3314a98822b22946', '05', '18', '01', '06', '04', '05.18.01.06.04', 'Peta Reproduksi', '2021-08-06 04:24:43'),
('bc418f2a0f861ce0ac0c118b9cc77f8b', '05', '18', '01', '06', '05', '05.18.01.06.05', 'Peta Wilayah', '2021-08-06 04:24:43'),
('85aa14a9e19be792c02dad6b1a801db5', '05', '18', '01', '06', '06', '05.18.01.06.06', 'Peta Keadaan Tanah', '2021-08-06 04:24:43'),
('639870800ef574ceadc84354a82e331a', '05', '18', '01', '06', '07', '05.18.01.06.07', 'Peta Udara', '2021-08-06 04:24:43'),
('0e914e1a8ea460776d7481caa0e05ac1', '05', '18', '01', '06', '08', '05.18.01.06.08', 'Mosaik', '2021-08-06 04:24:43'),
('d209390aa8ab60f668ef2ba7eb9f225e', '05', '18', '01', '06', '09', '05.18.01.06.09', 'Klisexstrixklise', '2021-08-06 04:24:43'),
('8b079cb04832589024a4034783066f36', '05', '18', '01', '06', '10', '05.18.01.06.10', 'Peta Distribusi', '2021-08-06 04:24:43'),
('2c0a5980b75a31fd9371837d3f5a0cb4', '05', '18', '01', '06', '11', '05.18.01.06.11', 'Globe', '2021-08-06 04:24:43'),
('cb7c239adead8f8e397223d75400ac18', '05', '18', '01', '06', '12', '05.18.01.06.12', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b3a4d5d2502c6ad5c942edf354b5c0f8', '05', '18', '01', '07', '*', '05.18.01.07.*', 'Bendaxstrixbenda Berserajah', '2021-08-06 04:24:43'),
('efb1a31c9b56bb92dc029d78f8749664', '05', '18', '01', '07', '01', '05.18.01.07.01', 'Barang koleksi rumah tangga', '2021-08-06 04:24:43'),
('1c54cb0c0ebf066c84f170cd2aa7757a', '05', '18', '01', '07', '02', '05.18.01.07.02', 'Lukisan Bersejarah', '2021-08-06 04:24:43'),
('6eac4f051f06dc61aafd85bcb4a57674', '05', '18', '01', '07', '03', '05.18.01.07.03', 'Koleksi Mata uang', '2021-08-06 04:24:43'),
('d97e3f7a74f7ece9129e68ae2a2073f6', '05', '18', '01', '07', '04', '05.18.01.07.04', 'Bendaxstrixbenda Purbakala', '2021-08-06 04:24:43'),
('dfb7def287bdda73d8fec8a81c9c6eeb', '05', '18', '01', '07', '05', '05.18.01.07.05', 'Dukumentasi Bersejarah', '2021-08-06 04:24:43'),
('46e2edfe5c892b0159bac9b178010d3d', '05', '18', '01', '07', '06', '05.18.01.07.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('66ce6d1c5b94b2d0ac8243e9cba9f734', '05', '18', '01', '08', '*', '05.18.01.08.*', 'Barang Kerajinan', '2021-08-06 04:24:43'),
('e4c2fddfa71b2ed0644df482e4eceb9f', '05', '18', '01', '08', '01', '05.18.01.08.01', 'Keramik xkkurixGuci, Piringxkkurnanx', '2021-08-06 04:24:43'),
('690c0228eee6d266563dc9c109f2f767', '05', '18', '01', '08', '02', '05.18.01.08.02', 'Logam xkkurixGong, Mandauxkkurnanx', '2021-08-06 04:24:43'),
('0e2fd102d665bf7eba4e05e0e8ce90db', '05', '18', '01', '08', '03', '05.18.01.08.03', 'Kayu xkkurixSampit, Telabangxkkurnanx', '2021-08-06 04:24:43'),
('7c6ecffb6afa4641bf60487ce4e07418', '05', '18', '01', '08', '04', '05.18.01.08.04', 'Anyaman xkkurixTikar, Rotanxkkurnanx', '2021-08-06 04:24:43'),
('986c4dd4377de0358296994d3ef1d47f', '05', '18', '01', '08', '05', '05.18.01.08.05', 'Tenunan Sutra', '2021-08-06 04:24:43'),
('491d14b9f0238e1bb386606d59e632ec', '05', '18', '01', '08', '06', '05.18.01.08.06', 'Anyaman Purun', '2021-08-06 04:24:43'),
('e2fda2314788123d4a49c569aedef80e', '05', '18', '01', '08', '07', '05.18.01.08.07', 'Anyaman Bambu', '2021-08-06 04:24:43'),
('7a42aa065a8ad7f9971b1bf53111d3fd', '05', '18', '01', '08', '08', '05.18.01.08.08', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('15d18809a2277febdf1f1534dc4c56c7', '05', '18', '02', '*', '*', '05.18.02.*.*', 'ALAT OLAH RAGA LAINNYA', '2021-08-06 04:24:43'),
('3a3d98c5332cf16e6ad37c2abda0380d', '05', '18', '02', '01', '*', '05.18.02.01.*', 'Senam', '2021-08-06 04:24:43'),
('a86d53ee8141d1568309c9df22c730a8', '05', '18', '02', '01', '01', '05.18.02.01.01', 'Palang Sejajar', '2021-08-06 04:24:43'),
('f890a6fabeb0a8b9943f484e8f827352', '05', '18', '02', '01', '02', '05.18.02.01.02', 'Lapang Kuda', '2021-08-06 04:24:43'),
('067d2802d650ea0d25fe3755b6b4aec2', '05', '18', '02', '01', '03', '05.18.02.01.03', 'Matras', '2021-08-06 04:24:43'),
('229491e4709f6b3e8c776d57d3c192d8', '05', '18', '02', '01', '04', '05.18.02.01.04', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('8fe31a47281b6399044c3a8c9bd47754', '05', '18', '02', '02', '*', '05.18.02.02.*', 'Alat Olah Raga Air', '2021-08-06 04:24:43'),
('0d6ed6e6f35c16a797b9a177baee1ca3', '05', '18', '02', '02', '01', '05.18.02.02.01', 'Ski Air', '2021-08-06 04:24:43'),
('365ec6cba989d762083310a2ca37e559', '05', '18', '02', '02', '02', '05.18.02.02.02', 'Ski Diving', '2021-08-06 04:24:43'),
('4681f8d855a9f27171b1d350cc7d570a', '05', '18', '02', '02', '03', '05.18.02.02.03', 'Selancar', '2021-08-06 04:24:43'),
('56cd922adef51a4daf209b7db7a2f197', '05', '18', '02', '02', '04', '05.18.02.02.04', 'Perahu Karet', '2021-08-06 04:24:43'),
('09118c8279e5f1415a7c563e2153812d', '05', '18', '02', '02', '05', '05.18.02.02.05', 'Perahu Layar', '2021-08-06 04:24:43'),
('da8bc0f3face3e5e73d1d31a6bfc2347', '05', '18', '02', '02', '06', '05.18.02.02.06', 'Alat Arum Jeram', '2021-08-06 04:24:43'),
('c47388961d584748a9ca6e0b029034e2', '05', '18', '02', '02', '07', '05.18.02.02.07', 'Alat Dayung', '2021-08-06 04:24:43'),
('51e09ea54b24bc350bfcf8a61809891c', '05', '18', '02', '02', '08', '05.18.02.02.08', 'Kaca Mata Air', '2021-08-06 04:24:43'),
('e2915fd7f32783ab150abc448eeab80d', '05', '18', '02', '02', '09', '05.18.02.02.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('cd509813cc9cde4359bf21f3fc552385', '05', '18', '02', '03', '*', '05.18.02.03.*', 'Alat Olah Raga Udara', '2021-08-06 04:24:43'),
('43fe199d53550287a38be98b070af0e7', '05', '18', '02', '03', '01', '05.18.02.03.01', 'Gantole', '2021-08-06 04:24:43'),
('a88e287a79a190ab24d968ba3b3e331a', '05', '18', '02', '03', '02', '05.18.02.03.02', 'Balon Udara', '2021-08-06 04:24:43'),
('0b0b75d9e3a46fead08252d3db0b9604', '05', '18', '02', '03', '03', '05.18.02.03.03', 'Payung UdaraxgmringxParasut', '2021-08-06 04:24:43'),
('1b00467fa897bb2e76aa1d2975ca2947', '05', '18', '02', '03', '04', '05.18.02.03.04', 'Alat Terbang Layang', '2021-08-06 04:24:43'),
('5df450304f940bb12bd3dd577d0c7588', '05', '18', '02', '03', '05', '05.18.02.03.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('aca14bb06471abc40c035228e49e0e14', '05', '18', '02', '04', '*', '05.18.02.04.*', 'Alat Olah Raga Lainnya', '2021-08-06 04:24:43'),
('b987f5672766b5767553b1d24abd9ed0', '05', '18', '02', '04', '01', '05.18.02.04.01', 'Catur', '2021-08-06 04:24:43'),
('df58c72fbf0f07911dea4f97ce988a2f', '05', '18', '02', '04', '02', '05.18.02.04.02', 'Sarung Tinju', '2021-08-06 04:24:43'),
('11247dbf3777e886c869f46f59467ef8', '05', '18', '02', '04', '03', '05.18.02.04.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b403e898377b88d86fc2713aa8238c1b', '05', '19', '*', '*', '*', '05.19.*.*.*', 'HEWAN DAN TERNAK SERTA TANAMAN', '2021-08-06 04:24:43'),
('26e38ad4c8f156b18ed30568fb74a506', '05', '19', '01', '*', '*', '05.19.01.*.*', 'HEWAN', '2021-08-06 04:24:43'),
('5de3e9367f846b73bad0f731c8c50b4b', '05', '19', '01', '01', '*', '05.19.01.01.*', 'Binatang Ternak', '2021-08-06 04:24:43'),
('e59e553007824f206e13247bb7a4cfc8', '05', '19', '01', '01', '01', '05.19.01.01.01', 'Sapi', '2021-08-06 04:24:43'),
('62e25d8989086b77100d78c3b9eac788', '05', '19', '01', '01', '02', '05.19.01.01.02', 'Kerbau', '2021-08-06 04:24:43'),
('81f09932f0944088113e44e571d79d73', '05', '19', '01', '01', '03', '05.19.01.01.03', 'Kuda', '2021-08-06 04:24:43'),
('bd69a457d5992ba1dd6eac78017b6f5f', '05', '19', '01', '01', '04', '05.19.01.01.04', 'Babi', '2021-08-06 04:24:43'),
('d2a70642725ac7fa9a4fa35dbbc1dbb8', '05', '19', '01', '01', '05', '05.19.01.01.05', 'Kambing', '2021-08-06 04:24:43'),
('174472b1ac489cb63302e120a0c3bd8a', '05', '19', '01', '01', '06', '05.19.01.01.06', 'Anjing', '2021-08-06 04:24:43'),
('23a0e084f428c883085ecdf8087f2a20', '05', '19', '01', '01', '07', '05.19.01.01.07', 'Birixstrixbiri', '2021-08-06 04:24:43'),
('36faf23001c32926e4700a13896274b1', '05', '19', '01', '01', '08', '05.19.01.01.08', 'Kelinci', '2021-08-06 04:24:43'),
('2f4855158f999f0dd51dafd1fda8206f', '05', '19', '01', '01', '09', '05.19.01.01.09', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('23bf06d0590644fcc1c24af1a233b31b', '05', '19', '01', '02', '*', '05.19.01.02.*', 'Binatang Unggas', '2021-08-06 04:24:43'),
('8bbc6c5e37f29a1647fd98f9b42264c7', '05', '19', '01', '02', '01', '05.19.01.02.01', 'Ayam', '2021-08-06 04:24:43'),
('af74b1833753bafbcd856375cf9c5ad9', '05', '19', '01', '02', '02', '05.19.01.02.02', 'Itik', '2021-08-06 04:24:43'),
('47148ba5963fbdabc2eb3b2705b7bb69', '05', '19', '01', '02', '03', '05.19.01.02.03', 'Bebek', '2021-08-06 04:24:43'),
('04b84bdcd203e725ce94067dddc9b4a9', '05', '19', '01', '02', '04', '05.19.01.02.04', 'Angsa', '2021-08-06 04:24:43'),
('1880936dcb6f442f2c81647a4e2bbb69', '05', '19', '01', '02', '05', '05.19.01.02.05', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('b3032236c51974a655d4b239e52453d4', '05', '19', '01', '03', '*', '05.19.01.03.*', 'Binatang Melata', '2021-08-06 04:24:43'),
('fe4b165ac6ab45bcfcc6070267536e1e', '05', '19', '01', '03', '01', '05.19.01.03.01', 'Ular', '2021-08-06 04:24:43'),
('bc5363b0872b2b6aa0e9d7d84e4c734b', '05', '19', '01', '03', '02', '05.19.01.03.02', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('39fb55fcb0cea3be1c95887d1e1aa445', '05', '19', '01', '04', '*', '05.19.01.04.*', 'Binatang Ikan', '2021-08-06 04:24:43'),
('9bfd4062cade01dc6cf3eada9982ccca', '05', '19', '01', '04', '01', '05.19.01.04.01', 'Induk Ikan Arwana', '2021-08-06 04:24:43'),
('a54d513a6ba1ddea2f21cb25410a15f8', '05', '19', '01', '04', '02', '05.19.01.04.02', 'Induk Ikan Bandeng', '2021-08-06 04:24:43'),
('804954c4d708a76a1fde2bd532c4e073', '05', '19', '01', '04', '03', '05.19.01.04.03', 'Induk Ikan Krapu', '2021-08-06 04:24:43'),
('2cc6242609bb5876e1a07d6d126dce09', '05', '19', '01', '04', '04', '05.19.01.04.04', 'Induk Ikan Mas', '2021-08-06 04:24:43'),
('882478ddb1f9498bcd9e3adf4624a863', '05', '19', '01', '04', '05', '05.19.01.04.05', 'Induk Ikan Napolem', '2021-08-06 04:24:43'),
('fa5cdd454c06831bfee4ccae17d0ce78', '05', '19', '01', '04', '06', '05.19.01.04.06', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('308a05514f1c5ee973aca1da895c0ea8', '05', '19', '01', '05', '*', '05.19.01.05.*', 'Hewan Kebun Binatang', '2021-08-06 04:24:43'),
('6dfa8d64f62a492d90ae9fca3c8f9224', '05', '19', '01', '05', '01', '05.19.01.05.01', 'Gajah', '2021-08-06 04:24:43'),
('4e517dee738a1f761ebbc01bb537da13', '05', '19', '01', '05', '02', '05.19.01.05.02', 'Badak', '2021-08-06 04:24:43'),
('79ab6a4f79839707b68579df00692482', '05', '19', '01', '05', '03', '05.19.01.05.03', 'Jerapah', '2021-08-06 04:24:43'),
('1fa926344bf242bead62d33912c216ef', '05', '19', '01', '05', '04', '05.19.01.05.04', 'Banteng', '2021-08-06 04:24:43'),
('e10c64c9af827e71f68834cc2adaf3aa', '05', '19', '01', '05', '05', '05.19.01.05.05', 'Zebra', '2021-08-06 04:24:43'),
('becec865cf9d344451c00435aee3d543', '05', '19', '01', '05', '06', '05.19.01.05.06', 'Singa', '2021-08-06 04:24:43'),
('f9439a44638bcdd3490546756455189f', '05', '19', '01', '05', '07', '05.19.01.05.07', 'Harimau', '2021-08-06 04:24:43'),
('d5d430946a9c37c120e155f3ebb03926', '05', '19', '01', '05', '08', '05.19.01.05.08', 'Anoa', '2021-08-06 04:24:43'),
('0456c8bf9896f10a255096f5b4f5743f', '05', '19', '01', '05', '09', '05.19.01.05.09', 'Babirusa', '2021-08-06 04:24:43'),
('2c7380d6dbf2386f645f2285bfeb3f16', '05', '19', '01', '05', '10', '05.19.01.05.10', 'Rusa', '2021-08-06 04:24:43'),
('3cc3e4cddde9d9dcbf4dbdb7572f75b7', '05', '19', '01', '05', '11', '05.19.01.05.11', 'Tapir', '2021-08-06 04:24:43'),
('2a71c3d2cc4958bb20946ad0e0cc6cf5', '05', '19', '01', '05', '12', '05.19.01.05.12', 'Kancil', '2021-08-06 04:24:43'),
('e32c352e0f71b0c75b7bbe7cd7721cae', '05', '19', '01', '05', '13', '05.19.01.05.13', 'Beruang', '2021-08-06 04:24:43'),
('dd1125bd1d979754b945f97f5f3575ba', '05', '19', '01', '05', '14', '05.19.01.05.14', 'Onta', '2021-08-06 04:24:43'),
('39dafb430dc992be823748c287f91be6', '05', '19', '01', '05', '15', '05.19.01.05.15', 'Bisao', '2021-08-06 04:24:43'),
('daf6458674f74235832bee13434128a1', '05', '19', '01', '05', '16', '05.19.01.05.16', 'Keledai', '2021-08-06 04:24:43'),
('f962e599c667d09b1f6342e35be529a0', '05', '19', '01', '05', '17', '05.19.01.05.17', 'Linsang', '2021-08-06 04:24:43'),
('55232e4ac67c40e3a8ead4a471de2bb7', '05', '19', '01', '05', '18', '05.19.01.05.18', 'Landak', '2021-08-06 04:24:43'),
('65ccd694215a87d493b821262590df5b', '05', '19', '01', '05', '19', '05.19.01.05.19', 'Bangsa Monyet', '2021-08-06 04:24:43'),
('61d3a9287bce6e246876e8870e04dc2c', '05', '19', '01', '05', '20', '05.19.01.05.20', 'Bangsa Binatang Melata', '2021-08-06 04:24:43'),
('68ce3b859639a37cbea431cca8bfcd3a', '05', '19', '01', '05', '21', '05.19.01.05.21', 'Bangsa Binatang Unggas', '2021-08-06 04:24:43'),
('2ef24f780524d5d8eb5f424e09dd698f', '05', '19', '01', '05', '22', '05.19.01.05.22', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('fc26758330aa6cd64421feecef2bea74', '05', '19', '01', '06', '*', '05.19.01.06.*', 'Hewan Pengamanan', '2021-08-06 04:24:43'),
('edeef57415b26e9081bb5510fd0ed0e6', '05', '19', '01', '06', '01', '05.19.01.06.01', 'Anjing Pelacak', '2021-08-06 04:24:43'),
('2ecf05fbdd956fbe6a0212cec67c3c52', '05', '19', '01', '06', '02', '05.19.01.06.02', 'Anjing Penjaga', '2021-08-06 04:24:43'),
('be1ef2955d4b0340450875c47e67719f', '05', '19', '01', '06', '03', '05.19.01.06.03', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('bdf26514322f98c26f345dc36a4c7355', '05', '19', '02', '01', '*', '05.19.02.01.*', 'TANAMAN', '2021-08-06 04:24:43'),
('e589f6d4f039fbb06a72e26541957ed3', '05', '19', '02', '01', '01', '05.19.02.01.01', 'Cacao', '2021-08-06 04:24:43'),
('1ba8becd5c775d723697f8113b13220b', '05', '19', '02', '01', '02', '05.19.02.01.02', 'Cengkeh', '2021-08-06 04:24:43'),
('1e7ab4d48d091c0f47701b9aebc33356', '05', '19', '02', '01', '03', '05.19.02.01.03', 'Jambu Mete', '2021-08-06 04:24:43'),
('d787e9bd3ca2f460633cd3684ac6f0d3', '05', '19', '02', '01', '04', '05.19.02.01.04', 'Karet', '2021-08-06 04:24:43'),
('a2784df27ef7651ddc56da897a327d29', '05', '19', '02', '01', '05', '05.19.02.01.05', 'Kelapa', '2021-08-06 04:24:43'),
('20aef88b440c9a63b2370ae1292dd62b', '05', '19', '02', '01', '06', '05.19.02.01.06', 'Kopi', '2021-08-06 04:24:43'),
('701948eef0253ed74fbb529a4dc313ad', '05', '19', '02', '01', '07', '05.19.02.01.07', 'Lainxstrixlain', '2021-08-06 04:24:43'),
('25e8f8648214c113c020bd1e8b74d71c', '05', '19', '02', '02', '*', '05.19.02.02.*', 'Tanaman Holtikultura', '2021-08-06 04:24:43'),
('3ca96a9b98e43fbc57984852e3cc69e5', '05', '19', '02', '02', '01', '05.19.02.02.01', 'Alpukat', '2021-08-06 04:24:43'),
('7052aaa8a6e2708cf99685118fee4757', '05', '19', '02', '02', '02', '05.19.02.02.02', 'Apel', '2021-08-06 04:24:43'),
('9618ee7a012bb281bb94867f80062f27', '05', '19', '02', '02', '03', '05.19.02.02.03', 'Duku', '2021-08-06 04:24:43'),
('448b7aa6031e2c9903f7e65b0c15eba9', '05', '19', '02', '02', '04', '05.19.02.02.04', 'Durian', '2021-08-06 04:24:43');
INSERT INTO `m_kib_kode` (`kd`, `golongan`, `bidang`, `kelompok`, `kelompok_sub`, `kelompok_sub_sub`, `kode`, `nama`, `postdate`) VALUES
('cc30ca3fd51470eb217a89a2b5107005', '05', '19', '02', '02', '05', '05.19.02.02.05', 'Jambu', '2021-08-06 04:24:43'),
('33a8e3f7d29f911042e5a9be9bf338ea', '05', '19', '02', '02', '06', '05.19.02.02.06', 'Jeruk', '2021-08-06 04:24:43'),
('c264e2cd58970a0d504c9029569a53e0', '05', '19', '02', '02', '07', '05.19.02.02.07', 'Mangga', '2021-08-06 04:24:43'),
('ba62f9024733bd5bc08f50847f79c6f1', '05', '19', '02', '02', '08', '05.19.02.02.08', 'Rambutan', '2021-08-06 04:24:43'),
('ec101cebb0e66950ca5c53d3c9ae690d', '05', '19', '02', '02', '09', '05.19.02.02.09', 'Lainxstrixlain', '2021-08-06 04:24:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_majelis`
--

CREATE TABLE `m_majelis` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `lat_x` longtext DEFAULT NULL,
  `lat_y` longtext DEFAULT NULL,
  `alamat_googlemap` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_majelis`
--

INSERT INTO `m_majelis` (`kd`, `nama`, `alamat`, `telp`, `email`, `postdate`, `lat_x`, `lat_y`, `alamat_googlemap`) VALUES
('e807f1fcf82d132f9bb018ca6738a19f', 'MAJELIS DIKDASMEN KABUPATEN KENDAL', '1', '1', '1', '2021-08-28 16:02:08', '-6.9217721533469945', '110.20413801403278', 'Jl. Raya Semarang xstrix Batang No.199,  Karanggeneng,  Pegulon,  Kec. Kendal,  Kabupaten Kendal,  Jawa Tengah 51313,  Indonesia');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_sekolah`
--

CREATE TABLE `m_sekolah` (
  `kd` varchar(50) NOT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `kode_nss` varchar(100) DEFAULT NULL,
  `kode_nds` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `usernamex` varchar(100) DEFAULT NULL,
  `passwordx` varchar(100) DEFAULT NULL,
  `thn_berdiri` varchar(4) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `sk_status_nomor` varchar(100) DEFAULT NULL,
  `sk_status_tgl` varchar(100) DEFAULT NULL,
  `penyelenggara` varchar(100) DEFAULT NULL,
  `sk_pendirian_nomor` varchar(100) DEFAULT NULL,
  `sk_pendirian_tgl` varchar(100) DEFAULT NULL,
  `ks_nama` varchar(100) DEFAULT NULL,
  `ks_ijazah` varchar(100) DEFAULT NULL,
  `ks_kode` varchar(100) DEFAULT NULL,
  `sarpras_luas_tanah` varchar(100) DEFAULT NULL,
  `sarpras_status` varchar(100) DEFAULT NULL,
  `sarpras_luas_bangunan` varchar(100) DEFAULT NULL,
  `sarpras_masjid` varchar(100) DEFAULT NULL,
  `postdate_update` datetime DEFAULT NULL,
  `lat_x` longtext DEFAULT NULL,
  `lat_y` longtext DEFAULT NULL,
  `alamat_googlemap` longtext DEFAULT NULL,
  `total_siswa_l` varchar(50) DEFAULT NULL,
  `total_siswa_p` varchar(50) DEFAULT NULL,
  `total_siswa` varchar(50) DEFAULT NULL,
  `total_kelas` varchar(50) DEFAULT NULL,
  `total_pegawai_l` varchar(50) DEFAULT NULL,
  `total_pegawai_p` varchar(50) DEFAULT NULL,
  `total_pegawai` varchar(50) DEFAULT NULL,
  `spp_bulanan` varchar(100) DEFAULT NULL,
  `total_login` varchar(100) DEFAULT NULL,
  `total_entri` varchar(100) DEFAULT NULL,
  `total_gps` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_sekolah`
--

INSERT INTO `m_sekolah` (`kd`, `cabang`, `kode`, `kode_nss`, `kode_nds`, `nama`, `alamat`, `telp`, `email`, `postdate`, `usernamex`, `passwordx`, `thn_berdiri`, `status`, `sk_status_nomor`, `sk_status_tgl`, `penyelenggara`, `sk_pendirian_nomor`, `sk_pendirian_tgl`, `ks_nama`, `ks_ijazah`, `ks_kode`, `sarpras_luas_tanah`, `sarpras_status`, `sarpras_luas_bangunan`, `sarpras_masjid`, `postdate_update`, `lat_x`, `lat_y`, `alamat_googlemap`, `total_siswa_l`, `total_siswa_p`, `total_siswa`, `total_kelas`, `total_pegawai_l`, `total_pegawai_p`, `total_pegawai`, `spp_bulanan`, `total_login`, `total_entri`, `total_gps`) VALUES
('00546ca9ac4d94549ce1dedcd23deedc', 'CABANG 6', '11', NULL, NULL, 'MTS BIASAWAE 3', '', NULL, NULL, '2021-03-25 15:57:19', '11', '6512bd43d9caa6e02c990b0a82652dca', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '8', '0', '1'),
('a06844aab3573565040dc1076cebab8c', 'CABANG 6', '12', NULL, NULL, 'MTS BIASAWAE 2', '', NULL, NULL, '2021-03-25 15:57:19', '12', 'c20ad4d76fe97759aa27a0c99bff6710', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2', '0', '1'),
('ea2ca9bcfd05c6017bbc50172221d7f5', 'CABANG 5', '16', NULL, NULL, 'SD BIASAWAE 3', '', NULL, NULL, '2021-03-25 15:57:19', '16', 'c74d97b01eae257e44aa9d5bade97baf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '5', '0', '3'),
('f64ec1f810289a5ba00e1e6d770a62e0', 'CABANG 4', '13', NULL, NULL, 'SMK BIASAWAE 3', '', NULL, NULL, '2021-03-25 15:57:19', '13', 'c51ce410c124a10e0db5e4b97fc2af39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '4', '0', '0'),
('0523679c15f5e4ac9c26f16a48f9710f', 'CABANG 3', '21', NULL, NULL, 'SMP BIASAWAE 1', '', NULL, NULL, '2021-03-25 15:57:19', '21', '3c59dc048e8850243be8079a5c74d079', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2', '0', '1'),
('18e3d1fe9cfe9e7ac941a1dc55e7084c', 'CABANG 2', '18', NULL, NULL, 'SMA BIASAWAE 3', '', NULL, NULL, '2021-03-25 15:57:19', '18', '6f4922f45568161a8cdf4ad2299f6d23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '5', '0', '2'),
('aedb9eb8b17c23ecd46de5498ee7899c', 'CABANG 1', '51', NULL, NULL, 'SD BIASAWAE 1', '', NULL, NULL, '2021-03-25 16:01:23', '51', '2838023a778dfaecdc212708f721b788', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '0', '0'),
('ad6918ab849ae690290632883c6c3daa', 'CABANG 2', '23', NULL, NULL, 'SMA BIASAWAE 1', '', NULL, NULL, '2021-03-25 16:01:23', '23', '37693cfc748049e45d87b8c7d8b9aacd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '4', '0', '0'),
('c96016e08fae6f81ae9b7e5f4a6a4eae', 'CABANG 2', '22', NULL, NULL, 'SMA BIASAWAE 2', '', NULL, NULL, '2021-03-25 16:01:23', '22', 'b6d767d2f8ed5d21a44b0e5886680cb9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '5', '0', '2'),
('b67fedde63fee8e56fbc72e82e241e50', 'CABANG 7', '40', NULL, NULL, 'MA BIASAWAE 3', '', NULL, NULL, '2021-03-25 16:01:23', '40', 'd645920e395fedad7bbbed0eca3fe2e0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2', '0', '0'),
('3523e284f14bfb30dd8bfe81d0680e69', 'CABANG 7', '27', NULL, NULL, 'MA BIASAWAE 2', '', NULL, NULL, '2021-03-25 16:01:23', '27', '02e74f10e0327ad868d138f2b4fdd6f0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0', '0', '0'),
('2aa1b4e6654360ea4519dc265402780a', 'CABANG 1', '123', '123x', '123x', '123 BIASAWAE', '123xxxx', '123', '123', '2021-08-04 17:21:23', '123', '202cb962ac59075b964b07152d234b70', '2', '2', '2', '2021-08-06', '2', '2', '2021-08-02', '1', '2', '3', '4', '5', '6', '7', '2021-08-28 16:20:30', '-6.9217557427867735', '110.20410657560376', 'Jl. Raya Semarang xstrix Batang No.199,  Karanggeneng,  Pegulon,  Kec. Kendal,  Kabupaten Kendal,  Jawa Tengah 51313,  Indonesia', '83', '89', '172', '109', '140', '47', '187', '5', '108', '99', '387');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_uang_keluar`
--

CREATE TABLE `m_uang_keluar` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_uang_keluar`
--

INSERT INTO `m_uang_keluar` (`kd`, `nama`, `postdate`) VALUES
('ccc5383c54fa47f50c02f131436d29e7', 'BELANJA', NULL),
('c96be21cfba4af8cdac670ce221c6923', 'GAJI', NULL),
('474b5f3956e875b0b07fb5069411e165', 'KEGIATAN', NULL),
('fd705dde85b152f654b34cee62f3e06e', 'TAMU', '2021-08-06 17:21:26'),
('612166dabadff41260f061f05e959925', 'SPPD', '2021-08-06 17:21:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_uang_masuk`
--

CREATE TABLE `m_uang_masuk` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `m_uang_masuk`
--

INSERT INTO `m_uang_masuk` (`kd`, `nama`, `postdate`) VALUES
('a434e0a6aec72cae759ccb703f4bfb54', 'SPP', NULL),
('8599f59c80c80910cf46976eb74700ca', 'KOMITE', NULL),
('e938c5bbd29de26c3a600a0330465f8b', 'BOS', NULL),
('5e8b017d23aed17f061f188e1ff9c22b', 'HUTANG', NULL),
('2924d3575cfe08faba30164ec19077df', 'DONATUR', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_cp_artikel`
--

CREATE TABLE `sekolah_cp_artikel` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_cp_artikel`
--

INSERT INTO `sekolah_cp_artikel` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `kategori`, `judul`, `isi`, `postdate`) VALUES
('079d7bd48b3ee3d122784b8af72f2d2a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', 'Kegiatan', 'ok ya...', 'xkkirixpxkkananxok...xkkirixxgmringxpxkkananx', '2021-08-11 09:10:28'),
('c501fff7cb4f9a0dd7ca40f7ce577291', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', 'Berita', '1', 'xkkirixpxkkananx1xkkirixxgmringxpxkkananx', '2021-08-11 09:40:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_cp_foto`
--

CREATE TABLE `sekolah_cp_foto` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `ket` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_cp_foto`
--

INSERT INTO `sekolah_cp_foto` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `ket`, `postdate`) VALUES
('b5d4bd1c9b16d212896d4c17c072531f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', 'sffff', '2021-08-11 09:42:51'),
('0a5ac8c7c912d2c618c7cbb192641874', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', 'xxxxoksdfff', '2021-08-11 09:23:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_cp_youtube`
--

CREATE TABLE `sekolah_cp_youtube` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `filex` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_cp_youtube`
--

INSERT INTO `sekolah_cp_youtube` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `judul`, `filex`, `postdate`) VALUES
('2a068276751b9321dae7094311d03810', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', 'coba1', 'https:xgmringxxgmringxwww.youtube.comxgmringxwatch?v=LlMK9zWQTfU', '2021-08-11 09:35:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_jml_karyawan`
--

CREATE TABLE `sekolah_jml_karyawan` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `nourut` varchar(5) DEFAULT NULL,
  `keadaan` varchar(100) DEFAULT NULL,
  `jml_l` varchar(5) DEFAULT NULL,
  `jml_p` varchar(5) DEFAULT NULL,
  `jml_total` varchar(5) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_jml_karyawan`
--

INSERT INTO `sekolah_jml_karyawan` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `nourut`, `keadaan`, `jml_l`, `jml_p`, `jml_total`, `keterangan`, `postdate`) VALUES
('2aa1b4e6654360ea4519dc265402780a7', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '7', 'Kary. Tidak Tetap', '7', '8', '15', '6', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '6', 'KT Persyarikatan', '3', '4', '7', '6', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '5', 'GTTxgmringx Honorer', '3', '2', '5', '2', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '4', 'GT Persyarikatan', '7', '6', '13', '4', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '3', 'Guru BantuxgmringxKontrak', '92', '10', '102', '8', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2', 'GT Negeri Depag', '5', '6', '11', '8', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '1', 'GT Negeri P dan K', '23', '11', '34', '4', '2021-08-21 14:41:09'),
('61639f850b03e034e2e836ddb74d805f1', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '1', 'GT Negeri P dan K', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f2', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '2', 'GT Negeri Depag', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f3', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '3', 'Guru BantuxgmringxKontrak', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f4', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '4', 'GT Persyarikatan', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f5', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '5', 'GTTxgmringx Honorer', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f6', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '6', 'KT Persyarikatan', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f7', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '7', 'Kary. Tidak Tetap', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('0d26d05f3cdf29155ac0c569f3bf5ead7', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '7', 'Kary. Tidak Tetap', '3', '0', '3', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead6', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '6', 'KT Persyarikatan', '0', '0', '0', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead5', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '5', 'GTTxgmringx Honorer', '5', '7', '12', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead4', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '4', 'GT Persyarikatan', '2', '0', '2', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead3', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '3', 'Guru BantuxgmringxKontrak', '0', '0', '0', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead2', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '2', 'GT Negeri Depag', '0', '1', '1', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead1', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '1', 'GT Negeri P dan K', '0', '0', '0', '0', '2021-08-27 09:38:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_jml_siswa`
--

CREATE TABLE `sekolah_jml_siswa` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `jml_l` varchar(5) DEFAULT NULL,
  `jml_p` varchar(5) DEFAULT NULL,
  `jml_total` varchar(5) DEFAULT NULL,
  `jml_kelas` varchar(5) DEFAULT NULL,
  `spp_bulanan` varchar(15) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_jml_siswa`
--

INSERT INTO `sekolah_jml_siswa` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `kelas`, `jml_l`, `jml_p`, `jml_total`, `jml_kelas`, `spp_bulanan`, `postdate`) VALUES
('2aa1b4e6654360ea4519dc265402780a6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '6', '51', '52', '103', '71', '5', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '5', '5', '2', '7', '21', '5', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '4', '5', '6', '11', '6', '5', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '3', '5', '0', '5', '5', '5', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2', '5', '6', '11', '4', '5', '2021-08-21 14:41:09'),
('2aa1b4e6654360ea4519dc265402780a1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '1', '12', '23', '35', '2', '4', '2021-08-21 14:41:09'),
('6', '', '', '', NULL, '6', '51', '52', '103', '71', '5', '2021-08-21 14:37:05'),
('5', '', '', '', NULL, '5', '5', '2', '7', '21', '5', '2021-08-21 14:37:05'),
('4', '', '', '', NULL, '4', '5', '6', '11', '6', '5', '2021-08-21 14:37:05'),
('3', '', '', '', NULL, '3', '5', '0', '5', '5', '5', '2021-08-21 14:37:05'),
('2', '', '', '', NULL, '2', '5', '6', '11', '4', '5', '2021-08-21 14:37:05'),
('1', '', '', '', NULL, '1', '12', '23', '35', '2', '4', '2021-08-21 14:37:05'),
('61639f850b03e034e2e836ddb74d805f1', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '1', '0', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f2', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '2', '0', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f3', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '3', '0', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f4', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '4', '0', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f5', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '5', '0', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('61639f850b03e034e2e836ddb74d805f6', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '', '6', '0', '0', '0', '0', '0', '2021-08-26 11:53:11'),
('0d26d05f3cdf29155ac0c569f3bf5ead6', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '6', '0', '0', '0', '0', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead5', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '5', '0', '0', '0', '0', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead4', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '4', '0', '0', '0', '0', '0', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead3', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '3', '12', '6', '18', '1', '80000', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead2', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '2', '16', '6', '22', '1', '80000', '2021-08-27 09:38:29'),
('0d26d05f3cdf29155ac0c569f3bf5ead1', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '1', '7', '7', '14', '1', '80000', '2021-08-27 09:38:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_kib_a`
--

CREATE TABLE `sekolah_kib_a` (
  `kd` varchar(100) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `per_tahun` varchar(4) DEFAULT NULL,
  `barang_kode` varchar(100) DEFAULT NULL,
  `barang_nama` varchar(100) DEFAULT NULL,
  `register` varchar(100) DEFAULT NULL,
  `luas` varchar(100) DEFAULT NULL,
  `tahun_ada` varchar(4) DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `status_hak` varchar(100) DEFAULT NULL,
  `status_sertifikat_tgl` varchar(100) DEFAULT NULL,
  `status_sertifikat_nomor` varchar(100) DEFAULT NULL,
  `penggunaan` varchar(100) DEFAULT NULL,
  `asal_usul` varchar(100) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_kib_a`
--

INSERT INTO `sekolah_kib_a` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `per_tahun`, `barang_kode`, `barang_nama`, `register`, `luas`, `tahun_ada`, `alamat`, `status_hak`, `status_sertifikat_tgl`, `status_sertifikat_nomor`, `penggunaan`, `asal_usul`, `harga`, `ket`, `postdate`) VALUES
('2aa1b4e6654360ea4519dc265402780ac4ca4238a0b923820dcc509a6f75849b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '9', '8', '7', '2021-08-19 09:12:03'),
('2aa1b4e6654360ea4519dc265402780aa87ff679a2f3e71d9181a67b7542122c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '4', '5', '6', '7', '8', '9', '8', '7', '6', '5', '4', '3', '2', '2021-08-19 09:12:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_kib_b`
--

CREATE TABLE `sekolah_kib_b` (
  `kd` varchar(100) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `per_tahun` varchar(4) DEFAULT NULL,
  `barang_kode` varchar(100) DEFAULT NULL,
  `barang_nama` varchar(100) DEFAULT NULL,
  `register` varchar(100) DEFAULT NULL,
  `jumlah` varchar(100) DEFAULT NULL,
  `satuan` varchar(100) DEFAULT NULL,
  `merk_type` varchar(100) DEFAULT NULL,
  `ukuran_cc` varchar(100) DEFAULT NULL,
  `bahan` varchar(100) DEFAULT NULL,
  `tahun_beli` varchar(4) DEFAULT NULL,
  `nomor_pabrik` varchar(100) DEFAULT NULL,
  `nomor_rangka` varchar(100) DEFAULT NULL,
  `nomor_mesin` varchar(100) DEFAULT NULL,
  `nomor_polisi` varchar(100) DEFAULT NULL,
  `nomor_bpkb` varchar(100) DEFAULT NULL,
  `asal_usul` varchar(100) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_kib_b`
--

INSERT INTO `sekolah_kib_b` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `per_tahun`, `barang_kode`, `barang_nama`, `register`, `jumlah`, `satuan`, `merk_type`, `ukuran_cc`, `bahan`, `tahun_beli`, `nomor_pabrik`, `nomor_rangka`, `nomor_mesin`, `nomor_polisi`, `nomor_bpkb`, `asal_usul`, `harga`, `ket`, `postdate`) VALUES
('2aa1b4e6654360ea4519dc265402780ae4da3b7fbbce2345d7772b0674a318d5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '5', '6', '5', '6', '5', '6', '5', '7', '8', '9', '0', '9', '8', '7', '6', '5', '4', '2021-08-19 08:45:43'),
('2aa1b4e6654360ea4519dc265402780a45c48cce2e2d7fbdea1afc51c7c6ad26', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '9', '8', '7', '6', '5', '4', '3', '2', '1', '2', '4', '5', '6', '7', '8', '9', '8', '2021-08-19 08:45:43'),
('2aa1b4e6654360ea4519dc265402780a7647966b7343c29048673252e490f736', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '89', '89', '8', '7', '6', '5', '6', '7', '8', '7', '9', '8', '7', '8', '8', '9', '8', '2021-08-19 08:45:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_kib_c`
--

CREATE TABLE `sekolah_kib_c` (
  `kd` varchar(100) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `per_tahun` varchar(4) DEFAULT NULL,
  `barang_kode` varchar(100) DEFAULT NULL,
  `barang_nama` varchar(100) DEFAULT NULL,
  `register` varchar(100) DEFAULT NULL,
  `kondisi` varchar(100) DEFAULT NULL,
  `kontruksi_tingkat` varchar(100) DEFAULT NULL,
  `kontruksi_beton` varchar(100) DEFAULT NULL,
  `luas_lantai` varchar(100) DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `dokumen_tgl` varchar(100) DEFAULT NULL,
  `dokumen_nomor` varchar(100) DEFAULT NULL,
  `tanah_luas` varchar(100) DEFAULT NULL,
  `tanah_status` varchar(100) DEFAULT NULL,
  `tanah_kode` varchar(100) DEFAULT NULL,
  `asal_usul` varchar(100) DEFAULT NULL,
  `tahun_ada` varchar(4) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_kib_c`
--

INSERT INTO `sekolah_kib_c` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `per_tahun`, `barang_kode`, `barang_nama`, `register`, `kondisi`, `kontruksi_tingkat`, `kontruksi_beton`, `luas_lantai`, `alamat`, `dokumen_tgl`, `dokumen_nomor`, `tanah_luas`, `tanah_status`, `tanah_kode`, `asal_usul`, `tahun_ada`, `harga`, `ket`, `postdate`) VALUES
('2aa1b4e6654360ea4519dc265402780af7177163c833dff4b38fc8d2872f1ec6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '44', '5', '6', '7', '8', '9', '8', '7', '6', '5', '4', '5', '6', '8', '7', '8', '7', '2021-08-19 09:01:04'),
('2aa1b4e6654360ea4519dc265402780a45c48cce2e2d7fbdea1afc51c7c6ad26', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '9', '8', '7', '6', '5', '6', '7', '8', '9', '8', '7', '6', '5', '3', '4', '3', '5', '2021-08-19 09:01:04'),
('2aa1b4e6654360ea4519dc265402780aa87ff679a2f3e71d9181a67b7542122c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '4', '5', '6', '7', '8', '9', '0', '9', '8', '7', '6', '5', '4', '4', '3', '4', '5', '2021-08-19 09:01:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_kib_d`
--

CREATE TABLE `sekolah_kib_d` (
  `kd` varchar(100) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `per_tahun` varchar(4) DEFAULT NULL,
  `barang_kode` varchar(100) DEFAULT NULL,
  `barang_nama` varchar(100) DEFAULT NULL,
  `register` varchar(100) DEFAULT NULL,
  `kontruksi` varchar(100) DEFAULT NULL,
  `panjang` varchar(100) DEFAULT NULL,
  `lebar` varchar(100) DEFAULT NULL,
  `luas` varchar(100) DEFAULT NULL,
  `lokasi` longtext DEFAULT NULL,
  `dokumen_tgl` varchar(100) DEFAULT NULL,
  `dokumen_nomor` varchar(100) DEFAULT NULL,
  `tanah_status` varchar(100) DEFAULT NULL,
  `tanah_kode` varchar(100) DEFAULT NULL,
  `asal_usul` varchar(100) DEFAULT NULL,
  `tahun_ada` varchar(4) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `kondisi` varchar(100) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_kib_d`
--

INSERT INTO `sekolah_kib_d` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `per_tahun`, `barang_kode`, `barang_nama`, `register`, `kontruksi`, `panjang`, `lebar`, `luas`, `lokasi`, `dokumen_tgl`, `dokumen_nomor`, `tanah_status`, `tanah_kode`, `asal_usul`, `tahun_ada`, `harga`, `kondisi`, `ket`, `postdate`) VALUES
('2aa1b4e6654360ea4519dc265402780aeccbc87e4b5ce2fe28308fd9f2a7baf3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '3', '4', '5', '6', '7', '8', '9', '8', '7', '6', '5', '4', '5', '6', '7', 'RUSAK', '7', '2021-08-19 09:04:15'),
('2aa1b4e6654360ea4519dc265402780aa87ff679a2f3e71d9181a67b7542122c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '4', '5', '6', '', '7', '8', '7', '6', '5', '4', '3', '', '4', '5', '6', 'BAIK', '6', '2021-08-19 09:04:15'),
('2aa1b4e6654360ea4519dc265402780ac4ca4238a0b923820dcc509a6f75849b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '1', '2', '3', '4', '5', '6', '5', '4', '3', '2', '1', '2', '3', '4', '5', 'BAIK', '5', '2021-08-19 09:04:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_kib_e`
--

CREATE TABLE `sekolah_kib_e` (
  `kd` varchar(100) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `per_tahun` varchar(4) DEFAULT NULL,
  `barang_kode` varchar(100) DEFAULT NULL,
  `barang_nama` varchar(100) DEFAULT NULL,
  `register` varchar(100) DEFAULT NULL,
  `buku_judul` longtext DEFAULT NULL,
  `buku_spek` longtext DEFAULT NULL,
  `corak_asal` varchar(100) DEFAULT NULL,
  `corak_pencipta` varchar(100) DEFAULT NULL,
  `corak_bahan` varchar(100) DEFAULT NULL,
  `hewan_jenis` varchar(100) DEFAULT NULL,
  `hewan_ukuran` varchar(100) DEFAULT NULL,
  `jumlah` varchar(100) DEFAULT NULL,
  `tahun_cetak` varchar(4) DEFAULT NULL,
  `asal_usul` varchar(100) DEFAULT NULL,
  `tahun_beli` varchar(4) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_kib_e`
--

INSERT INTO `sekolah_kib_e` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `per_tahun`, `barang_kode`, `barang_nama`, `register`, `buku_judul`, `buku_spek`, `corak_asal`, `corak_pencipta`, `corak_bahan`, `hewan_jenis`, `hewan_ukuran`, `jumlah`, `tahun_cetak`, `asal_usul`, `tahun_beli`, `harga`, `ket`, `postdate`) VALUES
('2aa1b4e6654360ea4519dc265402780ac9f0f895fb98ab9159f51fd0297e236d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '8', '7', '6', '', '', '', '', '', '4', '5', '6', '7', '8', '9', '8', '7', '2021-08-19 09:05:55'),
('2aa1b4e6654360ea4519dc265402780aa87ff679a2f3e71d9181a67b7542122c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '4', '3', '2', '', '', '3', '4', '5', '6', '7', '8', '9', '8', '7', '6', '44', '2021-08-19 09:05:55'),
('2aa1b4e6654360ea4519dc265402780ac4ca4238a0b923820dcc509a6f75849b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '1', '2', '3', '4', '5', '', '', '', '', '', '7', '6', '5', '4', '3', '3', '2021-08-19 09:05:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_kib_f`
--

CREATE TABLE `sekolah_kib_f` (
  `kd` varchar(100) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `per_tahun` varchar(4) DEFAULT NULL,
  `barang_kode` varchar(100) DEFAULT NULL,
  `barang_nama` varchar(100) DEFAULT NULL,
  `register` varchar(100) DEFAULT NULL,
  `kontruksi_tingkat` varchar(100) DEFAULT NULL,
  `kontruksi_beton` varchar(100) DEFAULT NULL,
  `luas` varchar(100) DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `dokumen_tgl` varchar(100) DEFAULT NULL,
  `dokumen_nomor` varchar(100) DEFAULT NULL,
  `mulai_tgl` varchar(100) DEFAULT NULL,
  `tanah_status` varchar(100) DEFAULT NULL,
  `tanah_kode` varchar(100) DEFAULT NULL,
  `asal_usul` varchar(100) DEFAULT NULL,
  `harga` varchar(100) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_kib_f`
--

INSERT INTO `sekolah_kib_f` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `per_tahun`, `barang_kode`, `barang_nama`, `register`, `kontruksi_tingkat`, `kontruksi_beton`, `luas`, `alamat`, `dokumen_tgl`, `dokumen_nomor`, `mulai_tgl`, `tanah_status`, `tanah_kode`, `asal_usul`, `harga`, `ket`, `postdate`) VALUES
('2aa1b4e6654360ea4519dc265402780ac4ca4238a0b923820dcc509a6f75849b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '1', '2', '3', '4', '5', '6', '7', '8', '9', '8', '7', '6', '5', '4', '3', '2021-08-19 09:07:20'),
('2aa1b4e6654360ea4519dc265402780ac81e728d9d4c2f636f067f89cc14862c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '2', '7', '6', '5', '4', '3', '2', '1', '2', '3', '4', '5', '6', '7', '8', '2021-08-19 09:07:20'),
('2aa1b4e6654360ea4519dc265402780aeccbc87e4b5ce2fe28308fd9f2a7baf3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'WELERI', '2021', '3', '9', '8', '7', '6', '5', '4', '3', '2', '3', '4', '5', '6', '7', '8', '2021-08-19 09:07:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_ks`
--

CREATE TABLE `sekolah_ks` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `ijazah` varchar(100) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_ks`
--

INSERT INTO `sekolah_ks` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `nama`, `kode`, `ijazah`, `tgl_awal`, `tgl_akhir`, `postdate`) VALUES
('51782a1071e5ae0016e7329bf4f8f544', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '1', '1', '1', '2021-08-01', '2021-08-25', '2021-08-09 06:06:57'),
('884f08a4788923525ea45b0aec9be5fe', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '6', '6', '6', '2021-08-09', '2021-08-18', '2021-08-09 06:10:25'),
('71befde55e3bee9165809cacb8641bbb', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '', 'NASIRUL FATAH, SP', '1052756', 'S1', '2017-06-12', '2021-06-12', '2021-08-27 07:17:24'),
('d43b5eb3403434ed02468d5600830584', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Sugiarto, S. Ag.', '900 783', 'S1 xgmringx PAI', '2000-07-10', '2000-07-10', '2021-08-27 08:33:20'),
('17e5a6264e24e6b6977b5ecf3ebc4390', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'ROWOSARI', 'SUHARTI', '196706122000032001', 'S1', '2000-07-13', '2010-07-12', '2021-08-27 10:41:32'),
('f9b5e53546c153c503a7057751ad3749', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'ROWOSARI', 'SUHARTI', '196706122000032001', 'S1', '2000-07-13', '2010-07-12', '2021-08-27 10:43:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_pegawai`
--

CREATE TABLE `sekolah_pegawai` (
  `kd` varchar(50) NOT NULL,
  `nbm` varchar(100) DEFAULT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `kelamin` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `no_karpeg` varchar(100) DEFAULT NULL,
  `nuptk` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `pangkat` varchar(100) DEFAULT NULL,
  `pangkat_sk` varchar(100) DEFAULT NULL,
  `pangkat_sk_no` varchar(100) DEFAULT NULL,
  `pangkat_sk_tgl` varchar(100) DEFAULT NULL,
  `tmt` varchar(100) DEFAULT NULL,
  `kualifikasi` varchar(100) DEFAULT NULL,
  `mk_gol` varchar(100) DEFAULT NULL,
  `mk_total` varchar(100) DEFAULT NULL,
  `pensiun` varchar(100) DEFAULT NULL,
  `pensiun_tgl` date DEFAULT NULL,
  `jenis_ptk` varchar(100) DEFAULT NULL,
  `lahir_tmp` varchar(100) DEFAULT NULL,
  `lahir_tgl` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `bekerja_sejak_disini` varchar(100) DEFAULT NULL,
  `bekerja_sejak_dimuh` varchar(100) DEFAULT NULL,
  `ijazah` varchar(100) DEFAULT NULL,
  `ijazah_pddkn` varchar(100) DEFAULT NULL,
  `mengajar` varchar(100) DEFAULT NULL,
  `sertifikasi` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `sumber_gaji` varchar(100) DEFAULT NULL,
  `jml_anak` varchar(5) DEFAULT NULL,
  `status_sertifikasi` varchar(100) DEFAULT NULL,
  `jml_gaji_pokok` varchar(100) DEFAULT NULL,
  `jml_tunjangan_pasangan` varchar(100) DEFAULT NULL,
  `jml_tunjangan_bpjs` varchar(100) DEFAULT NULL,
  `jml_honor` varchar(100) DEFAULT NULL,
  `jml_tunjangan_profesi` varchar(100) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `usernamex` varchar(100) DEFAULT NULL,
  `passwordx` varchar(100) DEFAULT NULL,
  `email` longtext DEFAULT NULL,
  `user_sarpras` enum('true','false') DEFAULT 'false',
  `user_keu` enum('true','false') DEFAULT 'false',
  `user_kepeg` enum('true','false') NOT NULL DEFAULT 'false',
  `postdate_user` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_pegawai`
--

INSERT INTO `sekolah_pegawai` (`kd`, `nbm`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `nama`, `kelamin`, `alamat`, `telp`, `kode`, `no_karpeg`, `nuptk`, `jabatan`, `pangkat`, `pangkat_sk`, `pangkat_sk_no`, `pangkat_sk_tgl`, `tmt`, `kualifikasi`, `mk_gol`, `mk_total`, `pensiun`, `pensiun_tgl`, `jenis_ptk`, `lahir_tmp`, `lahir_tgl`, `status`, `bekerja_sejak_disini`, `bekerja_sejak_dimuh`, `ijazah`, `ijazah_pddkn`, `mengajar`, `sertifikasi`, `postdate`, `sumber_gaji`, `jml_anak`, `status_sertifikasi`, `jml_gaji_pokok`, `jml_tunjangan_pasangan`, `jml_tunjangan_bpjs`, `jml_honor`, `jml_tunjangan_profesi`, `ket`, `usernamex`, `passwordx`, `email`, `user_sarpras`, `user_keu`, `user_kepeg`, `postdate_user`) VALUES
('c81e728d9d4c2f636f067f89cc14862c', '2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2', NULL, '2', '2', '2', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SUDAH', '2021-08-22', NULL, '2', '2021-08-02', NULL, '2', '2', '2', '2', '2', '2', '2021-08-05 01:45:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', '035f105ff246dd73af10ab601c34bebd', '2', 'false', 'true', 'true', '2021-08-08 12:40:20'),
('eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '3', NULL, '3111', '3', '3', NULL, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SUDAH', '2021-08-02', NULL, '3', '2021-08-08', NULL, '3', '3', '3', '3', '3', '3', '2021-08-18 11:15:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', 'false', 'true', 'false', '2021-08-08 12:40:23'),
('c4ca4238a0b923820dcc509a6f75849b', '1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '1', NULL, '1', '1', '1', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2021-08-08', NULL, '1', '1', '1', '1', '1', '1', '2021-08-05 01:45:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', 'true', 'true', 'true', '2021-08-08 12:40:18'),
('663a7589bc5acf889d96ea17a96e57d8', '5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '5', NULL, '5', '5', '5', NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BELUM', '2021-08-17', NULL, '5', '2021-08-17', NULL, '5', '5', '5', '5', '5', '5', '2021-08-18 11:17:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5', 'e4da3b7fbbce2345d7772b0674a318d5', '5', 'false', 'false', 'false', NULL),
('681d2345d913eec7d11b39863e5bf33b', '1267206', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '', 'SAFIxpsijixI', NULL, 'Caruban', '089530940043', '1267206', NULL, NULL, 'Kepala Madrasah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BELUM', '0000-00-00', NULL, 'Kendal', '1983-11-29', NULL, '2005', '2005', 's1', 'STIT MUHAMMADIYAH KENDAL', 'guru mapel', 'sudah', '2021-08-26 11:48:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1267206', '111dad60fc1c15fbe0e7ee00b6afd76a', 'bangsafiixtkeongxgmail.com', 'false', 'false', 'false', '2021-08-26 11:58:51'),
('0c334de5708156f06b51174c5c71e2fd', '112281071021299', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '', 'Khosiun', NULL, 'caruban, rt.03 rw.04, Ringinarum', '0859160122624', '112281071021299', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BELUM', '0000-00-00', NULL, 'Kendal', '1981-09-24', NULL, '2005', '2005', 's1', 'STAIN', 'guru kelas', 'sudah', '2021-08-26 11:53:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '112281071021299', '650bd6d2ad23d325e03dfc2bd9826101', 'khosiun838xtkeongxgmail.com', 'false', 'false', 'false', NULL),
('031fddc3c03b414c1b3d3cc76cdc059b', '900 783', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Sugiarto, S. Ag.', NULL, 'RT.02 xgmringx RW.02 Ds. Caruban Kec. Ringinarum, Kab. Kendal', '0838 6744 9774', '900 783', NULL, NULL, 'Kepala Madrasah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'BELUM', '0000-00-00', NULL, 'Kendal', '1969-07-15', NULL, '2000', '2000', 'S1 xgmringx PAI', 'xstrix', 'Bahasa Indonesia', 'Sudah', '2021-08-27 09:56:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '900 783', '4cd5a0b6bc293f1e306e0d23170d649c', 'xstrix', 'false', 'false', 'false', NULL),
('eea7130a727cabd9748f2bf1b1cb0b44', '727 841', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Drs. H. Mahmudi', NULL, 'RT 03 RW 11 Ds. Penyangkringan, Kec. Weleri, Kab. Kendal', '0813 2571 9048', '727 841', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1960-07-02', NULL, '2002', '1991', 'S1xgmringxGEOGRAFI', '', 'IPS Terpadu', 'Tidak', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '727 841', 'eea7130a727cabd9748f2bf1b1cb0b44', '', 'false', 'false', 'false', NULL),
('eab61d6d14ec9c85188ea51cde9b01f5', '740 569', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Drs. Solikhin', NULL, 'Ds. Purworejo RT 01 xgmringx RW 01 Kec. Ringinarum Kab. Kendal', '0882 1514 6748', '740 569', NULL, NULL, 'Guru xgmringx Waka Kesiswaan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1963-09-25', NULL, '1993', '1993', 'S1xgmringxPAI', '', 'Akidah Akhlak', 'Sudah', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '740 569', 'eab61d6d14ec9c85188ea51cde9b01f5', '', 'false', 'false', 'false', NULL),
('a8156b7a705fa443acd455b5f8f33de6', '1 089 641', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Drs. Sunardi', NULL, 'Ds. Karanganom RT 06 RW 02 Karanganom Weleri', '0838 3879 3583', '1 089 641', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sukoharjo', '1964-06-05', NULL, '2002', '2002', 'S1xgmringxPAI', '', 'PKNxgmringxSeni Budaya', 'Sudah', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 089 641', 'a8156b7a705fa443acd455b5f8f33de6', '', 'false', 'false', 'false', NULL),
('af1143f8c9598faa220b81aec6093a56', '900 778 xgmringx 197409292007102005', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Hj. Erli Baroroh, ST.', NULL, 'RT 3 RW 1 Desa Karangsari Kec . Rowosari, Kab. Kendal', '0856 4083 3059', '900 778 xgmringx 197409292007102005', NULL, NULL, 'Guru xgmringx Waka Kurikulum', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1974-09-29', NULL, '2000', '2000', 'S1xgmringxKIMIA', '', 'IPA', 'Sudah', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '900 778 / 197409292007102005', 'c284bf7c579341dcf004df50207a7c72', '', 'false', 'false', 'false', NULL),
('e3bfb56a85e953f5008121a851ee460d', '1 017 723', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Mohamad Abdul Ghofur, S. Pd. I.', NULL, 'Desa Sukolilan RT 04xgmringx RW 02 Kec. Patebon Kab. Kendal', '0813 2576 5470', '1 017 723', NULL, NULL, 'Guru xgmringx Operator', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1975-04-16', NULL, '2000', '2000', 'S1xgmringxPAI', '', 'Bahasa Arab', 'Sudah', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 017 723', 'e3bfb56a85e953f5008121a851ee460d', '', 'false', 'false', 'false', NULL),
('f29a48224c0a5c166d55ef54fe1d3d34', '884 185', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Surip Untung, S. Pd. I.', NULL, 'RT 03 RW 02 Ds. Tanjunganom, Kec. Rowosari, Kab. Kendal', '0823 1458 5423', '884 185', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1978-11-06', NULL, '2002', '2002', 'S1xgmringxPAI', '', 'SKI xgmringx Fikih xgmringx Kemuhammadiyahan', 'Sudah', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '884 185', 'f29a48224c0a5c166d55ef54fe1d3d34', '', 'false', 'false', 'false', NULL),
('21e3c0bb33f6f6da79b632aeddf69c3f', '1 089 464', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Hj. Rohbiati, S. Pd. I.', NULL, 'RT 04 RW 03 Ds. Sidomukti, Kec. Weleri, Kab Kendal', '0819 0563 9470', '1 089 464', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1976-12-08', NULL, '2008', '2008', 'S1xgmringxPAI', '', 'Keterampilan', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 089 464', '21e3c0bb33f6f6da79b632aeddf69c3f', '', 'false', 'false', 'false', NULL),
('bc0ebbfc7e5aefd22f787e5263f81bf9', '1 126 400', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Amin Nur Rahmawati, S. Pd.', NULL, 'Ds. Tratemulyo RTxgmringxRW 01xgmringx01 Kec. Weleri, Kab. Kendal.', '0838 3803 4395', '1 126 400', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Salatiga', '1978-06-25', NULL, '2010', '2010', 'S1xgmringxBIOLOGI', '', 'Matematika', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 126 400', 'bc0ebbfc7e5aefd22f787e5263f81bf9', '', 'false', 'false', 'false', NULL),
('bcfd2032e21fe46c0ce5bb9462913297', '1 089 462', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Romanto, S. Pd. I.', NULL, 'Tanjunganom RT 03 RW.02 Kec. Rowosari Kab. Kendal kode pos 51354', '0852 1796 2541', '1 089 462', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1974-08-03', NULL, '2021', '2006', 'S1xgmringxPAI', '', 'Kemuhammadiyahan', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 089 462', 'bcfd2032e21fe46c0ce5bb9462913297', '', 'false', 'false', 'false', NULL),
('dbd66ba85b9b6991226ea44fa7745183', '1 089 460', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Lies Arofah, S. Pd. I.', NULL, 'Desa Tanjungsari RT 4xgmringx4 Kec. Rowosari Kab. Kendal', '0838 3817 4352', '1 089 460', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1991-06-18', NULL, '2010', '2010', 'S1xgmringxPAI', '', 'AlxstrixQurxpsijixan Hadits', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 089 460', 'dbd66ba85b9b6991226ea44fa7745183', '', 'false', 'false', 'false', NULL),
('ae9f3c3c8330790e0e59f1383e24796d', '1 312 581', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Angga Prasetya Nugraha, S. Pd.', NULL, 'Ds. Penyangkringan, RT.01 RW.12 Kec, Weleri, Kab. Kendal', '0896 6568 6146', '1 312 581', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1989-11-12', NULL, '2016', '2016', 'S1xgmringxPend. PJOK', '', 'Penjas Orkes', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 312 581', 'ae9f3c3c8330790e0e59f1383e24796d', '', 'false', 'false', 'false', NULL),
('458db9a2130d1d5880765bc4e2f825a1', '1 283 222', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Laela Fitriana, S. Pd.', NULL, 'Ds. Penaruban, Kec. Weleri, Kab. Kendal', '0857 7925 4474', '1 283 222', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1994-03-05', NULL, '2017', '2017', 'S1xgmringxMatematika', '', 'MatematikaxgmringxTIK', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 283 222', '458db9a2130d1d5880765bc4e2f825a1', '', 'false', 'false', 'false', NULL),
('fe84b84af40e94583c63ea0a31645ad0', '1 337 658', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Luluk Fatwiana Muslimah, S. Pd.', NULL, 'Dk. Gendogosari, Ds. Krengseng Rt. 02 RW. 05 Kec. Gringsing, Kab. Batang', '0822 4152 4938', '1 337 658', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Klaten', '1994-05-13', NULL, '2019', '2019', 'S1xgmringxBahasa Inggris', '', 'Bahasa Inggris', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 337 658', 'fe84b84af40e94583c63ea0a31645ad0', '', 'false', 'false', 'false', NULL),
('0ac907cd4f1e01d8666d29f68b9bf206', '1 089 493', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Astrini Ningsih, S. Pd.', NULL, 'Ds. Sambongsari RT 04 RW 01 Kec. Weleri, Kab. Kendal', '087 764 485 708', '1 089 493', NULL, NULL, 'Guru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kendal', '1983-09-11', NULL, '2020', '2008', 'S1xgmringxSosiologi', '', 'IPS Terpadu', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 089 493', '0ac907cd4f1e01d8666d29f68b9bf206', '', 'false', 'false', 'false', NULL),
('d41d8cd98f00b204e9800998ecf8427e', '', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Ichsan Slamet', NULL, '', '', '', NULL, NULL, 'Ka. Lab.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00', NULL, '2021', '', '', '', '', '', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'd41d8cd98f00b204e9800998ecf8427e', '', 'false', 'false', 'false', NULL),
('f35d2163aea69b0d928cb37d606e4c84', '1 331 090', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', 'Sulistiyanto', NULL, 'Dk. Gondangsari RT 01 RW 05 Ds. Randu, Kec. Pecalungan, Kab. Batang', '0823 2280 7280', '1 331 090', NULL, NULL, 'TU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Batang', '1996-07-11', NULL, '2018', '2013', 'SLTA', '', 'Staff Tata Usaha', 'Belum Sertifikasi', '2021-08-27 10:11:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1 331 090', 'f35d2163aea69b0d928cb37d606e4c84', '', 'false', 'false', 'false', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_struktur_organisasi`
--

CREATE TABLE `sekolah_struktur_organisasi` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `nourut` varchar(10) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_struktur_organisasi`
--

INSERT INTO `sekolah_struktur_organisasi` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `nourut`, `kode`, `nama`, `jabatan`, `tgl_awal`, `tgl_akhir`, `postdate`) VALUES
('4a35540c85896ff829821847ab8351f4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '1', '1', '1', '1', '2021-08-03', '2021-08-19', '2021-08-18 17:39:36'),
('78b665e92a01b3a86c1ff6f9b331548a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2', '2', '2', '2', '2021-08-18', '2021-08-26', '2021-08-18 17:40:04'),
('87ed60b6c32e03d0e3cd382cf09d83c3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '3', '3', '3', '3', '2021-08-17', '2021-08-26', '2021-08-18 17:40:15'),
('2e054eed69271027394623d3380b33c5', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '1', '900783', 'Sugiarto, S. Ag.', 'Kepala Madrasah MTs Muhammadiyah 1 Weleri', '2000-07-10', '2000-07-10', '2021-08-26 11:50:03'),
('1af2fff38cb4383e972b3481ef3aa5c6', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '2', '900 778 xgmringx1974092920071 02 005', 'Hj. Erli Baroroh, ST.', 'Waka Kurikulum xgmringx Guru', '2000-07-10', '2000-07-10', '2021-08-27 08:43:06'),
('135b4e768fae0a6139c6230574912c0f', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '3', '740 569', 'Drs. Solikhin', 'Waka Kesiswaan xgmringx Guru', '1993-07-12', '1993-07-12', '2021-08-27 08:45:17'),
('56d4a51dc312d37f8b219c578c1d9fe9', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '4', '727 841', 'Drs. H. Mahmudi', 'Guru', '2002-07-15', '2020-01-06', '2021-08-27 08:53:14'),
('e37b82e67a9cc06f55c8234751a19913', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '5', '1089 641', 'Drs. Sunardi', 'Guru', '2002-07-15', '2002-07-15', '2021-08-27 08:54:54'),
('e1351d66896f3487f7ce98b652983655', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '6', '1017 723', 'Mohamad Abdul Ghofur, S. Pd. I.', 'Guru xgmringx Operator', '2000-07-10', '2000-07-10', '2021-08-27 08:57:16'),
('5bba5795b42e47e51c28e6fd7a489379', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '7', '884 185', 'Surip Untung, S. Pd. I.', 'Guru', '2002-07-15', '2002-07-15', '2021-08-27 09:01:40'),
('c4317d4db585465d8bacbcd6781b7659', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '8', '1089 464', 'Hj. Rohbiati, S. Pd. I.', 'Guru', '2008-07-14', '2008-07-14', '2021-08-27 09:02:50'),
('b09bd21055c8c19d3f2f1f6583c07913', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '9', '1126 400', 'Amin Nur Rahmawati, S. Pd.', 'Guru', '2010-07-12', '2010-07-12', '2021-08-27 09:04:30'),
('5f8cd6b30d18b0e0ddf6009906a95410', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '10', '1089 460', 'Lies Arofah, S. Pd. I.', 'Guru', '2010-07-12', '2010-07-12', '2021-08-27 09:05:47'),
('7cbe63e0497f46fafb9e37edbf20a39d', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '11', '1312 581', 'Angga Prasetya Nugraha, S. Pd.', 'Guru', '2016-07-11', '2016-07-11', '2021-08-27 09:07:17'),
('8ab09ffa8359cfd34b1dab198f0ca0ce', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '12', '1283 222', 'Laela Fitriana, S. Pd.', 'Guru', '2017-07-10', '2017-07-10', '2021-08-27 09:08:19'),
('7d4a19a6f905fcf61c6870f811de1509', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '13', '1337 658', 'Luluk Fatwiana Muslimah, S. Pd.', 'Guru', '2019-07-15', '2019-07-15', '2021-08-27 09:10:01'),
('7907df8135035eb1ed083d47c69c63dc', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '14', '1089 462', 'Romanto, S. Pd. I.', 'Guru', '2021-07-12', '2021-07-12', '2021-08-27 09:11:15'),
('a8ae47ad278f653363545aac29871477', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '15', '1089 493', 'Astrini Ningsih, S. Pd.', 'Guru', '2020-07-13', '2020-07-13', '2021-08-27 09:12:54'),
('42e54993ebdaafa3439efc6444aaa36c', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '16', '1331 090', 'Sulistiyanto', 'Staff TU', '2018-07-16', '2018-07-16', '2021-08-27 09:14:00'),
('298ff8c05d54237aff47f3f18f64d1ba', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '17', 'xstrix', 'Sodikin', 'Petugas Kebersihan', '2008-06-16', '2008-06-16', '2021-08-27 09:15:31'),
('beb8d9d06abdcc9824867d8322986012', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', 'WELERI', '18', 'xstrix', 'Ichsan Slamet', 'Ka. Lab.', '2021-07-12', '2021-07-12', '2021-08-27 09:16:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_uang_keluar`
--

CREATE TABLE `sekolah_uang_keluar` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `per_tgl` date DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nilai` varchar(50) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_uang_keluar`
--

INSERT INTO `sekolah_uang_keluar` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `per_tgl`, `tipe`, `kode`, `nama`, `nilai`, `ket`, `postdate`) VALUES
('a9fe0f45a20fdfb0f3bd7ecd23ecfe1c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2021-08-05', 'BELANJA', 'DK001', 'belanja beras', '180000', 'xstrix', '2021-08-07 03:42:35'),
('442a05e954630a8c7317684480fb04e4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2021-08-13', 'KEGIATAN', 'KG001', 'agustusan', '2500000', 'xstrix', '2021-08-07 03:42:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sekolah_uang_masuk`
--

CREATE TABLE `sekolah_uang_masuk` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `cabang` varchar(100) DEFAULT NULL,
  `per_tgl` date DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nilai` varchar(50) DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sekolah_uang_masuk`
--

INSERT INTO `sekolah_uang_masuk` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `cabang`, `per_tgl`, `tipe`, `kode`, `nama`, `nilai`, `ket`, `postdate`) VALUES
('e1407cfbb2a3faf3f0552d9bb26d041e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2021-08-19', 'KOMITE', 'KD001', 'coba', '140000', 'xstrix', '2021-08-07 03:39:06'),
('77bb08ad06b7b72251159d8cd9dfc55d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2021-08-04', 'HUTANG', '147', '147', '1900000', 'xstrix', '2021-08-07 03:38:11'),
('e7bab78287b3a936b0b17367e3e36f46', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'CABANG 1', '2021-08-17', 'HUTANG', '111', '111', '289000', 'xstrix', '2021-08-09 06:22:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `user_kd` varchar(100) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `postdate`) VALUES
('7ce12f077f14764fa25c0ffa0b26a7a4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '7ce12f077f14764fa25c0ffa0b26a7a4', '8', '8', 'SEKOLAH', 'PEGAWAI', '2021-08-10 05:08:18'),
('2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', 'TU', 'TATA USAHA', 'SEKOLAH', 'TATA USAHA', '2021-08-10 05:07:42'),
('c4713f0d864e2f8c3fe229dea2cf0044', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4713f0d864e2f8c3fe229dea2cf0044', '8', '8', 'SEKOLAH', 'PEGAWAI', '2021-08-10 05:13:43'),
('e2c74b84ca8cf054bd1a4ccb5463dccf', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', 'TU', 'TATA USAHA', 'SEKOLAH', 'TATA USAHA', '2021-08-13 17:34:57'),
('c4ca4238a0b923820dcc509a6f75849b', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'SEKOLAH', 'KEPEGAWAIAN', '2021-08-17 17:14:47'),
('aac132913633edf298b43ce4063258f9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'aac132913633edf298b43ce4063258f9', '7', '7', 'SEKOLAH', 'PEGAWAI', '2021-08-17 17:18:08'),
('009d3025204e107a90b3bd80abcc86df', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '009d3025204e107a90b3bd80abcc86df', '8', '8', 'SEKOLAH', 'PEGAWAI', '2021-08-17 17:20:44'),
('55fcbfed5bba4a04a0f5ce5d2caf537b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '55fcbfed5bba4a04a0f5ce5d2caf537b', '8', '8', 'SEKOLAH', 'PEGAWAI', '2021-08-17 17:27:35'),
('735d3c21abcd01cbe1b6dafda9e940f4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '735d3c21abcd01cbe1b6dafda9e940f4', '9', '9', 'SEKOLAH', 'PEGAWAI', '2021-08-17 17:28:41'),
('1abed4e929ccac662985531da42a7b4d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '1abed4e929ccac662985531da42a7b4d', '8', '8', 'SEKOLAH', 'PEGAWAI', '2021-08-17 17:29:42'),
('f87a0e6006d39d2d16f1258ff4036c18', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'f87a0e6006d39d2d16f1258ff4036c18', '4', '4', 'SEKOLAH', 'PEGAWAI', '2021-08-17 17:31:32'),
('53d259f99454c7910a63fa5ab3400b08', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '53d259f99454c7910a63fa5ab3400b08', '8', '8', 'SEKOLAH', 'PEGAWAI', '2021-08-17 17:32:43'),
('09bb261d4786e01bc654a01f1efa607f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '09bb261d4786e01bc654a01f1efa607f', '8', '8', 'SEKOLAH', 'PEGAWAI', '2021-08-17 22:19:47'),
('663a7589bc5acf889d96ea17a96e57d8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '2021-08-18 11:16:06'),
('3b6b780721f92ed4fb8031ce543baa8e', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', '3b6b780721f92ed4fb8031ce543baa8e', '6', '6', 'CABANG', 'PEGAWAI', '2021-08-19 16:03:38'),
('2b9e106b361857497d40f343f8a11aec', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', '2b9e106b361857497d40f343f8a11aec', '6', '6', 'CABANG', 'PEGAWAI', '2021-08-19 16:05:41'),
('3c3f5d6a250c9eb90625ea5e7150450c', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', '3c3f5d6a250c9eb90625ea5e7150450c', '1', '1', 'CABANG', 'PEGAWAI', '2021-08-19 16:08:24'),
('fd9189c6d6bcc8869fafe813529d10ed', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', 'fd9189c6d6bcc8869fafe813529d10ed', '1', '1', 'CABANG', 'PEGAWAI', '2021-08-19 16:10:31'),
('a8109d33c253792bcc8970b5bfc8c97d', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', 'a8109d33c253792bcc8970b5bfc8c97d', '2', '2', 'CABANG', 'PEGAWAI', '2021-08-19 16:11:12'),
('13b37d0451dbd8f56cf428a95a6a0ce7', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', '13b37d0451dbd8f56cf428a95a6a0ce7', '3', '3', 'CABANG', 'PEGAWAI', '2021-08-19 16:11:50'),
('8e1241bd58939d1247f1ddef0ec7fcb3', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', '8e1241bd58939d1247f1ddef0ec7fcb3', '3', '3', 'CABANG', 'PEGAWAI', '2021-08-19 16:13:10'),
('1cbb7ab4cbc098f24c8eb30e09418ddf', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', '1cbb7ab4cbc098f24c8eb30e09418ddf', '3', '3', 'CABANG', 'PEGAWAI', '2021-08-19 16:15:20'),
('63bf7be4295dd152ae49789ca0460ba2', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', '63bf7be4295dd152ae49789ca0460ba2', '7', '7', 'CABANG', 'PEGAWAI', '2021-08-19 16:17:16'),
('c1473dbdcd81e57317b480726f667d41', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', 'c1473dbdcd81e57317b480726f667d41', '1', '1', 'CABANG', 'PEGAWAI', '2021-08-19 16:23:13'),
('d4d8e1b56cea0b8195a64fa811904041', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', 'd4d8e1b56cea0b8195a64fa811904041', '2', '2', 'CABANG', 'PEGAWAI', '2021-08-19 16:23:31'),
('20224469d9d66d1bd34ae99bb5166ce7', '', 'MAJELIS', 'MAJELIS', '20224469d9d66d1bd34ae99bb5166ce7', '1', '1', 'MAJELIS', 'PEGAWAI', '2021-08-25 11:17:47'),
('b2e817a7e175542a5724cb30ef59f46b', '', 'MAJELIS', 'MAJELIS', 'b2e817a7e175542a5724cb30ef59f46b', '1', '1', 'MAJELIS', 'PEGAWAI', '2021-08-25 11:19:28'),
('2df1370b6992160bb47a5981d226356a', '', 'MAJELIS', 'MAJELIS', '2df1370b6992160bb47a5981d226356a', '1', '1', 'MAJELIS', 'PEGAWAI', '2021-08-25 11:20:28'),
('53e4a38db9ddd711318d74c10a21352a', '', 'MAJELIS', 'MAJELIS', '53e4a38db9ddd711318d74c10a21352a', '1', '1', 'MAJELIS', 'PEGAWAI', '2021-08-25 11:21:09'),
('c66a721f2a27d6149c5e17717d1bddfe', '', 'MAJELIS', 'MAJELIS', 'c66a721f2a27d6149c5e17717d1bddfe', '2', '2', 'MAJELIS', 'PEGAWAI', '2021-08-25 11:21:36'),
('39b0d5ce3de8ccdc1c7e236f0eb0dce5', '', 'MAJELIS', 'MAJELIS', '39b0d5ce3de8ccdc1c7e236f0eb0dce5', '2', '2', 'MAJELIS', 'PEGAWAI', '2021-08-25 11:21:56'),
('0796c2abd0b6e76e2d2b3ae48e81247c', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', 'TU', 'TATA USAHA', 'SEKOLAH', 'TATA USAHA', '2021-08-26 11:37:01'),
('ad6918ab849ae690290632883c6c3daa', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'ad6918ab849ae690290632883c6c3daa', 'TU', 'TATA USAHA', 'SEKOLAH', 'TATA USAHA', '2021-08-26 11:42:15'),
('32001d7b102c279327a34c49320859e8', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', 'TU', 'TATA USAHA', 'SEKOLAH', 'TATA USAHA', '2021-08-26 11:45:07'),
('681d2345d913eec7d11b39863e5bf33b', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '681d2345d913eec7d11b39863e5bf33b', '1267206', 'SAFIxpsijixI', 'SEKOLAH', 'PEGAWAI', '2021-08-26 11:48:39'),
('0c334de5708156f06b51174c5c71e2fd', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '0c334de5708156f06b51174c5c71e2fd', '112281071021299', 'Khosiun', 'SEKOLAH', 'PEGAWAI', '2021-08-26 11:53:33'),
('5f3232a257290bb1ada9c5be5cbd93c2', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', 'TU', 'TATA USAHA', 'SEKOLAH', 'TATA USAHA', '2021-08-27 07:17:41'),
('0d26d05f3cdf29155ac0c569f3bf5ead', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', '0d26d05f3cdf29155ac0c569f3bf5ead', 'TU', 'TATA USAHA', 'SEKOLAH', 'TATA USAHA', '2021-08-27 09:41:02'),
('031fddc3c03b414c1b3d3cc76cdc059b', '0d26d05f3cdf29155ac0c569f3bf5ead', '38', 'MTsM 1 Weleri', '031fddc3c03b414c1b3d3cc76cdc059b', '900 783', 'Sugiarto, S. Ag.', 'SEKOLAH', 'PEGAWAI', '2021-08-27 09:56:47'),
('cef73538b0868c9a1183b335066a6caa', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', 'TU', 'TATA USAHA', 'SEKOLAH', 'TATA USAHA', '2021-08-27 10:53:39'),
('3bf21607a990f50b88a4dc62f04411dc', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', '3bf21607a990f50b88a4dc62f04411dc', '1', '1', 'CABANG', 'PEGAWAI', '2021-08-28 16:18:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_filebox`
--

CREATE TABLE `user_filebox` (
  `kd` varchar(50) NOT NULL,
  `user_kd` varchar(100) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_kategori` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `ket` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_filebox`
--

INSERT INTO `user_filebox` (`kd`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_kategori`, `user_jabatan`, `judul`, `ket`, `postdate`) VALUES
('25f45f6bad80ce8e9dbd1370c59b9570', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', '123', 'SEKOLAH', 'PEGAWAI', 'teori marketplace', 'teori marketplace nih...', '2021-08-18 17:27:34'),
('64ae8656e92767fd60294a6e8884afd3', '2aa1b4e6654360ea4519dc265402780a', '', '', '', 'SEKOLAH', 'TATA USAHA', 'coba linux', 'coba saja...', '2021-08-18 21:18:35'),
('1969d83f5b7e638abc8a4ead442cd850', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', '123', 'SEKOLAH', 'KEPEGAWAIAN', 'belajar...', 'coba...', '2021-08-18 21:23:45'),
('2546cb60b9a44fb308a13a0620e81c1e', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', '123', 'SEKOLAH', 'SARPRAS', 'ok..', 'coba...', '2021-08-18 21:26:59'),
('5c1dd92cd4c6f11b8c8e27a0bff9dff2', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', '123', 'SEKOLAH', 'KEUANGAN', 'teknik lagi', 'tekniknih.....', '2021-08-18 21:32:00'),
('62f8ebe8d5349ea061d438d547afd8b8', 'c4ca4238a0b923820dcc509a6f75849b', '', '', 'WELERI', 'CABANG', 'PEGAWAI', 'coba ah', 'coba uh..', '2021-08-24 09:48:13'),
('a8aa9703f5570ed60154a60cea2baf11', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', '123x', 'SEKOLAH', 'SARPRAS', 'coba apache', 'xstrix', '2021-08-24 10:41:41'),
('c4a7f766187aa72e16e3a59c9f74b998', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', '123x', 'SEKOLAH', 'KEUANGAN', 'okkkk', 'oke', '2021-08-24 10:47:43'),
('b137c34e2a936e9ab6f26f24a939bb65', '51e2e1372367f9d1cc28da23ba043f0c', '', '', '', 'CABANG', 'TATA USAHA', 'aku', 'coba...', '2021-08-26 05:59:04'),
('cf0351ae8077fc561576e5137e96b46b', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '', 'MAJELIS', 'TATA USAHA', 'coba', 'ok...', '2021-08-26 06:01:55'),
('067820d79e7456a94e37c8c6f71eeff0', '2aa1b4e6654360ea4519dc265402780a', '', '', '', 'SEKOLAH', 'TATA USAHA', 'coba lagi', 'ok..', '2021-08-26 08:05:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_forum`
--

CREATE TABLE `user_forum` (
  `kd` varchar(50) NOT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `jml_dilihat` varchar(100) DEFAULT NULL,
  `jml_komentar` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_forum`
--

INSERT INTO `user_forum` (`kd`, `judul`, `isi`, `jml_dilihat`, `jml_komentar`, `postdate`) VALUES
('2680c270b63b495c504828ccb15fc80f', 'Uji Coba Entri Data', 'Silahkan bisa melakukan entri data, pada tiap harinya. atau selembat xstrix lambatnya, tiap minggu ada entri data secara rutin.', '7', '2', '2021-08-27 10:04:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_forum_comment`
--

CREATE TABLE `user_forum_comment` (
  `kd` varchar(50) NOT NULL,
  `forum_kd` varchar(50) DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_kategori` varchar(100) DEFAULT NULL,
  `user_ket` varchar(100) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_forum_comment`
--

INSERT INTO `user_forum_comment` (`kd`, `forum_kd`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_kategori`, `user_ket`, `isi`, `postdate`) VALUES
('9a065b6324bdcb7d905ebf3dfb703646', '2680c270b63b495c504828ccb15fc80f', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'Tata Usaha', 'MAJELIS', 'MAJELIS', 'ok...', '2021-08-27 10:04:19'),
('9abab0bbea093f6e83b242e8b021c4fa', '2680c270b63b495c504828ccb15fc80f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '123', 'siap', '2021-08-27 16:18:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_log_entri`
--

CREATE TABLE `user_log_entri` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `ket` longtext DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_log_entri`
--

INSERT INTO `user_log_entri` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `ket`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('bc43d8f011f78a194a3df0ecd56240ee', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : KEPALA SEKOLAH]. UPDATE xstrix&ampxkommaxgtxkommax 1. 1', 'true', '2021-08-19 09:13:46', '2021-08-09 06:07:32'),
('51782a1071e5ae0016e7329bf4f8f544', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : KEPALA SEKOLAH]. ENTRI BARU xstrix&ampxkommaxgtxkommax 1. 1', 'true', '2021-08-19 09:13:46', '2021-08-09 06:06:57'),
('884f08a4788923525ea45b0aec9be5fe', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : KEPALA SEKOLAH]. ENTRI BARU : 6. 6', 'true', '2021-08-19 09:13:46', '2021-08-09 06:10:25'),
('0d47036450a6f51e2c9ff726e6a10ea6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : KEPALA SEKOLAH]. UPDATE : 1. 1', 'true', '2021-08-19 09:13:46', '2021-08-09 06:10:36'),
('033097c937d4fc1261fa7fe6703fa1ac', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : PROFIL SEKOLAH]. UPDATE', 'true', '2021-08-19 09:13:46', '2021-08-09 06:12:09'),
('a5e4636cf9b62c2d262ca93a06a26f46', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2021-08-19 09:13:46', '2021-08-09 06:12:58'),
('e7bab78287b3a936b0b17367e3e36f46', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEUANGAN]. Uang Masuk. ENTRI BARU : [HUTANG]. PER TANGGAL : 2021xstrix08xstrix17', 'true', '2021-08-19 09:13:46', '2021-08-09 06:22:02'),
('e77c51eb48306f4704f20de02d84eefe', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. ENTRI BARU : 7', 'true', '2021-08-19 09:13:46', '2021-08-10 05:02:00'),
('fe06ccdc1b537c58140e30159c4efec6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. HAPUS DATA', 'true', '2021-08-19 09:13:46', '2021-08-10 05:07:49'),
('5885426baea06602c8125ccc03a5bc96', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. ENTRI BARU : 8', 'true', '2021-08-19 09:13:46', '2021-08-10 05:08:18'),
('91ca86f0c14cc75dce5b308dc870bf5d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. ENTRI BARU : 8', 'true', '2021-08-19 09:13:46', '2021-08-10 05:13:43'),
('ed27dead20ab32bc548b67cbdce7be28', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. ArtikelxgmringxInfoxgmringxBerita]. ENTRI : 1', 'true', '2021-08-19 09:13:46', '2021-08-11 09:40:59'),
('8be736f88cb9f783342c892cbba82119', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. Galeri Foto]. ENTRI FOTO : sffff', 'true', '2021-08-19 09:13:46', '2021-08-11 09:42:51'),
('ded2919f7a8299fa75852c487d7c97d8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. Link Youtube]. ENTRI : 2233', 'true', '2021-08-19 09:13:46', '2021-08-11 09:44:05'),
('0000d211161e7fcfe1705caa854a604d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. Link Youtube]. UPDATE :', 'true', '2021-08-19 09:13:46', '2021-08-11 09:44:24'),
('a100fd0208bda96979d4ff8ecbf4ccbc', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [KIRIM NASKAH]. Galeri Foto]. KIRIM NASKAH FOTO : sfffff', 'true', '2021-08-19 09:13:46', '2021-08-11 17:23:43'),
('282ef6f9fa4bf7f0c5a44ccb9d568115', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [KIRIM NASKAH]. Galeri Foto]. KIRIM NASKAH FOTO : drrrr', 'true', '2021-08-19 09:13:46', '2021-08-11 17:23:56'),
('0a5bf31ba6003ccb3a33b9d64c97994f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [KIRIM NASKAH]. Link Youtube]. ENTRI : sfrrr', 'true', '2021-08-19 09:13:46', '2021-08-11 17:29:32'),
('6dfe08d3ac47bb371fe8cf124e6c483f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [KIRIM NASKAH]. Link Youtube]. HAPUS :', 'true', '2021-08-19 09:13:46', '2021-08-11 17:29:45'),
('f6454e9e08a9c8d16b5a2d9687fdc534', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [KIRIM NASKAH]. Link Youtube]. ENTRI : sfggg', 'true', '2021-08-19 09:13:50', '2021-08-11 17:30:25'),
('6ce8fc5584fcc0c68debfee901007527', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [KIRIM NASKAH]. Link Youtube]. HAPUS :', 'true', '2021-08-19 09:13:50', '2021-08-11 17:30:30'),
('c50bf93a6d68716dd58628777f6b11e4', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2021-08-13 17:35:16', '2021-08-13 17:34:51'),
('5b9867eb7c94ec790d9aa46136bffaf3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. ENTRI BARU : 7', 'true', '2021-08-18 10:02:31', '2021-08-17 17:18:08'),
('f87ac8e7b703adfcc6f43c2c3ecc97ff', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. ENTRI BARU : 8', 'true', '2021-08-18 10:02:31', '2021-08-17 17:20:44'),
('3afcaba02ad20497c77ad21d2f445603', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2021-08-18 10:02:31', '2021-08-17 17:21:58'),
('ac1981d49b643d28396e8ecf9b193a5e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2021-08-18 10:02:34', '2021-08-17 17:22:50'),
('cdc87dc290d480ce5da3d84818991dd6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. ENTRI BARU : 8', 'true', '2022-05-12 23:07:44', '2021-08-17 17:27:35'),
('a5f75a8274ccd2082222c3c8cc9edadc', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. ENTRI BARU : 9', 'true', '2022-05-12 23:07:44', '2021-08-17 17:28:41'),
('334b443058412ef9b0c8bbef9198aa76', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:29:20'),
('c3d47a9e62597d2d6a594e7fc6110b9d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. ENTRI BARU : 8', 'true', '2022-05-12 23:07:44', '2021-08-17 17:29:42'),
('89fcdcc037b1c2c95cac204b2db35af1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:31:13'),
('4c74f65df8f7dbc57bc91615f3c7b60b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. ENTRI BARU : 4', 'true', '2022-05-12 23:07:44', '2021-08-17 17:31:32'),
('30900397a7641d926f47d46b1831f99c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:32:19'),
('505fcfc41541b08e90f9d5936293ab7d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. ENTRI BARU : 8', 'true', '2022-05-12 23:07:44', '2021-08-17 17:32:43'),
('868f118976042034366d28cd00140413', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:32:55'),
('d5657690447c17ff9b1521af1d5b9036', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:34:52'),
('32a61a7cfd2d1eec0a6ba8f1146a58d8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:39:13'),
('0362094142ab751687c9bd710822bab0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:39:49'),
('6b90339fd070674599562b97ce78df29', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:39:58'),
('e56098ecd0424b11330874ae07389e7c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:41:29'),
('f2fcf8d4dac2398f5356641fcb195b19', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:42:04'),
('706a390a3b90780312a4c23a8e408f26', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:42:27'),
('675136e2b8e4ed0d0e6d3085492da867', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:42:37'),
('50de09420b22cdf62d3451dcf1950e54', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:43:07'),
('17f3c70debf7f2d6bf20051dea46fc65', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:43:50'),
('4c5ad3df4324ceb4037eba1c6c519a74', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:44:23'),
('173f928950ba85457634ebd81c2d8b70', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:44:28'),
('7d5557c73397abe50b9ed2f199dfadfb', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:45:00'),
('bcfd6b0d49207355ce4eb47a4d9e5184', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:45:05'),
('c2f5ab88b2f5671aed417757d16dba0a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:46:26'),
('ca0a94fceabd089253f6b84346df9da1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:46:37'),
('9cc250d1b3d578bde19a938d90e8bd1d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:47:16'),
('2e11faaffe6f4e2edd50093f78a12686', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:47:22'),
('4b3a3a794b0ede6f37432c95c760fe79', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. IMPORT DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:47:51'),
('a2dee3379d6acf02cb5ad7275267b019', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:07:44', '2021-08-17 17:47:58'),
('e9ca70e311d818780a4f6ce9ddcaffe5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. IMPORT DATA', 'true', '2021-08-26 17:15:25', '2021-08-17 22:13:59'),
('1a8acfff5a5c50cd69d3ac69643eafb3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. IMPORT DATA', 'true', '2021-08-26 17:15:25', '2021-08-17 22:15:45'),
('a71c10c76314c82bb8eab4ab3d24f87b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. IMPORT DATA', 'true', '2021-08-26 17:15:25', '2021-08-17 22:16:34'),
('1c49011c426e90abe7d9451d7c950414', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. IMPORT DATA', 'true', '2021-08-26 17:15:25', '2021-08-17 22:17:44'),
('f74d83e225a69e90021e648cbe7a96d4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. HAPUS DATA', 'true', '2021-08-27 16:16:30', '2021-08-17 22:17:51'),
('12bc317d3a77ff17d99feec041061d36', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. IMPORT DATA', 'true', '2021-08-27 16:16:30', '2021-08-17 22:18:26'),
('ef6fe65feb4ed4726c6ebe1a1b7748b1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:04:11', '2021-08-17 22:18:35'),
('a417488e8c7ad6d6b905e23646075afc', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. ENTRI BARU : 8', 'true', '2022-05-12 23:09:09', '2021-08-17 22:19:47'),
('59aa14ae02ac30c4cc887bded2551c01', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. UPDATE : 8', 'true', '2022-05-12 23:09:09', '2021-08-17 22:19:55'),
('563acbd8709b3f99f16464eb8b132484', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. HAPUS DATA', 'true', '2022-05-12 23:09:09', '2021-08-17 22:19:58'),
('cd78ae03fa7c5ed8e4b83f9a5a1d9d38', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixA', 'true', '2022-05-12 23:09:09', '2021-08-18 09:24:56'),
('6b7ab12734f3262574175af2c5934eb0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', 'Profil Diri. UPDATE :', 'false', NULL, '2021-08-18 11:14:43'),
('47b5a0fc91321741d26ebe1ede042c47', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', 'Profil Diri. UPDATE :', 'false', NULL, '2021-08-18 11:14:46'),
('356c0926d27cc63de3428811f2d163fe', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', 'Profil Diri. UPDATE :', 'false', NULL, '2021-08-18 11:15:05'),
('91b0781cbf35b2d935d09e54b33623ad', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', 'Profil Diri. UPDATE :', 'false', NULL, '2021-08-18 11:15:10'),
('8d8f861e758e2b42dc9bb9be57ee31a3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', 'Profil Diri. UPDATE :', 'false', NULL, '2021-08-18 11:15:14'),
('55783dd2411bf27a3fefa5df0ba39cb7', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '[DATA]. Data Pegawai. ENTRI BARU : 5', 'true', '2022-05-12 23:07:44', '2021-08-18 11:16:06'),
('fdb8c8a9b733a4a719bde433c5d2cd0a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', 'Profil Diri. UPDATE :', 'true', '2021-08-18 11:19:15', '2021-08-18 11:17:01'),
('d55bc1378a9d95d5860d0171cd41dce7', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', 'Profil Diri. UPDATE PROFIL DIRI : 5. 5', 'true', '2021-08-18 11:19:15', '2021-08-18 11:17:57'),
('4a35540c85896ff829821847ab8351f4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH] STRUKTUR ORGANISASI]. ENTRI BARU : 1. 1', 'true', '2022-05-12 23:09:09', '2021-08-18 17:39:36'),
('78b665e92a01b3a86c1ff6f9b331548a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH] STRUKTUR ORGANISASI]. ENTRI BARU : 2. 2', 'true', '2022-05-12 23:09:09', '2021-08-18 17:40:04'),
('87ed60b6c32e03d0e3cd382cf09d83c3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH] STRUKTUR ORGANISASI]. ENTRI BARU : 3. 3', 'true', '2022-05-12 23:09:09', '2021-08-18 17:40:15'),
('638cf721295686efa85a26df889aa8f6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixA', 'true', '2022-05-12 23:09:09', '2021-08-19 08:44:05'),
('48aa9438afe73f9e893e75c2387b0e2e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixB', 'true', '2022-05-12 23:09:09', '2021-08-19 08:45:43'),
('8846a4fcfbeccc5f318c88cd4b3606be', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixC', 'true', '2022-05-12 23:09:09', '2021-08-19 08:47:58'),
('ead11984e8decf9328ed61d13f576161', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixC', 'true', '2022-05-12 23:09:09', '2021-08-19 08:54:26'),
('64f96b43c05d61bfcb512fe603f09214', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixC', 'true', '2022-05-12 23:09:09', '2021-08-19 08:55:42'),
('19ffcdd01d1660dc046c3b59e3e62b21', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixC', 'true', '2022-05-12 23:09:09', '2021-08-19 08:57:02'),
('23dbcf04a8605bf85b73ecdc3dfba933', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixC', 'true', '2022-05-12 23:09:09', '2021-08-19 08:58:59'),
('60acdad4c6ee0d8f29f3f2f2dd82e016', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixC', 'true', '2022-05-12 23:09:09', '2021-08-19 09:01:04'),
('0d1ba596c63f408365ce5e53604e9b49', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixD', 'true', '2022-05-12 23:09:09', '2021-08-19 09:02:29'),
('92cafffba09b1a27c656c2638d4b8328', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixD', 'true', '2022-05-12 23:09:09', '2021-08-19 09:04:15'),
('ed7a9a5bea2b1d7bc3c6651a62ab3e8d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixE', 'true', '2022-05-12 23:09:09', '2021-08-19 09:05:55'),
('7772ef173a0ee1ca58454c8885c6d4e5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixF', 'true', '2022-05-12 23:09:09', '2021-08-19 09:07:20'),
('bd3bdffa43c8ca632f9a12ec05f849db', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', 'K.I.B. IMPORT DATA : KIBxstrixA', 'true', '2022-05-12 23:09:09', '2021-08-19 09:10:49'),
('a9243bd7c474e3691cd0ce6bd5a2c6f6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', 'K.I.B. IMPORT DATA : KIBxstrixA', 'true', '2022-05-12 23:09:09', '2021-08-19 09:12:03'),
('1fe0df67f7994bcd015b36fe1d73fdbb', '', '', '', '', '', '', 'CABANG', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. Data Sekolah]. UPDATE', 'true', '2021-08-26 17:04:08', '2021-08-21 14:35:40'),
('ba333d27a406111316c17a2f60454d7f', '', '', '', '', '', '', 'CABANG', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. Data Sekolah]. UPDATE', 'true', '2021-08-26 17:04:08', '2021-08-21 14:35:48'),
('6447c9b999535b7582d46c84c3053e43', '', '', '', '', '', '', 'CABANG', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. Data Sekolah]. UPDATE', 'true', '2021-08-26 17:04:08', '2021-08-21 14:37:05'),
('d4cd6e15eda2c622ae422eaf3ee51910', '', '', '', '', '', '', 'CABANG', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. Data Sekolah]. UPDATE', 'true', '2021-08-26 17:04:08', '2021-08-21 14:40:59'),
('b87916e93fbe4a9dd48184b4d7ea6114', '', '', '', '', '', '', 'CABANG', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH]. Data Sekolah]. UPDATE', 'true', '2021-08-26 17:04:08', '2021-08-21 14:41:09'),
('cf07462416f99a928eaa9dcc801d0917', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Peta Lokasi. UPDATE', 'true', '2021-08-26 17:04:08', '2021-08-25 11:01:15'),
('52296a0b20d7f13966413097fae15478', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[MENU : [SETTING] KETUA]. ENTRI BARU : 1234. 1234', 'true', '2021-08-26 17:04:08', '2021-08-25 11:08:13'),
('9b5c94af6ee6d5e22729ed637833943d', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[MENU : [SETTING] STRUKTUR ORGANISASI]. ENTRI BARU : 122222. 122222', 'true', '2021-08-26 17:04:08', '2021-08-25 11:11:08'),
('c29a9089d071881b6f35cb3edafc6c4c', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Data Pegawai. ENTRI BARU : 1', 'true', '2021-08-26 17:04:08', '2021-08-25 11:17:47'),
('edab982bb986124c4372da01b66d5485', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Data Pegawai. ENTRI BARU : 1', 'true', '2021-08-26 17:04:08', '2021-08-25 11:19:28'),
('a9097f9394772916b4bb7dfb58bb815f', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Data Pegawai. ENTRI BARU : 1', 'true', '2021-08-26 17:04:08', '2021-08-25 11:20:28'),
('36299ccfdad0d48f018c61a833bb3ab6', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Data Pegawai. ENTRI BARU : 1', 'true', '2021-08-26 17:04:08', '2021-08-25 11:21:09'),
('772c878c451be9f0561f6f0a33766457', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Data Pegawai. ENTRI BARU : 2', 'true', '2021-08-26 17:04:08', '2021-08-25 11:21:36'),
('a47b84d9c1a5d9ada2a44d068aca6de4', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Data Pegawai. ENTRI BARU : 2', 'true', '2021-08-26 17:04:08', '2021-08-25 11:21:56'),
('14c6e9cbc67d017d99655eb94f3922bb', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Data Pegawai. HAPUS DATA', 'true', '2021-08-26 17:04:08', '2021-08-25 11:22:27'),
('0a7fe25961554659afe6b90cc398650f', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Data Pegawai. IMPORT DATA', 'true', '2021-08-26 17:04:08', '2021-08-25 11:22:32'),
('2521dfc1f05c7b80f9298120f1264f36', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 23:09:09', '2021-08-26 07:13:38'),
('b835a9193ec3d422deed5fcab48de239', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2021-08-26 20:14:11', '2021-08-26 10:50:06'),
('ed94c70a8132166880ad446aef89a3fb', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2021-08-26 20:14:11', '2021-08-26 10:50:12'),
('691afa7ff5f925bc04181c5d383557c3', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 23:09:09', '2021-08-26 11:12:00'),
('a4545fd34eb2702601beccaf3ffe8f24', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. RESET PASWORD', 'true', '2022-05-12 23:09:09', '2021-08-26 11:15:35'),
('8f6af0ab77bd8e445bf99ed53c1f7dae', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', 'Ganti Password. UPDATE', 'true', '2021-08-26 17:03:59', '2021-08-26 11:33:54'),
('1fa08c7597fc1c365214233935b9fecf', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2021-08-27 09:01:19', '2021-08-26 11:46:14'),
('048ce62c6333a47c873c7229527eb507', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2021-08-27 09:01:19', '2021-08-26 11:46:29'),
('1cf53709a991988cf7c04d04ac1b21b2', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2021-08-27 09:01:19', '2021-08-26 11:47:38'),
('1126107e8c112caec62c2c5d1a1573f4', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. ENTRI BARU : 1267206', 'true', '2022-05-12 22:43:20', '2021-08-26 11:48:39'),
('04d0aa0fd229b29457ab8b973505e7b5', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-26 11:48:53'),
('60133d0affd66c3644176805ffc89476', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-26 11:49:47'),
('c41e5e023badbe4922293756974bb2a7', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-26 11:50:02'),
('c741c7d0c5ab12bcd479dc7dbee82d13', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', 'Ganti Password. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-26 11:51:27'),
('0f64d1c5a01eb98e500d71b37d8c65fd', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH] Data Profil]. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-26 11:53:11'),
('04475616e3e7c34931bb8414321f5c85', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[KEPEGAWAIAN]. Data Pegawai. ENTRI BARU : 112281071021299', 'true', '2022-05-12 22:43:20', '2021-08-26 11:53:33'),
('3ca7ff2d606868b53f0baa558b568a44', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-26 11:54:34'),
('6e96c528d8e897320aa7f2193e2021d4', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[KEUANGAN]. Uang Masuk. ENTRI BARU : [BOS]. PER TANGGAL : 0021xstrix10xstrix11', 'true', '2022-05-12 22:43:20', '2021-08-26 11:56:18'),
('02e241e69956e3d7e37817d430b49510', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[KEUANGAN]. Uang Masuk. HAPUS DATA', 'true', '2022-05-12 22:43:20', '2021-08-26 11:56:33'),
('ef683d0eaf255259a0983d9a28c33b88', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[KEUANGAN]. Uang Masuk. HAPUS DATA', 'true', '2022-05-12 22:43:20', '2021-08-26 11:56:36'),
('6e42e1c5cf0b319af0dc0cc263ad2ce6', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[KEUANGAN]. Uang Masuk. HAPUS DATA', 'true', '2022-05-12 22:43:20', '2021-08-26 11:56:48'),
('e7b28bcae9365ccb5c5e771d6d003279', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[KEUANGAN]. Uang Masuk. HAPUS DATA', 'true', '2022-05-12 22:43:20', '2021-08-26 11:57:07'),
('b0fbacd360beb00ba877584e9dc8a351', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '[SETTING]. User Kepegawaian. SET PEGAWAI : 1267206', 'true', '2022-05-12 22:43:20', '2021-08-26 11:58:51'),
('71befde55e3bee9165809cacb8641bbb', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH] KEPALA SEKOLAH]. ENTRI BARU : 1052756. NASIRUL FATAH, SP', 'true', '2022-05-12 22:43:20', '2021-08-27 07:17:24'),
('f57bd8fdff895fc9937ab1468fa73299', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-27 08:55:26'),
('bbccbc9747f56d01f09052bb98ba7a3d', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-27 10:15:06'),
('17e5a6264e24e6b6977b5ecf3ebc4390', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH] KEPALA SEKOLAH]. ENTRI BARU : 196706122000032001. SUHARTI', 'true', '2022-05-12 22:43:20', '2021-08-27 10:41:32'),
('f9b5e53546c153c503a7057751ad3749', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '[MENU : [PROFIL SEKOLAH] KEPALA SEKOLAH]. ENTRI BARU : 196706122000032001. SUHARTI', 'true', '2022-05-12 22:43:20', '2021-08-27 10:43:05'),
('004392235bbdc5fe0b2d7587108a6625', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', 'Ganti Password. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-27 11:19:33'),
('62486a20916c96166f6e8ed868552f97', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 23:09:09', '2021-08-27 16:11:57'),
('900f92626bddc166a11c2b4ffbd667c2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 23:09:09', '2021-08-27 16:13:12'),
('fb273eb57545b9d1dc756c396a4a7ab2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 23:09:09', '2021-08-27 16:13:26'),
('2770de82f61f54da4072885b5f6fb54c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 23:09:09', '2021-08-27 16:13:33'),
('90f694f36ee46f872db6be2e8bdc0974', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '[SETTING]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-28 16:02:09'),
('225383bc6d7ddb7840f6f7773035d3bb', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'CABANG', 'TATA USAHA', '[PROFIL CABANG]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-28 16:17:11'),
('e7c06b0f58b3100261dd1dbf37b22e74', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'CABANG', 'TATA USAHA', '[PROFIL CABANG]. Peta Lokasi. UPDATE', 'true', '2022-05-12 22:43:20', '2021-08-28 16:17:27'),
('3f700c6fe05da65ebda4010899493ea1', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'CABANG', 'TATA USAHA', '[PROFIL CABANG]. Data Pegawai. ENTRI BARU : 1', 'true', '2022-05-12 22:43:20', '2021-08-28 16:18:27'),
('cbca474e048df4303475764823734686', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 23:09:09', '2021-08-28 16:20:32'),
('a0aade0d638bb5cc11e57c112c624524', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '[PROFIL SEKOLAH]. Peta Lokasi. UPDATE', 'true', '2022-05-12 23:09:09', '2021-08-28 16:20:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_log_gps`
--

CREATE TABLE `user_log_gps` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `lat_x` varchar(100) DEFAULT NULL,
  `lat_y` varchar(100) DEFAULT NULL,
  `alamat_googlemap` longtext DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_log_gps`
--

INSERT INTO `user_log_gps` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `lat_x`, `lat_y`, `alamat_googlemap`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('1b334152e24e830bfd2758a67657e933', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729091', '110.1501363', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 06:43:10'),
('e7003b307686dca31249f5f1cd550db8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729091', '110.1501363', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 06:43:20'),
('d9024bffa4d366c1f17b99652b27ad41', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729155', '110.1501428', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 06:46:02'),
('43f3607783b1c56637766023f55db3c9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972904799999999', '110.15012859999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 06:47:50'),
('10059e53644c7d0b9ed5cdb61e1078e9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972904799999999', '110.15012859999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 06:48:00'),
('98a4c82718431d74d204cc2a0176e6f8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729721', '110.1501999', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 09:34:56', '2021-08-09 06:49:17'),
('6ccaa70002918017462596acf1aa5290', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729638', '110.1501839', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 11:24:32'),
('364d7db65cee5e64e88a0867d078adee', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '52.132633', '5.291265999999999', 'Van Weerden Poelmanweg 220,  3768 MN Soest,  Netherlands, , , ,', 'true', '2021-08-26 09:34:56', '2021-08-09 12:20:48'),
('e4862ec17f17b06da862c42ccac6692b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9728901', '110.15011709999999', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 09:34:56', '2021-08-09 12:22:21'),
('a6c705a694029d8f07c80cb16937e90a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9728901', '110.15011709999999', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 09:34:56', '2021-08-09 12:22:25'),
('130132dede941f846a7b085118813a6e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9728962', '110.1501232', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 09:34:56', '2021-08-09 12:22:48'),
('dafe2868b7b99cd4a148d7e5411e924b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9728962', '110.1501232', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 09:34:56', '2021-08-09 12:22:53'),
('6c1f5812a1a4c09c3024113b12f57233', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729018', '110.15012519999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:23:49'),
('cea629efe93dff0f8cf5a26452efdb39', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729682', '110.1501948', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 09:34:56', '2021-08-09 12:24:19'),
('9098e8ba16221b256356b00603f407dc', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729677', '110.150193', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:24:30'),
('7f338afc4b5006474efecd5f0732cb5e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729253', '110.15015109999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:24:50'),
('150a999ba1053d45ec0b2eb213e3d616', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729253', '110.15015109999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:24:54'),
('0b79f7e7be912223c1539f4ccf9497a2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729301999999995', '110.1501572', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:25:21'),
('7d94cbef0558007ef4bcdd2ee957425e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729012', '110.15012519999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:26:26'),
('5382a7eaee6b79f645f7e7baa438a704', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729657', '110.1501909', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:26:41'),
('47bea31a28cc69f3a2c4173fb463d7d2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729059', '110.15013309999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:26:57'),
('2558aef46d597b160d70abefb072b8a2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729094', '110.1501337', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:27:24'),
('0b9101a3e616d2a025fc4090c909bafd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729094', '110.1501337', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:27:44'),
('9a20d9bd9404207e790912573f85c1f1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '52.132633', '5.291265999999999', 'Van Weerden Poelmanweg 220,  3768 MN Soest,  Netherlands, , , ,', 'true', '2021-08-26 09:34:56', '2021-08-09 12:29:07'),
('067a84a6dd39776487f086b76c968b58', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729774', '110.1501957', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 09:34:56', '2021-08-09 12:30:19'),
('e16a28794dc3601b4419824d31ceb74b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.1501288', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:31:13'),
('fec5a072754c38383f245294415f7ff9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.1501288', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:31:22'),
('3c90e057f52ac6aeaa24f0e4a6247b72', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.1501288', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 09:34:56', '2021-08-09 12:31:43'),
('7efd0c665c977ad56ef1890c4dfdc682', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.1501288', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:21', '2021-08-09 12:31:48'),
('9be4c1d60abc1fcc2733df99306a1981', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.1501288', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:21', '2021-08-09 12:32:04'),
('5542b776fb16d1dc8f81f84ab661a22c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.1501288', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:21', '2021-08-09 12:32:19'),
('be3dbf694e474fc3b65d27cb7bb91a7a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972929499999999', '110.1501535', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:32:56'),
('d69bf5a158051b2a5af02ce2d804139f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972929499999999', '110.1501535', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:33:01'),
('05cc9873d99f705a880b1f56e314e635', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729509', '110.1501732', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:34:32'),
('7394ccad3d6071be10b06ca25ebbf0b9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729154', '110.15014269999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:34:39'),
('857e07e67a25877ccdc949477f2e44d5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729154', '110.15014269999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:35:31'),
('449684f558b733593385232f2b364ae9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729447', '110.1501568', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:37:08'),
('cc3c57c73e05874e761d798612b0e3cf', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729101', '110.15013379999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:37:45'),
('7d7cc8319e531dab80d46a16f8282f94', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9728962', '110.1501232', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-09 12:38:17'),
('8568e665149b1430d9cc13d0ea863f25', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.1501288', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:39:26'),
('ebf377d9b027e1c90b7dc88eb22b629c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.1501288', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 12:39:31'),
('c5635cda77fe37bc0347a01a3f2e2d3b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972897799999999', '110.15012120000002', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-09 12:40:02'),
('474716c9c3b3e0fde99cf966c39a2618', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972897799999999', '110.15012120000002', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-09 12:40:06'),
('aeebed71fb167b26bd94a8db628c2cca', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972897799999999', '110.15012120000002', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-09 12:40:11'),
('be1a135634fa92ed1a1987cb6f00a9fc', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972897799999999', '110.15012120000002', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-09 12:40:17'),
('088e3f7eae28d446ad85a0a1cf85202a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972946299999999', '110.15017069999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 17:51:55'),
('877cabb0d89a6fe62f5191b81548504c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729505', '110.150177', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 17:54:10'),
('95e5df187e7da4a90642e6aea0b1ca54', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729505', '110.150177', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 17:54:14'),
('fe5acee33f48a88121469db617f5ca91', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972948', '110.15017420000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 17:54:33'),
('13b2f96a97f0f0572624b8ae294119b0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '52.132633', '5.291266', 'N413,  3768 Soest,  Netherlands, , , ,', 'true', '2021-08-10 10:22:28', '2021-08-09 18:10:47'),
('b68b936a139a74f689de424fcc366463', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729525', '110.1501794', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-09 18:14:16'),
('2b7a9f7dd4720acc3d282a73604fe5c0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-09 18:23:59'),
('50406f7803528556f0442c321aba9237', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729755', '110.1501986', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-10 04:40:25'),
('901630f0cccf260d50318a4f35c83fc4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9728993', '110.15012229999999', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-10 04:42:30'),
('77f87785c810a0749be4d376bbe54734', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729655', '110.1501908', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-10 04:46:46'),
('c05932a8bb0aaa2557a70d531779ccfe', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729687', '110.1501931', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-10 04:49:55'),
('c37cb68810403400205650f78cd87613', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729709', '110.1501969', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-10 04:52:36'),
('6882206391257d4497f910af611b80a7', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-10 05:07:12'),
('511c71f6725b37c487f0b4ed2a5059bb', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972924099999999', '110.15014970000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-10 05:19:07'),
('211879fd9f67bbce2d12becbc833ae46', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972924099999999', '110.15014970000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-10 10:22:28', '2021-08-10 05:19:12'),
('84401cf37cacb6b82dadb3cf4311bd72', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729717', '110.1501974', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-10 10:22:28', '2021-08-10 05:19:36'),
('027ed7103919c566d68bfc9871eb19e6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729566', '110.1501809', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:22:33'),
('7a2e1d5b2fbbae3281b725aa7a664b51', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729566', '110.1501809', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:22:38'),
('95345e2659626a1734a7f57c9e710206', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729341', '110.1501614', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:31:46'),
('d861aa059714bdd18667f6d7c6873196', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729239', '110.15014970000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:32:24'),
('a5a3cfcb9f9011b38a3091eb6698e9c5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729334', '110.15015749999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:32:49'),
('76280308999b008325a9aaecca931e12', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729239', '110.15014970000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:33:36'),
('eb687b9f875a6287b79e21702d53e490', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729239', '110.15014970000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:33:46'),
('a710a3b959aeb235dd8711b119a6a029', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729334', '110.15015749999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:33:52'),
('d6f13f5ada50a2f5bbd63a87e7ea152f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972924099999999', '110.15014970000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:34:06'),
('5991dc2b122d13c4ec992934128d3111', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729725', '110.1501979', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-12 21:58:34', '2021-08-10 10:34:21'),
('9564e95406dcbd5268d40521807c70b9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729334', '110.15015749999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:35:35'),
('f41f13bfda4e36560b9ce7438027166c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729647', '110.1501882', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:44:23'),
('e6db044f004d504b890a8affe49c637a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972926800000001', '110.15015029999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:44:48'),
('f597945942ccbd3e7e8c6569f05c5c71', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972926800000001', '110.15015029999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:45:00'),
('49d4c309d1374ee78e95de0a30b3e2a4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729596', '110.1501844', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:45:19'),
('d386fd75f5429047da33d920462e7595', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972965', '110.1501891', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:46:31'),
('c0bdd2e597cff477e86255d0fc42dad5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729567', '110.1501805', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:49:26'),
('9dfd45c431d22ff507bc43a74768c986', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729177', '110.1501435', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:49:59'),
('d202a6aa3706b328fd6ac7e2fdcff539', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729567', '110.1501805', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:50:33'),
('0c3e54fbd4563d41ee2794c89810eac1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729567', '110.1501805', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:50:48'),
('2085bb06a22bb7af0aadbbaf0d775882', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729539', '110.150178', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:51:00'),
('313c6b8e0f434510ed7943a96363a530', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972971', '110.1501954', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-12 21:58:34', '2021-08-10 10:52:35'),
('58200b0549d317ed115fa7edaed8f155', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729228', '110.15015029999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:52:43'),
('dbc57306a278a19a90f351b3cce9bf3f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.97297', '110.1501947', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-12 21:58:34', '2021-08-10 10:53:47'),
('e884e70d2c0875e003aab084df1aec5f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729262', '110.15014970000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:54:09'),
('d0638981fd3855d995c93ffff52afa87', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972965', '110.1501891', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:55:22'),
('d55125974536f1e59deca4e8bcaad66c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972930799999999', '110.1501548', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 10:56:02'),
('6f2c50e1dc868a33bdddd9f7c185c5bf', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729386', '110.1501649', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 21:58:34', '2021-08-10 21:31:55'),
('94cb760e10685413d72cd4c1802c6154', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729721', '110.1501952', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-12 21:58:34', '2021-08-10 21:34:07'),
('3a57d1738107c685122cb05c2ca50f35', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 21:58:34', '2021-08-10 21:42:10'),
('b060d58986b1c23e8c78750291a75cd5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 21:59:19', '2021-08-10 21:44:52'),
('5d34a260412275caef14d1a87a5f925c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 21:59:19', '2021-08-10 21:45:26'),
('c9d9e213fb220240023de174ead30646', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 22:00:24', '2021-08-10 21:47:58'),
('565c2d54ecbe257716c1ba13f3c8c03c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-10 21:48:29'),
('d239f940d543310be1ece2e1e8634dea', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-10 21:48:39'),
('4b9f2792ef59dad38045c9d8c80f13f2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 22:00:24', '2021-08-10 21:48:52'),
('5513b3918bd73da14f72cb903683859e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 22:00:24', '2021-08-10 21:49:00'),
('3adb5ecac00636ebb31a50ebd92e5c5f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-10 21:49:33'),
('dae5e70b33c47b2f441eef02d4ebefa0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-10 21:51:37'),
('1f8c4d45ee9fed55eb1b521c2b1b64c3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 22:00:24', '2021-08-10 21:52:46'),
('c5656c936e51e86f2bda05a158ae53c3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 22:00:24', '2021-08-10 21:59:14'),
('6f62fce2129041ad2b076cc22071efe7', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-10 21:59:36'),
('667ba738a4f927b8de41ac6670bc0743', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 22:00:24', '2021-08-10 22:01:16'),
('becc7d8e27506be214512a2010107b44', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-10 22:02:26'),
('c917e07fe8b652e063425e020107841d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-10 22:02:46'),
('5f3c35db4ba3efb6c7b1d8b7f0eddbb5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-10 22:03:23'),
('e5781611886f04a5e3ea8a1738bfb31d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-12 22:00:24', '2021-08-10 22:03:36'),
('586d1d3178bcf6c3b2b61e200ba479a4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729626', '110.1501839', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 22:00:24', '2021-08-11 08:28:39'),
('eb2a94df035e01e85240dafd00cb8245', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972916100000001', '110.15013929999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 22:00:24', '2021-08-11 08:29:20'),
('cd607b86cee0db5334cceb6339653c84', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972924099999999', '110.15014970000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 22:00:24', '2021-08-11 16:46:00'),
('8c2070b3b76b21303a0bd1e2553a6d75', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729752', '110.1501987', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-11 16:49:18'),
('e0dff6b9e139ed33492a87e9547ce198', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972933599999999', '110.1501579', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 22:00:24', '2021-08-11 16:51:51'),
('943045473b92b64095bf1c4f887c5f2f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972933599999999', '110.1501579', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 22:00:24', '2021-08-11 16:52:43'),
('9bf28c73b9ae12be8de806d5f8fe4118', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729724', '110.1501986', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-11 16:53:29'),
('e9de2280d3f296c64769971e69c02c98', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729253', '110.15015059999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 22:00:24', '2021-08-11 16:55:43'),
('a2dc3e248d30c94e4f8d2832100abff8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729743', '110.1502022', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-11 16:58:04'),
('31dd9f4a37734c3c432412f6d19f6faa', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972919399999999', '110.15014269999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-12 22:00:24', '2021-08-12 08:33:57'),
('0eaee82637879ab381911ab4631637f6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729722', '110.1501992', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-12 22:00:24', '2021-08-12 08:34:11'),
('59e6251ceab73cd2b2e63b7a45e9b203', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972975', '110.1501988', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-13 17:33:34', '2021-08-12 21:49:10'),
('10fe3ff10729d0c48bbb10275d36427c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-13 17:33:34', '2021-08-12 21:54:49'),
('b5ce8923798a7be875b46c7b72a6c5ad', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:00:15'),
('78efa37d75cd2c4384808fd874b587b9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:00:20'),
('5d6c28167a6f3439785decc438f83e34', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:01:12'),
('470390eecf6e335a7481ba5c66dbc20a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-13 17:33:34', '2021-08-12 22:02:58'),
('7806da19976b9eb2173b5bfe66518714', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:20:22'),
('9da2c83e7eefcec9242580ce6f9703bc', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-13 17:33:34', '2021-08-12 22:23:21'),
('fc5c6778cb74048768564fe01fc56201', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-13 17:33:34', '2021-08-12 22:24:04'),
('07a08793355eef9bb2a28dba2d6e9207', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:24:26'),
('857c6fec4db8a0496a0b58c0c2764e92', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-13 17:33:34', '2021-08-12 22:24:48'),
('b777a6277cd07ab23d69483b60dddd0a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:25:11'),
('64e5b1b3aeea9abda75d4f9b0a52aa1c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:27:26'),
('c93922eff358f8d81f9cb79a5418199e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-13 17:33:34', '2021-08-12 22:28:01'),
('bd5e98de9012b285e4951e96f41f236a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:28:21'),
('763418aae8359e4ed7acdd0904995344', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-13 17:33:34', '2021-08-12 22:28:26'),
('cf5f0234cd88a9f76440c0fad7a150ea', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-17 17:00:24', '2021-08-12 22:30:28'),
('25613361a394b5f905a5469fb76e8620', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-17 17:00:24', '2021-08-12 22:31:11'),
('24af09020161d55d4bac7752e7c356d6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-17 17:00:24', '2021-08-12 22:31:26');
INSERT INTO `user_log_gps` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `lat_x`, `lat_y`, `alamat_googlemap`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('5a265b342d2a4fe99c6987664cb9fd94', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-17 17:00:24', '2021-08-12 22:31:34'),
('a41b0c317f814eabba9f18f640f4ef25', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-17 17:00:24', '2021-08-12 22:31:50'),
('d218c2c131dc861887128690ece1b410', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-17 17:00:24', '2021-08-12 22:32:06'),
('5eb6a08c518784a7c2331eec2250a46d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-17 17:00:24', '2021-08-12 22:32:32'),
('74fe195d97e62ae34f40539d59c9b3f5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-17 17:00:24', '2021-08-12 22:34:11'),
('f05ce4b9117c1ad7400c44b139fcda66', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-17 17:00:24', '2021-08-12 22:34:51'),
('57df532edad7a955ea7488da2888912c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-18 16:26:09', '2021-08-12 22:35:25'),
('28ad73168dfb039b7a1a5d43d2009e5e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-18 16:26:09', '2021-08-12 22:41:07'),
('19a6f96368ecaf1cd3f969503d56ee08', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-18 16:26:09', '2021-08-12 22:41:54'),
('d2c99285086274c3bfb24a19dcd0603a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729751', '110.1501988', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:26:09', '2021-08-13 17:32:35'),
('c8e7734e7a0b8c6f0b1d94c75d47a745', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-18 16:26:09', '2021-08-13 17:33:14'),
('210311069cb6a2af00e5f3e244d009bc', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'SEKOLAH', 'TATA USAHA', '-6.9729755', '110.1501986', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-13 17:35:29', '2021-08-13 17:34:19'),
('b154dd06d1cb6f539d452610f9bd3660', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'SEKOLAH', 'TATA USAHA', '-6.9729751', '110.1501988', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-13 17:35:29', '2021-08-13 17:35:00'),
('029313fe3c757e43833a697b6abff727', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'false', NULL, '2021-08-13 17:35:39'),
('67a3afcf7be0d29b18503a103a2f925e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729424', '110.1501657', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 16:26:09', '2021-08-14 10:10:50'),
('950eb4aba6712f28510ad3466e20552b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729357', '110.15015869999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 16:26:09', '2021-08-14 16:20:44'),
('6f4c56b5953e0419cfc7176366d8cd17', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729763', '110.1501983', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:26:09', '2021-08-14 17:05:42'),
('9b6c74dbf666b41da23b11ae21d71c01', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729635', '110.1501893', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 16:26:09', '2021-08-15 09:33:18'),
('3222310ab4ae411b9102d4755f83a8cd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729687', '110.1501931', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:26:09', '2021-08-15 11:43:19'),
('0b296d50c3988a5bfb7ed8b754a90fcb', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.97297', '110.1501947', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-17 09:07:07'),
('cf689d66f71540978fe8e94cc30ad38e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729312', '110.1501579', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-17 09:08:35'),
('d055a37346042870612d9f4fbc1547e2', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'SEKOLAH', 'TATA USAHA', '-6.972965', '110.1501891', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2021-08-17 09:08:45'),
('da56044235495ebb8a0af89c6f8a2658', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'SEKOLAH', 'TATA USAHA', '-6.9729424', '110.1501657', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2021-08-17 10:37:49'),
('33ce5a97170bdd5dcd42eda706275ff4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729424', '110.1501654', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-17 16:22:02'),
('ffacd8c9b36ce7689b62d6e21fcf6f0e', '', '', '', '', '', '', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729672', '110.1501921', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2021-08-17 16:46:15'),
('dd4a9b0150c1d68b44f44155108f8880', '', '', '', '', '', '', 'SEKOLAH', 'KEPEGAWAIAN', '-6.972916799999999', '110.15014359999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2021-08-17 16:46:50'),
('d95244ad9566bfc86b2818c941eb4dca', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '', '', '', 'SEKOLAH', 'KEPEGAWAIAN', '-6.97297', '110.150194', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-17 20:53:04', '2021-08-17 16:51:50'),
('347374d3f19332f122f801108142faa8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '', '', '', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729687', '110.1501931', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-17 20:53:04', '2021-08-17 16:51:56'),
('e0fddfd6790fa9700061376ac52a1e56', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '', '', '', 'SEKOLAH', 'KEPEGAWAIAN', '-6.972934899999999', '110.15015869999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 20:53:04', '2021-08-17 16:52:12'),
('57fbbdea386985ec6a1c8cdf940e10d3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '', '', '', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729566', '110.1501813', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 20:53:04', '2021-08-17 16:54:21'),
('9c4888a550c92c5bb11dbc926117fc2e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '', '', '', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729566', '110.1501813', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 20:53:04', '2021-08-17 16:54:25'),
('0e688c58897012eeb60e7afa3c2e4451', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729675', '110.1501937', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-17 16:55:08'),
('fd80911d7474a9fee350f4193daa69cc', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729688', '110.1501947', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-17 16:56:13'),
('c50d42bb1838e087eed87ff6dee3b308', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729688', '110.1501947', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-17 16:56:18'),
('a394662361857e91f1d685a80f666aaa', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729688', '110.1501947', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-17 16:56:23'),
('9fa03fb8729d59020b3bb05b59bb202c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729688', '110.1501947', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-17 16:56:28'),
('8143755fa4c936c997414d63b3ccd44c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729052000000005', '110.15013069999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-17 16:56:33'),
('04d6bf0f0a7abbef464046ec0215aef8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729454', '110.1501691', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-17 16:57:05'),
('031183cf647b93de99ba6528711fb4cb', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729094', '110.1501325', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-17 16:58:49'),
('45536a5d397095fe86d1b6069dcba8a7', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729046', '110.1501313', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 22:09:21', '2021-08-17 17:06:58'),
('b3dc73f9f6d91cb7ff11627120912acf', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729108', '110.1501372', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 22:09:21', '2021-08-17 17:07:18'),
('0b7c91ff60041d2b6426043ab6e8904f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729108', '110.1501372', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 22:09:21', '2021-08-17 17:07:28'),
('e0c424fc6cda1edad895ddf9cbba1a17', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729177', '110.1501402', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 22:09:21', '2021-08-17 17:07:50'),
('1bda406e51090dd679882112906670e6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729177', '110.1501402', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 22:09:21', '2021-08-17 17:07:59'),
('7b2b137c1fb76173e2898e6f5bddac02', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729489', '110.1501735', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-17 22:09:21', '2021-08-17 17:10:48'),
('63f4e51c33998e6f7cd91f5b93e6396a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '52.132633', '5.291265999999999', 'N413,  3768 Soest,  Netherlands, , , ,', 'true', '2021-08-17 22:09:21', '2021-08-17 20:42:52'),
('a230edf7bfbaf03ff3cd21080b029b5a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-17 22:09:21', '2021-08-17 20:55:50'),
('a795f8ea74495022c3758bdb25de04eb', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-18 09:09:14', '2021-08-17 20:59:41'),
('0488ab9e205b90275bb14c9030ad553d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729603', '110.1502144', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 09:17:59', '2021-08-17 21:01:06'),
('9ad8d9e34a44407d89b6ae62ed65f853', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729603', '110.1502144', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 09:17:59', '2021-08-17 21:01:11'),
('8715f3078007e0064b4153c57fb6fd0e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729647', '110.1501908', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 09:17:59', '2021-08-17 21:02:43'),
('fe143670a7ae8ee1c4de02f96a7ce849', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.972902899999999', '110.15012569999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 09:17:59', '2021-08-17 21:03:31'),
('f50ce1f0c6a42ae006495776f5fc5b7d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729065', '110.15012960000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 09:17:59', '2021-08-17 21:04:44'),
('b1461e617c6f7c5c0910bcfeebb6fcbe', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729065', '110.15012960000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 09:17:59', '2021-08-17 21:05:27'),
('69a8fbd2f8dfb5e2a234ec803dc0c9af', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.972931099999999', '110.1501576', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 09:17:59', '2021-08-17 21:06:14'),
('f98cae35e69f903b2d1fda13dcbaecc4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729759', '110.1501985', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 09:17:59', '2021-08-17 21:12:04'),
('5b9dee386269ff2517f49e6a0bceac9c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729771', '110.1502316', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 09:17:59', '2021-08-17 21:13:34'),
('2ae5413e1fb7ff672c54741d1f9c582d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729759', '110.1501985', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 09:17:59', '2021-08-17 21:17:12'),
('2540fb241acca39e1cc7261a8e3f1c05', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-18 09:17:59', '2021-08-17 21:17:39'),
('4be1fb0a6b4b2d1d131fd1970216b69c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729701', '110.1502268', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 10:02:10', '2021-08-17 21:21:01'),
('a3ca3179328be49af5b74175e554aed8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729731', '110.1502018', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 10:02:10', '2021-08-17 21:23:36'),
('3ef6e69fc325e4fd45354badc3f596ab', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729709', '110.1502309', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 10:04:56', '2021-08-17 21:23:53'),
('2c1158ff766ae371897d84ce8b6656eb', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729228', '110.15014930000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 10:05:46', '2021-08-17 21:24:29'),
('ba3239516388530ff2e9483b6b843302', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729709', '110.1502309', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 10:06:16', '2021-08-17 22:05:20'),
('06440bbc726f6904bc637d06c866e374', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729709', '110.1502309', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 10:06:16', '2021-08-17 22:05:36'),
('b0563909439265c3f654aa50b27995da', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729732', '110.1502228', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 10:06:16', '2021-08-17 22:06:32'),
('5bd8725d010000f1d116c82318436fec', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729054', '110.1501318', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 10:06:16', '2021-08-17 22:07:36'),
('abb422d4afaf135a80debea6bb8471d9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-18 10:06:16', '2021-08-17 22:08:56'),
('e6689152262760482c3adaa2661e1701', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-18 10:06:16', '2021-08-17 22:12:39'),
('1922589e527f48a0f652bf9ef2f9a28b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-18 10:06:16', '2021-08-17 22:13:15'),
('878620b561e57280cfb9825a78d9269f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-27 16:15:53', '2021-08-17 22:13:40'),
('7cccc543aef2c3011df496f8c62e582d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729659', '110.1502226', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-17 22:20:23'),
('a498b354c0b14c11a46d935fa32dec86', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729228', '110.15014930000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-17 22:22:59'),
('15b23efa50eff6a680b3621a77766160', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729715', '110.1502317', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-17 22:24:17'),
('4ec03ca417b57b62932427d184e7ad45', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729721', '110.1502323', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-17 22:24:32'),
('fd4648fd4f174e467129064de1fe3b98', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.972925399999999', '110.15015050000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 11:21:45', '2021-08-18 09:05:07'),
('b97a28f418b7b6ea301ebf5018934d0e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.972926800000001', '110.15015', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 11:21:45', '2021-08-18 09:06:02'),
('fd6d559bc90bdfa749fa614440bb1510', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729171999999995', '110.1501404', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 11:21:45', '2021-08-18 09:15:12'),
('90f0d29b7de5e7b86ca1e67a43bcc6be', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729709', '110.1501969', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 11:21:45', '2021-08-18 09:18:22'),
('bf8962158e76883a6bb75508f8a7eaa3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729709', '110.1501969', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 11:21:45', '2021-08-18 09:20:55'),
('b20b470a1ee65d4e88c86b451fa70854', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.972925399999999', '110.15015050000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 11:21:45', '2021-08-18 09:21:32'),
('22769400e9bb681d090b2f51fed86bf1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729320999999995', '110.15015559999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 11:21:45', '2021-08-18 09:22:36'),
('2d30608e52a949fb8b5336fb23d26239', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729353', '110.1501586', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 11:21:45', '2021-08-18 09:23:42'),
('7db9333042372210bcd6288ddea763c2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729259', '110.150149', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-18 09:29:09'),
('1ab13b6fa73411baa014f56b251c7aed', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.972916799999999', '110.15014359999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 11:21:45', '2021-08-18 09:30:47'),
('75b09028f1b2f4904511fb78df83e151', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729312', '110.1501579', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 11:21:45', '2021-08-18 09:31:26'),
('31973e37a0c82978486d45e2a755fae7', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-6.9729725', '110.1501979', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 11:21:45', '2021-08-18 09:43:49'),
('d4c1057063cdbd31d78c377a2f08a42d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-6.972908899999999', '110.15013169999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 16:35:51', '2021-08-18 09:55:44'),
('8002f7b53555457cce73cf2140c530f5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-6.9729724', '110.1501986', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:01:08'),
('9b4a730b42a449baf9abb31b62c00b4a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-6.9729688', '110.150226', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:04:13'),
('40e68b3c97f777f718705d6dd77e2e37', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-6.9729725', '110.1501979', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:04:55'),
('78d0a06d25dcff78cc4f0857c0080b34', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.972971', '110.1502279', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:05:16'),
('8e888034ee359a5e50895dc10e9700c2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729675', '110.1502248', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:05:59'),
('c0040fc1c01b124bf25fd5c0d28a7166', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729611', '110.1502167', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-18 10:31:30'),
('be5a7995af01c6c1fcd517faba92e235', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '-6.9729721', '110.1501999', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:48:23'),
('e41b6723b364699d0ca19afab49d106a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '-6.9729721', '110.1501999', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:48:33'),
('ca523879871c6cdacf0d9f59307a568c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '-6.9729697', '110.1502286', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:49:18'),
('f6aec0258a44f2e95325cf4fb36625a9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '-6.9729714', '110.1501993', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:49:42'),
('8d8b4241403f786e15572a02e9c86f9b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '-6.9729697', '110.1502286', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:50:58'),
('e697f99ea2ba53c5f29369e068acdb5f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '-6.972893399999999', '110.1501163', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-18 16:35:51', '2021-08-18 10:51:19'),
('61f6ebb51243b249eaab0a2de6a07e6d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '-6.9729646', '110.150188', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 16:35:51', '2021-08-18 11:04:49'),
('b6746cf481de13b9ca0fd36baa618ca2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '-6.9729646', '110.150188', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 16:35:51', '2021-08-18 11:04:54'),
('8f76adeda02574eafcaa309bff1e8037', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729548', '110.1501805', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 16:35:51', '2021-08-18 11:15:44'),
('567e3bc4bfdd0d6f1d8975390b83c75e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-6.9729559', '110.1501727', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-18 17:28:23', '2021-08-18 11:16:29'),
('87ba1abae9e05856c1b7932fef3b0134', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-6.9729699', '110.1501962', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-19 09:14:00', '2021-08-18 11:21:18'),
('d4f0e0f191c2785e8edf5b15791b5d2d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9729097', '110.1501327', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-18 11:22:02'),
('6c5dc9ee3d2dd54d727e32df5191f2b8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9729174', '110.1501401', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-18 11:23:51'),
('72d5d9e1399dde04205918f3d23639fd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-6.9729655', '110.1501908', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-19 09:14:00', '2021-08-18 11:24:06'),
('87fcde6dfd54d7b9a405f311488e8ffd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9729622', '110.1501853', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-18 16:17:35'),
('da41b9477caa7521f4603b50fff3d0f9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729636', '110.1502191', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-18 16:19:04'),
('b78c56d3de87168dda16bcfb375b2df9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729627', '110.1501874', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-19 09:14:00', '2021-08-18 16:20:17'),
('26278b254c495ff511765eeb23fd1b4a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9729353', '110.1501586', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-18 16:20:39'),
('ea91fcc8fceeb1394b7b1e42b3fe5f5b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729543', '110.1501774', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-18 16:24:34'),
('9b4ea573925663890aadef0c12edfcaa', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729686', '110.1501915', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-19 09:14:00', '2021-08-18 16:26:36'),
('e843d5e2c6e8f6613763524db91e6227', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729320999999995', '110.15015559999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-18 16:29:32'),
('908af88741a1faf603f3907c17c2c5b3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729622', '110.1501853', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-19 09:14:00', '2021-08-18 16:31:33'),
('597166dd8013e75786373fb023af92e6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-6.9729577', '110.150211', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-19 09:14:00', '2021-08-18 16:34:34'),
('e273c405dd881fbd062ae0d4874fbde3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-6.9729331', '110.15015749999999', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-19 09:14:00', '2021-08-18 16:36:39'),
('301582a321c97a7db55e523ec8d3a7d3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-6.973008200000001', '110.15012560000001', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-19 09:14:00', '2021-08-18 17:20:01'),
('0dc9716f15a5ddcc645dcdc84b0ae1ef', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-6.9730058999999995', '110.1501133', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-19 09:14:00', '2021-08-18 17:20:24'),
('e61bca3081b868f8c46135dd04dc0d9a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-6.9729809', '110.1502191', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-19 09:14:00', '2021-08-18 17:26:49'),
('b64ab7472ff2808eb8166daa71bb7f3e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-6.9729729', '110.1502211', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-19 09:14:00', '2021-08-18 17:27:54'),
('2745eb38d93ea8bf13cc6a3773c72868', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-6.9729046', '110.1500583', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-19 09:14:00', '2021-08-18 17:28:12'),
('f367eb3f5e1f916fab7c2a8ec8ee7277', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729808', '110.1502193', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-18 17:32:56'),
('942f89b98cadb7b11325d0b4e501d891', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729755', '110.1501986', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-18 21:18:03'),
('4bf29b32c811031c1604e65ef0f6dcd0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-19 09:14:00', '2021-08-18 21:19:44'),
('931fe7352d0b6a13b9541c1434a1022f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-19 09:14:00', '2021-08-18 21:24:10'),
('d36147f0862a3d789ffb4151c39f1ee9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-19 09:14:00', '2021-08-18 21:25:46'),
('3da1eeaa5d048996f5ff2ba520d974d0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2022-05-12 23:05:17', '2021-08-18 21:27:31'),
('4d0131af1e898119737bd4af42ec339f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-19 09:14:00', '2021-08-18 21:28:10'),
('44a67c1088d09f1b42ef8e556053122e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-19 09:14:00', '2021-08-18 21:28:26'),
('5f3548332a03f20ad93f37d50e8b893b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-19 09:14:00', '2021-08-18 21:32:08'),
('ae92d630ce5580074c954b2e7d90e3c3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729601', '110.1501854', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-19 08:37:06'),
('cfed71ed0d17b94457710a0749ad8395', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729062', '110.15013280000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-19 09:14:00', '2021-08-19 09:10:25'),
('bc172339a4d8c7f135cf7f319f16490a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729633', '110.1502179', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-19 09:24:42'),
('6c4372626bb204e4e3d89255118f15d9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729437', '110.1501643', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2021-08-19 09:34:11'),
('186f0b622a568a1dd37a83324147a048', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729005', '110.1501254', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2021-08-19 09:34:56'),
('d4686f8c50e5d31917b3a4f9fb192399', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'KEUANGAN', '-6.9729005', '110.1501254', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2021-08-19 09:35:46'),
('68ee08f65465715fa5da1acdea633ca3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729702', '110.1502264', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-20 16:15:05'),
('24f367eae6cfc526eb9ff3444ae4e4c3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-20 16:22:31'),
('43d0ed45984cd32f54a4903166fa27ee', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729573', '110.1502112', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-20 16:24:21');
INSERT INTO `user_log_gps` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `lat_x`, `lat_y`, `alamat_googlemap`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('de2e5b00b4fe90c818e4097839ca55a9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729284', '110.15016820000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:53', '2021-08-20 16:25:57'),
('dc1c8f702475e591fd6331aa3f7d3b4c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-27 16:15:53', '2021-08-20 16:29:55'),
('de12a62ddc3bf9ae6d4f96dd0e1f4284', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729638', '110.1502199', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:15:45', '2021-08-20 16:34:44'),
('1ca294007ed8b3b788df6e80f79002f6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729592', '110.1502144', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:20:28', '2021-08-20 16:35:27'),
('637b37b926a41240e21dccfc83241013', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729623', '110.1502179', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:20:28', '2021-08-20 16:37:22'),
('36705dc9a8acbb49703d09684446aee0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729123', '110.15014839999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:20:28', '2021-08-20 16:37:51'),
('cc5fd09f9071e5c27b65ca6fbdec0984', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729038', '110.15014339999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:20:28', '2021-08-20 16:39:13'),
('b1ad5a1d60ef7df6b039f7505ad5b564', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729731', '110.1502018', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:20:28', '2021-08-20 16:39:27'),
('50236cf82457c3110d790489e999ed70', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729667', '110.150193', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:20:28', '2021-08-20 16:40:07'),
('0bc25081e45d8ff5a19e82f123777c72', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729667', '110.150193', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:20:28', '2021-08-20 16:40:12'),
('2e74021188b4e6a1cce3e7d67d85f5ab', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729079', '110.1501438', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-27 16:20:28', '2021-08-20 16:40:38'),
('15faefbd6fb447d4e52a605c0e7409f3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729079', '110.1501438', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:04:08', '2021-08-20 16:41:16'),
('3b319828609560c6eb1ac2e7a92be898', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729088', '110.15014869999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:04:08', '2021-08-20 16:43:20'),
('6c325bc3b50b85eb6939b56d054cc0bb', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.9729569', '110.1501821', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:04:08', '2021-08-20 16:47:10'),
('2a6878e940cffe2036eda816c1f0e02b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729383', '110.1501773', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:25:47', '2021-08-20 16:50:14'),
('512e5caeec779868ae6352acd14cc14c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729383', '110.1501773', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:25:47', '2021-08-20 16:50:19'),
('ddd45263eb39e4b714bb5ecd4214e9bd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729611', '110.1502167', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:25:47', '2021-08-20 16:50:44'),
('6b8eef482c26fb93c40fc839b07cab14', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729611', '110.1502167', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:25:47', '2021-08-20 16:51:01'),
('04e2fbf2c9d9cc4b33ac72006c8e8cc0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729611', '110.1502167', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:25:47', '2021-08-20 16:51:06'),
('90c3139b466b0912f41b6b4a453a0d91', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'SARPRAS', '-6.9729636', '110.1502191', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:25:47', '2021-08-20 16:51:30'),
('08fd2b47c1c8babcc44a6314cfbb4b3a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9729081', '110.1501443', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:25:47', '2021-08-20 16:53:46'),
('c540ac92180441e9a6cebdedb1a4b25a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9729675', '110.1502248', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:25:47', '2021-08-20 16:55:04'),
('f207f7159adad61393f9938153848b36', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.972912099999999', '110.1501492', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:25:47', '2021-08-20 16:56:41'),
('cfd5fa46b918a3b2e3e082e5acf41eac', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9729688', '110.1501947', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:25:47', '2021-08-20 16:57:53'),
('8b6cfe1bb47b067c8be26c1de526117c', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'BOJA', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'BOJA', 'CABANG', 'TATA USAHA', '-6.972969', '110.1502254', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'false', NULL, '2021-08-20 17:01:06'),
('f3925bdb0749e207ebec5319963656d1', '171a103a423d9c800c868393c7074ad9', '14', 'PEGANDON', '171a103a423d9c800c868393c7074ad9', '14', 'PEGANDON', 'CABANG', 'TATA USAHA', '-6.9729695', '110.1502235', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'false', NULL, '2021-08-23 11:40:57'),
('32deb2945122abfafcc3f61518253626', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9730065', '110.1500655', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 11:28:24'),
('9af619c9bf56ba8f3bd68d43f417bc12', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9730005', '110.1500646', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 11:27:55'),
('abbb57a7f0663160c5499738912f7008', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729974', '110.1500641', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 11:27:21'),
('555fc9723c2a2d84579acffc50062379', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9730093', '110.1500658', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 11:26:42'),
('b764b37721ddf008ddd14f1ac31f8686', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-6.9730139', '110.1500678', '', 'true', '2022-05-12 23:10:18', '2022-05-12 11:26:29'),
('4a7a9f07439578baf5eb305058efc9d6', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-6.9730123', '110.1500678', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 22:56:27', '2022-05-12 11:24:46'),
('c41ccf68b61551d0c11653961a4db855', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9730127', '110.1500672', '', 'true', '2022-05-12 22:43:15', '2022-05-12 11:24:08'),
('edfec63437e5ef2a11c85006e927a27f', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9728137', '110.1500256', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:04:08', '2021-08-24 10:11:52'),
('7b02f2a7dc4e85d892551974bc199a1d', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9729044', '110.1501414', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:04:08', '2021-08-24 10:12:50'),
('04a63562595893837514bee75ede1dbb', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9728106', '110.1500333', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2022-05-12 23:04:08', '2021-08-24 10:13:00'),
('329bd404e536792ef36fdcc744e2bcd1', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9728995', '110.1500437', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2022-05-12 23:04:08', '2021-08-24 10:17:04'),
('a463b202f1fc9b175f95d06e5055133b', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9728101', '110.150029', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:04:08', '2021-08-24 10:18:12'),
('0ca36f05831a3fb24b578b9b61130a3c', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9728101', '110.150029', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:04:08', '2021-08-24 10:19:05'),
('277dca775adbaf148b3667d42693d313', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 23:04:08', '2021-08-24 10:20:26'),
('1deb924d96fbe1088380b6db36265a62', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2022-05-12 23:04:08', '2021-08-24 10:20:58'),
('2fbfe91bee2f17344d98ac17b1db7eeb', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2022-05-12 23:04:08', '2021-08-24 10:21:13'),
('88d8f18ec228899f132f27429a8402cf', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 23:04:08', '2021-08-24 10:22:09'),
('0aafe0b95b7cc42922629ab684cfb6dc', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-24 10:25:47', '2021-08-24 10:22:30'),
('796eb83d0d151779ae03c348153b1bb8', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-24 10:25:47', '2021-08-24 10:23:39'),
('09ad6e633973d3062e62ab6288c93eba', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9728756999999995', '110.15011249999999', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:43:24', '2021-08-24 10:24:14'),
('c918655d9d087cab874eefb116e1d7d6', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9728109', '110.1500296', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:43:24', '2021-08-24 10:24:38'),
('face4757020ca9404585f0fd249d8c6d', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729044', '110.1501414', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:43:24', '2021-08-24 10:26:24'),
('b6e309c35243ebbe358566ec857fc6e7', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9729383', '110.1501773', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:43:24', '2021-08-24 10:26:40'),
('783cc7b8f0c1e8109527d300806c97d0', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9729301999999995', '110.1501701', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:43:24', '2021-08-24 10:28:18'),
('b6ae9b185c32c32ba6631ae2d994e6f2', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9729301999999995', '110.1501701', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:43:24', '2021-08-24 10:28:35'),
('fd4fb530fd9ffde6bccacab4898bb6ce', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9729301999999995', '110.1501701', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:43:24', '2021-08-24 10:28:40'),
('80bddfbf73dc745d31315537ef4cec2c', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9728106', '110.150027', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:28:56'),
('0fcf762ff5aebb437bff84dc0da55655', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.972909', '110.1501455', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:29:04'),
('de7d5a989577d64cbfe95a2bd1bba661', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.972909', '110.1501455', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:29:41'),
('cfef9b505fe30a0b000d9b859353bc3f', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9729682', '110.1502107', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:30:15'),
('06a72e2e64e6a7c448441f9ad629dac4', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.972923199999999', '110.15011849999999', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:30:48'),
('177cfc82dd01b23a2b98113ea1effc5e', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9729237', '110.1500791', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:32:58'),
('4255fd25d4ea119c896bd5d5b77360cb', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729237', '110.1500791', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:33:16'),
('da638c7a7b947d2eef1edfcb7b226012', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.972912099999999', '110.1501492', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:34:19'),
('34ee5b15faca3a3c19b71eb15193d5d4', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.972912099999999', '110.1501492', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:34:28'),
('8632603e06ee2a4dab349d74f00e89d6', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.972912099999999', '110.1501492', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:34:49'),
('cfacf47d7f83797ab52a199e9d553cc3', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729388', '110.1501751', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:37:40'),
('a5032e2b60a77703e70adc72e30d96c5', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729088', '110.15014869999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:37:56'),
('20c1c6fe6dc7bc6e3f8ce3e5ede2922a', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729088', '110.15014869999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:38:06'),
('bbea6e80b909e105add8a5712d8425c1', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9729088', '110.15014869999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:38:16'),
('b91dd6ee054ccbab8fff10faa56694b0', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9728088', '110.1500325', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:38:40'),
('937b3a8ff7a82d80604f376612508af9', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9728088', '110.1500325', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:38:45'),
('9f3e7158cc7686422eb95bc357edf571', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9728088', '110.1500325', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:38:50'),
('79ae802e0cf23232d62f2918baf1f0e7', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9728088', '110.1500325', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:38:55'),
('1835e4bd6428685b3a2e690f47761973', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9728088', '110.1500325', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:39:37'),
('13e946977228cf60ff95bda95b1c65ee', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9728088', '110.1500325', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:39:58'),
('67daada19769766a3393be1cd9fa62cd', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9728151', '110.1500331', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:40:10'),
('ca7166f7d50af1e94ae7aa024d2213e5', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9728151', '110.1500331', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:40:15'),
('c0dc02cd1725eacd21e17760c32baf9e', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9728127', '110.1500312', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:43:10'),
('fa600ec1b5f9554fa13af8f224830ea0', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9728127', '110.1500312', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:43:39'),
('595485c89efee6386857a3d655fb9c3a', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.972897', '110.15013309999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:43:53'),
('cdb46b06e82d6cdd15c45c3b98a550e3', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9729043', '110.15014409999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:44:38'),
('5d08fe2f56398ff6e36903228d36bb68', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9729043', '110.15014409999999', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-24 10:47:56', '2021-08-24 10:45:25'),
('4057b77f077bafa329724062938afee0', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9728226', '110.1500263', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:45:35'),
('28e0a83134a5b6b302e7c0f530ad2039', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9728226', '110.1500263', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:45:39'),
('b002606f7a3c5f28037d4411c3545867', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9728273', '110.1500255', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-24 10:47:56', '2021-08-24 10:46:00'),
('1ab022f722d2e4f59ea1a38af8ec0938', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9728179', '110.1500262', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'false', NULL, '2021-08-24 10:48:14'),
('543959a30560ec7b8151821fc35a5ec6', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728179', '110.1500262', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2022-05-12 23:05:17', '2021-08-24 10:48:38'),
('3c58502cd178dd1ea3a434097d5ca5d2', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728179', '110.1500262', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2022-05-12 23:05:17', '2021-08-24 10:50:02'),
('719b2ffece3ea6a134700bfdd444f6d4', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728179', '110.1500262', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2022-05-12 23:05:17', '2021-08-24 10:50:07'),
('6522169392b5c1478a4d6c290daf122b', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728179', '110.1500262', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2022-05-12 23:05:17', '2021-08-24 10:50:12'),
('ce3e2ae54475263a91af1e4e443bb2e3', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 23:05:17', '2021-08-24 10:52:28'),
('e9d6c460c88101343dfd81ab4f5c57ca', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 23:05:17', '2021-08-24 10:52:38'),
('0c5382749cfd80163b1e5784e82a6596', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 23:05:17', '2021-08-24 10:53:18'),
('3b898a45b6610c18a8cbdc760c78669a', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728992', '110.1501356', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-24 10:55:09'),
('ce19a598713de6b7b543990aa1becbbb', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728226', '110.1500263', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2022-05-12 23:05:17', '2021-08-24 10:55:37'),
('25fec5d380e26da3eb6e3b0b8d3ff4a1', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9729091', '110.15014610000001', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-24 10:56:48'),
('8687de39c9a30c868d419821b31dfb9b', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9729079', '110.1501438', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-24 10:57:18'),
('bef8b42ef45e60673871c7bf5599434f', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728093', '110.1500303', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-24 10:57:36'),
('e475480735c913cf754c30918c4e7660', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728093', '110.1500303', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-24 10:57:57'),
('553974534f9580625d8810333abbbcbd', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.972909199999999', '110.1501488', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:05:17', '2021-08-24 10:58:09'),
('d9ca7644bfb7e0b7aec83df1d97fc87e', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9729974', '110.1500641', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 11:22:38'),
('9c16f9f98d306b87211e9df4eaf085a1', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-6.972994', '110.1500636', '', 'true', '2022-05-12 23:10:18', '2022-05-12 11:21:46'),
('2d3fde637f38f7ed669f7c920cf39f18', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9728137', '110.1500256', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 17:06:54', '2021-08-26 06:18:28'),
('67eb0346b8158a016a12fe03c6845d02', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.972809', '110.1500333', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 17:06:54', '2021-08-26 05:59:24'),
('c273bdb1b31abe5eee70be79fa6ba89f', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-6.9729902', '110.150063', '', 'true', '2022-05-12 23:10:18', '2022-05-12 11:21:33'),
('64863976883a68d3a3befdc0d4e30eaa', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9728106', '110.150027', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 17:06:54', '2021-08-26 05:54:07'),
('eb90507a78c91a452a38836b682ad033', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-26 17:06:54', '2021-08-26 05:36:07'),
('9c8b7150c7c973115ab25f8661875ef8', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2021-08-26 17:06:54', '2021-08-26 05:35:57'),
('0e6ee13be462a299ff0b407791353d00', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.0051453', '110.4381254', 'Jl. Rambutan Raya No.18,  Lamper Lor,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50249,  Indonesia,', 'true', '2021-08-26 17:06:54', '2021-08-26 05:35:16'),
('a64821c665b9d2a6f9cd4b667e6d2e1e', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-6.9729902', '110.150063', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 22:56:27', '2022-05-12 11:21:01'),
('f46e9bef03de13352c9c6d9ec3f12a18', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9728093', '110.1500303', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 17:06:54', '2021-08-26 05:08:15'),
('7d3eb0151177ba34de63d0f7b5a4c5ca', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9729081', '110.1501443', 'Jalan Sunan Abinawa,  Pegandon,  Pangempon,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2021-08-26 17:06:54', '2021-08-25 16:42:17'),
('d60793c71b2fcfb1d5eb0d1605d722de', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9728179', '110.1500262', ', , , , , ,', 'true', '2022-05-12 23:04:08', '2021-08-26 08:02:24'),
('88fc1b910afa3f22f5c6f3029f32ecf2', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9729584', '110.1500621', '', 'true', '2022-05-12 22:43:15', '2022-05-12 11:20:35'),
('86e55ba9312cdfcc4c840ee175f46923', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9730112', '110.1500665', '', 'true', '2022-05-12 22:43:15', '2022-05-12 11:16:53'),
('cb2703ea0e597c4bf6a2baf49c15f512', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.972809', '110.1500333', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-26 17:06:54', '2021-08-26 08:13:14'),
('676bec4d1575f51a3a7b3490a8a543df', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.972809', '110.1500333', ', , , , , ,', 'true', '2021-08-26 17:06:54', '2021-08-26 08:13:48'),
('aeae48a7ba768ddca2bef2b1635d4874', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9172977', '110.0542311', 'Unnamed Road,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354,  Indonesia', 'true', '2022-05-12 23:07:42', '2021-08-26 09:34:17'),
('ba5d008511d2edb13f7731385185eb94', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9173001', '110.0542345', 'Unnamed Road,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354,  Indonesia', 'true', '2021-08-26 17:06:54', '2021-08-26 09:35:46'),
('30c42064a82bff3a83d6f36994350c2a', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9172128', '110.0541133', ', , , , , ,', 'true', '2021-08-26 17:06:54', '2021-08-26 10:18:36'),
('fe4ee1bf74ea5ed9764587aa5a39abcf', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', ', , , , , ,', 'true', '2022-05-12 23:07:42', '2021-08-26 10:36:48'),
('b936175a99050cbc94b7597d60817d94', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9172344', '110.0541258', 'Jl. Gempolsewu,  Rowosari,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354', 'true', '2021-08-26 17:06:54', '2021-08-26 10:45:58'),
('b2f22421decc2ccd82ba6dcf60c11286', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '-6.917234199999999', '110.0541232', 'Jl. Gempolsewu,  Rowosari,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354', 'true', '2021-08-26 17:06:54', '2021-08-26 10:49:48'),
('77958538d1d2f7ae847495c8b9d20a44', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.917234199999999', '110.0541232', 'Jl. Gempolsewu,  Rowosari,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354', 'true', '2022-05-12 23:09:07', '2021-08-26 10:50:53'),
('86507cdb850bd0d752c7d39e74edbb01', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9172098', '110.0541178', 'Jl. Gempolsewu,  Rowosari,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354', 'true', '2022-05-12 23:09:07', '2021-08-26 11:09:14'),
('5fab0cddea426fc2c8a8549d257599ae', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9730005', '110.1500646', '', 'true', '2022-05-12 22:43:15', '2022-05-12 11:15:31'),
('ef8615ccb69f127ddfd74826ad19bbec', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9730005', '110.1500646', '', 'true', '2022-05-12 22:43:15', '2022-05-12 11:16:16'),
('b0993c6d2b964acc33a0e35bab3fd98f', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9729557', '110.150056', '', 'true', '2022-05-12 22:43:15', '2022-05-12 10:44:02'),
('004799bcb4f8ec4c67123c9080a3a71a', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9171889', '110.0540923', ', , , , , ,', 'true', '2022-05-12 23:10:18', '2021-08-26 11:24:43'),
('300185214244d1ccd309dada31e6e802', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '-7.2392', '112.7362', ', , , , , ,', 'true', '2021-08-26 17:12:17', '2021-08-26 11:31:20'),
('66a3cbb4d39974c1315c60a35eadb296', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '-7.1046481', '110.0769438', ', , , , , ,', 'true', '2021-08-26 17:12:17', '2021-08-26 11:31:31'),
('db14a378cd5853164d171955f679e073', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '-6.9204229999999995', '110.0568888', 'Unnamed Road,  Krangkong,  Gempolsewu,  Kec. Rowosari,  Kabupat&eacutexkommaxn Kendal,  Jawa Tengah 51354,  Indonesia', 'true', '2021-08-26 17:12:17', '2021-08-26 11:34:32'),
('ece37fae2a121d62eb6328f19d9a89cf', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '-7.1046481', '110.0769438', ', , , , , ,', 'true', '2021-08-26 17:12:17', '2021-08-26 11:34:47'),
('40d877ea03fe0efdf248fbbf98a4ee65', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', ', , , , , ,', 'true', '2021-08-26 17:12:17', '2021-08-26 11:34:59'),
('2a9a736becdd9fda8a8fdba455e9ad3b', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', ', , , , , ,', 'true', '2021-08-26 17:12:17', '2021-08-26 11:35:52'),
('e0c41169d0b0d4d35a21db3aea60cf81', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', ', , , , , ,', 'true', '2021-08-27 09:00:59', '2021-08-26 11:37:23'),
('25990c6cdef9ed2627ca9f0b097da089', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'SEKOLAH', 'TATA USAHA', '-7.1625804', '109.9946202', ', , , , , ,', 'true', '2021-08-27 09:00:59', '2021-08-26 11:38:43'),
('251837b99bb112a5b5b144b98cc4451b', '0523679c15f5e4ac9c26f16a48f9710f', '21', 'MIM 2 Rowosari', '0523679c15f5e4ac9c26f16a48f9710f', '21', 'MIM 2 Rowosari', 'SEKOLAH', 'TATA USAHA', '-6.9172078', '110.0540659', 'Jl. Gempolsewu,  Rowosari,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354', 'true', '2021-08-27 09:00:59', '2021-08-26 11:40:35'),
('3437a59d354b1d6d755a731a0a53b830', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', ', , , , , ,', 'true', '2021-08-27 09:00:59', '2021-08-26 11:41:06'),
('59055c6fb65e14c1b969030ee5cdc9e3', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'SEKOLAH', 'TATA USAHA', '-6.9172451', '110.0541432', ', , , , , ,', 'true', '2021-08-27 09:00:59', '2021-08-26 11:41:40'),
('aad95cb38697998b5eb1c94aab5b3730', 'a06844aab3573565040dc1076cebab8c', '12', 'MIM Sarirejo', 'a06844aab3573565040dc1076cebab8c', '12', 'MIM Sarirejo', 'SEKOLAH', 'TATA USAHA', '-7.1625799', '109.994625', 'RXPV+WWV,  Ngargosari,  Purwo Sari,  Kec. Sukorejo,  Kabupaten Kendal,  Jawa Tengah 51363,  Indonesia', 'true', '2021-08-27 10:07:33', '2021-08-26 11:43:04'),
('5c9317578d0cf93e20c46183db8c11bf', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'SEKOLAH', 'TATA USAHA', '-6.9172472', '110.054133', ', , , , , ,', 'true', '2021-08-27 10:07:33', '2021-08-26 11:42:13'),
('07c74a76dcedec7dc14bf43dba51abaa', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', 'SEKOLAH', 'TATA USAHA', '-6.9172078', '110.0540659', ', , , , , ,', 'true', '2021-08-27 10:07:33', '2021-08-26 11:42:45'),
('63fae5341be5e2e80e5ded769945d394', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '-6.9172034', '110.0541081', ', , , , , ,', 'true', '2021-08-27 10:07:33', '2021-08-26 11:43:10'),
('01a25b9aaf4673e83853f7397b06ed75', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', 'SEKOLAH', 'TATA USAHA', '-6.9176683', '110.0548817', ', , , , , ,', 'true', '2021-08-27 16:08:53', '2021-08-26 11:43:13'),
('70eec1a2f990241cedbde10ec6ca6e9a', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', 'SEKOLAH', 'TATA USAHA', '-7.0051453', '110.4381254', ', , , , , ,', 'true', '2021-08-27 16:08:53', '2021-08-26 11:45:23'),
('c3722894b122b7b0d4ae2a4b66a1e888', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9172381', '110.0541267', 'Jl. Gempolsewu,  Rowosari,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354', 'true', '2021-08-27 16:08:53', '2021-08-26 11:46:33'),
('a358aed7d0debf946e225d09c9e987c7', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '-7.1046481', '110.0769438', ', , , , , ,', 'true', '2021-08-27 16:37:02', '2021-08-26 11:47:08');
INSERT INTO `user_log_gps` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `lat_x`, `lat_y`, `alamat_googlemap`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('7f46659b5e00789ac66afaa0dab203d1', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '-6.9172078', '110.0541148', ', , , , , ,', 'true', '2021-08-27 16:37:02', '2021-08-26 11:48:36'),
('7f0e86b25e375d164bd99197536e6484', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '-6.9172095', '110.0541269', 'Jl. Gempolsewu,  Rowosari,  Gempol Sewu I,  Gempolsewu,  Kec. Rowosari,  Kabupaten Kendal,  Jawa Tengah 51354', 'true', '2021-08-27 16:37:02', '2021-08-26 11:55:59'),
('87eb030d1919200c595178029b821cef', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '-6.9172072', '110.0541205', ', , , , , ,', 'true', '2021-08-27 16:37:02', '2021-08-26 11:58:59'),
('b575293c2570a5b40f28bd1e2725d9b6', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9172094', '110.0541147', ', , , , , ,', 'true', '2021-08-27 16:37:02', '2021-08-26 12:00:04'),
('b9832d9f7df8e451a71e30cf3c66ae12', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', 'SEKOLAH', 'TATA USAHA', '-6.9172212', '110.0541018', ', , , , , ,', 'true', '2021-08-27 16:37:02', '2021-08-26 12:01:24'),
('a4206ca3c1fe19658a2fb2863b33abff', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '-7.1046481', '110.0769438', ', , , , , ,', 'true', '2021-08-27 16:37:02', '2021-08-26 12:02:40'),
('8a3d6c788d53356eda913fe7cbf34ec0', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '-6.9171975', '110.0540976', ', , , , , ,', 'true', '2021-08-27 16:37:02', '2021-08-26 12:04:07'),
('204e576a644de92246d5b57984d39dd7', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '-7.10385', '110.0775621', ', , , , , ,', 'true', '2021-08-27 16:37:02', '2021-08-26 12:22:46'),
('568cea0e99221113be86a4dfb43309fa', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9728226', '110.1500263', 'Jl. Sunan Abinawa,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357,  Indonesia', 'true', '2021-08-27 16:37:02', '2021-08-26 16:38:32'),
('6e773ea76f824f9903982fb53265832f', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9729079', '110.1501438', '', 'true', '2021-08-27 16:37:02', '2021-08-26 17:07:59'),
('b46226fa88fe2ae3cfc60090a4cfa270', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9729011', '110.1501409', '', 'true', '2021-08-27 16:37:02', '2021-08-26 17:12:32'),
('e83df7f54052d7bc4c40b02c7bd93490', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9728179', '110.1500262', '', 'true', '2021-08-27 16:37:02', '2021-08-26 17:13:48'),
('2e55e146436dc336b070a3f4e9cf3b13', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '-6.9729011', '110.1501409', '', 'true', '2022-05-12 23:10:18', '2021-08-26 17:14:42'),
('317adf76adc8175227239b1cb1682841', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '-7.1038497', '110.07755780000001', '', 'true', '2021-08-27 16:37:02', '2021-08-27 07:09:06'),
('ad902f3e1a8b8342e71ef8fd809f0f0f', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '-7.1038557', '110.0775798', '', 'true', '2021-08-27 16:37:02', '2021-08-27 07:10:30'),
('01bab049ef4b75e5fd74c1609e836a08', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9729954', '110.1500627', '', 'true', '2022-05-12 22:43:15', '2022-05-12 10:41:28'),
('f0af522813364ebbd5e59aea29cd6ba5', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2021-08-27 16:37:02', '2021-08-27 08:51:43'),
('676fa30456515aaf71adecce4af4bd64', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'SEKOLAH', 'TATA USAHA', '-7.7129424', '110.0092652', '', 'true', '2021-08-27 16:37:02', '2021-08-27 09:34:06'),
('bcc83b654fba535ee8b231ce15a45127', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2021-08-27 16:37:02', '2021-08-27 09:34:39'),
('621dab846314aa78a14586f1b39bd0d3', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9729885', '110.1500616', '', 'true', '2022-05-12 22:43:15', '2022-05-12 10:40:34'),
('9bd087a1367f836b5ee988aff50342f4', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9728155', '110.1500325', '', 'true', '2021-08-27 16:37:02', '2021-08-27 09:44:51'),
('290a9b36253b3cae3f3239486b7a59e4', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '-6.9183305', '110.1045229', '', 'true', '2021-08-27 16:37:02', '2021-08-27 10:00:53'),
('6d7441c7e7a6e537314e7d866eae854a', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.0051453', '110.4381254', '', 'true', '2021-08-27 16:37:02', '2021-08-27 10:08:15'),
('7e61ebf54290f4afce88da9150e3a8c6', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '-6.9375637', '110.0563172', '', 'true', '2021-08-27 16:37:02', '2021-08-27 11:09:54'),
('c99a35aaf3fc4a180073bb21b7eec902', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '-7.056387099999999', '110.01252609999999', '', 'true', '2021-08-27 16:37:02', '2021-08-27 11:22:23'),
('2204765db75b9e7fbb1bac3129991af5', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2021-08-27 16:37:02', '2021-08-27 16:08:47'),
('b2883df36147e2a0cce894a92d315db5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-6.972809', '110.1500333', '', 'true', '2022-05-12 23:10:18', '2021-08-27 16:09:39'),
('e9f283b0dbbfc5cbd0610f44c977720e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2022-05-12 23:10:18', '2021-08-27 16:19:55'),
('746a21d2a8d1ae319968150c4f9e3504', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2022-05-12 11:15:57', '2021-08-27 16:36:05'),
('f7e141a8631141aef3ac829ed47e7cad', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 22:56:27', '2021-08-27 17:08:52'),
('8b1d9ed4955339fd63b8606cb5c6b965', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 22:56:27', '2021-08-27 17:11:33'),
('9d020f340b6898092e5228371cbe2ab3', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.972809', '110.1500333', '', 'true', '2022-05-12 11:15:57', '2021-08-28 16:01:07'),
('2c931cb4d430965ff40941fc31aabd0c', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2022-05-12 11:15:57', '2021-08-28 16:01:29'),
('9624518762f7d4004c07a7e021f198df', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2022-05-12 11:15:57', '2021-08-28 16:02:41'),
('1564866fd66147b6cb37ad917f1cd9e6', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9728088', '110.1500325', '', 'true', '2022-05-12 11:15:57', '2021-08-28 16:03:33'),
('dfd2941a63ec6c9a05477974d71cb56f', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9728137', '110.1500256', '', 'true', '2022-05-12 11:15:57', '2021-08-28 16:13:52'),
('8089f4326718ee04791dfac6a81b9570', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 22:56:27', '2021-08-28 16:14:25'),
('21309a9828cc34486c9ea92bc53216a2', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 22:56:27', '2021-08-28 16:15:13'),
('9a4fee387e418402e9bd7119acb1955d', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'CABANG', 'TATA USAHA', '-6.9728093', '110.1500303', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 11:15:57', '2021-08-28 16:15:29'),
('4b88e531364e5409602879a096e72312', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'CABANG', 'TATA USAHA', '-6.9728093', '110.1500303', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 11:15:57', '2021-08-28 16:18:03'),
('51d0607d6e86ff9a646f4a94c1f76565', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', '3bf21607a990f50b88a4dc62f04411dc', '1', '1', 'CABANG', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 11:15:57', '2021-08-28 16:18:39'),
('3bdeecc3c1ce6a17c3f3ddb6a6b67d59', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', '3bf21607a990f50b88a4dc62f04411dc', '1', '1', 'CABANG', 'PEGAWAI', '-6.9728106', '110.150027', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 22:43:15', '2021-08-28 16:19:00'),
('1d5ed87105135ba3a11814c20d169bce', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-6.9728106', '110.150027', '', 'true', '2022-05-12 23:10:18', '2021-08-28 16:19:39'),
('237dbd3af2cacfe1d868bcef6f44b580', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2022-05-12 23:10:18', '2021-08-28 16:20:11'),
('a8491eb0f1d2e251d61c7295b5ee85a6', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-6.9728106', '110.150027', '', 'true', '2022-05-12 23:10:18', '2021-08-28 16:20:44'),
('56b8e3454366ea2713cf0b4a36ba3967', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-7.150975', '110.14025939999999', '', 'true', '2022-05-12 23:10:18', '2021-08-28 16:20:53'),
('5340650a16870447887b3b3d71802feb', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728106', '110.150027', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2021-08-28 16:21:28'),
('fb7f427c99c47e4fa0032540e20a8235', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-7.150975', '110.14025939999999', 'Jl. Tlogo Jingga No.10,  Tlogowungu,  Palebon,  Kec. Pedurungan,  Kota Semarang,  Jawa Tengah,  Indonesia', 'true', '2022-05-12 23:10:18', '2021-08-28 16:21:47'),
('53dbb7bcccc3b51770b01368df789a54', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9728137', '110.1500256', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2021-08-28 16:23:34'),
('f7c870b7909a1c2f33d21d37102fcc95', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9728137', '110.1500256', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2021-08-28 16:23:46'),
('e53937685be96d4ed5b8a0c4831acb4d', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9728137', '110.1500256', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2021-08-28 16:24:03'),
('9643dbd9574c01e9fe703c67e5dd83d8', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-6.9728137', '110.1500256', 'Jalan Sunan Abinawa,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2021-08-28 16:24:14'),
('da1d2bd59f1519c0813a1942449c26f2', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9730198', '110.1500661', '', 'true', '2022-05-12 22:43:15', '2022-05-12 22:42:00'),
('becf2a5dac840cc6b7c15c5af89284ce', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '-6.9730225', '110.1500684', '', 'false', NULL, '2022-05-12 22:55:17'),
('6f85bac519b4756b721e0161babab85b', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-6.973023', '110.1500691', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 22:56:27', '2022-05-12 22:55:37'),
('358f285f5eee18b47bd53f0f6bb08a8c', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-6.9730209', '110.1500671', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2022-05-12 22:57:09'),
('2d7a1e1bc616f6afe1f12acafcfcc217', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-6.9730209', '110.1500671', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2022-05-12 22:57:19'),
('6a2044f4029805d45d660f7d2e53f172', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-6.9730209', '110.1500671', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2022-05-12 22:57:24'),
('6a547037d605fe8190f6c1edbc97888b', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-6.9730209', '110.1500671', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2022-05-12 22:58:19'),
('72ad4e5eb57452548f8c67ab03608e56', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '-6.9730215', '110.1500679', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'false', NULL, '2022-05-12 22:58:32'),
('21cbb61f6592c89098e1fd52e07e927a', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-6.9730215', '110.1500679', '', 'true', '2022-05-12 23:10:18', '2022-05-12 22:59:08'),
('5b741f229d5f3f0a5022ffd2d0f7bfc1', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '-6.9730212', '110.1500675', '', 'true', '2022-05-12 23:10:18', '2022-05-12 23:04:18'),
('5591fd0011c2b83be93c4695459ae262', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '-6.9730219', '110.1500688', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 23:04:33'),
('194356ea3a6382dd88f7576fe74ee0ec', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.973019', '110.1500652', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 23:05:53'),
('dce0953d8e38b1062540a9263315e932', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '-6.9730201', '110.1500662', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 23:08:12'),
('cfcd196dfce9159ce0c6f24e05bef2e6', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-6.9730201', '110.1500662', 'Jalan Sunan Abinawa Desa Tegorejo Rt 01 Rw 04,  Pegandon,  Kersan,  Tegorejo,  Kec. Pegandon,  Kabupaten Kendal,  Jawa Tengah 51357', 'true', '2022-05-12 23:10:18', '2022-05-12 23:08:33'),
('d533a901a6e878c8bd4d541545bb1c5f', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'SARPRAS', '-7.0025216', '110.4314368', 'Jl. Wonodri Baru Raya No.20,  Wonodri,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50242,  Indonesia,', 'true', '2022-05-12 23:10:18', '2022-05-12 23:09:37'),
('ad2f9299754c4ef61d010ae359f47539', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-7.0025216', '110.4314368', 'Jl. Wonodri Baru Raya No.20,  Wonodri,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50242,  Indonesia,', 'true', '2022-05-12 23:10:18', '2022-05-12 23:09:50'),
('ca4d8bdb4500d1f7cb8dd1acd1e048cd', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'SEKOLAH', 'KEUANGAN', '-7.0025216', '110.4314368', 'Jl. Wonodri Baru Raya No.20,  Wonodri,  Kec. Semarang Sel.,  Kota Semarang,  Jawa Tengah 50242,  Indonesia,', 'false', NULL, '2022-05-12 23:10:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_log_login`
--

CREATE TABLE `user_log_login` (
  `kd` varchar(50) NOT NULL,
  `sekolah_kd` varchar(50) DEFAULT NULL,
  `sekolah_kode` varchar(100) DEFAULT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `ipnya` varchar(100) DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_log_login`
--

INSERT INTO `user_log_login` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `ipnya`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('f36ea8e8349cf3ba87fa7595f875f186', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-17 17:00:09', '2021-08-09 05:59:04'),
('549ddb70eb42785e9c335264aaaa7483', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-17 17:00:09', '2021-08-09 06:01:24'),
('14a89bb4245acfd957c56e88a0823b4f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-17 17:00:09', '2021-08-09 11:24:28'),
('6f2fd6b515ab7118e9d043357bb264e2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-17 17:00:09', '2021-08-09 17:51:49'),
('49ac769754714ce7c49079d3b31a6c34', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-09 18:14:13'),
('fb06c65572f77415a5aa4db6d7d1f084', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-10 04:40:12'),
('5809818a3ed2d030c9a4e3ac900fdbde', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-10 05:07:11'),
('e20e105ef395573cd3d67d08a68a4b8f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-10 21:31:50'),
('96f83e7beb421a776c4470dddf1cf5e2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-11 08:28:36'),
('4985a2d2efd310fda2b9ded3fb61d04f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-11 16:58:03'),
('f8cd9f49abf67523d94a6ef725f914e1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-12 08:33:52'),
('8bc08ac18ab7d89748b875fbae82ac45', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-12 21:49:07'),
('cb57f2ae20ccf9f202f7ef5994bb30ee', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 09:08:51', '2021-08-12 22:20:21'),
('26db111ceb73a91fc6d3073fef7321fe', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:02:00', '2021-08-13 17:32:32'),
('43747c364345a2b7d41d7b6ec7352d86', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-13 17:35:27', '2021-08-13 17:34:18'),
('804abba513c518da8aac0968659a349e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:02:00', '2021-08-14 10:10:48'),
('bb3a23a18bb5f52c77f7faaf19149e33', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:02:00', '2021-08-14 16:20:40'),
('7812ec8389daeaa47854ff674fd5a44a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-14 17:05:40'),
('533c53c0fd6c33d11aa6272719719fc3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-15 09:33:16'),
('277bb659a529dcf4c9bc44b2fed133bc', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 09:06:58'),
('f91e384a0ed3b8db37e29c5edac26bf4', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', '234', '234', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'false', NULL, '2021-08-17 09:08:44'),
('ce3f4fa2e332b15cc28bd0f77174c270', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 16:21:52'),
('54eaf7866d6152ca2a598b19080f170c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 16:42:16'),
('d169eb629f7d1f3d0f0cc1a67522b8a1', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 16:42:53'),
('075c37e20aee5e8778c6d71de26e9a41', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 16:43:36'),
('2fdd28acf57458ae826d4cd323dc2574', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 16:46:13'),
('9717696c3dfb60af9cbd7432bcc1efd4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 16:55:07'),
('037094ef21ceb91f3b21df331f85df3c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 16:58:48'),
('109a85a0f8f18ea34c6e5315b2109764', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 10:31:33', '2021-08-17 17:00:46'),
('0fee65650cb35853f516c07659b0f376', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 21:18:58', '2021-08-17 17:06:56'),
('ae05517a58eb9c76220acff067a44f74', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 21:18:58', '2021-08-17 20:42:48'),
('768371462d83ed40e2849a0563e91613', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 21:18:58', '2021-08-17 20:55:49'),
('5d914f18bf2bc0a6c46c048aa2583cd6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-18 21:18:58', '2021-08-17 22:13:39'),
('253875a8c362d8997e62f07739b1c622', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-18 21:18:58', '2021-08-17 22:22:22'),
('6a434062784256e043498479b2772dcd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-17 22:22:58'),
('7008872ab6d5c889bb807310254edcf6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:05:00'),
('170cf8385e6fa0642211a748b8e6795d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:15:09'),
('aa5d1bf6cf6c15eacadf0334ea4f471a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:18:20'),
('f8ed3b5b445cdcd9a374acf0a497a368', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:20:54'),
('199e8d4425620104290952adeb46902c', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:21:30'),
('af6b363b7bea9943c161ac464f598762', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:21:58'),
('773ac3cbe8ce57bc33fdcd6bfc872e81', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:22:35'),
('47900fcce1f28fd1e456ab1cc3d40792', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:29:08'),
('90cc3e9d7987da0839ef0b198b2fd76b', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:30:45'),
('d049ab73f6d940b27ff47cca58654d22', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 09:43:47'),
('02a16de9f2e8590360b953d5e160b0a4', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 10:05:15'),
('ab7ad68f4e76706d9d5dc9b4f7f61c52', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 10:05:57'),
('ce280ea47b5c72d3cbaa96a4ddfed546', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-19 09:13:54', '2021-08-18 10:31:26'),
('1186115a3fa90b36f9625af281049cbe', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', '3', '3', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2021-08-18 11:04:54', '2021-08-18 10:48:14'),
('1413a48a35a423f9a16f1f15a4da8435', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-24 10:47:52', '2021-08-18 11:15:42'),
('4abe2536733ac765a312d14e0fe86bd9', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2021-08-18 17:20:20', '2021-08-18 11:16:27'),
('9270bf04746d69491a5d9e8381e48391', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2021-08-24 10:47:52', '2021-08-18 11:21:15'),
('37646455b5237e3bfab222eb16dbb318', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2021-08-18 11:22:01'),
('5860c70c96d7b07f2e6fb2b19d520425', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2021-08-18 17:20:20', '2021-08-18 11:24:05'),
('86f258179ee2b91cab6296ec1513262a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2021-08-18 16:17:20'),
('22205bed8b5562fd9790bffa441e5dd6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-24 10:47:52', '2021-08-18 16:19:03'),
('20a02ad39c2828185eed3ba279197f45', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-24 10:47:52', '2021-08-18 16:20:16'),
('4f2e6683ef0438f1c32d7e9434407eca', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2021-08-18 16:20:38'),
('fc8b4da7414c75118df253ed59da5082', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-26 09:35:16', '2021-08-18 16:24:33'),
('1b01b77ba37a1bac1f3ac40917b508d8', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-26 09:35:16', '2021-08-18 16:26:34'),
('2987a0528da9884f88146405891d60f6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-26 09:35:16', '2021-08-18 16:29:31'),
('9f16cc610c1c4ec6695479707eb22dfd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-26 11:21:13', '2021-08-18 16:31:31'),
('55f3e17ef8660df513a3cd6451345584', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2021-08-26 17:14:59', '2021-08-18 16:34:30'),
('55d27f100ba4b624aa5c23cfbe4808fd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2021-08-18 17:20:20', '2021-08-18 16:36:38'),
('33ebb56848e6e93a26133a4833e06e31', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'false', NULL, '2021-08-18 17:26:47'),
('9a5f9bdc35d4c905e4b113650d462240', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-18 17:32:55'),
('605d5ab35a72aa35914b22876d279137', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2021-08-18 21:13:17'),
('d6f303b583e427238dd627dbd75dbb9d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-18 21:18:02'),
('1e7362422e184f0c06eacfaeaa036973', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-18 21:19:41'),
('d87b4838cecf3e52c7446bd1f8c1e3e0', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-18 21:24:08'),
('929c4026e8c4af6ec77980bc38b72a4f', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2021-08-18 21:27:30'),
('7dc9f6af6e9f354a78697a148d3d4b55', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'false', NULL, '2021-08-18 21:28:09'),
('e21fc2e5fa605635f0d9269ba78195b2', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-18 21:28:22'),
('80d0cca5024adac7c65b44b148d16bf5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-19 08:37:02'),
('f1dba96a70bb087b30698d6e150e6de3', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-19 09:10:14'),
('5c9b844d201b1ec6f4a663bd90ef22d5', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-19 09:24:41'),
('8a29abea9c105bf24e68d3deb771f22e', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2021-08-27 16:13:49', '2021-08-19 09:34:08'),
('3f6f25179d4860fa3d235c2959fd02d6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2022-05-12 23:04:03', '2021-08-19 09:34:55'),
('cb874aec05be45190a42c80126038423', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2022-05-12 23:04:03', '2021-08-19 09:35:45'),
('077ab12326e14f0b3369032f781cfa8d', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 23:09:03', '2021-08-20 16:15:04'),
('1abea22501764e8474e9bd93fa4a0dd6', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-20 16:48:09'),
('b44c51677527561f4280b6299c0a3bfd', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-20 16:53:45'),
('5b8fdc3e99981773fdf0a76aca8baf51', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-20 16:55:02'),
('12228fe608725cf169a9d5a4d8c1eba1', '1', '1', 'BOJA', '1', '1', 'BOJA', 'CABANG', 'TATA USAHA', '127.0.0.1', 'false', NULL, '2021-08-20 17:01:05'),
('f4de28090244de6c22c100308532300d', '171a103a423d9c800c868393c7074ad9', '14', 'PEGANDON', '171a103a423d9c800c868393c7074ad9', '14', 'PEGANDON', 'CABANG', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-23 11:41:10', '2021-08-23 11:40:56'),
('328f49f7bc9b982de1366534702a1cdc', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-24 10:11:51'),
('71c2e24849b15214e74869dbcbdbcfd9', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-24 10:22:28'),
('67f24966868e91193f5543384764fdff', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-24 10:26:39'),
('9f58b36b8fdf046721841ab32b244ac2', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-24 10:33:15'),
('91f866bcbde1dc0b9be82183dbc6a500', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-24 10:39:56'),
('6c799f23b0b97a2cc3005598bd99de8f', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-24 10:43:52'),
('da0a8045efee17b563b985e8d32ec4d1', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2021-08-24 10:48:36'),
('2c7c785f4a445612789c1bbdfaa9a500', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-26 16:40:52', '2021-08-25 09:49:36'),
('252a99a07cb3802b4d4073ed771fa536', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-26 16:40:52', '2021-08-25 16:30:20'),
('3b64fb6f2e8587b6ffcb0f2a56cacadb', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-26 16:40:52', '2021-08-26 05:08:12'),
('6b2ca34fd3bbccfa1e1440cdab70c07a', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-26 16:40:52', '2021-08-26 05:35:14'),
('40cb0870445c57297afc484ff96b05a0', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-26 16:40:52', '2021-08-26 05:54:05'),
('0fb70f17b6bb692b67b77fbe9aff2e93', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2021-08-26 16:40:52', '2021-08-26 05:59:22'),
('8647ccbcb195775eb11740e2f6ec5a89', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '103.105.28.157', 'true', '2022-05-12 23:10:16', '2021-08-26 07:13:13'),
('cea2c4d1c24682efdd9196c2fa67aaa5', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.28.157', 'true', '2021-08-26 16:40:52', '2021-08-26 07:18:06'),
('fa1f97745b0b9a1c98c7a90ca4b802ce', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.28.157', 'true', '2021-08-26 16:40:52', '2021-08-26 07:21:46'),
('09c612d22e0f20817e549d854b6b0e3f', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '103.105.28.157', 'true', '2022-05-12 23:10:16', '2021-08-26 08:02:17'),
('174ebf4fc5a0c0943daf875d76947db8', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.28.157', 'true', '2021-08-26 16:40:52', '2021-08-26 08:13:12'),
('9d22d3593e2a9d7544cc07b5fccb5d6c', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.28.157', 'true', '2021-08-26 16:40:52', '2021-08-26 08:13:46'),
('36bd23f55368057c7ab30045a1e8f475', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '103.105.35.111', 'true', '2022-05-12 23:10:16', '2021-08-26 09:33:23'),
('b2c152b2f0ed488a32e46716d2518ed8', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '103.105.35.111', 'true', '2022-05-12 23:10:16', '2021-08-26 09:33:54'),
('7a78cf689b56d57ab97fba7549430726', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.111', 'true', '2021-08-26 16:40:52', '2021-08-26 09:35:43'),
('60149a6a22cbff8073f97e897211be70', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 11:28:22'),
('fdad518bcdfbeb36a4e95f7313461b7c', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.111', 'true', '2021-08-26 16:40:52', '2021-08-26 10:18:33'),
('5a9093c9d02a7d85d45b0678dfda91cb', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'false', NULL, '2021-08-26 10:35:39'),
('d1675ac74491e3ad079803e6cc6cb1a0', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'true', '2022-05-12 23:10:16', '2021-08-26 10:36:47'),
('e31a92e13fd2829fb2f90594fe7869a9', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 11:27:53'),
('3453c5f0229bfc9b20484c5b92e466b8', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 11:27:19'),
('1f1cd041cf4cc8c8687b4c0b7b404342', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-26 17:06:06', '2021-08-26 10:45:56'),
('bf8096a556f1b61312163bd11005796a', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-26 17:06:06', '2021-08-26 10:49:17'),
('fedf3dd69d4f83e2dd2f3412fd4130f3', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-26 20:13:56', '2021-08-26 10:49:47'),
('cd750be4b604f272637f88c1760b54aa', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'true', '2022-05-12 23:10:16', '2021-08-26 10:50:52'),
('f4c9eeefbc8c65ecad30de11b1ba362f', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2022-05-12 11:26:40'),
('41b53bfa574e833016a3f1d734c96688', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'true', '2022-05-12 23:10:16', '2021-08-26 11:09:13'),
('9abfec004fd9131df09fc6859ba6b044', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 11:26:29'),
('de0d73ab5ee3954d8757ffe17c78a354', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:06:06', '2021-08-26 11:14:17'),
('49725b736873878fa98fb3a79fbdcecd', 'c4ca4238a0b923820dcc509a6f75849b', 'MAJELIS', 'MAJELIS', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 22:56:24', '2022-05-12 11:24:44'),
('8e8186e03965ab23bafd3db518685abf', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:06:06', '2021-08-26 11:25:08'),
('9cc9ecd9848eec2ac8a305d53d4e9955', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '112.215.173.110', 'true', '2021-08-26 17:06:06', '2021-08-26 11:27:50'),
('daf0ff8b6b61c1da6bfaf291f5079183', 'f64ec1f810289a5ba00e1e6d770a62e0', '13', 'MIM Plantaran', 'f64ec1f810289a5ba00e1e6d770a62e0', '13', 'MIM Plantaran', 'SEKOLAH', 'TATA USAHA', '182.2.70.2', 'true', '2021-08-26 17:06:06', '2021-08-26 11:28:38'),
('fa88d4925434738f78bd2398fd20bc3c', '8f12011a3897a49c7e7e08fb99fb39c7', '25', 'MIM Wadas', '8f12011a3897a49c7e7e08fb99fb39c7', '25', 'MIM Wadas', 'SEKOLAH', 'TATA USAHA', '182.2.72.219', 'true', '2021-08-26 17:06:06', '2021-08-26 11:28:54'),
('254f93ced4c2d10adfd4692db67eba32', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '182.2.41.38', 'true', '2021-08-26 17:06:06', '2021-08-26 11:29:17'),
('404f75adf83d383a34e256451003a870', '8d0f155e01648834c5c3792a9120e43d', '10', 'MIM Purworejo', '8d0f155e01648834c5c3792a9120e43d', '10', 'MIM Purworejo', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:06:06', '2021-08-26 11:30:14'),
('a47f72bdb46c4d23d8cce88dec57065b', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '112.215.241.158', 'true', '2021-08-26 17:06:06', '2021-08-26 11:30:25'),
('cd0654b90f04c8025beec7b853b02c9c', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'SEKOLAH', 'TATA USAHA', '114.10.8.214', 'true', '2021-08-26 17:06:06', '2021-08-26 11:30:49'),
('2ef8291034f6eeab9ed6c5d9dc5b0444', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 22:43:12', '2022-05-12 11:24:07'),
('9247e1e01f79b1f5da3713e39357aa48', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:06:06', '2021-08-26 11:31:47'),
('507c14f09039858064e51990b332ff61', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '112.215.173.110', 'true', '2021-08-26 17:06:06', '2021-08-26 11:32:07'),
('521ee38f19a1ad6b9563fb7377504448', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:06:06', '2021-08-26 11:32:24'),
('d1ea647ac2205ce5686607110994d051', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2022-05-12 11:22:35'),
('b423b1a7f27cfb1564349da2752e4f4a', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:06:06', '2021-08-26 11:32:40'),
('ed7ba40f2364cf63752016a551cac812', 'a06844aab3573565040dc1076cebab8c', '12', 'MIM Sarirejo', 'a06844aab3573565040dc1076cebab8c', '12', 'MIM Sarirejo', 'SEKOLAH', 'TATA USAHA', '103.105.35.90', 'true', '2021-08-26 17:06:06', '2021-08-26 11:33:02'),
('93f9aeb141f4da8807e01e96326015d1', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:06:06', '2021-08-26 11:33:06'),
('887a108669a72eb258ee20da4e75700e', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:06:06', '2021-08-26 11:33:26'),
('1140cb729ae3e7b0b484da2dcf296905', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '182.2.41.38', 'true', '2021-08-26 17:06:06', '2021-08-26 11:33:39'),
('a3ac0a39feab029cd9e113d86144cfbb', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', '61639f850b03e034e2e836ddb74d805f', '24', 'MIM Sambongsari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:33:58'),
('80aba9261414fc25fd35f831205995b3', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '112.215.241.158', 'true', '2021-08-26 17:05:08', '2021-08-26 11:33:59'),
('05108285d8827269528b72a8e6def18a', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', 'SEKOLAH', 'TATA USAHA', '114.79.20.242', 'true', '2021-08-26 17:05:08', '2021-08-26 11:34:02'),
('59f10ed83dd27964fa643c4878b7e736', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:34:15'),
('24f1c11f33776b2f64e5dd99ba9b8fa7', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '112.215.241.158', 'true', '2021-08-26 17:05:08', '2021-08-26 11:36:08'),
('e6c1f654957fe8ae010c39a022c5a673', '0523679c15f5e4ac9c26f16a48f9710f', '21', 'MIM 2 Rowosari', '0523679c15f5e4ac9c26f16a48f9710f', '21', 'MIM 2 Rowosari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:36:18'),
('6bf9c1cae12bcae604a8a1e28b346449', '8f12011a3897a49c7e7e08fb99fb39c7', '25', 'MIM Wadas', '8f12011a3897a49c7e7e08fb99fb39c7', '25', 'MIM Wadas', 'SEKOLAH', 'TATA USAHA', '182.2.72.219', 'true', '2021-08-26 17:05:08', '2021-08-26 11:36:40'),
('1c442c5e035d3d35c3d9e7f4b055156c', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '209.95.56.53', 'true', '2021-08-26 17:05:08', '2021-08-26 11:37:58'),
('21117325848d585696d6741b786c2a9d', '8d0f155e01648834c5c3792a9120e43d', '10', 'MIM Purworejo', '8d0f155e01648834c5c3792a9120e43d', '10', 'MIM Purworejo', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:38:22'),
('7b1cfdfe09162b6e7804f22cdc51528e', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'SEKOLAH', 'TATA USAHA', '182.2.73.134', 'true', '2021-08-26 17:05:08', '2021-08-26 11:38:29'),
('d5e6692c21c71edb5d278c214045399d', 'f64ec1f810289a5ba00e1e6d770a62e0', '13', 'MIM Plantaran', 'f64ec1f810289a5ba00e1e6d770a62e0', '13', 'MIM Plantaran', 'SEKOLAH', 'TATA USAHA', '182.2.70.2', 'true', '2021-08-26 17:05:08', '2021-08-26 11:38:36'),
('02bbd5573d7db3648a199b3c4ee26224', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:38:43'),
('11feb16092f8ed1e3804f082f38ae24b', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'SEKOLAH', 'TATA USAHA', '140.213.57.14', 'true', '2021-08-26 17:05:08', '2021-08-26 11:39:07'),
('15b5da8b5bbd5490cf4ef4beb5775826', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 20:13:56', '2021-08-26 11:39:35'),
('fdd7ed1c52b4c96afbd6dc0880dad5ca', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'SEKOLAH', 'TATA USAHA', '114.10.4.231', 'true', '2021-08-26 17:05:08', '2021-08-26 11:39:43'),
('d0ed60bb1bfa490d4fae330ca4136d60', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:40:03'),
('700d5259ea59a115f347860b8949695e', 'f64ec1f810289a5ba00e1e6d770a62e0', '13', 'MIM Plantaran', 'f64ec1f810289a5ba00e1e6d770a62e0', '13', 'MIM Plantaran', 'SEKOLAH', 'TATA USAHA', '182.2.70.2', 'true', '2021-08-26 17:05:08', '2021-08-26 11:40:05'),
('46a778d7cd5a9e64279654c8bf61ba90', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 20:13:56', '2021-08-26 11:40:06'),
('02461114597551b761868fcfdec17d45', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', 'SEKOLAH', 'TATA USAHA', '140.213.218.28', 'true', '2021-08-26 17:05:08', '2021-08-26 11:40:06'),
('305b7d56d80cc68e35a42c239874317f', '0523679c15f5e4ac9c26f16a48f9710f', '21', 'MIM 2 Rowosari', '0523679c15f5e4ac9c26f16a48f9710f', '21', 'MIM 2 Rowosari', 'SEKOLAH', 'TATA USAHA', '140.213.58.230', 'true', '2021-08-26 17:05:08', '2021-08-26 11:40:12'),
('447497f918a9ea164fb5dd045a9a6450', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '112.215.241.158', 'true', '2021-08-26 17:05:08', '2021-08-26 11:40:17'),
('a2aec0a62a19ffb019b124274ff8c720', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 20:13:56', '2021-08-26 11:40:52'),
('97286967f0295e05dabf8854e91a946e', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', 'SEKOLAH', 'TATA USAHA', '140.213.218.28', 'true', '2021-08-26 17:05:08', '2021-08-26 11:40:59'),
('2e327958ee984a382d5254ecfd4ea01d', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:41:24'),
('d8c7cadaba73534e50c04c9d3a3a6855', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:41:29'),
('3e01473cba86c111a8949f8746f47722', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '209.95.56.53', 'true', '2021-08-26 17:05:08', '2021-08-26 11:41:34'),
('3f8ca5abea2f5ef6e1041aa6c470b83c', 'a06844aab3573565040dc1076cebab8c', '12', 'MIM Sarirejo', 'a06844aab3573565040dc1076cebab8c', '12', 'MIM Sarirejo', 'SEKOLAH', 'TATA USAHA', '103.105.35.90', 'true', '2021-08-26 17:05:08', '2021-08-26 11:41:38'),
('5b16fe3ad9fd62ef74e4772ceee21a93', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'ad6918ab849ae690290632883c6c3daa', '23', 'MIM  Damarjati', 'SEKOLAH', 'TATA USAHA', '140.213.57.14', 'true', '2021-08-26 17:05:08', '2021-08-26 11:41:56'),
('8697510589f9d665d0854a47b3e93b98', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'ea2ca9bcfd05c6017bbc50172221d7f5', '16', 'MIM Pucangrejo', 'SEKOLAH', 'TATA USAHA', '182.2.73.134', 'true', '2021-08-26 17:05:08', '2021-08-26 11:42:09'),
('ae9432b31c5e49072b64ebf470fb2e27', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-26 17:05:08', '2021-08-26 11:42:10'),
('1d41f845591c2135a3f51963606ea62b', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', 'SEKOLAH', 'TATA USAHA', '114.79.19.22', 'true', '2021-08-26 17:05:02', '2021-08-26 11:42:12'),
('261f63a9a8a52f16b0f324cdfb464891', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', '9ffed3ccefa45947619faa6c8ebdd3e1', '19', 'MIM Gempolsewu', 'SEKOLAH', 'TATA USAHA', '114.10.5.72', 'true', '2021-08-26 17:05:02', '2021-08-26 11:42:29'),
('48cdeb1a7912b767befec2f95fc515a6', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '112.215.241.158', 'true', '2021-08-26 17:05:02', '2021-08-26 11:42:45'),
('1dacc92c21dcc284f93310e4db929f4a', 'f64ec1f810289a5ba00e1e6d770a62e0', '13', 'MIM Plantaran', 'f64ec1f810289a5ba00e1e6d770a62e0', '13', 'MIM Plantaran', 'SEKOLAH', 'TATA USAHA', '182.2.70.2', 'true', '2021-08-26 17:05:02', '2021-08-26 11:43:31'),
('0dee78400db445e82026f4da519f76a8', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-26 17:05:02', '2021-08-26 11:45:21'),
('65e598522badbc49a6a550eafb3aa235', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-26 17:05:02', '2021-08-26 11:46:31'),
('4415a90020299fac70a51e6be56e7202', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '112.215.241.158', 'true', '2021-08-26 17:05:02', '2021-08-26 11:46:34'),
('f589f8383ad90644f920dd8e9e5554f5', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-26 17:05:02', '2021-08-26 11:48:34'),
('bb64d457453ffdadcd7a3785718f864d', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '182.2.73.207', 'true', '2021-08-26 17:05:02', '2021-08-26 11:50:05'),
('0d615ae2a026adc4d0af5990710a0343', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-27 08:57:46', '2021-08-26 11:50:44'),
('1d890347a8516a6d1471c023716c8caf', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-27 08:57:46', '2021-08-26 11:52:08'),
('d6ff2ea23fd137dd4408dd5513f93ac3', 'b67fedde63fee8e56fbc72e82e241e50', '40', 'MTsM 3 Ngargosari', 'b67fedde63fee8e56fbc72e82e241e50', '40', 'MTsM 3 Ngargosari', 'SEKOLAH', 'TATA USAHA', '182.2.68.131', 'true', '2021-08-27 08:57:46', '2021-08-26 11:52:22'),
('3359a0bb84eec1e2ef2f7c379843e48f', 'b67fedde63fee8e56fbc72e82e241e50', '40', 'MTsM 3 Ngargosari', 'b67fedde63fee8e56fbc72e82e241e50', '40', 'MTsM 3 Ngargosari', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-27 08:57:46', '2021-08-26 11:52:25'),
('4a457bd795c9ffe855ec7dbb45770d65', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '112.215.241.158', 'true', '2021-08-27 10:07:17', '2021-08-26 11:52:30'),
('2d46a66c0914ae1bcecf4d967b97dc2e', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '182.2.73.207', 'true', '2021-08-27 10:07:17', '2021-08-26 11:52:37'),
('86977b489951e2c1ae01175e936ccafc', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '112.215.173.110', 'true', '2021-08-27 10:07:17', '2021-08-26 11:54:24'),
('7e1ef2dfbd91dba528dfe34180d3db3b', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', '18e3d1fe9cfe9e7ac941a1dc55e7084c', '18', 'MIM 1 Rowosari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-27 10:07:17', '2021-08-26 11:55:15'),
('c923913e3cc4ac306c4dbbd7133e7cf2', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-27 10:07:17', '2021-08-26 11:59:16'),
('d7afd0a57a9a0a7b50f6f98cae45be17', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '36.81.36.157', 'true', '2021-08-27 10:07:17', '2021-08-26 12:00:45'),
('c98fbb7f5dead749c0e04cd887c7baf1', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', '12004ec5f9f0094f22959eee643f1e1a', '39', 'MTsM 2 Patean', 'SEKOLAH', 'TATA USAHA', '114.79.19.22', 'true', '2021-08-27 16:08:47', '2021-08-26 12:01:20'),
('7b60535562809a110a6a6485d20f749a', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'SEKOLAH', 'TATA USAHA', '103.105.35.125', 'true', '2021-08-27 16:08:47', '2021-08-26 12:04:52'),
('07314b171182555b5ae9f9a254ea61c6', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'SEKOLAH', 'TATA USAHA', '182.2.75.211', 'true', '2021-08-27 16:08:47', '2021-08-26 12:05:28'),
('68f8f33a42eeaf011d33c307360bbcf6', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '36.72.217.180', 'true', '2021-08-27 16:36:25', '2021-08-26 12:22:33'),
('f1a15a27c6abf4d718e4db553d0a3ade', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'SEKOLAH', 'TATA USAHA', '115.178.244.113', 'true', '2021-08-27 16:36:25', '2021-08-26 13:20:03'),
('58455279bfcc976ea409f044a5e580f3', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'SEKOLAH', 'TATA USAHA', '115.178.244.113', 'true', '2021-08-27 16:36:25', '2021-08-26 14:40:03'),
('85398d201cf3c8f253d8cee595f22a4f', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.117', 'true', '2021-08-27 16:36:25', '2021-08-26 16:38:28'),
('4a772c23854a794bafdf5a20c9732f1c', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.117', 'true', '2021-08-27 16:36:25', '2021-08-26 16:57:38'),
('e501d7b391f5343950d880115811813f', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.117', 'true', '2021-08-27 16:36:25', '2021-08-26 16:58:56'),
('21d3dd4466edef57765ae59c6bff7901', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.117', 'true', '2021-08-27 16:36:25', '2021-08-26 16:59:39'),
('f1dbdb75e20c6e3f9a1f22a8e229bc99', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.117', 'true', '2021-08-27 16:36:25', '2021-08-26 17:00:15'),
('403f7f691d5ba9c72fb72128fc22fb6a', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', '2aa1b4e6654360ea4519dc265402780a', '123', '123x', 'SEKOLAH', 'TATA USAHA', '103.105.35.117', 'true', '2022-05-12 23:10:16', '2021-08-26 17:14:41'),
('6d21d9b5ba2becddc77138e9bd90e69e', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '180.246.145.216', 'true', '2022-05-12 11:15:54', '2021-08-26 20:07:11'),
('cefe61c6097215701b5edb41df5141a8', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', '32001d7b102c279327a34c49320859e8', '9', 'MIM Caruban', 'SEKOLAH', 'TATA USAHA', '180.246.145.216', 'true', '2022-05-12 11:15:54', '2021-08-26 20:10:02'),
('533b388b592dbfb786d4dc3c3f659897', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '36.72.217.177', 'true', '2022-05-12 11:15:54', '2021-08-27 07:08:58'),
('676491ba1f17686548a06632c2ff1fec', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', '5f3232a257290bb1ada9c5be5cbd93c2', '14', 'MIM Pagersari', 'SEKOLAH', 'TATA USAHA', '36.72.217.177', 'true', '2022-05-12 11:15:54', '2021-08-27 07:10:26'),
('2eee935047cbbe26fc4cad0dea9a67ea', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 11:21:33'),
('a5f26669bbfb342b9d15b1a745a1a78d', 'c4ca4238a0b923820dcc509a6f75849b', 'MAJELIS', 'MAJELIS', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 22:56:24', '2022-05-12 11:20:58'),
('18cd3e7f3dae8bb9bad380397796b4c3', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', '00546ca9ac4d94549ce1dedcd23deedc', '11', 'MIM Sukomulyo', 'SEKOLAH', 'TATA USAHA', '103.105.27.88', 'true', '2022-05-12 11:15:54', '2021-08-27 08:34:56'),
('8dfba57353b72c628e76890311b4dd7c', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.89', 'true', '2022-05-12 11:15:54', '2021-08-27 08:51:40');
INSERT INTO `user_log_login` (`kd`, `sekolah_kd`, `sekolah_kode`, `sekolah_nama`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `ipnya`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('49efc8a623f1f28140564b91158490c9', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'c96016e08fae6f81ae9b7e5f4a6a4eae', '22', 'MIM  Mulyosari', 'SEKOLAH', 'TATA USAHA', '36.72.212.51', 'true', '2022-05-12 11:15:54', '2021-08-27 09:33:56'),
('3d0597e0aea4e3e2fb1b2a3c9b6d2e89', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.35.89', 'true', '2022-05-12 11:15:54', '2021-08-27 09:44:48'),
('ab83623ced38b7f8dd7de00a825e385f', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '118.99.104.25', 'true', '2022-05-12 11:15:54', '2021-08-27 10:00:49'),
('d1b354001dfaa9e6db5876b0554c70c1', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '118.99.104.25', 'true', '2022-05-12 11:15:54', '2021-08-27 10:02:17'),
('a07b2fb65c944ee5ceaadc32220d3af7', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'cef73538b0868c9a1183b335066a6caa', '20', 'MIM Pojoksari', 'SEKOLAH', 'TATA USAHA', '118.99.104.25', 'true', '2022-05-12 11:15:54', '2021-08-27 10:15:40'),
('aae557735d81d3d85425fd6dcf2a58b7', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '118.96.155.85', 'true', '2022-05-12 22:43:12', '2021-08-27 11:07:34'),
('738f166f2da4b757337c4b4c5b707fdb', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', '55b882a06206fd4f20df234a3fae70bb', '17', 'MIM Tanjunganom', 'SEKOLAH', 'TATA USAHA', '36.72.212.248', 'true', '2022-05-12 22:43:12', '2021-08-27 11:09:49'),
('13025176823664859dbdc1114fc9560b', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '118.96.155.85', 'true', '2022-05-12 22:43:12', '2021-08-27 11:18:34'),
('f87b1e2c843e1b719a531aba9c8b9fce', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', '0796c2abd0b6e76e2d2b3ae48e81247c', '15', 'MIM Bangunsari', 'SEKOLAH', 'TATA USAHA', '118.96.155.85', 'true', '2022-05-12 22:43:12', '2021-08-27 11:22:08'),
('8956849a427ebe788b80885f9cec9812', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.28.152', 'true', '2022-05-12 22:43:12', '2021-08-27 16:08:23'),
('38e99eacba2ad3ec6811523928fdae14', '2aa1b4e6654360ea4519dc265402780a', '123', '123', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'SEKOLAH', 'TATA USAHA', '103.105.28.152', 'true', '2022-05-12 23:10:16', '2021-08-27 16:09:38'),
('e73baed21d2e13262d851dd826159cf5', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 22:43:12', '2022-05-12 10:40:31'),
('c00da0514250524d8f92ff876a413eca', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '103.105.28.188', 'true', '2022-05-12 22:43:12', '2021-08-27 16:36:01'),
('ff5f9dfba0944172b8da742e6364d9b6', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', '', '', '', 'MAJELIS', 'PEGAWAI', '103.105.28.188', 'true', '2022-05-12 22:43:12', '2021-08-27 17:07:02'),
('9954d6dd8f2235d307bbc5f114527d0b', 'c4ca4238a0b923820dcc509a6f75849b', 'MAJELIS', 'MAJELIS', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '103.105.28.188', 'true', '2022-05-12 22:56:24', '2021-08-27 17:11:31'),
('b9b7d0358ff6e0ec13453a69d6884803', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 22:43:12', '2021-08-28 16:00:55'),
('8c3b1a3fbd39203ff1df9901c0a6336f', 'c4ca4238a0b923820dcc509a6f75849b', 'MAJELIS', 'MAJELIS', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 22:56:24', '2021-08-28 16:14:19'),
('47f076f59a4add30268b1e17ef6b5ad2', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'CABANG', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 22:43:12', '2021-08-28 16:15:28'),
('e7a5699fcf6686eb989bb65a816970c5', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', 'CABANG', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 22:43:12', '2021-08-28 16:18:02'),
('7a90e9c5dc43a7b9702bdc229cbbc02f', 'b8cc6b55576dffb4dc1763ffa1015d9e', '1', 'CABANG 1', '3bf21607a990f50b88a4dc62f04411dc', '1', '1', 'CABANG', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 22:43:12', '2021-08-28 16:18:38'),
('4860da2eafea2e9abb67019262793602', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-28 16:19:38'),
('d7fa3ad65fe8448a9ed263f16cd5a465', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2021-08-28 16:21:26'),
('34a42d7cff97e3c3691f2aa34b6a1e3d', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-28 16:23:45'),
('5315cde68f184ef62dc89d98fab43865', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2021-08-28 16:24:02'),
('01313fc3f51782be94383ad684a2b395', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'MAJELIS', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 22:43:12', '2022-05-12 22:41:56'),
('d76e51b50f2a2adb118f8f070fc5a107', 'c4ca4238a0b923820dcc509a6f75849b', 'MAJELIS', 'MAJELIS', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'MAJELIS', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 22:56:24', '2022-05-12 22:55:36'),
('6e6da85fbcbfa83fe7708e07aba96b38', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'TATA USAHA', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 22:59:07'),
('1a0b1738eef6cd2e2772880839fb4d00', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SEKOLAH', 'PEGAWAI', '127.0.0.1', 'true', '2022-05-12 23:07:36', '2022-05-12 23:04:31'),
('8d41f486eeddcdae8bd489d2cfedb739', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'KEPEGAWAIAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 23:05:50'),
('28708b58b8e356b6483fd5fce759c24c', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'SARPRAS', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 23:08:32'),
('d13eb656db297fe9a59f3ba187d15f35', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', '2aa1b4e6654360ea4519dc265402780a', '123', '123 BIASAWAE', 'SEKOLAH', 'KEUANGAN', '127.0.0.1', 'true', '2022-05-12 23:10:16', '2022-05-12 23:09:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_msg`
--

CREATE TABLE `user_msg` (
  `kd` varchar(50) NOT NULL,
  `user_kategori` varchar(100) DEFAULT NULL,
  `user_cabang` varchar(100) DEFAULT NULL,
  `user_cabang_kd` varchar(50) DEFAULT NULL,
  `user_sekolah` varchar(100) DEFAULT NULL,
  `user_sekolah_kd` varchar(50) DEFAULT NULL,
  `user_kd` varchar(100) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `uuser_kategori` varchar(100) DEFAULT NULL,
  `uuser_cabang` varchar(100) DEFAULT NULL,
  `uuser_cabang_kd` varchar(50) DEFAULT NULL,
  `uuser_sekolah` varchar(100) DEFAULT NULL,
  `uuser_sekolah_kd` varchar(50) DEFAULT NULL,
  `uuser_kd` varchar(100) DEFAULT NULL,
  `uuser_kode` varchar(100) DEFAULT NULL,
  `uuser_nama` varchar(100) DEFAULT NULL,
  `uuser_posisi` varchar(100) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `isi` longtext DEFAULT NULL,
  `dibaca` enum('true','false') DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_msg`
--

INSERT INTO `user_msg` (`kd`, `user_kategori`, `user_cabang`, `user_cabang_kd`, `user_sekolah`, `user_sekolah_kd`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `uuser_kategori`, `uuser_cabang`, `uuser_cabang_kd`, `uuser_sekolah`, `uuser_sekolah_kd`, `uuser_kd`, `uuser_kode`, `uuser_nama`, `uuser_posisi`, `judul`, `isi`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('f10f1722d83872f1eb1a61b488b4a00d', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'kkkkk', 'xkkirixpxkkananxsafggggxkkirixxgmringxpxkkananx', 'true', '2021-08-18 16:26:04', '2021-08-15 10:47:32'),
('404fe362c3b746130642370a8edcc2bd', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'tes lagi ya...', 'xkkirixpxkkananxok........xkkirixxgmringxpxkkananx', 'true', '2021-08-18 16:19:37', '2021-08-15 10:09:48'),
('ff94cfcca3f2a8ab06eb7223cc48c6f3', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'CABANG', 'BOJA', 'b8cc6b55576dffb4dc1763ffa1015d9e', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'sssf', 'xkkirixpxkkananxsfffgxkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-15 10:16:46'),
('faa1cfea2398b9ed503554b9f262c439', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'PEGAWAI', 'sfffff', 'xkkirixpxkkananxsfsfgggxkkirixxgmringxpxkkananx', 'true', '2021-08-18 16:20:50', '2021-08-15 10:04:08'),
('677fa88a1aecd70c1fcfd2d57f646da0', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'ok..', 'xkkirixpxkkananxcsddxkkirixxgmringxpxkkananx', 'true', '2021-08-18 16:19:43', '2021-08-15 10:03:54'),
('c3b55436ebbb592e1f0ae4c4a55edac2', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'BALAS : sdffff', 'xkkirixpxkkananxasfrrrrxkkirixxgmringxpxkkananx', 'true', '2021-08-18 21:18:52', '2021-08-15 10:48:38'),
('8d961fc7f9953247d7eaff22c3cd6dfd', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'BALAS : ok deh....', 'xkkirixpxkkananxsafdgrsfxkkirixxgmringxpxkkananx', 'true', '2021-08-18 21:18:50', '2021-08-15 10:49:18'),
('770a0ee48276eb7e99f910d853abbcb5', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'BALAS : BALAS : BALAS : ok deh....', 'xkkirixpxkkananxok....xkkirixxgmringxpxkkananx', 'true', '2021-08-18 16:19:32', '2021-08-15 10:52:09'),
('7cfe1db984c8757d90b29534b971a62c', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'BALAS : BALAS : ok..', 'xkkirixpxkkananxok..... sip...xkkirixxgmringxpxkkananx', 'true', '2021-08-17 22:23:43', '2021-08-15 11:08:22'),
('9a6757999fcfa531dc1d228b9f0d2ccc', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'MAJELIS', '', '', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'assss', 'xkkirixpxkkananxsssssxkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-15 11:08:52'),
('130555d465c31f1a50940a0630f4b688', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'CABANG', 'KALIWUNGU', '0a3a3d982dc7f051c5f25de40bc619b2', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'sdfff', 'xkkirixpxkkananxsdsffffxkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-15 11:09:24'),
('f202fd763d15e22af1a6c6b472ff70f1', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'Sekolah', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'BALAS : BALAS : BALAS : BALAS : ok..', 'xkkirixpxkkananxsdffffxkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-15 11:26:30'),
('5b36470ed337b7133f4adb5edd0fd61d', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'sdfff', 'xkkirixpxkkananxsafdcxkkirixxgmringxpxkkananx', 'true', '2021-08-17 09:16:32', '2021-08-15 11:34:11'),
('fe50ce8256422b9ac5cd049fa972d951', 'Sekolah', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'BALAS : BALAS : tes lagi ya...', 'xkkirixpxkkananxsdfffxkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-15 11:40:01'),
('fc62c97edad6ef5023faee8d6c7ab0de', 'Sekolah', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'MAJELIS', '', '', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'ssss', 'xkkirixpxkkananxsfsffxkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-15 11:40:40'),
('75d41488852fabdab3c7790d981b35fd', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'cob satu dua tiga.....', 'xkkirixpxkkananxok. sip.....xkkirixxgmringxpxkkananx', 'true', '2021-08-17 09:16:37', '2021-08-17 09:07:49'),
('e870b5910ec0b33081ef6c728acc2706', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'BALAS : BALAS : BALAS : BALAS : BALAS : ok deh....', 'xkkirixpxkkananxok... aq coba ya...xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-17 09:08:20'),
('0b7aba88a4fc9ed8a006730e664c34db', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'PEGAWAI', '222', 'xkkirixpxkkananx122222xkkirixxgmringxpxkkananx', 'true', '2021-08-18 11:23:41', '2021-08-17 21:42:54'),
('7903c082adf0141f05108425a2148d0e', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'coba ah...', 'xkkirixpxkkananxok ya...xkkirixxgmringxpxkkananx', 'true', '2021-08-17 22:23:21', '2021-08-17 21:39:48'),
('8328e72ec0130dae679f5caa2d7cf97f', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'cdddd', 'xkkirixpxkkananxsadadadxkkirixxgmringxpxkkananx', 'true', '2021-08-17 21:52:24', '2021-08-17 21:52:04'),
('bd51916dfdbb5a8dda84f669674fcc36', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'KEPEGAWAIAN', '1', '1', 'KEPEGAWAIAN', 'BALAS : BALAS : cdddd', 'xkkirixpxkkananxsipxkkirixxgmringxpxkkananx', 'true', '2021-08-17 21:54:39', '2021-08-17 21:52:29'),
('caf46e0136ac8aeb76bc0a28f398a5f8', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'CABANG', 'BOJA', 'b8cc6b55576dffb4dc1763ffa1015d9e', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', '12222', 'xkkirixpxkkananxsdsdxkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-17 21:57:46'),
('7ac25a982cd90016c5f43e45eb606e77', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'MAJELIS', '', '', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'sdfdf', 'xkkirixpxkkananxddfffxkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-17 21:58:28'),
('a787fbba407d11bd9fae0f05c388e54c', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'sdsdd', 'xkkirixpxkkananxsafafafxkkirixxgmringxpxkkananx', 'true', '2021-08-17 22:22:31', '2021-08-17 22:22:07'),
('7c147e17875152c90d778a7115a836f9', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'BALAS : BALAS : sdsdd', 'xkkirixpxkkananxtes... ok dong.....xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-17 22:22:41'),
('ec00bcc073fa87d903d8f322e1e08cae', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SARPRAS', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'sdffffff', 'xkkirixpxkkananxffffffffffffxkkirixxgmringxpxkkananx', 'true', '2021-08-18 16:19:27', '2021-08-18 09:11:35'),
('7741d21c3400d37f7bb15656243fc2e8', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SARPRAS', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'coba123', 'xkkirixpxkkananxcoba 123xkkirixxgmringxpxkkananx', 'true', '2021-08-18 09:16:57', '2021-08-18 09:14:54'),
('90c7e4e87e6a0f426eb3ae6908c5f567', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SARPRAS', 'BALAS : BALAS : coba123', 'xkkirixpxkkananxok deh kakak...xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-18 09:17:05'),
('d927ddaaaae92e0bd89b70554d1106bc', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'SARPRAS', 'SARPRAS', 'SARPRAS', 'SARPRAS', '123456', 'xkkirixpxkkananx123456xkkirixxgmringxpxkkananx', 'true', '2021-08-18 09:21:37', '2021-08-18 09:21:11'),
('dad20f86e3829d2d2a72a09321e5f78b', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SARPRAS', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'BALAS : BALAS : 123456', 'xkkirixpxkkananxsip. siap.....xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-18 09:21:44'),
('22e1b746663654685c5acfae581f9ee5', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEUANGAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'SARPRAS', 'SARPRAS', 'SARPRAS', 'SARPRAS', 'cek dan update', 'xkkirixpxkkananxcek lagi... update lagi...xkkirixxgmringxpxkkananx', 'true', '2021-08-18 10:06:04', '2021-08-18 10:03:18'),
('079e301ae2e7ca92b9e9adad24fd751f', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEUANGAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'KEPEGAWAIAN', 'sip deh.....', 'xkkirixpxkkananxapa saja yg sip...??xkkirixxgmringxpxkkananx', 'true', '2021-08-18 10:05:23', '2021-08-18 10:03:42'),
('44769329d9875b80574c05e6eccf6c65', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEUANGAN', 'BALAS : BALAS : sip deh.....', 'xkkirixpxkkananxok..xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-18 10:05:33'),
('e85bbf01b8dbc577859281c41a38e3c0', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'SARPRAS', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEUANGAN', 'BALAS : BALAS : cek dan update', 'xkkirixpxkkananxok..xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-18 10:06:10'),
('4bedd218596a3e1b566434d4540b618f', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'PEGAWAI', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'KEUANGAN', 'KEUANGAN', 'KEUANGAN', 'KEUANGAN', 'bu... kapan saya nih....', 'xkkirixpxkkananxbu, saya mau setor....xkkirixxgmringxpxkkananx', 'true', '2021-08-18 11:21:20', '2021-08-18 11:20:59'),
('7c7b906b9850be1a50dba08d3cfe026d', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEUANGAN', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'PEGAWAI', 'BALAS : BALAS : bu... kapan saya nih....', 'xkkirixpxkkananxok, segera......xkkirixxgmringxpxkkananx', 'true', '2021-08-18 11:24:13', '2021-08-18 11:21:30'),
('5622dad05dcaa4b520352e4d0444d9bd', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', '663a7589bc5acf889d96ea17a96e57d8', '5', '5', 'PEGAWAI', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEUANGAN', 'BALAS : BALAS : BALAS : BALAS : bu... kapan saya nih....', 'xkkirixpxkkananxsip... thanks ya..xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-18 11:26:19'),
('256e028f353d8c3017e118179848ce83', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'PEGAWAI', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'KEPEGAWAIAN', 'BALAS : BALAS : 222', 'xkkirixpxkkananxok......xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-18 16:18:31'),
('141304bc31ace3e109cc61d22be9c31a', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'PEGAWAI', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'ok... aq coba...', 'xkkirixpxkkananxsiap... aq coba...xkkirixxgmringxpxkkananx', 'true', '2021-08-18 16:19:11', '2021-08-18 16:18:50'),
('50df8918f86982a9d9b6ff4d874bdc45', 'SEKOLAH', 'WELERI', 'WELERI', '123', '2aa1b4e6654360ea4519dc265402780a', '2aa1b4e6654360ea4519dc265402780a', '123', '123', 'Tata Usaha', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'PEGAWAI', 'BALAS : BALAS : ok... aq coba...', 'xkkirixpxkkananxsip......xkkirixxgmringxpxkkananx', 'true', '2021-08-18 16:20:43', '2021-08-18 16:19:18'),
('cb8798e264426c8366a6ed6435c65cc2', 'CABANG', 'WELERI', 'WELERI', 'WELERI', '51e2e1372367f9d1cc28da23ba043f0c', '51e2e1372367f9d1cc28da23ba043f0c', '20', 'WELERI', 'Tata Usaha', 'CABANG', 'PEGANDON', '171a103a423d9c800c868393c7074ad9', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'coba ya', 'xkkirixpxkkananxtes... 123xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-23 11:20:36'),
('a7f74f2f39310e9ab2de9c5f1632e2ef', 'CABANG', '', '', 'WELERI', '51e2e1372367f9d1cc28da23ba043f0c', 'c4ca4238a0b923820dcc509a6f75849b', '1', '1', 'PEGAWAI', 'CABANG', 'PEGANDON', '171a103a423d9c800c868393c7074ad9', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', '1234', 'xkkirixpxkkananx1234567xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-24 09:48:45'),
('5efcd36e2d9c4503e0a66f47d80b8fec', 'SEKOLAH', '', '', '123', '2aa1b4e6654360ea4519dc265402780a', 'c4ca4238a0b923820dcc509a6f75849b', '123', '123', 'KEPEGAWAIAN', 'SEKOLAH', '', '', '234', 'e2c74b84ca8cf054bd1a4ccb5463dccf', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'aq lagi nih...', 'xkkirixpxkkananxsiapa lagi deh.....xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-24 10:39:20'),
('006fe459f5e76aad861c83c053f1a475', 'MAJELIS', '', '', 'MAJELIS', '21232f297a57a5a743894a0e4a801fc3', '21232f297a57a5a743894a0e4a801fc3', 'MAJELIS', 'MAJELIS', 'Tata Usaha', 'CABANG', 'WELERI', '51e2e1372367f9d1cc28da23ba043f0c', '', '', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'TATA USAHA', 'yuhu...', 'xkkirixpxkkananxcoba deh.....xkkirixxgmringxpxkkananx', 'false', NULL, '2021-08-26 05:54:40');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `adminx`
--
ALTER TABLE `adminx`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_artikel`
--
ALTER TABLE `cp_artikel`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_artikel_komentar`
--
ALTER TABLE `cp_artikel_komentar`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_bukutamu`
--
ALTER TABLE `cp_bukutamu`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_foto`
--
ALTER TABLE `cp_foto`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_katamutiara`
--
ALTER TABLE `cp_katamutiara`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_m_kategori`
--
ALTER TABLE `cp_m_kategori`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_m_link`
--
ALTER TABLE `cp_m_link`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_m_menu`
--
ALTER TABLE `cp_m_menu`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_m_mitra`
--
ALTER TABLE `cp_m_mitra`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_m_posisi`
--
ALTER TABLE `cp_m_posisi`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_m_slideshow`
--
ALTER TABLE `cp_m_slideshow`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_m_submenu`
--
ALTER TABLE `cp_m_submenu`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_newsletter`
--
ALTER TABLE `cp_newsletter`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_polling`
--
ALTER TABLE `cp_polling`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_profil`
--
ALTER TABLE `cp_profil`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_video`
--
ALTER TABLE `cp_video`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `cp_visitor`
--
ALTER TABLE `cp_visitor`
  ADD PRIMARY KEY (`kd`),
  ADD KEY `kd` (`kd`);

--
-- Indeks untuk tabel `info_dari_cabang`
--
ALTER TABLE `info_dari_cabang`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `info_dari_majelis`
--
ALTER TABLE `info_dari_majelis`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_ketua`
--
ALTER TABLE `majelis_ketua`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_pegawai`
--
ALTER TABLE `majelis_pegawai`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_rekap_sekolah`
--
ALTER TABLE `majelis_rekap_sekolah`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_sekolah_kib_a`
--
ALTER TABLE `majelis_sekolah_kib_a`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_sekolah_kib_b`
--
ALTER TABLE `majelis_sekolah_kib_b`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_sekolah_kib_c`
--
ALTER TABLE `majelis_sekolah_kib_c`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_sekolah_kib_d`
--
ALTER TABLE `majelis_sekolah_kib_d`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_sekolah_kib_e`
--
ALTER TABLE `majelis_sekolah_kib_e`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_sekolah_kib_f`
--
ALTER TABLE `majelis_sekolah_kib_f`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_sekolah_uang_keluar`
--
ALTER TABLE `majelis_sekolah_uang_keluar`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_sekolah_uang_masuk`
--
ALTER TABLE `majelis_sekolah_uang_masuk`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `majelis_struktur_organisasi`
--
ALTER TABLE `majelis_struktur_organisasi`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_cabang`
--
ALTER TABLE `m_cabang`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_keadaan_karyawan`
--
ALTER TABLE `m_keadaan_karyawan`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_kib_jenis`
--
ALTER TABLE `m_kib_jenis`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_kib_kode`
--
ALTER TABLE `m_kib_kode`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_majelis`
--
ALTER TABLE `m_majelis`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_sekolah`
--
ALTER TABLE `m_sekolah`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_uang_keluar`
--
ALTER TABLE `m_uang_keluar`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_uang_masuk`
--
ALTER TABLE `m_uang_masuk`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_cp_artikel`
--
ALTER TABLE `sekolah_cp_artikel`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_cp_foto`
--
ALTER TABLE `sekolah_cp_foto`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_cp_youtube`
--
ALTER TABLE `sekolah_cp_youtube`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_jml_karyawan`
--
ALTER TABLE `sekolah_jml_karyawan`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_jml_siswa`
--
ALTER TABLE `sekolah_jml_siswa`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_kib_a`
--
ALTER TABLE `sekolah_kib_a`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_kib_b`
--
ALTER TABLE `sekolah_kib_b`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_kib_c`
--
ALTER TABLE `sekolah_kib_c`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_kib_d`
--
ALTER TABLE `sekolah_kib_d`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_kib_e`
--
ALTER TABLE `sekolah_kib_e`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_kib_f`
--
ALTER TABLE `sekolah_kib_f`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_ks`
--
ALTER TABLE `sekolah_ks`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_pegawai`
--
ALTER TABLE `sekolah_pegawai`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_struktur_organisasi`
--
ALTER TABLE `sekolah_struktur_organisasi`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_uang_keluar`
--
ALTER TABLE `sekolah_uang_keluar`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `sekolah_uang_masuk`
--
ALTER TABLE `sekolah_uang_masuk`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `user_filebox`
--
ALTER TABLE `user_filebox`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `user_forum`
--
ALTER TABLE `user_forum`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `user_forum_comment`
--
ALTER TABLE `user_forum_comment`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `user_log_entri`
--
ALTER TABLE `user_log_entri`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `user_log_gps`
--
ALTER TABLE `user_log_gps`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `user_log_login`
--
ALTER TABLE `user_log_login`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `user_msg`
--
ALTER TABLE `user_msg`
  ADD PRIMARY KEY (`kd`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
